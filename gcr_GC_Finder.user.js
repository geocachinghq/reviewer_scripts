
// ==UserScript==
// @name        GCFinder v3
// @namespace   https://admin.geocaching.com
// @description Seek on GC code and end up on revsite
// @include     http*://*.geocaching.com/*
// @exclude		http*://*.geocaching.com/admin/*.aspx*
// @exclude		http*://forums.geocaching.com*
// @exclude		http*://admin.geocaching.com/*
// @exclude     http*://*.geocaching.com/help*
// @exclude     http*://labs.geocaching.com/*
// @exclude     http*://shop.geocaching.com/*
// @exclude  	http*://*.geocaching.com/admin/reviewer/sweeper
// @exclude     http*://*.geocaching.com/policies/*
// @exclude		https://*.geocaching.com/geogames*
// @version     03.06
// @grant		GM_getValue
// @grant		GM_setValue
// @grant		GM_xmlhttpRequest
// @grant		GM_addStyle
// @grant		GM_registerMenuCommand
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js
// @updateURL   https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_GC_Finder.user.js
// @downloadURL https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_GC_Finder.user.js
// ==/UserScript==

/*

Function:
 Adds review page search by GC-Code and quick select menu to header of various pages

*/

/* Change history
 *	03.06	2023-12-12	- begin Tag Manager logging for basic analytics
 *	03.05	2023-03-10	Update Urls, more specific script intiation requirement because of incremental page loads
*	03.04	2023-01-23	Update excludes to remove .com/geogames
 *  03.03   2022-11-15  Revove terms of use from list of pages on which the script runs
 *  03.02   2021-11-23  Correction so scripts works correctly on all pages
 *	03.01	2021-05-10	Removed sweeper tool from list of running page
 *	03.00	2021-05-05	Adjusted script to work with new react header, made adjustments to make the functionality more consistent across users
 */

 let regularPage = document.getElementById('gc-header'); //need specific element because page load may be slow

let loaded = function () {
	console.log('GC Finder load initiated');

	// Google Tag Manager setup
	function gtagFind(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
	if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
	function googleTagLogFind(dataAction){ //Method to do a gtag sending an action name
		if (document.URL.indexOf("www") < 0) return;
		try {
			gtagFind('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'Finder',/*'debug_mode':true,*/
				'send_to':'G-GRQE2910DL'});
			console.log('Script '+dataAction+' logged to Tag Manager.');
		}
		catch {console.log('There was an error using gtag.  Logging attempt failed.')}
	}
	if (document.URL.indexOf("/play/search") > -1) googleTagLogFind('initialize'); // only log a portion of the site to avoid spam

	var MagGlassSrc =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuODc7" +
		"gF0AAAPtSURBVDhPpZRbTFN3HMfdpg%2FbMmNMlizZsrgse%2BNhW7aH%2BbQXspABDyMO" +
		"NlAGCB2BKpObICAUWgItl1VsuRQECnSDQqWi5RYuVoJ0glA6Qa2rFUpLL5RbL9D2fHf%2B" +
		"dImpIg%2FzJN%2Bc%2Fz8nv8%2F%2F8%2F%2Bd8z9vHHrh4tY0Hvvk08%2B%2BcLpc31M%" +
		"2B3%2BdeL3XU5XYdcjicY06Xu399Y33ebrPaxUKu98XagDmrnP9Wnbg7qE02WC0buuscnH" +
		"yAkXtaDN59CPmYGq29SnAbpBtZnKudiemsb39iZB45EChs6fpKqlAqBiY0u8oZHVQaA2Yf" +
		"WaHW2jG9YMbwlBYt1yeQX92OtKIaVVza5eBXAn9v6PhIcmO0%2BZZS7Z7SLOOJwYGVNQom" +
		"O2C0UzDQY73Zg3uLNlSLx5FV1oJzBdWqU3Hnj78EvVhU%2FqagtfuHTsWdtYlZHfSrbhoC" +
		"mDeeZ3UdMNFZtlG4rV5DoUCB3AqxL%2B785fSXgIVcwfHadnlT78g0Fp%2FaaTNAt%2BrP" +
		"ks0PIkCyALlrjRRkt1eQUyHBb6yahz8mpB0LgHKuNH%2FMb5ZN909ooDc5YdsE1rb8AAMN" +
		"11uAZ1bsLURaoDMDk4s7KBb24VJFy9LplJxvAoCFVY0nSgUdmpvj92G0ubHuADadwMZ%2F" +
		"Wdv2W%2BppELHWGoFZnQ9XOiaQV9Vqpl%2FOqQBgOot%2FIqesXiMbUsFid2PLBWy7n4fM" +
		"CdxCmy%2FRposGQK0HBJI7YF2VmFNzSwOBjCzOh3QvlG29YzBatuDcAVy7%2FpAxgRMoMS" +
		"dt0JoAzTMKVc394DVd1zNzS78MMIxMvPBuan5lYUlNm3duQYcdDwWPD%2FB4QY%2F9YAcN" +
		"JZZk649WKIzPrEIoGQKvQdq%2F77dIW57MYAsXWnsGYTRbQVHYCwETKDElhmTLc0%2B2Ib" +
		"mlQlPPqIfNbw7dF%2FgzI%2FO9lEu8i7ncRmu7bBAG4yq8Xh98NHSXNiWGJrsP9xctkI%2" +
		"BqIZIOo0YsX45LzX4%2FNDR0%2FyMYy8z7IDWvoii7rH6jtqMPitFJzD14DPXCP5j5%2By" +
		"lujs%2Bia0CFaz0ju1lsoYd%2FrXvzTHxST0xMTAKTydwf%2Bsu5%2FKNJmeyIjBLBfHmD" +
		"FKKuYYjlSvw5MIXOgSmfuHfscV1H34Wk7PJUaW%2B%2FJeHsWYo21MXGxp6hoYdfebYZGU" +
		"WH0wq4QQXc%2BtPltZIUXt0fKfyGtpMSieRtUiQSiYJ4PJ6qsrISERERCAsLs0RHR%2F%2" +
		"BamJj4zoF%2FoIMe0lv9uqSkZJ7D4SAyMhLh4eFmAv3fQFKYnJwczGazVcXFxXtQYvpaQN" +
		"ryCN277wiUmIaEhOC1gKSYQOPj44MZDMZ8VFTUX%2F8CmzoL%2BxPXAH4AAAAASUVORK5C" +
		"YII%3D";

	//  Add style for waypoint input box.
	GM_addStyle(".wpfinder {border: 2px solid rgb(118, 132, 84) !important; margin-top: 2px; " +
			"border-radius:7px; -moz-border-radius:7px; background-color:rgb(227,221,194) !important; " +
			"padding:0 !important; } #form_wpFinder:hover {background-color: inherit;}");

	//  Get current URL.
	var UrlHere = document.URL;

	//  If tracking item by tracking ID, or owns tracking item, then add to log link.
	if (UrlHere.match(/\/track\/details\.aspx/i)) {
		//  Check for owned item.
		var e_lblTBID = document.getElementById("ctl00_ContentBody_lblTBID");
		if (e_lblTBID) {
			var tracker = e_lblTBID.childNodes[1].firstChild.data;
		} else {
			var tracker = UrlParm('tracker');
		}
		if (tracker && (!tracker.match(/^TB.*/i))) {
			var e_LogLink = document.getElementById("ctl00_ContentBody_LogLink");
			if (e_LogLink) {
				e_LogLink.href += '&tracker=' + tracker;
			}
		}

	//  If tracking log entry, and tracker number in URL, insert into entry box.
	} else if (UrlHere.match(/\/track\/log\.aspx\?wid=.*?&tracker=/i)) {
		var tracker = UrlParm('tracker');
		var e_tbCode = document.getElementById("ctl00_ContentBody_LogBookPanel1_tbCode");
		if (e_tbCode) {
			e_tbCode.value = tracker;
		}
	}

	// Create GCFinder list item and child items
	var gcFinderLi = document.createElement('li');
	var gcFinderBtn = document.createElement('button');
	gcFinderBtn.id = 'gcfinder';
	gcFinderBtn.innerHTML = "GCFinder";
	gcFinderBtn.title = 'GCFinder';
	gcFinderBtn.classList.add('dropdown-toggle');
	gcFinderBtn.classList.add('toggle-gcfinder');
	gcFinderBtn.addEventListener('click', displayMenu);
	gcFinderLi.appendChild(gcFinderBtn);

	//  Add Your Stuff menu items - Create new list entry for Your Stuff menu.
	var ulYourSubMenu = document.createElement('ul');
	ulYourSubMenu.id = 'ulYourSubMenu';
	ulYourSubMenu.style.visibility = "hidden";
	ulYourSubMenu.classList.add('hidden');
	ulYourSubMenu.classList.add('submenu');
	ulYourSubMenu.classList.add('dropdown-menu');

		fAddMenuItem('My Account',				'https://www.geocaching.com/account/settings/profile');
		fAddMenuItem('My Profile',				'https://www.geocaching.com/p/default.aspx');
		fAddMenuItem('Search another player',	'https://www.geocaching.com/find/default.aspx');
		fAddMenuItem('Advanced Search',		'https://www.geocaching.com/seek/nearest.aspx');
		fAddMenuItem('Adminpage',				'https://www.geocaching.com/admin');
		fAddMenuItem('My Lists',				'https://www.geocaching.com/my/lists.aspx');
		fAddMenuItem('PQ',						'https://www.geocaching.com/pocket');
		fAddMenuItem('Guidelines',				'https://www.geocaching.com/about/guidelines.aspx');
		fAddMenuItem('Help Center',			'https://support.groundspeak.com/index.php?pg=kb.book&id=1');
		fAddMenuItem('WIKI Challenges',		'https://wiki.groundspeak.com/display/VOL/Challenge+Caches');

	gcFinderLi.appendChild(ulYourSubMenu);

	//  Create new list entry for search box.
	var searchBoxLi = document.createElement('li');

	//  Create form.
	var form_wpFinder = document.createElement('form');
	form_wpFinder.id = 'form_wpFinder';
	form_wpFinder.name = 'form_wpFinder';
	form_wpFinder.style.display = 'flex';
	searchBoxLi.appendChild(form_wpFinder);
	form_wpFinder.action = 'javascript:func_wpFinder()';


	//  Create script.
	var script_wpFinder = document.createElement('script');
	script_wpFinder.language = "javascript";
	script_wpFinder.type = "text/javascript";
	script_wpFinder.appendChild(document.createTextNode(
			"function func_wpFinder() { \n" +
			"   var hostUrl = location.hostname; \n" +
			"	var xinput_wpFinder = document.getElementById('input_wpFinder'); \n" +
            "	xwp = xinput_wpFinder.value.toUpperCase(); \n" +
            "   console.log('xwp  is ' + xwp); \n" +
			"	for (var j = 0; j < xwp.length; j++) { \n" +
			"		if (xwp.charCodeAt(j) != 32) { \n" +
			"			xwp = xwp.substring(j); \n" +
			"			j = xwp.length; \n" +
			"		} \n" +
			"	} \n" +
			"	if (xwp.match(/^GC.*/)) { \n" +
			"		xwpurl = 'https:\/\/' + hostUrl + '/admin/review.aspx?wp=' + xwp; \n" +
			"	} else { \n" +
			"		xwpurl = 'https:\/\/' + hostUrl + '/track/details.aspx?tracker=' + xwp; \n" +
			"	} \n" +
			"	window.location.assign(xwpurl); \n" +
			"} \n"
	));
	form_wpFinder.appendChild(script_wpFinder);

	//  Create input box.
	var input_wpFinder = document.createElement('input');
	input_wpFinder.type = 'text';
	input_wpFinder.id = 'input_wpFinder';
	input_wpFinder.setAttribute('class', 'wpfinder');
	input_wpFinder.title = 'Enter waypoint or travel bug ID to find';
	input_wpFinder.size = 10;
	input_wpFinder.maxLength = 8;
	form_wpFinder.appendChild(input_wpFinder);

	//  Add image trigger.
	var img_wpFinder = document.createElement('input');
	img_wpFinder.type = 'image';
	img_wpFinder.id = 'img_wpFinder';
	img_wpFinder.src = MagGlassSrc;
	img_wpFinder.title = 'Click to go to cache or travel bug page';

	form_wpFinder.appendChild(img_wpFinder);

	//  Add GCFinder and search box to header.
	var navList = document.querySelectorAll("ul")[0];
   	navList.appendChild(gcFinderLi);
    navList.appendChild(searchBoxLi);

    // ----------------------------- FUNCTIONS ------------------------------//

	function displayMenu() {
		const subMenu = document.getElementById('ulYourSubMenu')
		if (subMenu.classList.contains('hidden')) {
			subMenu.classList.remove('hidden')
			subMenu.classList.add('visible');
			subMenu.style.visibility = "visible";
		}  else {
			subMenu.classList.remove('visible')
			subMenu.classList.add('hidden');
			subMenu.style.visibility = "hidden";
		}
	}

	//  Function to Add items to Your Stuff menu.
	function fAddMenuItem(sTitle, sUrl) {
		//  Create menu item.
		var newItem = document.createElement('li');
		var newLink = document.createElement('a');
		newLink.href = sUrl;
		newLink.id = sTitle;
		newLink.appendChild(document.createTextNode(sTitle));
		newItem.appendChild(newLink);
		ulYourSubMenu.appendChild(newItem);
	}

	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	console.log('GCFinder load complete.');
};

let intervalID = setInterval(function() {
	if(regularPage) {
		loaded();
		clearInterval(intervalID);
	}
	regularPage = document.getElementById('gc-header');
}, 500);