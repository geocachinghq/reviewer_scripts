﻿/*
Geocaching Log Entry Enhancement v3

// ==UserScript==
// @name           GC Log Entry v3
// @description    Assists in log entries for various log types based on templates.
// @namespace      http://www.geocaching.com/admin
// @version        03.41
// @scriptGuid     c3748a27-1ae8-45c3-9931-50a024c7fc8e
// @match          https://*.geocaching.com/live/geocache/*log*
// @match		   https://*.geocaching.com/live/log/GL*
// @match          https://*.geocaching.com/bookmarks/mark.aspx*
// @match          https://*.geocaching.com/geocache/GC*
// @icon           http://i.imgur.com/1GZXUf0.png
// @run-at		   document-start
// @grant          GM_info
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// @grant          GM_unregisterMenuCommand
// @grant		   window.close
// @unusedAPIs     -----------------------
// @grantXX        GM_listValues
// @grantXX        GM_deleteValue
// @grantXX        GM_getResourceText
// @grantXX        GM_getResourceURL
// @grantXX        GM_log
// @grantXX        GM_setClipboard
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_New_Log_Entry.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_New_Log_Entry.user.js
// ==/UserScript==

Change Log:
* v03.41 2025-02-11	- work-around for absent 'unpublished' tag on Event logs causing Reviewer note errors
* v03.40 2025-01-02	- correct a class name to correctly scrape the logtype
* v03.39 2024-10-08	- update detect username to fix a bookmark page error
* v03.38 2024-08-06	- detect images on copy (warn) and submit all (reject)
* v03.37 2024-01-06	- prep for native Update Coordinates link from Review Page
* v03.36 2024-05-14	- add submit button listener as trigger for saving log on exit
* v03.35 2024-03-07	- move 4plex of buttons back to the upper right where they belong
* v03.34 2024-01-11	- still trying to nail initializing so GA4 records accountID correctly
* v03.33 2024-01-06	- delayed GA4 logging for NewLog by 500ms, added xtraSubmitButton logging
* v03.32 2023-12-12	- begin Tag Manager logging for basic analytics
* v03.31 2023-11-15	- Limit bookmark autoclose, allow SubmitAll on Update Coords, post button coloring
* v03.30 2023-10-31	- Reversioning with Map key variable type correction
* v03.29 2023-10-25	- RSS-58 add log POST method as antidote to lazy loading.  Live update of logType Map.
* v03.28 2023-10-10	- Remove commented and old-to-new transition code.  Resurrect update coordinates template
* v03.27 2023-10-03	- Change anchor for second duplicate post button to avoid non-reviewer error
* v03.26 2023-09-26	- Activate hydration check with failsafe
* v03.25 2023-09-12	- Split off close tabs script and allow configurable loaded() timing.  Prep hydration check
* v03.24 2023-09-11	- versioning clarity on tweaks to timing
* v03.23 2023-09-07	- Add "close all review pages" button
* v03.22 2023-09-06	- Fix KZ/OAN autoclose behavior (allow close on successful bookmark and log), bookmark selection
* v03.21 2023-09-05	- Avoid multiple paste requests during lazy loading
* v03.20 2023-09-05	- Fix the "stutter" when typing in the text box with URL parameters
* v03.19 2023-08-29	- Fix the copy-across feature (.textContent to .value)
* v03.18 2023-07-25	- Add a THIRD post button next to cannot delete checkbox
* v03.17 2023-07-22	- Iteration on closing tabs and propogating tab contents to other tabs
* v03.16 2023-07-13	- Close tab after successful log template post
* v03.15 2023-07-10	- Adapt for localized log type names
* v03.14 2023-07-07	- Return to not triggering log type menu for simple load events
* v03.13 2023-07-04	- Defeat parsing avoidance on simple logs so that the auto-clicking always fires
* v03.12 2023-06-28c	- Have new log script start listening immediately for copy and post events
* v03.11 2023-06-28b	- Make sure "last log" event listener is set for simple log loads
* v03.10 2023-06-28	- Correct updateURL and downloadURL TM links
* v03.09 2023-06-13	- Correct checking "owner cannot delete" checkbox, move duplicate submit/cancel buttons down
* v03.08 2023-05-25	- More deconflicting of new/old flow KZOAN bookmarking, versioning, new flow auto submit kz/oan
* v03.07 2023-05-25	- More deconflicting of new/old flow KZOAN bookmarking
* v03.06 2023-05-25	- Post button, deconflict bookmarking, faster simple pages, allow long background time before template insert
* v03.05 2022-09-15	- Adapt script to new React log page and logging flow, with template transfer
* v02.04 2021-06-03 - Wrapped whole script in timing function
* v02.03 2021-05-06 - Fixed signedIn reference to work with new header
* v02.02 2020-07-31 - Removed include statement for http*://*.geocaching.com/track/log.aspx*
* v02.01 2017-08-17 - SignedInAs fix for new header release
* v02.00 2017-08-02 - Version reset to 2.0 and name change to add v2.

Log type codes:
	 2 = Found It
	 3 = Didn't Find It
	 4 = Write Note
	 5 = Archive
	 7 = Needs Archive
	 9 = Will Attend
	10 = Attended
	11 = Webcam Photo Taken
	12 = Unarchive
	18 = Reviewer Note (unpublished)
	22 = Disable
	23 = Enable
	24 = Publish
	25 = Retract Listing
	45 = Needs Maintenance
	47 = Update Coordinates
	68 = Reviewer Note (published)
	74 = Announcement
	76 = Submitted for Review
	99 = Unknown //this provides an option for unknown values in order to avoid errors

Auto-close:  
	Use URL parameter close=y to close the tab on redisplay. Requires the browser
	setting dom.allow_scripts_to_close_windows to be set to True.
	See http://tinyurl.com/5vom4r7 for instructions.

React adaptations:
	Much of the script interface now relies on sending events to the DOM to operate React
	features.  This is asynchronous because React must "react" to these events.  If an
	action requires multiple events, sending an unrelated event will disrupt the event flow.
	To avoid this, I've used an eventHold=true variable.  Any DOM event should test for
	eventHold, delay until eventHold=false, set eventHold, send the new event, then clear
	eventHold.  Isn't React fun? 
	*/

//  Set value for text copy localstorage key.
console.log('Begin new log script pre-load.');
let lsCopyKey = '812f9b8e-e8fc-40ba-9af9-c2c2f87b2ae3';
let logTypeMapCopyTrigger = '812f9b8f-e8fc-40ba-9af9-c2c2f87b2ae3';
let cachedSubmit = false;
let cachedNoDelete = false;
let cachedPlannedText = "blank";
let cachedPlannedLogValue = "1";
let launchEventHoldListener;
let plannedText = "";
let closePagesListener;
let suppressLogTypeSelection = false; 
let suspendManualPasteSensor = true;

// a map of all log types and their associated values
let logTypeNumtoText = new Map([
		["2","Found it"],
		["3","Didn't find it"],
		["4","Write note"],
		["5","Archive"],
		["7","Reviewer attention requested"],
		["9","Will attend"],
		["10","Attended"],
		["11","Webcam photo taken"],
		["12","Unarchive"],
		["18","Reviewer note"], //unpublished
		["22","Disable"],
		["23","Enable listing"],
		["24","Publish listing"],
		["25","Retract listing"],
		["45","Owner attention requested"],
		["47","Update coordinates"],
		["68","Reviewer note"], //published
		["74","Announcement"],
		["76","Submitted for review"],
		["99","Unknown"]]);

// Google Tag Manager setup
function gtagNL(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
if (typeof dataLayer === 'undefined') dataLayer = [];
function googleTagLogNL(dataAction){
	if (document.URL.indexOf("www") < 0) return;
	try {
		if (!googleTagLogInitialize) {
			gtagNL('event','data',{'data_label':'script_data','data_action':'initialize','data_category':'NewLogPage',/*'debug_mode':true,*/
			'send_to':'G-GRQE2910DL'});
			console.log('Script '+dataAction+' logged to Tag Manager.');
			googleTagLogInitialize = true;
			clearInterval(googleTagLogTimeout);
		}
		if (dataAction != 'initialize') {
			gtagNL('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'NewLogPage',/*'debug_mode':true,*/
				'send_to':'G-GRQE2910DL'});
			console.log('Script '+dataAction+' logged to Tag Manager.');
		}
	}
	catch {console.log('There was an error using gtag.  Logging attempt failed.')}
}

// launch a hydration listener.  Hydration is a client-side React process.  The page
// breaks if buttons are inserted before hydration is complete. 
window.addEventListener('appHydrated',fAppHydrated);
let hydrated = false;  //set this to false when the hydration event makes it into production 
function fAppHydrated (){
	console.log('just received the appHydrated event.');
	hydrated = true;
}

// begin listening immediately for storage events, and cache them; launch 'initialize' event when appropriate
let googleTagLogInitialize = false;
let googleTagLogTimeout;
if (document.URL.indexOf("/live/geocache/") > 0) {
	window.addEventListener('storage', fStorageCache, false);
	googleTagLogTimeout = setInterval(()=>{
		if ((!googleTagLogInitialize) && (hydrated)) {
			setTimeout(()=>{
				googleTagLogNL('initialize');
				googleTagLogInitialize = true;
				clearInterval(googleTagLogTimeout);
			},250);
		}
	},500);
}

// borrow the imgTextFont and insert it as the favicon to show listening has started
var imgTextFont =
	"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
	"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
	"wAAAHJSURBVDjLY%2Fj%2F%2Fz8DJZiBZgY4tN9wcO6%2B0erZd2uKc%2BfNfoeWGxMcW2" +
	"7Msiq%2B3GWUdIZXL%2FokI14D7JqvB%2Bcsf3Rv4p6X%2F%2Ft3Pf%2Ffvf35%2F8Ilj3" +
	"471V3bph9zmougC6xrr8mETbu7q3jl40%2FFKx5%2BLVzy8Ltd%2BeUZBvGnOYjygk3llf" +
	"KCZY%2B%2Bu3fcWutcd21B07on%2F61yz88kKgwsCi8qJc%2B%2B9yhu2p37ppnnQ4C4oW" +
	"blo%2F9WOReXEjTANOsCs1PD9VVZ8%2B9%2FN0k7m6Yfe5LLOPFMR%2BWyh%2F9dqq5eUv" +
	"c6xIbXALOs8zEZc%2B9%2FC%2Bq%2BddEw%2FrSfXuRxLfP0swuqgAYEt934pOq2nxenAU" +
	"bJZ0TjJt9%2BVbn80X%2Bv5huXrbLOb7LMOLfVterqjcYVj%2F%2BHtd38qey4TxqrAQax" +
	"pxntSy7PBvnVPO0MSmCZJ5%2FZWL7g%2Fv%2Bozlv%2Flex2K2EYoB9zigsYPS6lSx7%2B" +
	"j%2Bi59UYn6JgtTIGK635hdY%2FD9dnT7vxP6L%2F9X9F%2Bb4icxTYmFAMsMs6ti%2B2%" +
	"2F9S9hwu3%2FAc3X32oHHOlVdtoroGS%2FR0vb9%2FAip8ILrwLrrv33rbn63zD02F5Zy2" +
	"2GtM8LdDMAACVPr6ZjGHxnAAAAAElFTkSuQmCC";

let icon;
listenInterval = setInterval(()=>{
	if (!document.head) return;
	clearInterval(listenInterval);
	// = document.head || document.getElementsByTagName('head')[0];
	icon = document.head.querySelector('link[rel="icon"]');
	if (!icon) {
		icon = document.createElement('link');
		icon.rel = 'icon';
		icon.href = imgTextFont;
		document.head.appendChild(icon);
	}
},100);


function fStorageCache(e){
//  Update log to text in storage.
	var jsObj = new Object;
	console.log('fStorageCache just detected a storage change');
	if (e.key == lsCopyKey) {
		var jsonString = e.newValue;
		jsObj = JSON.parse(jsonString);
		if ((jsObj['submit']) == 'true') {
			sessionStorage.setItem('autoclose', 'true');
			cachedSubmit = true;
			sessionStorage.setItem('newLogAction','true'); //set up to close tab after successful log
		} else {
			suppressLogTypeSelection = false;
			cachedPlannedText         = jsObj['logtext'];
			cachedPlannedLogValue     = jsObj['logvalue'];
			cachedNoDelete = jsObj['nodelete'];
			console.log('Heard a request to set text to: \n'+cachedPlannedText+"\n and value to: \n"+cachedPlannedLogValue);
		}
	}
	else if (e.key == logTypeMapCopyTrigger) {
		logTypeNumtoText = new Map(Object.entries(JSON.parse(GM_getValue('logTypeMap'))));
		console.log('fStorageCache just uploaded the saved version of the logType Map');
		console.log(logTypeNumtoText);
	}	
}


// listen for log post, and close window when it happens
let closePageAfterPost = setInterval(()=>{
	if ((document.URL.indexOf("/live/log/GL") > 0) || 
		((document.URL.indexOf("/geocache/GC") > 0) && (document.URL.indexOf('/live/') <0 ))) {
		console.log('new log thinks it just landed after a successful log post');
		if (sessionStorage.getItem('newLogAction') == 'true'){
			console.log('attempting to close tab and terminate as part of new log flow');
//			sessionStorage.setItem('newLogAction','false'); //turning this off could get ugly, but it seems to be the best
															// way to allow repeated close attempts
			window.close();
		}
		else {
			console.log('Log page close not requested.  Aborting listener.');
			clearInterval(closePageAfterPost);
		}
	}
},500);

let logPageLabel = 'react-select-cache-log-type-input';
let bookmarkPageLabel = 'ctl00_ContentBody_Bookmark_ddBookmarkList';

//const regularPage = document.querySelectorAll("body")[0];
let logPage = document.getElementById(logPageLabel);
let bookmarkPage = document.getElementById(bookmarkPageLabel);

//launch monitor to close a bookmark tab after a successful bookmark
//this is easier than recoding bookmark tab behavior to cover the successful bookmark use case
let bookmarkCheckIterations = 0;
let closeOnBookmarkSuccess = setInterval(()=>{
	bookmarkCheckIterations++;
	if (bookmarkCheckIterations >20) { //10 seconds is enough waiting
		console.log('New log is turning off the tab-close listener for bookmark pages.');
		clearInterval(closeOnBookmarkSuccess); 
	}
	if ((document.URL.indexOf('geocaching.com/bookmarks/mark')>-1) && 
		(document.querySelector('p[class=Success]')) &&
		(sessionStorage.getItem('closeBookmarkOnSuccess') == "true")) {
		clearInterval(closeOnBookmarkSuccess);
		console.log('New log detected a successful KZ or OAN bookmark, and is closing the tab.  280');
		window.close();
	}
},500);

// need this setting to decide how long to delay launching the loaded function
let settingScriptHydrationDelay = GM_getValue('settingScriptHydrationDelay','0');

// Now monitor for the full loading of target pages before launching script functions
let loaded = function () { 
	console.log('New log entry page script actions beginning');

	var imgsrc_LogRestore =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw" +
		"9HKhAAAEn0lEQVRIS5WVe0xbVRzHy3AOnS5TNKAmc%2F7jFrNoMvYAYjTsD3VxMDFDxkNY" +
		"xmOabRjJ5lTA6JgowmBZWMJgA8bLMhjEDTISHsIc74cFRkvp%2Bi5Q6IOWvmgpfD3nYrsK" +
		"heBJvjn3nnPu73N%2B33vP73qwXFp2bRuw5MGyLy6xALBYHh6sTWTeg%2FYeLJbOssTyJA" +
		"N6q4Wl0c2xRBMGVmdOIpnZYKMA6bSW0aRWj8dyNQYFMjwYEeF%2BPx8V7YPIq%2B9DVk0H" +
		"TuXVICjlOgKTC8hONtgyq1uZ4I62hCUYTfOY0pjAV6jRx5Ogvn8cdQ95yLjzFwNIudVM%2" +
		"BqKNQVwBdpsNBpsZ0zorJEojxmRKcARytHBEaBx8jNy7vUwW7ZwxfHW9YWOQDPaTDOx2O%" +
		"2BwLNpgXFqE1mDChVGFMrEQ%2Fbwqdj2QIKXh6Ta1p2KWKZsaipl4FAuJvIyCumvQ1OBhX" +
		"hf2xlfCLLsPeyFJGFHB75swq0fE1ARcrlwG02e0W2IhNCyQLm90GvXUek3ojZCo9FDNaJ%" +
		"2BA32T44RIEU8NYXV9xD0sqbXABWBkBFrbISiI5AVHpil1a3LmBXQo57wPmiRheLapYtcl" +
		"V8NfypXSeqcPjqZpRLT%2BEn7ttO0Xs6vvNkpntAUt4fzgwWyEs2UYuIJKpHy3bRjMi4ym" +
		"TBofSncIMbhwtdu52i9x%2FlbsaO2J%2FdAxJyKp5YBDvMxJa7o%2Fk4VrQNiyS4Q1aLHY" +
		"HnPJHXF4MzjW84de7eQca6Vz%2F%2F1T0gIqPEaZF%2FYjEOXz6A8%2Ffex9FCL8Yq%2F3" +
		"8VQKzyS%2FBEdnsEYtm%2BiCp%2FCeGl2xF68xl8UvgsfKJ%2BcQ8ISbvBAIYUrYgpew1s" +
		"Xgrk84MIvbF1zW8%2BrHgbEut2Ir37ECqESThe4oOXI9awKCilEFdaz%2BL0nT0Y0NRCbO" +
		"mGwjrEQFZKQuYGDGy0aLNxX3MR9epUsKVfI%2F733fAOv7Q6A%2BLdmyH5O3Ct5yRE5m7w" +
		"za3gGGoxZKj7j%2FrmylE9k4Qc%2BbvIkh1wnoESZRSucsLxY0MMXvxsBYAE9z1auEWT1R" +
		"HG7JhvamGCu6p3royc2tO4LA90BnU9ZLWqZKQ2B6HkYQFeWAmgR9uRQW5HJMmgEwJzG7Pz" +
		"nrlSVM18iWy5%2F6rANIO8iQ9QOZ0AtjgZJypfx4NRrnsAhTjeQQJ7F7pUlejQF6BlNgfN" +
		"s9nupc1C3dR3uDYSibBiX%2FSJ%2F0QvX74eoMj5FUWXvYLM7iNo0mauWzmjSn2Q2RyDFn" +
		"49U8PWBdAi5Sh2UrUCaQ0f42zdHgagnTMxmiXSGC2YMZowM6cD%2BXVCbzRDqTUsA8bXyc" +
		"ABoOV6f2wF9hG9dyEBwflbmWuH9kbfIiWbKIqUblLC%2FaLL8c7xIljm7egXKLD9WLrbg7" +
		"bJOzQVPJEcOp0OarUaSqUSCoUCvdw2iMViCIVCRvRaKpUyc3QNXUufMRqNECnV8P70B%2F" +
		"cn%2Bbngb5kFAoUSPIkCI0IJOHwh%2Bkf56BkeReffw%2BjijKB3mIsB7jiGBWKMimXgyy" +
		"YhmJiGeFoDuUYPrw%2B%2FcQ94PuR7UHkFpxKl%2FC9tOUKeI4EdcvzV%2FgFNxeyVyQie" +
		"YAAAAABJRU5ErkJggg%3D%3D";
	
		var imgsrc_CloseTabs =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAB"+
		"B2lDQ1BJQ0MgUHJvZmlsZQAAKM9jYGA8wQAELAYMDLl5JUVB7k4KEZFRCuwPGBiBEAwSk4"+
		"sLGHADoKpv1yBqL+sykA44U1KLk4H0ByBWKQJaDjRSBMgWSYewNUDsJAjbBsQuLykoAbID"+
		"QOyikCBnIDsFyNZIR2InIbGTC4pA6nuAbJvcnNJkhLsZeFLzQoOBNAcQyzAUMwQxuDM4gf"+
		"wPUZK/iIHB4isDA/MEhFjSTAaG7a0MDBK3EGIqCxgY+FsYGLadR4ghwqQgsSgRLMQCxExp"+
		"aQwMn5YzMPBGMjAIX2Bg4IqGBQQOtymA3ebOkA+E6Qw5DKlAEU+GPIZkBj0gy4jBgMGQwQ"+
		"wAptY/P1N/5B4AAAAJcEhZcwAADsQAAA7EAZUrDhsAAAMaSURBVEhL7VVLSJRRFP7+10zq"+
		"aPNy1FEpfI1i2kPatKohiOixsUWraKFRiwqilZpUi1aBSYItWghFC7OooJXkKqJti0BqV4"+
		"vSUJRx/vejc+/8/59KkCiz65PDPZ45c757vnMuI3gElBGif5YNO+pgubnd90pIfv/qe+vA"+
		"CLaDpaY23/uDv8W2RRAU2grJjmbAJNks02ZsmMH+w93INNbBMAzougpZliFKCiKRXZh7M+"+
		"dnlbQfPHAIRbUAUZHw9PM8LnR1Y2n5F94uLG6YRUjQ29mJkdFhHMsfhQsBrss/h2U6sCwL"+
		"ExMTGBt/gLOnz2Dq0zw+TI5DVdfgOhYKK6vovzeGRwMXcfnx1AaCUKKamjjaWnKozTSjLt"+
		"WAhkwTt+bsHrS2tqGro4vnxeNxfHn+BEeuXIcoCzAsHQ48PLtxjRcf6NvH8wLI/gkhGoVu"+
		"s6EDtitAIGrbtQCJTtPGqlbkeZbrobCmYu7hfeSv3uSxAJODl2B8fO//V0IoUf+58xgaGk"+
		"bfwR6ShIgEF6ZjQiACJtHM9AvMzr5DOp3mXUgko0N/pm3RpUhSnW4rSSgUVvBj8RtevXzN"+
		"CcIOenu60dHeQpQUVARomkHDVai4QS04OHXyBI7n85zMcUgUx4Xj2RBFEaqhQxEViIIMl2"+
		"IzM9N+1XUzqKioQCxWxd4FbZFOxSO8iERfisgKqqtiSCXiqM/UIltfh2y2Ho0NWd5Ry569"+
		"SCR2I10bRyqVoDoxv+o6AkEQaDUNvj1sLdnJYi45TILAWNy2ST6ai2FY3Nd1k3/Hshzusz"+
		"NASMB2nhWUJFbI41IINABmDKXiJAvJUyL1eL6iKFRUp3UtXShKy8K6DxASsASmIW0cmYBK"+
		"kgz00CXSVqLHJksRfnJfjlDhKDeWU1VZzW/NcjzaQGYBQgK2GbZt021sKsI2x+VdmCa1TI"+
		"+NfWZTjBmLGboFXTNRLGpcJoH2mpGoqo5kMulXXbemo7fuIpfLUYLG5dI0jbfKCrPWmQXw"+
		"PPbMS1KSWPwiihwlz+F5Cws/cef2CM/d0e/BVhBKVC78J/gnykwA/AaJ/ig1kdTEFAAAAA"+
		"BJRU5ErkJggg==";

	var imgsrc_TabDup =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"BGdBTUEAAK%2FINwWK6QAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw9HKh" +
		"AAAFNklEQVRIS7WTe0xTdxTHD0QGMkPEF4oF5xbdxuZ0j%2BgkbWEtfZfHpbRUAZGqwHQx" +
		"m7AZFKOICnMGVAoVSB0w3cDR6FS2hGzL%2FnDGMIlWh0oQBEQeFpE3fdB79rvXYKIXk23Z" +
		"bvLJ73dvk%2FPJ%2BZ5TD0SE%2F%2FVhBKxkG9FMsZXc0wlphI2ETYRkQhLz%2FhKAwScW" +
		"krw6Ic7TANHeAJr5AFGLABTBT3la928LNpDiCQS9B4AWYsIPhtPh%2ByNokHmeAmoOEQT%" +
		"2BCwHTRQIsJaQT8kFP0HruAylY9CXR9M%2Bu46gtpdBTMbcRVAvmgPKfdJAKMyDF45ulu4" +
		"LtulORmHoxAVMvEM4nouHMejzwx2dYgen4HX5O3uPRSxHYDDKez1RML47oEzb%2FWR5bPK" +
		"3rapVotGWicWI7HnekY5EzjcXoSscjYymYbaNwz0AMFro3otgkQhAGVoCIByDmsXNlZ%2F" +
		"v0wgx2CgOcpSxCzB1Ixs3tIkxpF2LyXT4mtzAI2DOxORST74ThprZwNLSF4fLdK4gg%2BB" +
		"CIlhDBkmkEaWR4DBsh5NX9C51b7kgwomEFyhpW4srK1zAgexEdsGuxe2FWsHvBp4vpNSfe" +
		"w6im91H0%2B9u4OGM5gmDJOZC%2BAiBjmE7ArKKBXctDwuoQDL2yDEN%2BWYQ84wIEje9t" +
		"oHylEDVbDFFzQyFs9r61eWvpDy6%2BgT6JPBr4vHKQBMHLstfBT%2FYmCyei5VryY6IfeK" +
		"TChWBjAPqZfXFmyUz03OpLe2v9P5ynCQKInwUQR1ZSGaD137SU9tIGjXrweelBopUQHLEK" +
		"RJIIiFSoQU3gCOJUWhCIBeBPzV%2FnRfn%2B5h3tV%2B%2Bt9v%2FVK3J29Vuid%2BZFSC" +
		"SwWhUKoXIBBEpC1niFBZ1ZFbpm2bvC1SAShkOE8CMQi8SgUChZOAKlUhWglqvP6zS6O%2F" +
		"FRukYGPUFHaa%2FHUZpmhUyZo5YqQSWRR0aro69RcRqrXqO7oYvVNsfGxjZTFHVXpVJflc" +
		"sVYdMKZDJF%2BenT32K%2FrR%2B7urqws7MT2zvasbOjA1taWjAj8wsnny8Q63S6uxaLBX" +
		"%2Bq%2BxFramqwqqoKy8rKsLi4GHNzczEqKrpFKpVxO4iM1FxsaLhKOnv2oWmafKCxqKiE" +
		"5vNFhrS0j22XLl3Curo6rK6uxoqKCjSZTFhQUICFhYUYH6%2FvlcvVPpyIYmL0P1y%2BfI" +
		"Wt7nK50OFwoN1ux4mJCZbCwiJaIJAmpqRs6auvr8faWgtWVlZiaWkpHj16FPPy8lliY3Xd" +
		"FJUwgyNYv37zYGPjtWkF4%2BPjJIavMSkpdWTbth3uvXsPYXZ2LmZl7cOdO%2FdgZuZuzM" +
		"jIwh07sjAtbTu9YUP6PY4gJ%2Bcr1%2BjoKEfAFB8aGmLncu9eO7a2tuKtW7exqekWoQlv" +
		"3vwTrdYbeP26lcVqtWJOzpdujiA%2Fv8hJHo5gbGwMGTEDcx8ZGSbCQXz8eAAHBh5hf78N" +
		"Hz7sw56eHuzt7WWXIy%2FvmIsjOHzY5HQ47C8UDA8P4%2BDgICk6QIr2k6IPsa%2FvSeEH" +
		"Dx7g%2Ffv32S7b2loxP7%2BYKygoMDuZoT4%2F5KkO%2FongyJFSruDYsSrSgYMV0LQbJy" +
		"ddyERmtzNbNM5G9LzEZrM900V3dze2t7eRjTNzBSZTtYsRuN2TpDCzolOFR57J%2FdGjfr" +
		"TZmHh6SDzdJJ4uEk8ndpA%2FJUNbWwsajacmOTM4eND8%2BOTJ85Nm8zmn2XzWWV7%2BhL" +
		"IyC0tpqcV54kQtOWvJ%2Bb3TZGI44ywpqXEWF1c7jcYnkPvkgQPmIY5g6sN%2Fff4FFxwq" +
		"FbDK5UAAAAAASUVORK5CYII%3D";

	var imgsrc_SubmitAll =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw" +
		"9HKhAAADTElEQVRIS7WVWUwTURSG23Q6c4maGJeHxugLYoJ7AkKolUUUqpSCuIJFUFK2dt" +
		"pilZRFMGpkca0WFAFlMRE0mkCCoIJQCy0YWkopxVdjfLG%2BGX3QMMcZcKG2pcSGm5yne%" +
		"2Bb%2Fzn%2FuvWfYAMBa1PW%2FgOhLaFmInMuer7gZ7f8FZLcGy890hVSGyjCvEL8Asrat" +
		"pM6aTKm7tnuF%2BAdo30LW2g7ALauIOt21tYJf6N4u%2FwCPN5M1tmTQjSfCzTEhVfB8U0" +
		"XEP5B5AUItWlrYHXGyclDUftWUNHrFlGipNu23VJmElgrjXjpiPzCA2%2BMi0I7vg%2Btj" +
		"sVRBd3BluPrvmXgERJ7H2EU9AmmNWfKp2ZFPNTly4P5kFjROZkK9PR3q7Klwd%2BII1NpS" +
		"oMaW9AsghBvWPXDVEkWpujf8gbgB%2BEU450J%2FfF2LQ0a1TpHQPJUPTY7sGUDDZAYNkE" +
		"DdRCrcmTg8A9DZxHBrPAG01ngaEAvXxqKhysynVD2BlfxCjO0GKH0Ro2lxyKmH7xTQOiWH" +
		"ZkcePHBIacApaLBnwD37cRpwjK48hek7VI0K4OJICJQPb4QS43rQDK2Ds0M8IHtXO8MU2F" +
		"oXgPjmSt5da%2BaXWXESWqZkNCB3pnrtWCJcHA4DjSEQlP2rIL9vCeT1Ich7jSC%2FH4Fs" +
		"gA49AvkbBDkvCWekBtvFPEAXgOa5gGSEfwN01hQ4Z9wC8tfLIbeXFvMiSBoQMKEYRHQe8T" +
		"mqaFbcDXBZn%2FiIact1yz5Q63mzgv3E9ImnhPlQI14t1nFVcyOtDX%2BsHEKgNCJQmRgn" +
		"hDOqGBPMHR0uDkr6wvVnDWv%2BWJZ2E%2B93l2FiDLFwT%2FMm7QlOFowgOP2WaQ%2FhjC" +
		"7G%2BP%2FmuQAyO%2FABpoeM3ZxXxMcQKSdovkEmeYaTZ8wBQA4iZ3QpttNTriugE29n7C" +
		"qNBHWwkSv1NcJPdOAknf8ppsyzuNsZSJ7ipYxlhRF935bF4fkCiG5zD8wn7gaIq8Z2qEfR" +
		"tMKEvq0IYiFfgIXsu7SIE8Di0NfMoDajH5EXOGsWIuArx%2B0lx1VgEbSLr%2Bmd%2BFFf" +
		"Hy9k3%2BOwS67hZuTqCWNCPX1B%2FVxex%2FXuciwmqQG7zAtlY%2F4wfP1wuP6Iu9wihr" +
		"RY8RMWsG1Bj8nBYAAAAABJRU5ErkJggg%3D%3D";



	// -------------------------------------------------------------------------

	var imgBlockQuote =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAEvSURBVDjLY%2Fj%2F%2Fz8DJZiBagZEtO8QAuKlQPwTiP%2FjwbuAWAWbARtXHrz1" +
		"%2F%2Fefv%2F%2FxgS0n74MMuQ3EbHADgBweIP7z99%2B%2F%2Fx%2B%2B%2Ffv%2F8tO%" +
		"2F%2F88%2B%2Fvv%2F5P2%2F%2Fw%2Ff%2Fft%2F782%2F%2F7df%2Ff1%2F5xXE8OoFx0" +
		"GGmCEbIJcz9QBY8gVQ47MP%2F%2F4%2FBmp%2B8Pbf%2F7tQzddf%2FP1%2F9RnEgM5VZ0" +
		"EGeGM14ClQ86N3UM2v%2F%2F2%2F9RKi%2BQpQ88UnuA2AewHk%2FPtAW%2B%2B8%2Fvv%" +
		"2FJlDzted%2F%2F18Gar7wBGTAH7ABtYtOgAywxBqIIEOQAcg1Fx7%2FBRuMFoicuKLxDy" +
		"zK5u64Cjfo%2FecfYD5Q%2FDLWaMSGgQrvPH%2F3FabxOxDXEp0SgYp7Z267AtL4BYgLSU" +
		"rKQA1KQHwPiFPolxcGzAAA94sPIr7iagsAAAAASUVORK5CYII%3D";

	var imgTextLink =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAADpSURBVCjPY%2FjPgB8y0EmBHXdWaeu7ef9rHuaY50jU3J33v%2FVdVqkdN1SBEZtP" +
		"18T%2FL%2F7f%2FX%2Fwf%2BO96kM3f9z9f%2BT%2FxP8%2BXUZsYAWGfsUfrr6L2Ob9J%" +
		"2FX%2FpP%2BV%2F1P%2Fe%2F%2BJ2LbiYfEHQz%2BICV1N3yen%2B3PZf977%2F9z%2FQ%" +
		"2F%2FX%2Frf%2F7M81Ob3pu1EXWIFuZvr7aSVBOx1%2Fuf0PBEK3%2F46%2FgnZOK0l%2F" +
		"r5sJVqCp6Xu99%2F2qt%2Bv%2BT%2F9f%2BL8CSK77v%2Bpt73vf65qaYAVqzPYGXvdTvm" +
		"R%2Fz%2F4ZHhfunP0p%2B3vKF6%2F79gZqzPQLSYoUAABKPQ%2BkpVV%2FigAAAABJRU5E" +
		"rkJggg%3D%3D";

	var imgTextBold =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAADCSURBVCjPY%2FjPgB8yUEtBeUL5%2BZL%2FBe%2Bz61PXJ7yPnB8sgGFCcX3m%2F6" +
		"z9IFbE%2FJD%2FXucxFOTWp%2F5PBivwr%2Ff77%2FgfQ0F6ffz%2FaKACXwG3%2B27%2F" +
		"LeZjKEioj%2FwffN%2Bn3vW8y3%2Bz%2FVh8EVEf%2FN8LLGEy3%2BK%2F2nl5ATQF%2Fv" +
		"W%2B%2Fx3BCrQF1P7r%2FhcvQFPgVg%2B0GWq0zH%2FN%2FwL1aAps6x3%2B64M9J12g8p" +
		"%2F%2FPZcCigKbBJP1uvvV9sv3S%2FYL7%2Bft51SgelzghgBKWvx6E5D1XwAAAABJRU5E" +
		"rkJggg%3D%3D";

	var imgTextItalic =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAABxSURBVCjPY%2FjPgB8yUFtBdkPqh4T%2FkR%2BCD%2BA0Ie5B5P%2FABJwmxBiE%2" +
		"F%2Ff%2FgMeKkAlB%2F90W4FHg88Dzv20ATgVeBq7%2FbT7g8YXjBJf%2FRgvwKLB4YPFf" +
		"KwCnAjMH0%2F8a%2F3EGlEmD7gG1A%2FIHJDfQOC4wIQALYP87Y6unEgAAAABJRU5ErkJg" +
		"gg%3D%3D";

	var imgTextStrikethrough =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAACfSURBVCjPY%2FjPgB8yUFNBiWDBzOy01PKEmZG7sSrIe5dVDqIjygP%2FY1GQm5b2" +
		"P7kDwvbAZkK6S8L%2F6P8hM32N%2FzPYu2C1InJ36P%2FA%2Fx7%2Fbc%2BYoSooLy3%2F" +
		"D4Px%2F23%2BSyC5G8kEf0EIbZSmfdfov9wZDCvc0uzLYWyZ%2F2J3MRTYppn%2F14eaIv" +
		"KOvxxDgUma7ju1M%2FLlkmnC5bwdNIoL7BAAWzr8P9A5d4gAAAAASUVORK5CYII%3D";

	var imgTextColor =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAMOSURBVDjLVZNNaBxlAIafb%2Bab2Z3N7Oxv%2FnYTEyv2LzQJpKBgrQqNUKmY4kUI" +
		"XqUHT70p9iB48CKIiN5E0It6KFiwiv9FpAVpKUggNc3mZ7vpJpv9n93ZnZ35PNRI%2B8B7" +
		"e9%2Fn9gqlFAeIVUfPeN3zh0R0eVpYM1OanhvTCEY0f3tU79%2BctnpfHM73fuQhxIHAWH" +
		"nmkOGXPjgZyS09l5hnNv4YOdMhoQmigzqGt4nhfeub1fpnVsl%2Fe%2BhMv%2Fq%2FQKy%" +
		"2BMe0EO5dfso%2FOvzB8grgV4HGXJC7jwAQ2oxxDuC36xZ%2BRhe%2Bv6iutZf2iqklReN" +
		"e0tPSHZ2Nz84ujR7ht3iJKjcexiOIQI8SiixxcR7QtRORFlK7O9t0rlyy4KBEj5%2BYisV" +
		"eez85wy9zGIUeGDDYhDhYOITYuoh2BvTJ68y7B0GnCym8XGq%2BKL2U0MrE8Z2SRVhqdPm" +
		"lCsvgk8RlCkgAivRbUNKj1YPMeeu4wcnjRql7%2F%2BjVpyvxsPjbK3whi5LEAB0WWgBRg" +
		"qwAaFah04X4V7puwdwddz%2BFXjJMSbXI8aSTYCgU2oKMwEdgCEoDhug%2FG5SjsmFDUoV" +
		"%2BDXJ7BnpiUVCNBaJqEXfDVfwG6CjoKnF4crZGCVvNBug0IPXzPZOCnAunfk8W6ro7H2g" +
		"K3A02gGoDeA1MDGx2nkYG6C24bvDaMSzq7ZfxBsiC7O%2BaNDaWOn0oLfl0HMwDlQRCAHY" +
		"UkEGvFkLsp2G9Bo0n41AiNG6sMBvY1yZr6%2FJsV%2F%2FXZZ3WZaEp2t6DvgWFA1QRHQb" +
		"wjSDeTUGvCiSPU1ovU%2FtypQPIrTV0yrrl3vE%2B%2F%2B8XlaCIgq8H%2BBtSLUN2C2i" +
		"bsl8ArR%2BHYGE0rwvbvRTr96HsL6od1CUDDf%2BenK92JwT%2B982cWEswvRmiug6qAr0" +
		"E4AV4uoFXosnV1g8bN5kcp7E8eOZOYKtmUqm%2FZiDdfPhV3Zp6IM5k0SIUBstwmXKvCX5" +
		"UdM6y9n2b34wV1IXxEcEBU3J4dprU0zODpjFBTIyoIxgjXxlB%2FPIl1eUmdLjzc%2Fxce" +
		"OVXddrB6BQAAAABJRU5ErkJggg%3D%3D";

	
	var KZ_BookMark = GM_getValue("KZ_BookMark", 'blank');
	var KZ_Days_to_Respond = parseInt(GM_getValue("KZ_Days_to_Respond", "30"));
	var KZ_Auto_Close_Tabs = GM_getValue("KZ_Auto_Close_Tabs", false);
	var OAN_BookMark = GM_getValue("OAN_BookMark", 'blank');
	var OAN_Days_to_Respond = parseInt(GM_getValue("OAN_Days_to_Respond", "30"));
	var OAN_Auto_Close_Tabs = GM_getValue("OAN_Auto_Close_Tabs", false);

	let eventHold = false; //eventHold prevents the script from having multiple asynch processes sending events
	let lastSetPlannedValuesTextRequest = "blank";
	let lastSetPlannedValuesLogValueRequest = "Unknown";
	let cannotDeleteCheckboxFlag = true;


	// spawn a listener to adjust eventHoldClear250ms
	let eventHoldClear250ms = true;
	let eventHoldClear250msTimer = 0;
	setInterval(() => {
		if (eventHold) {
			clearEventHoldTimer();
			return;
		}
		eventHoldClear250msTimer += 25;
		if (eventHoldClear250msTimer > 250) eventHoldClear250ms = true;
	},25);

	function clearEventHoldTimer(){
		eventHoldClear250ms = false;
		eventHoldClear250msTimer = 0;
		return;
	}

	// -------------------------------------------------------------------------

	//  Get currently signed-on geocaching.com profile.  
	SignedInX = document.querySelectorAll('span[class*="username"]')[0];	
	if (SignedInX) {
		var SignedInAs =  SignedInX.firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { 
		console.log('user is not signed in - log entry script exiting');
		return; 
	} 

	// if on the bookmark page, update the bookmark list used by the kill zone scripts
	if (document.URL.indexOf("geocaching.com/bookmarks/mark") > 0) {
		updateBookmarkList(); //process all actions on the bookmark page
		console.log('exiting from log entry script on bookmarks page');
		return;
	}

	if (document.getElementById('gm_divCtrls')) { //mostly for debugging - avoids the script rerunning on error
		console.log('Exiting log script after log script error');
		return;
	}
	else {	insertControlButtons();} 

//	var encryptChk = document.getElementById('ctl00_ContentBody_LogBookPanel1_chkEncrypt'); //todo I'm not seeing an encrypt control
 	let cannotDeleteCheckbox = document.querySelectorAll('input[type=checkbox]')[0];
	if (cannotDeleteCheckbox == undefined) {cannotDeleteCheckbox = null};

// todo - I never see these console.logs, and the window.close()s are depricated, so I think this code is obsolete.
	//  Check session storage to see if auto-close was requested.
	if (sessionStorage.getItem('autoclose') == 'true') {
		if (sessionStorage.getItem('dbcu') == 'true') {
//			window.opener.close();
			console.log('on line 346, an autoclose was attempted on the window opener');
		}
//		window.close();
		console.log('on line 559, an autoclose was attempted');
		return;
	}

	//  If auto-close requested, set session storage value.
	if (UrlParm('close') == 'y') {
		//  Set session storage value to close invoking tab on redisplay.
		sessionStorage.setItem('autoclose', 'true');
		if (UrlParm('dbcu') == 'y') {
			//  Set session storage value to close tab on redisplay.
			sessionStorage.setItem('dbcu', 'true');
		}
	}

	// load the saved logType/logValue map, if it exists
	if (GM_getValue('logTypeMap',false)) {
		logTypeNumtoText = new Map(Object.entries(JSON.parse(GM_getValue('logTypeMap'))));
		console.log('New Log just loaded the saved version of the logType Map.');
	}
	// start periodic process to check to make sure the current logType/name combo is in the Map
	setInterval(() => {
		fCheckLogTypeMap();
	}, 1000);

	// Enhancement switches.
	// Set value to true or false.
	var HCA_AutoSubmit = true;	// Auto-submit handicap notices.

	// Set general auto submit value.
	if (UrlParm('auto') == 'y') {
		var AutoSubmit = true;
	}

	//  Array for language versions.
	var aLangText = new Array();

	// Set text, if passed.
	var dftText = decodeURIComponent(UrlParm('text'));

	// Locate textarea element.
	var logText = document.querySelectorAll('input[id=log-text-control]')[0];

	// Only apply following changes if signed on as a reviewer.
	if (cannotDeleteCheckbox != null) {
		updateReviewerList();

		// check to be sure that this reviewer has a bookmark list, and go get one if they don't
		let bookmarkOptions=GM_getValue(SignedInAs+'bmlist',"blank");
		if (bookmarkOptions == 'blank') {
			console.log('no bookmark list found.  Going and getting one');
			refreshBenchmarkList();
		}

		GM_registerMenuCommand('Edit "Kill Zone" Script Settings', fKZEditSettings);

		// NOTICE: Do not edit the text below. This is only default text.
		// Instead, use 'Edit Settings' from the User Script Commands menu.
		var dftRevNoteKZ =
				"I noticed that this cache has been temporarily disabled for " +
				"a period of time well in excess of the period of \"a few " +
				"weeks\" as contemplated by the cache guidelines published on " +
				"Geocaching.com. While I feel that Geocaching.com should hold " +
				"the location for you and block other caches from entering " +
				"the area around this cache for a reasonable amount of time, " +
				"we can't do so forever. Please either repair/replace this " +
				"cache, or archive it (using the [i]archive listing[/i] link " +
				"in the upper right) so that someone else can place a cache " +
				"in the area, and geocachers can once again enjoy visiting " +
				"this location." +
				"\n\n" +
				"If you plan on repairing this cache, please [b]log a note to " +
				"the cache[/b] (not email) within the next %D% days so I don't " +
				"archive the listing for non-communication.";
	
		var dftArcNoteKZ =
				"As there's been no cache to find for a long time, I'm archiving " +
				"it to keep it from showing up in search lists, " +
				"and to prevent it from blocking other cache placements. " +
				"\n\n" +			
				"Please note that if geocaches are archived by a reviewer " +
				"or Geocaching HQ for lack of maintenance, " +
				"they are not eligible for unarchival.";
	
		var KZ_RevNote = GM_getValue("KZ_RevNote", dftRevNoteKZ);
		var KZ_ArcNote = GM_getValue("KZ_ArcNote", dftArcNoteKZ);
	
		GM_registerMenuCommand('Edit "Owner Action Needed" Script Settings', fOANEditSettings);

		// NOTICE: Do not edit the text below. This is only default text.
		// Instead, use 'Edit Settings' from the User Script Commands menu.
		var OANdftRevNote =
				"The cache appears to be in need of owner intervention. " +
				"I'm temporarily disabling it, to give the owner an opportunity to " +
				"check on the cache, and take whatever action is necessary. " +
				"Please respond to this situation in a timely " +
				"manner (i.e., within %D% days) to prevent the " +
				"cache from being archived for non-responsiveness.";
	
		var OANdftArcNote =
				"The cache owner is not responding to issues with this geocache, " +
				"so I must regretfully archive it. " +
				"\n\n" +			
				"Please note that if geocaches are archived by a reviewer " +
				"or Geocaching HQ for lack of maintenance, " +
				"they are not eligible for unarchival.";
	
		var OANRevNote = GM_getValue("OAN_RevNote", OANdftRevNote);
		var OANArcNote = GM_getValue("OAN_ArcNote", OANdftArcNote);

		var settingAutopostAutocloseEnableUnarchive = GM_getValue('settingAutopostAutocloseEnableUnarchive','false');
		if (settingAutopostAutocloseEnableUnarchive == 'true') 
			menuAutopostEnable = GM_registerMenuCommand('Option: Turn OFF Auto-Post Enable and Unarchive',fAutopostEnable);
		else menuAutopostEnable = GM_registerMenuCommand('Option: Turn ON Auto-Post Enable and Unarchive',fAutopostEnable);
		menuScriptHydrationDelay = GM_registerMenuCommand('Adjust Script Initialization Delay ('+
			settingScriptHydrationDelay+' Seconds)',fScriptHydrationDelay);
		GM_registerMenuCommand('Template: Import All Templates', fImportTemplates);
		GM_registerMenuCommand('Template: Export All Templates', fExportTemplates);

		GM_registerMenuCommand('Template: View/Edit Proximity Template and Log Type', fEditProxTmpl);
		GM_registerMenuCommand('Template: Save Proximity Template and Log Type...', fSaveProxTmpl);
		GM_registerMenuCommand('Template: View/Edit Vacation Template and Log Type', fEditVacaTmpl);
		GM_registerMenuCommand('Template: Save Vacation Template and Log Type...', fSaveVacaTmpl);
		GM_registerMenuCommand('Template: View/Edit Handicap Template and Log Type', fEditHndiTmpl);
		GM_registerMenuCommand('Template: Save Handicap Template and Log Type...', fSaveHndiTmpl);
		GM_registerMenuCommand('Template: View/Edit Update Coordinates Template and Log Type', fEditUpcoTmpl);
		GM_registerMenuCommand('Template: Save Update Coordinates Template and Log Type...', fSaveUpcoTmpl);

		// experimenting with getting a log date - this code is good, though a date must already be selected
		logDate = document.getElementsByClassName('flatpickr-input')[0]; //logDate.value is still valid Oct12

		// populate the list of available log types 
		var okLogTypes = new Array();
		selectedLogType = document.querySelectorAll('input[name=logType]')[0];
		logTypeTrigger = document.getElementById('react-select-cache-log-type-input');

		//  Get LogType.
		var LogType = UrlParm('logType');  //this value seems to switch back and forth between LogType and logType
		if (!LogType) LogType = UrlParm('LogType');

		try {var plannedLogType = 1*LogType;} //LogType may be empty or have a text value
		catch {var plannedLogType = '99';} //placeholder to insert into log

		if (Number.isNaN(plannedLogType)) plannedLogType = '99';

		// where the server set the logType, don't do it again
		if (!(document.URL.indexOf('&')>0) ||
			(LogType=="47") ) {suppressLogTypeSelection = true;} //lt=47 is the only preselected type with parameters
		if (!suppressLogTypeSelection) getSetLogType("",true); //note that this is asynch, and other clicking processes need to not interfere

		// Parsing for kill zone and owner action needed scripts
		if (UrlParm('kz') == 'y'){
			suppressLogTypeSelection = true;
			if (LogType == '5') { //Archive
				plannedText=KZ_ArcNote.replace(/%D%/g, KZ_Days_to_Respond);
				plannedLogType = LogType;
			}
			else if (LogType == '68') { //Reviewer Note
				plannedText=KZ_RevNote.replace(/%D%/g, KZ_Days_to_Respond);
				plannedLogType = LogType;
			}
			if (KZ_Auto_Close_Tabs) {
				AutoSubmit = true;
				sessionStorage.setItem('newLogAction','true');
			}
		}

		if (UrlParm('oan') == 'y'){
			suppressLogTypeSelection = true;
			if (LogType == '5') { //Archive
				plannedText = OANArcNote.replace(/%D%/g, OAN_Days_to_Respond);
				plannedLogType = LogType;
			}
		
			if (LogType == '22') { //Disable
				plannedText = OANRevNote.replace(/%D%/g, OAN_Days_to_Respond);
				plannedLogType = LogType;
			}
			if (OAN_Auto_Close_Tabs) {
				AutoSubmit = true;
				sessionStorage.setItem('newLogAction','true');
			}
		}

		//  insert text from URL parameter, if necessary;
		if (!plannedText) {
			plannedText = dftText;
			if (dftText.length >0) googleTagLogNL('url_text');
		}

		//  If Review Note, set Cannot Delete, if option selected.
		if (LogType == '18' || LogType == '68') {
			if (GM_getValue('OptRevNoteUndel', 'Off') == 'On') {
				// setCannotDeleteCheckbox();
			}
		}

		//  If Disable note, default 'Cannot Delete' box to checked.
		if (LogType == '22') {
			// setCannotDeleteCheckbox();
		}

		//  If Unarchive note, set default text to 'Unarchive cache.'
		if (LogType == '12') {
			if (plannedText == "") {
				plannedText='Unarchiving cache.';
				if (settingAutopostAutocloseEnableUnarchive == 'true') {
					AutoSubmit = true;
					sessionStorage.setItem('newLogAction',"true");
				}
			}
		}

		//  If Enabling note, set default text to 'Enabling cache.'
		if (LogType == '23') {
			if (plannedText == "") {
				plannedText='Enabling cache.';
				if (settingAutopostAutocloseEnableUnarchive == 'true') {
					AutoSubmit = true;
					sessionStorage.setItem('newLogAction',"true");
				}
			}
		}

		//  If Publish note, set default text to 'Published'
		if (LogType == '24') {
			if (plannedText == "") {
				plannedText='Published';
			}
		}

		//  If Update Coordinates,  --- this code used by "update coordinates" link inserted on review page
		if (LogType == '47') {
			if (plannedText == "") {
				var upcoNote = GM_getValue('UpcoNote', 'Updating coordinates.');
//				var owner    = decodeURIComponent(UrlParm('owner'));
				var owner = document.querySelector('p>a[data-event-label="Cache Log - cache owner link"]').innerText;
//				var title    = decodeURIComponent(UrlParm('title'));
				var titleElement = document.querySelector('h2>a[data-event-label="Cache Log - cache name link"]');
				var title = titleElement.innerText;
//				var wptid    = decodeURIComponent(UrlParm('wptid'));
				var wptid = titleElement.href.substring(titleElement.href.indexOf('GC'));
				var wptno    = WptToID(wptid);
//				var coords   = decodeURIComponent(UrlParm('coords'));
				var coords = document.querySelector('div>input[data-event-label="Cache log - coordinate entry"]').value;
				console.log(coords);
				if (coords.length > 4) var dlatlon  = dm2dd(coords);
				else var dlatlon = ['unknown','unknown'];
				if (title.length) {
					upcoNote = upcoNote.replace(/%owner%/gi, owner);
					upcoNote = upcoNote.replace(/%title%/gi, title);
					upcoNote = upcoNote.replace(/%wptid%/gi, wptid);
					upcoNote = upcoNote.replace(/%wptno%/gi, wptno); 
					upcoNote = upcoNote.replace(/%coords%/gi, coords);
					upcoNote = upcoNote.replace(/%dlat%/gi, dlatlon[0]); 
					upcoNote = upcoNote.replace(/%dlon%/gi, dlatlon[1]); 

					var rtnLang = fParseLang(upcoNote);
					if (rtnLang[0] == 0) {
						plannedText = upcoNote;
					} else {
						if (rtnLang[1] >= 0) {
							plannedText = aLangText[rtnLang[1]].trim();
						}
					}
				}

			}
		}


		//  If Proximity Note, get passed data and template.
		if (LogType == 'prox') {
			suppressLogTypeSelection = false;
			getSetLogType("",true);
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var nctitle  = decodeURIComponent(UrlParm('nctitle'));
			var ncwptid  = decodeURIComponent(UrlParm('ncwptid'));
			var ncdistft = decodeURIComponent(UrlParm('ncdistft'));
			var ncdistm  = decodeURIComponent(UrlParm('ncdistm'));
			var ncdir    = decodeURIComponent(UrlParm('ncdir'));
			if (ncdir == 'Here') { ncdir = ''; }
			var proxNote = GM_getValue('ProxNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			proxNote = proxNote.replace(/%owner%/gi,    owner);
			proxNote = proxNote.replace(/%title%/gi,    title);
			proxNote = proxNote.replace(/%wptid%/gi,    wptid);
			proxNote = proxNote.replace(/%wptno%/gi,    wptno);
			proxNote = proxNote.replace(/%coords%/gi,   coords);
			proxNote = proxNote.replace(/%dlat%/gi,     dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,     dlatlon[1]);
			proxNote = proxNote.replace(/%nctitle%/gi,  nctitle);
			proxNote = proxNote.replace(/%ncwptid%/gi,  ncwptid);
			proxNote = proxNote.replace(/%ncdistft%/gi, ncdistft);
			proxNote = proxNote.replace(/%ncdistm%/gi,  ncdistm);
			proxNote = proxNote.replace(/%ncdir%/gi,    ncdir);
			proxNote = proxNote.replace(/%dlat%/gi,     dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,     dlatlon[1]);

			var rtnLang = fParseLang(proxNote);
			if (rtnLang[0] == 0) {
				plannedText = proxNote;
				if (AutoSubmit) sessionStorage.setItem('newLogAction',"true");
			} else {
				if (rtnLang[1] >= 0) {
					plannedText = aLangText[rtnLang[1]].trim();
				}
			}

			plannedLogType = GM_getValue('ProxType', '18');
		}


		//  If Litmus-1 Note, get passed data and template.
		if (LogType == 'lit1') {
			suppressLogTypeSelection = false;
			getSetLogType("",true);
			var owner       = decodeURIComponent(UrlParm('owner'));
			var title       = decodeURIComponent(UrlParm('title'));
			var wptid       = decodeURIComponent(UrlParm('wptid'));
			var wptno       = WptToID(wptid);
			var coords      = decodeURIComponent(UrlParm('coords'));
			var dlatlon     = dm2dd(coords);
			var nctitle     = decodeURIComponent(UrlParm('nctitle'));
			var ncwptid     = decodeURIComponent(UrlParm('ncwptid'));
			var nctype      = decodeURIComponent(UrlParm('nctype'));
			var ncdistft    = decodeURIComponent(UrlParm('ncdistft'));
			var ncdistftfuz = decodeURIComponent(UrlParm('ncdistftfuz'));
			var ncdistm     = decodeURIComponent(UrlParm('ncdistm'));
			var ncdistmfuz  = decodeURIComponent(UrlParm('ncdistmfuz'));
			var ncdir       = decodeURIComponent(UrlParm('ncdir'));
			if (ncdir == 'Here') { ncdir = ''; }
			var ncdirx      = fExpandDir(ncdir);
			var proxNote = GM_getValue('Lit1Note', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			proxNote = proxNote.replace(/%owner%/gi,       owner);
			proxNote = proxNote.replace(/%title%/gi,       title);
			proxNote = proxNote.replace(/%wptid%/gi,       wptid);
			proxNote = proxNote.replace(/%wptno%/gi,       wptno);
			proxNote = proxNote.replace(/%coords%/gi,      coords);
			proxNote = proxNote.replace(/%dlat%/gi,        dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,        dlatlon[1]);
			proxNote = proxNote.replace(/%nctitle%/gi,     nctitle);
			proxNote = proxNote.replace(/%ncwptid%/gi,     ncwptid);
			proxNote = proxNote.replace(/%nctype%/gi,      nctype);
			proxNote = proxNote.replace(/%ncdistft%/gi,    ncdistft);
			proxNote = proxNote.replace(/%ncdistftfuz%/gi, ncdistftfuz);
			proxNote = proxNote.replace(/%ncdistm%/gi,     ncdistm);
			proxNote = proxNote.replace(/%ncdistmfuz%/gi,  ncdistmfuz);
			proxNote = proxNote.replace(/%ncdir%/gi,       ncdir);
			proxNote = proxNote.replace(/%ncdirx%/gi,      ncdirx);

			var rtnLang = fParseLang(proxNote);
			if (rtnLang[0] == 0) {
				plannedText = proxNote;
			} else {
				if (rtnLang[1] >= 0) {
					plannedText = aLangText[rtnLang[1]].trim();
				}
			}

			plannedLogType = GM_getValue('Lit1Type', '18');
		}


		//  If Vacation Note, get passed data and template.
		if (LogType == 'vaca') {
			suppressLogTypeSelection = false;
			getSetLogType("",true);
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var vdistmi  = decodeURIComponent(UrlParm('vdistmi'));
			var vdistkm  = decodeURIComponent(UrlParm('vdistkm'));
			var vacaNote = GM_getValue('VacaNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			vacaNote = vacaNote.replace(/%owner%/gi,   owner);
			vacaNote = vacaNote.replace(/%title%/gi,   title);
			vacaNote = vacaNote.replace(/%wptid%/gi,   wptid);
			vacaNote = vacaNote.replace(/%wptno%/gi,   wptno);
			vacaNote = vacaNote.replace(/%coords%/gi,  coords);
			vacaNote = vacaNote.replace(/%dlat%/gi,    dlatlon[0]);
			vacaNote = vacaNote.replace(/%dlon%/gi,    dlatlon[1]);
			vacaNote = vacaNote.replace(/%vdistmi%/gi, vdistmi);
			vacaNote = vacaNote.replace(/%vdistkm%/gi, vdistkm);

			var rtnLang = fParseLang(vacaNote);
			if (rtnLang[0] == 0) {
				plannedText= vacaNote;
				if (AutoSubmit) sessionStorage.setItem('newLogAction',"true");
				//				e_LogInfo.value = vacaNote;
			} else {
				if (rtnLang[1] >= 0) {
					plannedText=aLangText[rtnLang[1]].trim();
				}
			}
			plannedLogType = GM_getValue('VacaType', '18');
			console.log('set vaca data with '+plannedLogType+" "+plannedText);
		}

		//  If Handicap Note, get passed data and template.
		if (LogType == 'hndi') {
			suppressLogTypeSelection = false;
			getSetLogType("",true);
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var hndiNote = GM_getValue('HndiNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			hndiNote = hndiNote.replace(/%owner%/gi,  owner);
			hndiNote = hndiNote.replace(/%title%/gi,  title);
			hndiNote = hndiNote.replace(/%wptid%/gi,  wptid);
			hndiNote = hndiNote.replace(/%wptno%/gi,  wptno);
			hndiNote = hndiNote.replace(/%coords%/gi, coords);
			hndiNote = hndiNote.replace(/%dlat%/gi,   dlatlon[0]);
			hndiNote = hndiNote.replace(/%dlon%/gi,   dlatlon[1]);

			var rtnLang = fParseLang(hndiNote);
			if (rtnLang[0] == 0) {
				plannedText = hndiNote;
			} else {
				if (rtnLang[1] >= 0) {
					plannedText = aLangText[rtnLang[1]].trim();
				}
				AutoSubmit = false;
			}

			plannedLogType = GM_getValue('HndiType', '18');
		}
		if (plannedText.length>0) {
			if (!(plannedText == dftText)) googleTagLogNL('template_paste'); //the dftText tag was already sent
			setPlannedValues (plannedLogType,plannedText);
		}
	}
	else console.log("not a reviewer - exiting");
	endScriptProcessing();
	return;

	function endScriptProcessing(){
		// now process any cached events
		if (cachedPlannedText != "blank") {
			suppressLogTypeSelection = false;
			if (cachedNoDelete) setCannotDeleteCheckbox();
			else clearCannotDeleteCheckbox();
			console.log('found cached text and type');
			setPlannedValues(cachedPlannedLogValue,cachedPlannedText);
		}

		if (cachedSubmit) fPostLogByPOSTMethod(); //fSubmitLog();

		// replace the caching event listener with the one that will act immediately
		window.removeEventListener('storage', fStorageCache, false);
		window.addEventListener('storage', fStorageChanged, false);
//		document.head.removeChild(icon);  Leave "a" icon in place to indicate listening status
		
		//  Add event to save text if leaving log entry page.
		window.addEventListener("beforeunload", fBeforeExit, true);

		if (AutoSubmit) {
			fPostLogByPOSTMethod();
	//		fSubmitLog();
			console.log('the autosubmit option fired');
		}

		// Startup process to detect manual pastes into log text area
		let oldLogLength = logText.value.length;
		let delayPasteListen=false;
		setInterval(()=>{
			if (suspendManualPasteSensor) {
				suspendManualPasteSensor = false;
				delayPasteListen=true;
				return;
			}
			if (delayPasteListen) {
				delayPasteListen = false;
				oldLogLength = logText.value.length;
				return;
			}
			var difference = logText.value.length - oldLogLength;
			if (difference > 100) {
				googleTagLogNL('manual_paste');
			}
			oldLogLength = logText.value.length;
		},2000);

//		console.log('New log has completed its setup actions');
	}

	//  Before exit from page.
	function fBeforeExit() {
		if (logText.value.trim().length > 0) {
			GM_setValue('LastLogText', logText.value);
		}
	}


	// ---------------------------------- Functions --------------------------------------------------------------------------------------- //

	// detect whether images have been appended to a log
	function fDetectImages() {
		let anchorDiv = document.getElementsByClassName('editor-wrapper')[0].nextSibling; 
		if (anchorDiv.getElementsByTagName('img')[0]) return true;
		else return false;
	}

	// set log values to planned values
	function setPlannedValues (sentPlannedLogValue,plannedText) {
		clearEventHoldTimer();
		suspendManualPasteSensor = true;
//		console.log("initial setPlannedValues data is: "+sentPlannedLogValue+", "+plannedText);
		lastSetPlannedValuesTextRequest = plannedText;
		if (!isNaN(sentPlannedLogValue)) { //this is for backwards compatibility
			plannedLogValue =  getLogTypeValue(1*sentPlannedLogValue);
			console.log('setPlannedValues converted a numerical log type to: '+plannedLogValue);
		}
		else plannedLogValue = sentPlannedLogValue;
		lastSetPlannedValuesLogValueRequest = plannedLogValue;
		if (!suppressLogTypeSelection) {
			getSetLogType("",true); //we didn't do it before, so we have to do it now
		} 
		let okLogTypesTrialCount = 0;
		console.log('trying setPlannedValues with '+plannedLogValue+" and "+plannedText);
		let insertValues = setInterval(()=>{ //waiting to be sure that okLogTypes is populated
			okLogTypesTrialCount++;
			if (okLogTypesTrialCount == 20) {
				console.log("New Log is waiting a long time - is it lazy loading?");
			}
			if (suppressLogTypeSelection && !eventHold) {
				clearInterval(insertValues);
				console.log('Inserted plannedText without a log Type');
				insertTextContent(plannedText);
			}
			else if (okLogTypes.length>0 && !eventHold) { //we're not waiting for log types to populate
				clearInterval(insertValues);
				insertTextContent(plannedText); //set the message text
//				setTodaysDate(); //set the date - looks like this is now website automatic
				if (!suppressLogTypeSelection) {
					if (okLogTypes.indexOf(plannedLogValue)>-1) {
							getSetLogType(plannedLogValue);  //set the log type
							console.log('Log type set on first time through.');
					}
					else {
						if (plannedLogValue == 'Unknown') return
						else {
							console.log('The #'+plannedLogValue+"# was not in the existing list.  Refreshing list. Old list was: "+okLogTypes);
							console.log('The index value is: '+okLogTypes.indexOf(plannedLogValue));
							console.log('The first value is: '+okLogTypes[0]);
							getSetLogType("",true);
							insertValues = setInterval(()=>{
								if (!eventHold) {
									clearInterval(insertValues);
									console.log('The refreshed list of log types is: '+okLogTypes);
									if (okLogTypes.indexOf(plannedLogValue)>-1) {
										getSetLogType(plannedLogValue);  //set the log type
									}
									else alert("The "+plannedLogValue+" action is not available on this cache."+
										"\nIf you got this error using a template, re-save the template.");
								}
							},100);
						}
					}
				}
			}
		},100); 
		console.log('setPlannedValues fired');
	}
	
	// get the list of available log types and set a target type.  Reset = true returns a new list, = false just sets the type. 
	// this will take as long as it needs, but can be derailed if other events are fired while it's running.  That's why there
	// is an eventHold flag.  
	function getSetLogType(target,reset=false) { //reset flag to refresh okLogTypes
		if (target == 'Unknown') {
			return;
		}
		let anotherLoop = setInterval(()=>{
			if (eventHold) return;
			else {
				eventHold = true;
				clearInterval(anotherLoop);
				//check to see if the listbox is already set correctly
				let logTypeListbox = document.getElementById('react-select-cache-log-type-listbox');
				if (logTypeListbox) {
					let logTypeList = logTypeListbox.querySelectorAll('span');
					if (logTypeList) {
						if (logTypeList[0] == target) {
							eventHold = false;
							return; //the listbox has already been set
						}
					}
				}
				dispatchThis(logTypeTrigger,"focusin");
				dispatchThis(logTypeTrigger,"mousedown");
				if (reset && !(okLogTypes == undefined)) {
					let oldLength = okLogTypes.length;
					for (var i=0;i<oldLength;i++) {okLogTypes.pop();}
				}
				let outerLoop = setInterval(()=>{
					let logTypeListbox = document.getElementById('react-select-cache-log-type-listbox');
					if (logTypeListbox) {
						let logTypeList = logTypeListbox.querySelectorAll('span');
						if (logTypeList) {
							clearInterval(outerLoop);
							for (var i=0; i<logTypeList.length;i++) {
								if (reset) {okLogTypes.push(logTypeList[i].innerText);}
								if (okLogTypes[i] == target) {var selectThis = logTypeList[i]};
							}
							if (selectThis) {dispatchThis(selectThis,"click");}
							else {
								var placeholder=document.getElementById('react-select-cache-log-type-placeholder');
								if (placeholder) {
									dispatchThis(placeholder,"mousedown");
								} else {
									var oldOption = document.getElementsByClassName('log-type-option')[0].getElementsByTagName('span')[0];
									dispatchThis(oldOption,"mousedown");
								}
							}
							eventHold = false;
						}
					}
				},100);
			}
		},50);
	}

	// get the log type numerical value based on the text value
	function getLogTypeKey (logTypeName) {
		let logType = '99';
		if (logTypeName) {
			logTypeNumtoText.forEach((value,key) => {
				if (value==logTypeName) logType=key;
			})
		}
		return logType;
	}

	// get the log type text value based on the numerical value
	function getLogTypeValue (logTypeKey) {
		if (logTypeKey) 
			if (logTypeNumtoText.has(logTypeKey.toString())) {
				//console.log('returning from has statement');
				return logTypeNumtoText.get(logTypeKey.toString());
			}
		//console.log('returning after the HAS statement failed');
		//console.log(logTypeNumtoText);
		return logTypeNumtoText.get('99');
	}


	// maintain a list of users of this browser with reviewer credentials
	function updateReviewerList() {
		let reviewerList = GM_getValue("reviewers",'blank');
		if (reviewerList.search(SignedInAs+"xx")>-1) {
			return; //the stored list already includes this reviewer
		}
		else {
			reviewerList += SignedInAs+"xx"; //add this reviewer to the stored list
			GM_setValue("reviewers",reviewerList);
		}
	}

	// if on bookmark page and signed in as reviewer, update the list of valid bookmark pages
	// this code also dispatches the bookmark as requested by the KZ suite
	function updateBookmarkList(){
		console.log("log entry found a bookmarks page");
		var reviewerList = GM_getValue("reviewers","blank");
		if (reviewerList.indexOf(SignedInAs+"xx")>-1) { // the user is signed in as a reviewer
			console.log("user is a reviewer");
			var e_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
			if (!e_btnSubmit) {
				console.log('no create bookmark button'); //we must be here since a cache was just bookmarked
				if (((UrlParm('oan').indexOf('y') > -1) && OAN_Auto_Close_Tabs) || 
					((UrlParm('kz').indexOf('y') > -1) && KZ_Auto_Close_Tabs)) {
					console.log('The idea is to autoclose, but the feature is turned off 1165.');
					//window.close();
					return; //and call it a day
				}
			}
			else { //there is a submit button, so we can populate the bookmark list and mark a cache if requested.
				var testDuplicateBookmark = document.getElementsByClassName('Validation')[0];
				if (testDuplicateBookmark) {
					console.log('This cache is already on the intended bookmark.  Exiting log script on bookmark page.');
					return;
				}
				var targetBookmark = "none";
				let IncDays = 30;
				//if there is an incoming request, set the bookmark to oan or kz
				let KZOANautoclose = false;
				if (UrlParm('oan') == 'yn') {
					targetBookmark = OAN_BookMark;
					IncDays = OAN_Days_to_Respond;
					KZOANautoclose = GM_getValue("OAN_Auto_Close_Tabs",false);
				}
				else if (UrlParm('kz') == 'yn') {
					targetBookmark = KZ_BookMark;
					IncDays = KZ_Days_to_Respond;
					KZOANautoclose = GM_getValue("KZ_Auto_Close_Tabs",false);
				}
				var bmList = document.getElementById('ctl00_ContentBody_Bookmark_ddBookmarkList');
				var ic = bmList.options.length;
				if (ic == undefined) return;
				let selectCompleted=false;
				let bmNames = new Array();
				for (var i = 0; i < ic; i++) {
					bmNames.push(bmList.options[i].innerText); //load array with bookmark list name choices
					if (bmNames[i] == targetBookmark) {
						bmList.options[i].selected = true;
						selectCompleted = true;
						console.log('we selected the '+targetBookmark+ 'bookmark');
					}
					else if (targetBookmark !== 'none') {
						bmList.options[i].selected = false;
						console.log('new log deselected: '+bmList.options[i]);
					}
				}
				GM_setValue(SignedInAs+'bmlist',bmNames.toString()); //save the bookmark list
				if (targetBookmark !== "none") { //we must have a kz or oan request
					e_BookmarkName = document.getElementById("ctl00_ContentBody_Bookmark_tbName");
					if (e_BookmarkName) {
						// Append current date to cache name.
						var d = new Date();
						d.setDate(d.getDate() + IncDays);
						var curr_date = d.getDate(d) + '';
						if (curr_date.length < 2) {curr_date = '0' + curr_date}
						var curr_month = d.getMonth(d) + 1;	// Month is zero indexed.
						curr_month += '';
						if (curr_month.length < 2) {curr_month = '0' + curr_month}
						var curr_year = d.getFullYear(d) + '';
						var NewDate = curr_year  + "-" + curr_month + "-" + curr_date;
						// Insert date before cache name.
						e_BookmarkName.value = NewDate + ' . . . ' + e_BookmarkName.value;
						if (selectCompleted) {
							if (KZOANautoclose) sessionStorage.setItem('closeBookmarkOnSuccess',"true");
							e_btnSubmit.click(); //submit the bookmark listing
							console.log('bookmark request posted');
						}
						else {
							alert('The desired bookmark list #'+targetBookmark+'# could not be found.');
							return; //there was no match for the target bookmark, and we want to error out
						}
					}
				}
				if (document.URL.indexOf('bmlist=y') > -1) {
					window.close();
					console.log('tried to close window according to bmlist request');
				}
			}
		}
	}

	//  Autosubmit a log, if requested.
	function fSubmitLog() {
		//  Function to click the log submit button.
		let postButton = document.querySelectorAll('button[class~=submit-button]')[0];
		let successfulIteration = 0;
		var outerloop = setInterval(()=>{
			console.log('fSubmitLog is sitting and spinning.');
			if (eventHold || (cachedPlannedText != 'blank')) {
				successfulIteration = 0;
				return;
			}
			successfulIteration++; //this makes sure that at least 250ms went by with no eventhold
			if (successfulIteration < 10) return;
			clearInterval(outerloop);
			setTimeout(()=>{
				dispatchThis(postButton,'click');  
				console.log('postbutton was clicked');
			},500);
		},27);
	}
	
	function fSubmitLogNow(){
		let submitButton = document.querySelectorAll('button[class~=submit-button]')[0];
		googleTagLogNL('xtraSubmitButton');
		dispatchThis(submitButton,'click');
	}

	// insert the control buttons to reload the last log, copy logs and submit open log tabs
	function insertControlButtons(){
//		var controlButtonHook = document.getElementsByClassName('content-container')[0].firstElementChild; 
		var controlButtonHook = document.getElementsByClassName('hidden-by')[0]; 
		controlButtonHook.style.marginBottom = '0px';
		controlButtonHook.style.marginTop = '0px';
		
		if (!controlButtonHook) {
			console.log("The base element for the control buttons was not found.  Retrying.");
			setTimeout(insertControlButtons,100);
			return;
		}

		//  Add div to screen.
		GM_addStyle(".quickbuttonxl {border:solid 1px rgb(68,142,53); margin-right:12px; " +
				"border-radius:5px; -moz-border-radius:7px; background-color:rgb(239,239,239); " +
				"padding:3px; cursor:pointer; }");

		divCtrls = document.createElement('div');
		divCtrls.id = 'gm_divCtrls';
		divCtrls.setAttribute('style', 'float:right; width: 180px; height:32px; margin-top:0px; ');
		controlButtonHook.appendChild(divCtrls);

		// add duplicate submit and cancel buttons
		var buttonSpan = document.createElement("span");
		buttonSpan.setAttribute('style', 'float:right; width: 150px; height:30px; margin-top:15px');

		var submitButton = document.createElement("button");
		submitButton.innerHTML = "Post";
		submitButton.id = 'scriptXtraPostButton1';
		submitButton.setAttribute('style','float:right; font-size:14px; background-color:green; color:white; width:100px; padding:10px');
		buttonSpan.appendChild(submitButton);
		let secondButtonSpan = buttonSpan.cloneNode(true);
		secondButtonSpan.querySelector('button').id = 'scriptXtraPostButton2';
		let dupsLocation = document.getElementsByClassName('row log-meta-controls')[0];
		if (dupsLocation) dupsLocation.appendChild(buttonSpan);
		else dupsInterval = setInterval(()=>{
			dupsLocation = document.getElementsByClassName('row log-meta-controls')[0];
			if (dupsLocation) {
				clearInterval(dupsInterval);
				dupsLocation.appendChild(buttonSpan);			}
		},100);

		let secondDupsLocation = document.getElementsByClassName('row space-between stay-row favorite-point-row')[0];
		if (secondDupsLocation) {
			secondDupsLocation.appendChild(secondButtonSpan);
		}
		else secondDupsInterval = setInterval(()=>{
			secondDupsLocation = document.getElementsByClassName('row space-between stay-row favorite-point-row')[0];
			if (secondDupsLocation) {
				clearInterval(secondDupsLocation);
				secondDupsLocation.appendChild(secondButtonSpan);
			}
		},100);

		setInterval(fPostButtonColor,1000);

		var imgLogRestore = document.createElement('img');
		imgLogRestore.src = imgsrc_LogRestore;
		imgLogRestore.id = 'imgLogRestore';
		imgLogRestore.setAttribute('action', 'restore');
		imgLogRestore.title = 'Restore last log text.';
		imgLogRestore.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgLogRestore);

		var imgTabDup = document.createElement('img');
		imgTabDup.src = imgsrc_TabDup;
		imgTabDup.id = 'imgTabDup';
		imgTabDup.setAttribute('action', 'copy');
		imgTabDup.title = 'Copy Log Text and Attributes to Open Log Tabs.';
		imgTabDup.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgTabDup);

		var closePages = document.createElement('img');
		closePages.src = imgsrc_CloseTabs;
		closePages.id = 'imgCloseTabs';
		closePages.title = 'Download new close tabs script.';
		closePages.classList.add('quickbuttonxl');
		divCtrls.appendChild(closePages);

		var imgSubmitAll = document.createElement('img');
		imgSubmitAll.src = imgsrc_SubmitAll;
		imgSubmitAll.id = 'imgSubmitAll';
		imgSubmitAll.setAttribute('action', 'submit');
		imgSubmitAll.title = 'Submit All Open Log Tabs.';
		imgSubmitAll.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgSubmitAll);

		imgTabDup.addEventListener('click', fCopy2Storage, false);
		imgSubmitAll.addEventListener('click', fCopy2Storage, false); //difference from above is in action attribute
		imgLogRestore.addEventListener('click', fLogRestore, false);
		closePagesListener = closePages.addEventListener('click',fSignalCloseTabsClicked, false);
		fCheckCloseTabScriptDownload(); // Only need this during a transition to make sure everybody gets it
		submitButton.addEventListener('click',fSubmitLogNow);
		secondButtonSpan.querySelector('button').addEventListener('click',fSubmitLogNow);
		let submitButtonInstance = document.querySelectorAll('button[class~=submit-button]')[0];
		submitButtonInstance.addEventListener('click',fBeforeExit);
	}

	function fCheckCloseTabScriptDownload(){
		if (!(GM_getValue("CloseTabScriptDownloaded", "false")=="true")) {
			if (document.hasFocus()){
				fSignalCloseTabsClicked();
				GM_setValue("CloseTabScriptDownloaded","true");
			}
		}
	};

	//  Parse into individual languages, create buttons, and return number
	//  of languages and index to default language (or -1 if none).
	function fParseLang(note) {
		var sp = document.createElement('span');
		sp.innerHTML = note;
		var j = sp.getElementsByTagName('lang').length - 1;
		var dftIdx = -1;
		if (j >= 0) {
			//  Add span to page to contain buttons.
			spanLangButtons = document.createElement('span');
			spanLangButtons.style.marginRight = '15px';
			insertAheadOf(spanLangButtons, e_LogBookPanel1_LogButton); //element depricated - will not work in new log flow.
			for (i = 0; i<=j; i++) {
				var langNode = sp.getElementsByTagName('lang')[i];
				var langName = langNode.id;
				aLangText[i] = langNode.textContent;
				aLangText[i] = aLangText[i].replace(new RegExp("^\\n*", 'gi'),"");
				//  Create buttons.
				var langButton = document.createElement('input');
				langButton.type = 'button';
				langButton.value = langName;
				langButton.id = 'lButton_' + i.toString();
				langButton.style.marginLeft = '4px';
				langButton.setAttribute('index', i);
				spanLangButtons.appendChild(langButton);
				langButton.addEventListener('click', fLangClicked, false);
				//  If first default, store index.
				var isDft = eval(langNode.getAttribute('default', 'false'));
				if (isDft && (dftIdx < 0)) {
					dftIdx = i;
					langButton.style.fontWeight = "bold";
				}
				//  If append, prepend "right-arrow" symbol to name, and set attribute.
				var isAppend = eval(langNode.getAttribute('append', 'false'));
				if (isAppend) {
					langButton.value = '\u21D2' + langButton.value;
					langButton.setAttribute('append', true);
				}
				//  If auto-submit, append "enter" symbol to name, and set attribute.
				var isAutoSubmit = eval(langNode.getAttribute('autosubmit', 'false'));
				if (isAutoSubmit) {
					langButton.value += '\u21B5';
					langButton.setAttribute('autosubmit', true);
				}
			}
		}
		//  Return number of languages, and the default index.
		return [j + 1, dftIdx];
	}


	//  Process language button clicked. [this appears to language-localize template text through clicking a button.  Depricated.]
	function fLangClicked() {
		var idx = this.getAttribute('index',0) - 0;
		var isAppend = eval(this.getAttribute('append', false));
		var isAutoSubmit = eval(this.getAttribute('autosubmit', false));
		if (!isAppend) {
			e_LogInfo.value = fRebracket(aLangText[idx].trim()); //todo - e_LogInfo is depricated, so what's the point of this?
		} else {
			//  Strip trailing newlines.
			var xTxt = e_LogInfo.value.replace(/\n+$/, '');
			//  Append to existing text.
			e_LogInfo.value = xTxt + '\n\n' + fRebracket(aLangText[idx].trim());
			e_LogInfo.scrollTop = e_LogInfo.scrollHeight;
		}
		if (isAutoSubmit) {
			fSubmitLog();
		} else {
			e_LogBookPanel1_LogButton.disabled = false;
		}
	}


	//  Retrieve log text.
	function fLogRestore() {
		fFlashButton(this);
		var lastLog = GM_getValue('LastLogText', "");
		if (!lastLog) {
			alert('No text saved.');
		} else {
			var logText = document.querySelectorAll('input[id=log-text-control]')[0];

			if (logText.value == "") {
				insertTextContent(lastLog);				
			}
			else {
				var Resp = confirm('Last log text will be appended to any text\n' +
						'currently in the text area.\n\nContinue?');
				if (Resp) {
					insertTextContent(logText.value+lastLog);
				} else {
					alert('Action canceled.');
				}
			} 
		}
	}


	//  Copy log attributes to storage. The random number is included to ensure the
	//  storage string is different each time it is stored, to trigger the change event.
	function fCopy2Storage() {
		fFlashButton(this);
		var jsObj = new Object;
		let logType = fGetLogType();
		if (this.getAttribute('action') == 'copy') {
			if (logType == '47') {
				alert('The copy-across-tabs function does not work \n'+
					  'for Update Coordinates logs.');
				return;
			}
			else {
				if (fDetectImages()) alert('You have an image attached to this log.\n\n'+
					'The "copy to other log tabs" button will not copy an\n'+
					'image to other tabs.  Images must by attached manually\n'+
					'from each log tab.');
				var logText = document.querySelectorAll('input[id=log-text-control]')[0];
				if (logText.value.trim().length > 0) {
					jsObj['logtext'] = logText.value;
					let logTypeArea = document.querySelector('div[class^=log-type-option]');
					if (!logTypeArea) jsObj['logvalue'] = "Unknown";
					else {
						let selectedLogType = logTypeArea.querySelector('span').textContent;
						jsObj['logvalue'] = selectedLogType;
					}
					console.log("the selected log value is: "+jsObj['logvalue']);
					jsObj['nodelete']= cannotDeleteCheckbox.checked;
					jsObj['submit']  = 'false';
					jsObj['random']  = Math.random();
					var jsonString = JSON.stringify(jsObj);
					localStorage.setItem(lsCopyKey, jsonString);
				} else {
					alert('No log message to display.\n\nEnter a log message if you want to copy to other tabs.');
				} 
			}
		} else if (this.getAttribute('action') == 'submit') {
			jsObj['submit'] = 'true';
			jsObj['random'] = Math.random();
			var jsonString = JSON.stringify(jsObj);
			sessionStorage.setItem('autoclose', 'true');
			sessionStorage.setItem('newLogAction','true');
			localStorage.setItem(lsCopyKey, jsonString);
//			fSubmitLog(); //commented out in favor of the following POST method
			if (logType == '47' || fDetectImages()) fSubmitLog(); //POST method does not work for update coordinates logs or attached images
			else fPostLogByPOSTMethod(); //post the log on the current tab (ie. where the submit button was pressed)
		}
	}

	// send request to close all review pages
	function fSignalCloseTabsClicked(){
		let closePages = document.getElementById('imgCloseTabs');
		if (closePages.title == 'Download new close tabs script.') {//editme
			GM_openInTab('https://gitlab.com/geocachinghq/reviewer_scripts/-/raw/tweaks-closeButton/gcr_CloseTabs.user.js?ref_type=heads');
			alert(	'A new tab was opened to install the CloseTabs helper script.\n'+
					'When the helper script is operating, you will no longer see the\n'+
					'install option.  The helper script only operates on tabs opened\n'+
					'after it was installed, so it will not help you this first time\n'+
					'through.\n'+
					'\n'+
					'If you got this message and thought you had already downloaded\n'+
					'the helper script, makes sure CloseTabs is active in the\n'+
					'Tampermonkey menu.');
		}
		else window.removeEventListener(closePagesListener); // No action - the title was changed by the helper script
	}

	//  Update log to text in storage.
	function fStorageChanged(e) {
		var jsObj = new Object;
		console.log('fStorageChange detected a change in storage');
		if (e.key == lsCopyKey) {
			console.log('Heard a request to update log text and log type');
			var jsonString = e.newValue;
			jsObj = JSON.parse(jsonString);
			if ((jsObj['submit']) == 'true') {
				sessionStorage.setItem('autoclose', 'true');
				sessionStorage.setItem('newLogAction','true'); //set up to close tab after successful log
				if (fGetLogType() == '47') fSubmitLog(); //use post button for Update Coordinates logs
				else fPostLogByPOSTMethod();
			} else {
				googleTagLogNL('paste_received');
				if (eventHold) {//there's already a change queued
					if (cachedPlannedText == 'blank') {// launch an interval to make the changes when the queue is cleared
						copyListenerIterations = 0;
						launchEventHoldListener = setInterval(()=>{
							if (copyListenerIterations>3) {//we've waited long enough
								clearInterval(launchEventHoldListener);
								suppressLogTypeSelection = false;
								if (cachedNoDelete) setCannotDeleteCheckbox();
								else clearCannotDeleteCheckbox();
								if (cachedPlannedText != 'blank') {
									setPlannedValues(cachedPlannedLogValue,cachedPlannedText);
									cachedPlannedText = 'blank'; //clear the cache once it's published
								}
							}
							else if (!eventHold) copyListenerIterations++;
							else copyListenerIterations = 0;
						},33);
					}
					cachedPlannedText = jsObj['logtext']; //it's OK to change the values already in the queue
					cachedPlannedLogValue = jsObj['logvalue'];  
					cachedNoDelete = jsObj['nodelete'];
				}
				else {//this will be the only log change in queue
					suppressLogTypeSelection = false;
					plannedText         = jsObj['logtext'];
					plannedLogValue     = jsObj['logvalue'];
					console.log(plannedText);
					console.log(plannedLogValue);
					if (jsObj['nodelete']) setCannotDeleteCheckbox();
					else clearCannotDeleteCheckbox();
					setPlannedValues(plannedLogValue,plannedText);
				}
			}
		}
		else if (e.key == logTypeMapCopyTrigger) {
			logTypeNumtoText = new Map(Object.entries(JSON.parse(GM_getValue('logTypeMap'))));
			console.log('fStorageChanged loaded the saved version of the Map');
			console.log(logTypeNumtoText);
		}
}

	//  Change button background to white, then fade to gray.
	function fFlashButton(obj) {
		var obj;
		var rgb = 256;
		var si = window.setInterval(fFade2Gray, 40);
		//  Function to fade-in blackout div.
		function fFade2Gray() {
			rgb -= 1;
			obj.style.backgroundColor = 'rgb('+rgb+','+rgb+','+rgb+')';
			if (rgb <= 239) {
				window.clearInterval(si);
			}
		}
	}


	//  Replace all left brackets with entity code, except for those enclosing the "lang" tag.
	//  This allows brackets to be seen as text, rather than child nodes.
	function fUnbracket(work) {
		var work = work.replace(/</g, '&lt;');
		work = work.replace(new RegExp("&lt;lang ", 'gi'),"<lang ");
		work = work.replace(new RegExp("&lt;/lang\\s*>", 'gi'),"</lang>");
		return work;
	}


	//  Change any left bracket entities back to actual brackets.
	function fRebracket(work) {
		var work = work.replace(/&lt;/g, '<');
		return work;
	}


	//  Set the Always Cannot Delete option on/off.
	function fOptRevNoteUndel() {
		var OptRevNoteUndel = GM_getValue('OptRevNoteUndel', 'On');
		var Resp = confirm(
				'Click OK to always automatically set the Cannot Delete\n' +
				'option for a log.\n\n' +
				'Click CANCEL to turn this option Off.\n\n' +
				'This option is currently turned ' + OptRevNoteUndel + '.');
		if (Resp) {
			OptRevNoteUndel = 'On';
		} else {
			OptRevNoteUndel = 'Off';
		}
		GM_setValue('OptRevNoteUndel', OptRevNoteUndel);
		alert('Auto Cannot Delete has been turned ' + OptRevNoteUndel + '.');
	}


	// Generic load log template to form
	function fEditGeneric (noteName,typeName) {
		googleTagLogNL('template_paste');
		if (suppressLogTypeSelection) {
			getSetLogType("",true);
			suppressLogTypeSelection = false;
		}
		let wait = setInterval(()=>{
			if (okLogTypes.length<1) return;
			clearInterval(wait);
			var theAlert='';
			plannedText = fRebracket(GM_getValue(noteName, ''));
			if (plannedText == "") theAlert = 'No template has been saved.  Write one and save it.';
			else {
				plannedLogValue = GM_getValue(typeName, "Unknown"); //that's the key  
				if (!isNaN(plannedLogValue)) {
					plannedLogValue = getLogTypeValue(1*plannedLogValue);
				}
				if (okLogTypes.indexOf(plannedLogValue) != -1) {
					setPlannedValues (plannedLogValue,plannedText);
				} 
				else {
					suppressLogTypeSelection = true;
					setPlannedValues (plannedLogValue,plannedText);
					theAlert += 'Template log type of '+plannedLogValue+' is unavailable.  Please check it before posting,'+
						'and then re-save the template with the corrected log type.';
				}
			}
			if (theAlert != '') alert(theAlert);
		},100);
	}

	function fGetSelectedLogValue (){ // we're switching back to using logType now that we're updating the logType Map
		let selectedLogValue = "Unknown";
		let logTypeArea = document.querySelector('div[class^=log-type-option]');
		if (logTypeArea) selectedLogValue = logTypeArea.querySelector('span').textContent;
		console.log("the selected log value is: "+selectedLogValue);
		return selectedLogValue;
	}

	// Generic save the current form text and log type to a template
	function fSaveGeneric (noteName,typeName,templateName) {
		var Resp = confirm('This will save the current text and log type as the '+templateName+'\n' +
				'template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		let selectedLogType = document.querySelectorAll('input[name=logType]')[0];
		if (Resp) {
			if (selectedLogType.value) {
				GM_setValue(noteName,fUnbracket(logText.value));
				GM_setValue(typeName,selectedLogType.value);
				alert('Template has been saved.');
			}
			else alert('No cache type selected.  Save aborted.');
		} else {
			alert('Save aborted.');
		}		
	}


	//  Edit Proximity Note Template.
	function fEditProxTmpl() {
		fEditGeneric('ProxNote','ProxType');
	}

	//  Save Proximity Note Template.
	function fSaveProxTmpl() {
		fSaveGeneric ('ProxNote','ProxType','proximity note');
	}

	//  Edit Litmus-1 Note Template.
	function fEditLit1Tmpl() {
		fEditGeneric('Lit1Note','Lit1Type');
	}

	//  Save Litmus-1 Note Template.
	function fSaveLit1Tmpl() {
		fSaveGeneric ('Lit1Note','Lit1Type','Litmus-1 note');
	}

	//  Edit Vacation Template.
	function fEditVacaTmpl() {
		fEditGeneric('VacaNote','VacaType');
	}


	//  Save Vacation Template.
	function fSaveVacaTmpl() {
		fSaveGeneric ('VacaNote','VacaType','vacation note');
	}


	//  Edit Handicap Template.
	function fEditHndiTmpl() {
		fEditGeneric('HndiNote','HndiType');
	}


	//  Save Handicap Template.
	function fSaveHndiTmpl() {
		fSaveGeneric ('HndiNote','HndiType','handicap note');
	}


	//  Edit Updating Coordinates Template.
	function fEditUpcoTmpl() {
		var upcoNote = GM_getValue('UpcoNote', "Updating coordinates.");
		insertTextContent(fRebracket(upcoNote));
	}


	//  Save Updating Coordinates Template.
	function fSaveUpcoTmpl() {
		var Resp = confirm('This will save the current text as the update coordinates \n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			upcoNote = fUnbracket(logText.value);
			GM_setValue('UpcoNote', upcoNote);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}


	//  Set log type. Default to Review Note if log type not available. //as of 10/2022, this function appears obsolete
	function fSetLogType(logType) {
		if (OkLogTypes.indexOf(logType) != -1) {
			e_LogType.value = logType;
		} else {
			if (OkLogTypes.indexOf('18') != -1) {
				e_LogType.value = '18';
			} else {
				e_LogType.value = '68';
			}
		}
	}


	//  Process log type change. //as of 10/2022, this function appears obsolete
	function TypeChange() {
		newType = this.value - 0;
		var LogIsBlank = (e_LogInfo.value.trim().length == 0);
		switch(newType) {
			case 24:
				if (LogIsBlank) { e_LogInfo.value = 'Published'; }
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 25:
				if (LogIsBlank) { e_LogInfo.value = 'Listing retracted.'; }
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 18: case 68:
				if (GM_getValue('OptRevNoteUndel', 'Off') == 'On') {
				}
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
		}
	}



	//  Export All Templates.
	function fExportTemplates() {
		var jsObj = new Object;
		jsObj['UpcoNote'] = {
			'template': GM_getValue('UpcoNote',''),
			'logtype' : GM_getValue('UpcoType','47'),
			'logtypei': 'UpcoType'};
		jsObj['ProxNote'] = {
			'template': GM_getValue('ProxNote',''),
			'logtype' : GM_getValue('ProxType',''),
			'logtypei': 'ProxType'};
		jsObj['Lit1Note'] = {
			'template': GM_getValue('Lit1Note',''),
			'logtype' : GM_getValue('Lit1Type',''),
			'logtypei': 'Lit1Type'};
		jsObj['VacaNote'] = {
			'template': GM_getValue('VacaNote',''),
			'logtype' : GM_getValue('VacaType',''),
			'logtypei': 'VacaType'};
		jsObj['HndiNote'] = {
			'template': GM_getValue('HndiNote',''),
			'logtype' : GM_getValue('HndiType',''),
			'logtypei': 'HndiType'};
		jsObj['KZ_ArcNote'] = {
			'template': KZ_ArcNote,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['KZ_RevNote'] = {
			'template': KZ_RevNote,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['KZ_Days_to_Respond'] = {
			'template':KZ_Days_to_Respond,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['KZ_BookMark'] = {
			'template':KZ_BookMark,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['KZ_Auto_Close_Tabs'] = {
			'template':KZ_Auto_Close_Tabs,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['OAN_ArcNote'] = {
			'template': OANArcNote,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['OAN_RevNote'] = {
			'template': OANRevNote,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['OAN_Days_to_Respond'] = {
			'template':OAN_Days_to_Respond,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['OAN_BookMark'] = {
			'template':OAN_BookMark,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		jsObj['OAN_Auto_Close_Tabs'] = {
			'template':OAN_Auto_Close_Tabs,
			'logtype': 'ignore',
			'logtypei':'ignore'};
		var tstr = JSON.stringify(jsObj,null,4);
		insertTextContent(tstr);
	}

	//  Import Templates.
	function fImportTemplates() {
		if (!logText.value.trim()) {
			alert('Text box is empty. Paste the entire Template Import\n' +
					'Text into the text box, and invoke the option again.');
			return;
		}
		var Resp = confirm('This will replace existing templates with\n' +
				'the Import Text currently in the text box.\n\n' +
				'Are you SURE you want to continue?');
		if (Resp) {
			try {
				var jsObj = JSON.parse(logText.value);
			}
			catch(err) {
				alert('Error: ' + err.description + '\n\n' +
				'Unable to parse the Import Text. It may be corrupted,\n' +
				'or not copied in full.\n\n' +
				'Import process aborted. Existing Templates are unchanged.');
				return;
			}
			var totimp = 0;
			for (var i in jsObj) {
				if (jsObj.hasOwnProperty(i)) {
					GM_setValue(i, jsObj[i]['template']);
					if (jsObj[i]['logtypei'] != 'ignore')
						GM_setValue(jsObj[i]['logtypei'], jsObj[i]['logtype']);
					++totimp;
				}
			}
			alert(totimp + ' template(s) imported.');
		} else {
			alert('Import canceled');
		}
	}


	// Expand directions.
	function fExpandDir(dir) {
		if (dir.length == 2) {
			dir = dir.substr(0,1) + '-' + dir.substr(1,1);
		}
		dir = dir.replace(/N/g,'north');
		dir = dir.replace(/S/g,'south');
		dir = dir.replace(/E/g,'east');
		dir = dir.replace(/W/g,'west');
		return dir;
	}


	//  Find true left (index 0) and top (index 1) position of any object.
	//  Returned: rtnvar[0] = left; rtnvar[1] = top;
	function fFindPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
			return [curleft,curtop];
		}
	}


	//  Escape a string for regex operations.
	function escStr(str) {
		return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
	}


	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}


	//	Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	//  Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}



	//  Convert Waypoint to ID number.
	function WptToID(Wpt) {
		var BASE_GC = "0123456789ABCDEFGHJKMNPQRTVWXYZ";
		var BASE_31 = "0123456789ABCDEFGHIJKLMNOPQRSTU";
		var WptWork, Wpt31 = '', idNum;
		// Strip off leading 'GC'.
		WptWork = Wpt.replace(/^GC/,'');
		// If old-style base-16 value.
		if ((WptWork.length <= 4) && (WptWork.search(/^(\d|[A-F]).*/)) >= 0) {
			idNum = parseInt(WptWork, 16);
		// If new-style base-31 value.
		} else {
			for (var i = 0; i < WptWork.length; i++) {
				Wpt31 += BASE_31.substr(BASE_GC.indexOf(WptWork.substr(i, 1)), 1);
			}
			idNum = parseInt(Wpt31, 31) - 411120;
		}
		return idNum;
	}


	//  Convert single coordinate decimal-minutes string to latitude & logitude decimal degree values.
	function dm2dd(inStr) {
		var RegEx1 = new RegExp('^([N|S])(\\d{1,2}).*?(\\d{1,2}[.]\\d{1,3})\\s+' +
				'([E|W])(\\d{1,3}).*?(\\d{1,2}[.]\\d{1,3})', 'ig');
		var v = RegEx1.exec(inStr);
		if (!v) { // try the format of the coordinates in the New Log page
			RegEx1 = new RegExp('^([N|S])\\s(\\d{1,2})..(\\d{1,2}[.]\\d{1,3})..' +
			'([E|W])\\s(\\d{1,3})..(\\d{1,2}[.]\\d{1,3}).', 'ig');
			v = RegEx1.exec(inStr);
		}
		var lat = ((v[2] - 0) + (v[3] / 60)).toFixed(6);
		var lon = ((v[5] - 0) + (v[6] / 60)).toFixed(6);
		if (v[1] == 'S') { lat *= -1; }
		if (v[4] == 'W') { lon *= -1; }
		return [lat.toString(), lon.toString()];
	}





	// ---------------------------------- Setting Functions ------------------------------------------------------------------------- //

	// Show Edit Settings box for 'Owner Action Needed' items
	function fOANEditSettings() {
		// If div already exists, reposition browser, and show alert.
		refreshBenchmarkList();
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * 0.5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('span');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.appendChild(document.createTextNode('Owner Action Needed - Script Settings'));
		divSet.appendChild(ds_Heading);

		// Create duration selector.
		var ds_DaysP = document.createElement('p');
		ds_DaysP.style.textAlign = 'left';
		ds_DaysP.style.marginLeft = '6px';
		ds_DaysP.appendChild(document.createTextNode("Warning Duration in Days: "));
		var ds_DaysSel = document.createElement("select");
		ds_DaysSel.id = 'ds_DaysSel';
		for (var i = 1; i <= 90; i++) {
			var ds_DaysOpt = document.createElement("option");
			ds_DaysOpt.value = i;
			ds_DaysOpt.id = 'ds_DaysOpt' + i;
			ds_DaysOpt.text = i.toString();
			if (i == OAN_Days_to_Respond) { ds_DaysOpt.selected = true; }
			ds_DaysSel.appendChild(ds_DaysOpt);
		}

		ds_DaysP.appendChild(ds_DaysSel);
		ds_DaysP.appendChild(document.createTextNode(" Use %D% to insert value into text."));
		divSet.appendChild(ds_DaysP);

		// Create warning text area.
		var ds_WarnTextAreaP = document.createElement("p");
		ds_WarnTextAreaP.style.textAlign = 'left';
		ds_WarnTextAreaP.style.marginLeft = '6px';
		ds_WarnTextAreaP.appendChild(document.createTextNode("Enter warning text for Disable log:"));
		ds_WarnTextAreaP.appendChild(document.createElement('br'));
		var ds_WarnTextArea = document.createElement("textarea");
		ds_WarnTextArea.id = 'ds_WarnTextArea';
		ds_WarnTextArea.style.width = popwidth - 16 + 'px';
		ds_WarnTextArea.style.height = '80px';
		ds_WarnTextAreaP.appendChild(ds_WarnTextArea);
		divSet.appendChild(ds_WarnTextAreaP);
		ds_WarnTextArea.value = OANRevNote;

		// Create archive text area.
		var ds_ArchiveTextAreaP = document.createElement("p");
		ds_ArchiveTextAreaP.style.textAlign = 'left';
		ds_ArchiveTextAreaP.style.marginLeft = '6px';
		ds_ArchiveTextAreaP.appendChild(document.createTextNode("Enter Archive log text:"));
		ds_ArchiveTextAreaP.appendChild(document.createElement('br'));
		var ds_ArchiveTextArea = ds_WarnTextArea.cloneNode(true);
		ds_ArchiveTextArea.id = 'ds_ArchiveTextArea';
		ds_ArchiveTextAreaP.appendChild(ds_ArchiveTextArea);
		divSet.appendChild(ds_ArchiveTextAreaP);
		ds_ArchiveTextArea.value = OANArcNote;

		// Add bookmark selector to page.
		var ds_BookmarkListP = document.createElement("p");
		ds_BookmarkListP.style.textAlign = 'left';
		ds_BookmarkListP.style.marginLeft = '6px';
		ds_BookmarkListP.appendChild(document.createTextNode("Select Bookmark List to use"+
			" (if you don't see a new bookmark, hit 'cancel' and open these settings again):"));
		ds_BookmarkListP.appendChild(document.createElement('br'));

		var ds_BookmarkList =document.createElement('select');
		ds_BookmarkList.id = "ds_BookmarkList";
		ds_BookmarkList.name = "ds_BookmarkList";
		ds_BookmarkList.setAttribute('style', '');
		ds_BookmarkList.disabled = false;
		let bookmarkOptions=GM_getValue(SignedInAs+'bmlist',"blank");
		if (bookmarkOptions=='blank'){
			alert('No bookmarks were found.\nVisit /bookmarks/mark.aspx to confirm you have some.');
			return;
		}
		var bookmarkArray = bookmarkOptions.split(',');
		let bookmarkFound = false;
		for (var i=0; i<bookmarkArray.length; i++) {
			option = document.createElement('option');
			option.innerText = bookmarkArray[i];
			option.selected=false;
			if (bookmarkArray[i] == OAN_BookMark) {
				option.selected=true;
				bookmarkFound = true;
			}
			ds_BookmarkList.appendChild(option);
		}
		if (!bookmarkFound) {
			OAN_BookMark = bookmarkArray[0]; // give the KZ bookmark a useful value, as the old one didn't exist anymore
			GM_setValue("OAN_BookMark",OAN_BookMark); 
		}

		ds_BookmarkListP.appendChild(ds_BookmarkList);
		divSet.appendChild(ds_BookmarkListP);

		// Create auto-close checkbox.
		var ds_AutoCloseP = document.createElement("p");
		ds_AutoCloseP.style.textAlign = 'left';
		ds_AutoCloseP.style.marginLeft = '6px';
		var ds_AutoCloseLabel = document.createElement("label");
		ds_AutoCloseLabel.style.fontWeight = 'normal';
		ds_AutoCloseLabel.setAttribute('for', 'ds_AutoCloseCB_OAN');
		ds_AutoCloseLabel.appendChild(document.createTextNode("Auto-submit Log and Close Tabs on Completion:"));
		ds_AutoCloseP.appendChild(ds_AutoCloseLabel);

		var ds_AutoCloseCB = document.createElement("input");
		ds_AutoCloseCB.id = "ds_AutoCloseCB_OAN";
		ds_AutoCloseCB.name = "ds_AutoCloseCB";
		ds_AutoCloseCB.type = "Checkbox";
		ds_AutoCloseCB.style.marginLeft = '6px';
		ds_AutoCloseCB.checked = OAN_Auto_Close_Tabs;
		ds_AutoCloseP.appendChild(ds_AutoCloseCB);

		var ds_AutoCloseInfo = document.createElement("a");
		ds_AutoCloseInfo.style.fontSize = 'x-small';
		ds_AutoCloseInfo.style.marginLeft = '9px';
		ds_AutoCloseInfo.href = 'https://sites.google.com/site/allowscriptstoclosetabs/';
		ds_AutoCloseInfo.title = 'Click to view instructions in a new tab.';
		ds_AutoCloseInfo.target = '_blank';
		ds_AutoCloseInfo.appendChild(document.createTextNode("(Requires configuration change)"));
		ds_AutoCloseP.appendChild(ds_AutoCloseInfo);

		divSet.appendChild(ds_AutoCloseP);

		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  100);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);
		document.body.appendChild(divSet);

		// Save values.
		function fSaveButtonClicked() {
			OAN_Days_to_Respond = ds_DaysSel.options[ds_DaysSel.selectedIndex].value;
			GM_setValue("OAN_Days_to_Respond", OAN_Days_to_Respond);
			OANRevNote = ds_WarnTextArea.value;
			GM_setValue("OAN_RevNote", OANRevNote);
			OANArcNote = ds_ArchiveTextArea.value;
			GM_setValue("OAN_ArcNote", OANArcNote);
			OAN_BookMark = ds_BookmarkList.options[ds_BookmarkList.selectedIndex].value;
			GM_setValue("OAN_BookMark", OAN_BookMark);
			OAN_Auto_Close_Tabs = ds_AutoCloseCB.checked;
			GM_setValue("OAN_Auto_Close_Tabs", OAN_Auto_Close_Tabs);
			alert('Settings have been saved. OAN_BookMark is ' + OAN_BookMark);
			removeNode(divSet);
		}

		// Cancel request.
		function fCancelButtonClicked() {
			var resp = confirm('Cancel requested. You will lose any changes.\n\n' +
					'Press OK to exit without saving changes. Otherwise, press Cancel.');
			if (resp) {
				removeNode(divSet);
			}
		}
	}



	// Show Edit Settings box for 'Kill Zone' items
	function fKZEditSettings() {

		// If div already exists, reposition browser, and show alert.
		refreshBenchmarkList();
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * 0.5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('p');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.style.textAlign = 'center';
		ds_Heading.appendChild(document.createTextNode('Kill Zone - Script Settings'));
		divSet.appendChild(ds_Heading);

		// Create duration selector.
		var ds_DaysP = document.createElement('p');
		ds_DaysP.style.textAlign = 'left';
		ds_DaysP.style.marginLeft = '6px';
		ds_DaysP.style.marginRight = '6px';
		ds_DaysP.appendChild(document.createTextNode("Warning Duration in Days: "));

		var ds_DaysSel = document.createElement("select");
		ds_DaysSel.id = 'ds_DaysSel';
		for (var i = 1; i <= 90; i++) {
			var ds_DaysOpt = document.createElement("option");
			ds_DaysOpt.value = i;
			ds_DaysOpt.id = 'ds_DaysOpt' + i;
			ds_DaysOpt.text = i.toString();
			if (i == KZ_Days_to_Respond) { ds_DaysOpt.selected = true; }
			ds_DaysSel.appendChild(ds_DaysOpt);
		}

		ds_DaysP.appendChild(ds_DaysSel);
		ds_DaysP.appendChild(document.createTextNode(" Use %D% to insert value into text."));
		divSet.appendChild(ds_DaysP);

		// Create warning text area.
		var ds_WarnTextAreaP = document.createElement("p");
		ds_WarnTextAreaP.style.textAlign = 'left';
		ds_WarnTextAreaP.style.marginLeft = '6px';
		ds_WarnTextAreaP.style.marginRight = '6px';
		ds_WarnTextAreaP.appendChild(document.createTextNode("Enter warning text for Reviewer log:"));
		ds_WarnTextAreaP.appendChild(document.createElement('br'));

		var ds_WarnTextArea = document.createElement("textarea");
		ds_WarnTextArea.id = 'ds_WarnTextArea';
		ds_WarnTextArea.style.width = '100%';
		ds_WarnTextArea.style.height = '80px';
		ds_WarnTextAreaP.appendChild(ds_WarnTextArea);
		divSet.appendChild(ds_WarnTextAreaP);
		ds_WarnTextArea.value = KZ_RevNote;

		// Create archive text area.
		var ds_ArchiveTextAreaP = document.createElement("p");
		ds_ArchiveTextAreaP.style.textAlign = 'left';
		ds_ArchiveTextAreaP.style.marginLeft = '6px';
		ds_ArchiveTextAreaP.style.marginRight = '6px';
		ds_ArchiveTextAreaP.appendChild(document.createTextNode("Enter Archive log text:"));
		ds_ArchiveTextAreaP.appendChild(document.createElement('br'));

		var ds_ArchiveTextArea = ds_WarnTextArea.cloneNode(true);
		ds_ArchiveTextArea.id = 'ds_ArchiveTextArea';
		ds_ArchiveTextAreaP.appendChild(ds_ArchiveTextArea);
		divSet.appendChild(ds_ArchiveTextAreaP);
		ds_ArchiveTextArea.value = KZ_ArcNote;

		// Add bookmark selector to page.
		var ds_BookmarkListP = document.createElement("p");
		ds_BookmarkListP.style.textAlign = 'left';
		ds_BookmarkListP.style.marginLeft = '6px';
		ds_BookmarkListP.style.marginRight = '6px';
		ds_BookmarkListP.appendChild(document.createTextNode("Select Bookmark List to use"+
			" (if you don't see a new bookmark, hit 'cancel' and open these settings again):"));
		ds_BookmarkListP.appendChild(document.createElement('br'));

		var ds_BookmarkList =document.createElement('select');
		ds_BookmarkList.id = "ds_BookmarkList";
		ds_BookmarkList.name = "ds_BookmarkList";
		ds_BookmarkList.setAttribute('style', '');
		ds_BookmarkList.disabled = false;
		let bookmarkOptions=GM_getValue(SignedInAs+'bmlist',"blank");
		if (bookmarkOptions=='blank'){
			alert('No bookmarks were found.\nVisit /bookmarks/mark.aspx to confirm you have some.');
			return;
		}
		var bookmarkArray = bookmarkOptions.split(',');
		let bookmarkFound = false;
		for (var i=0; i<bookmarkArray.length; i++) {
			option = document.createElement('option');
			option.innerText = bookmarkArray[i];
			option.selected=false;
			if (bookmarkArray[i] == KZ_BookMark) {
				option.selected=true;
				bookmarkFound = true;
			}
			ds_BookmarkList.appendChild(option);
		}
		if (!bookmarkFound) {
			KZ_BookMark = bookmarkArray[0]; // give the KZ bookmark a useful value, as the old one didn't exist anymore
			GM_setValue("KZ_BookMark",KZ_BookMark); 
		}
		ds_BookmarkListP.appendChild(ds_BookmarkList);
		divSet.appendChild(ds_BookmarkListP);

		// Create auto-close checkbox.
		var ds_AutoCloseP = document.createElement("p");
		ds_AutoCloseP.style.textAlign = 'left';
		ds_AutoCloseP.style.marginLeft = '6px';
		ds_AutoCloseP.style.marginRight = '6px';

		var ds_AutoCloseLabel = document.createElement("label");
		ds_AutoCloseLabel.style.fontWeight = 'normal';
		ds_AutoCloseLabel.setAttribute('for', 'ds_AutoCloseCB_KZ');
		ds_AutoCloseLabel.appendChild(document.createTextNode("Auto-submit Log and Close Tabs on Completion:"));
		ds_AutoCloseP.appendChild(ds_AutoCloseLabel);

		var ds_AutoCloseCB = document.createElement("input");
		ds_AutoCloseCB.id = "ds_AutoCloseCB_KZ";
		ds_AutoCloseCB.name = "ds_AutoCloseCB";
		ds_AutoCloseCB.type = "Checkbox";
		ds_AutoCloseCB.style.marginLeft = '6px';
		ds_AutoCloseCB.checked = KZ_Auto_Close_Tabs;
		ds_AutoCloseP.appendChild(ds_AutoCloseCB);

		var ds_AutoCloseInfo = document.createElement("a");
		ds_AutoCloseInfo.style.fontSize = 'x-small';
		ds_AutoCloseInfo.style.marginLeft = '9px';
		ds_AutoCloseInfo.href = 'https://sites.google.com/site/allowscriptstoclosetabs/';
		ds_AutoCloseInfo.title = 'Click to view instructions in a new tab.';
		ds_AutoCloseInfo.target = '_blank';
		ds_AutoCloseInfo.appendChild(document.createTextNode("(Requires configuration change)"));
		ds_AutoCloseP.appendChild(ds_AutoCloseInfo);

		divSet.appendChild(ds_AutoCloseP);

		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';

		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);

		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Resize/reposition on window resizing.
		function fSetLeftPos() {
			var divSet = document.getElementById('gm_divSet');
			if (divSet) {
				var popwidth = parseInt(window.innerWidth * 0.5);
				divSet.style.width = popwidth + 'px';
				var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
				divSet.style.left = leftpos + 'px';
			}
		}

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);

		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + 0.05;
			divBlackout.style.opacity = op;
			if (op >= 0.75) {
				window.clearInterval(si);
			}
		}

		// Save values.
		function fSaveButtonClicked() {
			KZ_Days_to_Respond = ds_DaysSel.options[ds_DaysSel.selectedIndex].value;
			GM_setValue("KZ_Days_to_Respond", KZ_Days_to_Respond);
			KZ_RevNote = ds_WarnTextArea.value;
			GM_setValue("KZ_RevNote", KZ_RevNote);
			KZ_ArcNote = ds_ArchiveTextArea.value;
			GM_setValue("KZ_ArcNote", KZ_ArcNote);
			KZ_BookMark = ds_BookmarkList.options[ds_BookmarkList.selectedIndex].value;
			GM_setValue("KZ_BookMark", KZ_BookMark);
			KZ_Auto_Close_Tabs = ds_AutoCloseCB.checked;
			GM_setValue("KZ_Auto_Close_Tabs", KZ_Auto_Close_Tabs);
			alert('Settings have been saved.');
			removeNode(divSet);
			removeNode(divBlackout);
			window.removeEventListener('resize', fSetLeftPos, true);
		}

		// Cancel request.
		function fCancelButtonClicked() {
			var resp = confirm('Cancel requested. You will lose any changes.\n\n' +
					'Press OK to exit without saving changes. Otherwise, press Cancel.');
			if (resp) {
				removeNode(divSet);
				removeNode(divBlackout);
				window.removeEventListener('resize', fSetLeftPos, true);
			}
		}
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	function fAutopostEnable(){
		GM_unregisterMenuCommand(menuAutopostEnable);
		if (settingAutopostAutocloseEnableUnarchive == 'false') {
			settingAutopostAutocloseEnableUnarchive = 'true';
			menuAutopostEnable = GM_registerMenuCommand('Option: Turn OFF Auto-Post Enable and Unarchive',fAutopostEnable);
		}
		else {
			settingAutopostAutocloseEnableUnarchive = 'false';
			menuAutopostEnable = GM_registerMenuCommand('Option: Turn ON Auto-Post Enable and Unarchive',fAutopostEnable);
		}
		GM_setValue('settingAutopostAutocloseEnableUnarchive',settingAutopostAutocloseEnableUnarchive);
	}

	function fScriptHydrationDelay() {
		let success = null;
		while (!success) {
			success =
				prompt(	'For some users, a delay is needed between the time a log page\n'+
						'is loaded and the time the script inserts buttons onto the page.\n'+
						'If this delay is not long enough, the page ceases to function.\n'+
						'\n'+
						'Use this setting to select a delay between 0 and 9 seconds.  The\n'+
						'current delay is '+settingScriptHydrationDelay+' seconds.\n'+
						'\n'+
						'Reload the page after changing this setting to activate the change.',settingScriptHydrationDelay);
			if (success) {
				success.trim();
				if ((success.length == 1) && !isNaN(success)) {
					GM_setValue('settingScriptHydrationDelay',success);
					GM_unregisterMenuCommand(menuScriptHydrationDelay);
					settingScriptHydrationDelay = success;
					menuScriptHydrationDelay = GM_registerMenuCommand('Adjust Script Initialization Delay ('+
						settingScriptHydrationDelay+' Seconds)',fScriptHydrationDelay);
				}
			}
		}
	}

	function fSetOptions() {
		if (!fCreateSettingsDiv('Log Entry Script Options')) { return; };
		var divSet = document.getElementById('gm_divSet');
		divSet.setAttribute('winTopPos', document.documentElement.scrollTop);  // snapback code

		//  Generate one unique DOM name for all setting controls being created.
		settingName = 'sc_' + Math.floor(Math.random()*100000001).toString();

		GM_addStyle('legend.gmsettings { background: cornsilk; border: solid 2px slategray; border-radius: 8px; padding: 6px; !important;}');
		GM_addStyle('fieldset.gmsettings { background: lightyellow; border-radius: 10px; padding: 5px; border: solid 2px green; margin-bottom: 30px; !important; }' );


		//****************************************************************************************//
		//                                                                                        //
		//                             Start of Control Creation                                  //
		//                                                                                        //
		//	fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt)          //
		//                                                                                        //
		//		vID = Identifier for control, and for saving setting data.                        //
		//		vType = checkbox (input + type.checkbox), text (input), textarea, select          //
		//		vLabel = Text for control label. Above textareas, or to the right of all others.  //
		//		vTitle = Title text for input control and label, or vLabel if not specified.      //
		//		vDftVal = May be text, true/false, or matches a vSelVal array element.            //
		//		vSize = Length of input (text) box, or height of textarea (e.g., '20px').         //
		//		vSelVal = Array of select option values.                                          //
		//		vSelTxt = Array of select option text. If omitted, SelVal values are used.        //
		//                                                                                        //
		//****************************************************************************************//


/*
		var vDftVal = GM_getValue('ExampleText_' + SignedInAs, 100);
		var vLabel = 'Example label text.';
		var txtDistWarn = fCreateSetting('ExampleText', 'text', vLabel, '', vDftVal, '4em');

		vDftVal = GM_getValue('ExampleTextArea_' + SignedInAs, 100);
		vLabel = 'Example label text.';
		var txtDistWarn = fCreateSetting('ExampleTextArea', 'textarea', vLabel, '', vDftVal, '30px');

		vDftVal = GM_getValue('ExampleCheckbox_' + SignedInAs, false);
		vLabel = 'Example label text.';
		var cbReverseSeq = fCreateSetting('ExampleCheckbox', 'checkbox', vLabel, '', vDftVal);

		vDftVal = GM_getValue('ExampleSelect_' + SignedInAs, 'Top');
		var vSelVal = [1, 2, 3];
		var vSelTxt = ['One', 'Two', 'Three'];
		var selOpenUcCacheAt = fCreateSetting('ExampleSelect', 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);
*/



		var repvar1 = 
				"REPLACEMENT VARIABLES" +
				"\n" + 
				"\n %owner% = Cache owner's account name" +
				"\n %title% = Name of the cache." +
				"\n %wptid% = Waypoint ID." +
				"\n %coords% = Coordinates of the cache in HDD° MM.mmm HDDD° MM.mmm format." +
				"\n %vdistmi% = Distance between home coordinates and cache in miles, rounded to nearest 5 mile increment" +
				"\n %vdistkm% = Distance between home coordinates and cache in kilometers, rounded to nearest 5 km increment" +
				"\n %ncdir% = Compass direction (N,S,E,W,NE,NW, etc.) the nearby cache is from the cache being reviewed." +
				"\n %nctitle% = Name of the cache the owner's cache is too near." +
				"\n %ncwptid% = Waypoint ID of the cache the owner's cache is too near." +
				"\n %ncdistft% = Distance between the two caches in feet, rounded to the nearest 5 foot increment." +
				"\n %ncdistm% = Distance between the two caches in meters (not rounded)."
				"\n %contact% = Contact text, as you have specified above."
		;

		
		

		//  Log Settings: Contact info.
		var vSettingID = 'ContactText';
		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Enter your contact info.\nTo insert the contact info into the subsequent templates, use %contact% as replacement variable.\n';
		fCreateSetting(vSettingID, 'textarea', vLabel, '', vDftVal, '200px');




		//  Log Settings: Posted Near Posted.
		var vSettingID = 'ProxType';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'ProxNote';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Posted coordinates too near existing Posted coordinates', ['ProxType', 'ProxNote']);

		fAddEditControls('txtaraProxNote');



		//  Log Settings: Posted Near Wpt.
		var vSettingID = 'Lit1Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit1Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Posted coordinates too near existing Waypoint coordinates', ['Lit1Type', 'Lit1Note']);

		fAddEditControls('txtaraLit1Note');



		//  Log Settings: Wpt Near Posted.
		var vSettingID = 'Lit2Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit2Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Waypoint coordinates too near existing Posted coordinates', ['Lit2Type', 'Lit2Note']);

		fAddEditControls('txtaraLit2Note');




		//  Log Settings: Wpt Near Wpt.
		var vSettingID = 'Lit3Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit3Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Waypoint coordinates too near existing Waypoint coordinates', ['Lit3Type', 'Lit3Note']);

		fAddEditControls('txtaraLit3Note');

		//****************************************************************************************//
		//                                                                                        //
		//                              End of Control Creation                                   //
		//                                                                                        //
		//****************************************************************************************//


		fOpenSettingsDiv();
	}


	// sets the Log date select box to today's date - now performed natively
	function setTodaysDate () {
		const today = new Date();
		let dateInput = document.querySelectorAll('input[class=flatpickr-input]')[0]; 
		let outerLoop = setInterval(()=> {
			if (eventHold) return;
			else {
				clearInterval(outerLoop);
				eventHold = true;
				dispatchThis(dateInput,"click");
				outerLoop = setInterval(()=>{ //you have to wait for the React calendar picklist to appear
					let datePickerZone = document.getElementsByClassName('dayContainer')[0];
					if (datePickerZone) {
						let datePickerList = datePickerZone.getElementsByClassName('flatpickr-day today')[0];
						if (datePickerList) {
							clearInterval(outerLoop);
							dispatchThis(datePickerList,"click");
						eventHold = false;
						}
					}
				},50);
			}
		},50);
	}



	// put a check into the 'owner cannot delete log' checkbox
	function setCannotDeleteCheckbox () {
		cannotDeleteCheckboxFlag = true;
		var outerloop = setInterval(()=>{
			if (eventHold) return;
			else {
				clearInterval(outerloop);
				eventHold = true;
				if (!cannotDeleteCheckbox.checked) {
					dispatchThis(cannotDeleteCheckbox,"click");
				}
				eventHold = false;
			}	
		},54);
	}

	// put a check into the 'owner cannot delete log' checkbox
	function clearCannotDeleteCheckbox () {
		cannotDeleteCheckboxFlag = false;
		var outerloop = setInterval(()=>{
			if (eventHold) return;
			else {
				clearInterval(outerloop);
				eventHold = true;
				if (cannotDeleteCheckbox.checked) {
					dispatchThis(cannotDeleteCheckbox,"click");
				}
				eventHold = false;
			}	
		},59);
	}


	// insert string into log text area
	function insertTextContent(contentString) {
		if (contentString == null) return;
		let outerLoop = setInterval(()=>{
			if (!eventHold) {
				eventHold= true;
				clearInterval(outerLoop);
				var logText = document.querySelectorAll('input[id=log-text-control]')[0];
				logText.value = contentString;
				dispatchThis(logText,"click");  
				eventHold=false;
				if (cachedPlannedText == contentString) cachedPlannedText = 'blank';
			}
		},73);
	}


	// launch an event at an element 
	function dispatchThis (elementToClick,eventType) {
		const myEvent = new Event(eventType, {bubbles: true,isTrusted:true});
		elementToClick.dispatchEvent(myEvent);
	}
	

	//  Cancel button clicked.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}


	//  Save button clicked. Store all settings.
	function fSaveButtonClicked() {
		//  Get array of setting controls.
		var scs = document.getElementsByName(settingName);
		//  Process each control.
		var scl = scs.length;
		for (var i = 0; i < scl; i++) {
			var sc = scs[i];
			var rawid = sc.getAttribute('rawid', '');
			var ctrltype = sc.getAttribute('ctrltype', '');
			if (ctrltype == 'checkbox') {
				GM_setValue(rawid, sc.checked);
			} else {
				GM_setValue(rawid, sc.value);
			}
		}
		fCloseSettingsDiv();
	}


	//  Create Div for settings GUI.
	function fCreateSettingsDiv(sTitle) {

		//  If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return false;
		}

		//  Set styles for titles and elements.
		GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important; line-height:120% !important; } ' ); +

		//  Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		//  Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		popwidth = parseInt(window.innerWidth * .7);
		divSet.style.width = popwidth + 'px';

		//  Create heading.
		var ds_Heading = document.createElement('div');
		ds_Heading.setAttribute('class', 'SettingTitle');
		ds_Heading.appendChild(document.createTextNode(sTitle));
		divSet.appendChild(ds_Heading);

		var ds_Version = document.createElement('div');
		ds_Version.setAttribute('class', 'SettingVersionTitle');
		ds_Version.appendChild(document.createTextNode('Script Version ' + GM_info.script.version));
		divSet.appendChild(ds_Version);

		//  Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		//  Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);

		return true;
	}




	/*
	vID = GetSetting ID, and type + vID is control ID.
	vType = checkbox (input + type.checkbox), text (input), textarea, select
	vLabel = Text for control label.
	vTitle = Title text for input control and label, or vLabel if not specified.
	vDftVal = May be text, true/false, or matches a vSelVal array element.
	vSize = Length of input (text) box, or height of textarea, in pixels.
	vSelVal = Array of select option values.
	vSelTxt = Array of select option text.
	*/
	//  Create settings from passed data.
 	function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		var divSet = document.getElementById('gm_divSet');
		var kParagraph = document.createElement('p');
		kParagraph.id = 'p' + vID;
		kParagraph.setAttribute('class', 'SettingElement');
	
		switch (vType) {
			case 'checkbox':
				var kElem = document.createElement('input');
				kElem.id = 'cb' + vID;
				kElem.type = 'checkbox';
				kElem. checked = vDftVal;
				break;
			case 'text':
				if (!vSize) { vSize = '150px'; }
				var kElem = document.createElement('input');
				kElem.id = 'txt' + vID;
				kElem.style.width = vSize;
				kElem.value = vDftVal;
				kElem.addEventListener('focus', fSelectAllText, true);
				break;
			 case 'textarea':
				if (!vSize) { vSize = '80px'; }
				var kElem = document.createElement('textarea');
				kElem.id = 'txtara' + vID;
				kElem.style.width = '100%';
				kElem.style.height = vSize;
				kElem.value = vDftVal;
				break;
			case 'select':
				var kElem = document.createElement('select');
				kElem.id = 'sel' + vID;
				if (vSelVal) {
					if (vSelVal.constructor == Array) {
						for (var i in vSelVal) {
							var kOption = document.createElement('option');
							kOption.value = vSelVal[i];
							kOption.selected = (vSelVal[i] == vDftVal);
							if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								var kTxtNode = vSelTxt[i];
							} else {
								var kTxtNode = vSelVal[i];
							}
							kOption.appendChild(document.createTextNode(kTxtNode));
							kElem.appendChild(kOption);
						}
					}
				}
				break;
				
		}

		var kLabel = document.createElement('label');
		kLabel.setAttribute('class', 'SettingLabel');
		kLabel.setAttribute('for', kElem.id);
		kLabel.appendChild(document.createTextNode(vLabel));
		if (!vTitle) { vTitle = vLabel; }
		kElem.title = vTitle;
		kLabel.title = kElem.title;
		kElem.name = settingName;
		kElem.setAttribute('ctrltype', vType);
		kElem.setAttribute('rawid', vID);

		kLabel.innerHTML = kLabel.innerHTML.replace(/\n/g,'<br>');
		kLabel.innerHTML = kLabel.innerHTML.replace(/\t/g,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');

		if (vType == 'textarea') {
			kParagraph.appendChild(kLabel);
			kParagraph.appendChild(document.createElement('br'));
			kParagraph.appendChild(kElem);
		} else {
			kParagraph.appendChild(kElem);
			kParagraph.appendChild(kLabel);
		}
		divSet.appendChild(kParagraph);

		return kElem;
	}


	//  Routine for creating setting groupings.
	//  Use after the settings group has been established.
	//  Pass the heading text, and an array of setting IDs.
	//  Example:
	//  [Create SettingName1 here]
	//  [Create SettingName2 here]
	//  [Create SettingName3 here]
	//  fGroupSettings('Group Heading Text', ['SettingName1', 'SettingName2', 'SettingName3']);
	function fGroupSettings(legendText, aID) {
		var divSet = document.getElementById('gm_divSet');
		var fldset = document.createElement('fieldset');
		fldset.classList.add('gmsettings');
		divSet.appendChild(fldset);
		var fldsetlgd = document.createElement('legend');
		fldsetlgd.classList.add('gmsettings');
		fldset.appendChild(fldsetlgd);
		fldsetlgd.appendChild(document.createTextNode(legendText));
		var ic = aID.length;
		for (var i = 0; i < ic; i++) {
			fldset.appendChild(document.getElementById('p' + aID[i]));
		}
	}


	//  Resize/reposition on window resizing.
	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
		if (divSet) {
			var popwidth = parseInt(window.innerWidth * .7);
			divSet.style.width = popwidth + 'px';
			var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			divSet.style.left = leftpos + 'px';
		}
	}


	function fOpenSettingsDiv() {
		//  Create buttons.
		fCreateSaveCancleButtons();

		//  Add blackout and setting divs.
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.style.visibility = 'visible';
		divBlackout.style.visibility = 'visible';
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);
		//  Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}
	}


	//  Create Save/Cancel Buttons.
	function fCreateSaveCancleButtons() {
		var divSet = document.getElementById('gm_divSet');
		var ds_ButtonsP = document.createElement('div');
		ds_ButtonsP.setAttribute('class', 'SettingButtons');
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);
	}


	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0;  // snapback code
		divSet.parentNode.removeChild(divSet);
		divBlackout.parentNode.removeChild(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos;  // snapback code
	}

	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	function fSelectAllText() {
		this.select();
	}

	GM_addStyle('.EditIcons {vertical-align: text-bottom; margin-bottom: 2px; !important; } ' );

	function fAddEditControls(ctrl2add) {
		//  Create span to hold new controls.
		var spanCtrls = document.createElement("span");
		spanCtrls.name = 'spanCtrls';
		spanCtrls.style.whiteSpace = 'nowrap';
		spanCtrls.style.fontSize = '20px';
		spanCtrls.setAttribute('TextAreaID', ctrl2add);
		ctrl2add = document.getElementById(ctrl2add);
		insertAheadOf(spanCtrls, ctrl2add);


		//  Create block-quote controls and add to span.
		var lnkQuoteB = document.createElement("a");
		var lnkQuoteBImg = document.createElement("img");
		lnkQuoteBImg.src = imgBlockQuote;
		lnkQuoteBImg.classList.add('EditIcons');
		lnkQuoteB.appendChild(lnkQuoteBImg);
		lnkQuoteB.title = 'Forum-style Block Quoting';
		lnkQuoteB.setAttribute('editType', 'quote');
		lnkQuoteB.style.marginLeft = '3px';
		lnkQuoteB.href = 'javascript:void(0)';
		lnkQuoteB.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkQuoteB);


		//  Create link control and add to span.
		var lnkTextLink = document.createElement("a");
		var lnkTextLinkImg = document.createElement("img");
		lnkTextLinkImg.src = imgTextLink;
		lnkTextLinkImg.classList.add('EditIcons');
		lnkTextLink.appendChild(lnkTextLinkImg);
		lnkTextLink.title = 'Insert hyperlink';
		lnkTextLink.style.marginLeft = '3px';
		lnkTextLink.href = 'javascript:void(0)';
		lnkTextLink.addEventListener("click", fTextLink, true);
		spanCtrls.appendChild(lnkTextLink);

		//  Create bold control and add to span.
		var lnkTextBold = document.createElement("a");
		var lnkTextBoldImg = document.createElement("img");
		lnkTextBoldImg.src = imgTextBold;
		lnkTextBoldImg.classList.add('EditIcons');
		lnkTextBold.appendChild(lnkTextBoldImg);
		lnkTextBold.title = 'Bold';
		lnkTextBold.setAttribute('editType', 'b');
		lnkTextBold.style.marginLeft = '3px';
		lnkTextBold.href = 'javascript:void(0)';
		lnkTextBold.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextBold);

		//  Create italic control and add to span.
		var lnkTextItalic = document.createElement("a");
		var lnkTextItalicImg = document.createElement("img");
		lnkTextItalicImg.src = imgTextItalic;
		lnkTextItalicImg.classList.add('EditIcons');
		lnkTextItalic.appendChild(lnkTextItalicImg);
		lnkTextItalic.title = 'Italic';
		lnkTextItalic.setAttribute('editType', 'i');
		lnkTextItalic.style.marginLeft = '3px';
		lnkTextItalic.href = 'javascript:void(0)';
		lnkTextItalic.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextItalic);

		//  Create Strikethrough control and add to span.
		var lnkTextStrikethrough = document.createElement("a");
		var lnkTextStrikethroughImg = document.createElement("img");
		lnkTextStrikethroughImg.src = imgTextStrikethrough;
		lnkTextStrikethroughImg.classList.add('EditIcons');
		lnkTextStrikethrough.appendChild(lnkTextStrikethroughImg);
		lnkTextStrikethrough.title = 'Strikethrough';
		lnkTextStrikethrough.setAttribute('editType', 's');
		lnkTextStrikethrough.style.marginLeft = '3px';
		lnkTextStrikethrough.href = 'javascript:void(0)';
		lnkTextStrikethrough.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextStrikethrough);


		//  Create Font selector and add to span.
		var fontSelect = document.createElement("select");
		fontSelect.name = 'fontSelect';
		fontSelect.style.borderStyle = 'solid';
		fontSelect.style.borderWidth = 'thin';
		fontSelect.style.padding = "1px";
		fontSelect.style.borderColor = 'rgb(165, 172, 178)';
		fontSelect.style.marginBottom = '3px';
		fontSelect.style.marginLeft = '6px';
		fontSelect.title = 'Select text font';
		spanCtrls.appendChild(fontSelect);

		//  Add options to Font selector.
		var lastFont = GM_getValue("EditTextFont", 'Andale Mono');
		var aFonts = ['Andale Mono', 'Arial', 'Arial Black', 'Book Antiqua', 'Century Gothic',
				'Comic Sans Ms', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma',
				'Times New Roman', 'Trebuchet Ms'];
		for (var c = 0; c < aFonts.length; c++) {
			var cOption = document.createElement("option");
			cOption.value = aFonts[c];
			cOption.selected = (aFonts[c] == lastFont);
			fontSelect.appendChild(cOption);
			var tFont = aFonts[c];
			cOption.appendChild(document.createTextNode(tFont));
			cOption.style.fontFamily = tFont;
			cOption.style.fontSize = 'small';
		}
		fontSelect.style.fontFamily = lastFont;
		fontSelect.style.fontSize = 'small';


		//  Create Font control and add to span.
		var lnkTextFont = document.createElement("a");
		var lnkTextFontImg = document.createElement("img");
		lnkTextFontImg.src = imgTextFont;
		lnkTextFontImg.classList.add('EditIcons');
		lnkTextFont.name = 'textFont';
		lnkTextFont.appendChild(lnkTextFontImg);
		lnkTextFont.title = 'Apply selected font';
		lnkTextFont.setAttribute('editType', lastFont);
		lnkTextFont.style.marginLeft = '6px';
		lnkTextFont.href = 'javascript:void(0)';
		lnkTextFont.addEventListener("click", fEditTextFont, true);
		spanCtrls.appendChild(lnkTextFont);


		//  Create Color selector and add to span.
		var colorSelect = document.createElement("select");
		colorSelect.style.borderStyle = 'solid';
		colorSelect.style.borderWidth = 'thin';
		colorSelect.style.padding = "1px";
		colorSelect.style.borderColor = 'rgb(165, 172, 178)';
		colorSelect.style.marginBottom = '3px';
		colorSelect.style.marginLeft = '10px';
		colorSelect.style.fontSize = 'small';
		colorSelect.title = 'Select text color';
		spanCtrls.appendChild(colorSelect);

		//  Add options to Color selector.
		var aColors = ['beige', 'black', 'blue', 'brown', 'gold', 'green', 'limegreen', 'maroon',
				'navy', 'orange', 'pink', 'purple', 'red', 'teal', 'violet', 'white', 'yellow'];
		for (var c = 0; c < aColors.length; c++) {
			var cOption = document.createElement("option");
			cOption.value = aColors[c];
			cOption.selected = (aColors[c] == 'black');
			colorSelect.appendChild(cOption);
			var tColor = aColors[c].substr(0,1).toUpperCase() + aColors[c].substr(1);
			cOption.appendChild(document.createTextNode(tColor));
			cOption.style.color = tColor;
			cOption.style.fontSize = 'small';
			cOption.style.backgroundColor = 'Lavender';
		}

		//  Create Color control and add to span.
		var lnkTextColor = document.createElement("a");
		var lnkTextColorImg = document.createElement("img");
		lnkTextColorImg.src = imgTextColor;
		lnkTextColorImg.classList.add('EditIcons');
		lnkTextColor.appendChild(lnkTextColorImg);
		lnkTextColor.name = 'colorPicker';
		lnkTextColor.title = 'Apply selected color';
		lnkTextColor.style.marginLeft = '6px';
		lnkTextColor.href = 'javascript:void(0)';
		lnkTextColor.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextColor);

	}


	//  Insert hyperlink.
	function fTextLink() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		var tlRtnVar = prompt('Enter the complete URL for the hyperlink.', 'http://');
		if (tlRtnVar == null) {
			alert('Link insertion canceled.');
			return;
		}
		//  [url=tlRtnVar][/url]
		var opnBBCode = '[url=' + tlRtnVar.trim() + ']';
		var clsBBCode = '[/url]';

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save that text.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}
		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);
		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;
		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply text edits (block-quotes, bold, italic, strikethrough, and color).
	function fEditText() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		if (this.name == 'colorPicker') {
			var newColor = this.previousSibling.value;
			var opnBBCode = '[' + newColor + ']';
			var clsBBCode = '[/' + newColor + ']';
		} else {
			var editType = this.getAttribute('editType');
			var opnBBCode = '[' + editType + ']';
			var clsBBCode = '[/' + editType + ']';
		}

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save starting and ending points.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;

		//  If block quote, use enhances mode, if turned on.
		if (editType == 'quote') {
			if (selStart == selEnd) {
				e_LogTextArea.select();
				selStart = e_LogTextArea.selectionStart;
				selEnd = e_LogTextArea.selectionEnd;
			}
		}

		//  Get selected text.
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}

		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);

		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;

		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply text edits (fonts).
	function fEditTextFont() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		// var newFont = this.getAttribute('editType');
		if (this.name = 'textFont') {;
			var newFont = this.previousSibling.value;
			var opnBBCode = '[font=' + newFont + ']';
			var clsBBCode = '[font=Verdana]';
		}

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save that text.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}
		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);
		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;
		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply Auto Trim to selected text.
	function fAutoTrim(curLogTextArea) {
		var atStart = curLogTextArea.selectionStart;
		var atEnd = curLogTextArea.selectionEnd;
		if (atEnd > atStart) {
			var atTxt = curLogTextArea.value;
			var atLastChar = atTxt.substr(atEnd-1, 1);
			while (atLastChar.match(/\s/)) {
				atEnd--;
				var atLastChar = atTxt.substr(atEnd-1, 1);
			}
			curLogTextArea.selectionEnd = atEnd;
		}
	}

	// Go get the list of current bookmarks for this user
	function refreshBenchmarkList() {
		let targetURL = document.URL.substring(0,(document.URL.indexOf('geocaching.com')+14)) + 
			"/bookmarks/mark.aspx?view=legacy&bmlist=y";
		GM_openInTab(targetURL);
	}
	
	function fPostButtonColor () {
		let nativePostButton = document.querySelector('div[class=post-button-container]>button');
		let scriptButton1 = document.getElementById('scriptXtraPostButton1');
		let scriptButton2 = document.getElementById('scriptXtraPostButton2');

		if (nativePostButton && scriptButton1 && scriptButton2) {
			if (nativePostButton.attributes.class.value.indexOf('disabled') > 0) {
				scriptButton1.style.backgroundColor = 'white';
				scriptButton1.style.color = 'gray';
				scriptButton2.style.backgroundColor = 'white';
				scriptButton2.style.color = 'gray';
			}
			else {
				scriptButton1.style.backgroundColor = 'green';
				scriptButton1.style.color = 'white';
				scriptButton2.style.backgroundColor = 'green';
				scriptButton2.style.color = 'white';
			}
		}
	}

	// This builds a map of logType# : logTypeName needed to correctly select lot type from the NextJS dropdown menus
	function fCheckLogTypeMap(){ 
		let postLogType = fGetLogType();
		if (postLogType == '99') return;
//		let logTypeArea = document.querySelector('input[name=logType]');
//		if (!logTypeArea) return;
//		else postLogType = logTypeArea.value;
//		if (!postLogType) return;
		let logValueArea = document.querySelector('div[class^=log-type-option]>span');
		if (!logValueArea) return;
		let logValue = logValueArea.textContent;
		if (!(getLogTypeKey(logValue) == '99')) return; // the logValue is already in our map
		console.log('attempting to set the key/value pair: '+postLogType+'/'+logValue);
		logTypeNumtoText.set(postLogType,logValue);
		if (postLogType == 18) logTypeNumtoText.set(68,logValue); // these two LogType# have the same text name
		if (postLogType == 68) logTypeNumtoText.set(18,logValue);
		GM_setValue('logTypeMap',JSON.stringify(Object.fromEntries(logTypeNumtoText)));
		setTimeout(()=>{
			localStorage.setItem(logTypeMapCopyTrigger,Math.random());
		},75); //This fires faster than Tampermonkey updates the TM storage item, so a delay is required
		console.log('New Log just updated the saved version of the logType Map.');
	}

	function fGetLogType(){
		let logTypeArea = document.querySelector('input[name=logType]');
		if (!logTypeArea) return '99';
		else postLogType = logTypeArea.value;
		if (!postLogType) return '99';
		return postLogType.toString();
	}

	function fPostLogByPOSTMethod() {
		if (fDetectImages()) {
			alert('The "Submit All" button can not be used on this log.\n\n'+
				'"Submit All" does not work for attached images.  There is\n'+
				'an image attached to this log.');
			return;
		}
		googleTagLogNL('POSTed_log');
		var rightNow = new Date();
		let postLogBody = new Object;
		let postLogText = "blank";
		let postLogValue = "Unknown";
		let postLogType = 99;
		if (!eventHoldClear250ms) { // a change to the log is in the lazy loading queue
			if (cachedPlannedText != "blank") { // if it isn't blank, it has the highest priority
				postLogText = cachedPlannedText;
				postLogValue = cachedPlannedLogValue; //need to convert to the logType version(?)
			}
			else {
				postLogText = lastSetPlannedValuesTextRequest;
				postLogValue = lastSetPlannedValuesLogValueRequest;
			}
			postLogType = getLogTypeKey(postLogValue);
			let published=true;
			if (document.querySelector('span[class="badge unpublished"]')) published = false;
			if (published && postLogType==18) postLogType = 68;
			if (!published && postLogType==68) postLogType = 18;
		}
		else { // there is no change to the log pending - we'll grab the log info from the page
			var logText = document.querySelectorAll('input[id=log-text-control]')[0];
			if (logText.value.trim().length > 0) {
				postLogText = logText.value;
				let logTypeArea = document.querySelector('input[name=logType]');
				if (!logTypeArea) return;
				else postLogType = logTypeArea.value;
			}
			else {
				console.log('There was no log text, so we are ignoring the submit request.');
				return; 
			}
		}

		// now for some URL parsing
		if (postLogType == 99) {
			alert(	'The correct logType was not found.  The correct value for \n'+
					postLogValue+' could not be found.  It is probably an issue of the\n'+
					'script learning the logTypes for non-English languages.  Try\n'+
					'opening a log page, and cycling through the log types one at a\n'+
					'time.  The script will learn them as each is selected.  No need\n'+
					'to actually post a log.');
		}
		let gcLocation=document.URL.indexOf("GC");
		let gcCode = document.URL.substring(gcLocation,document.URL.indexOf("/",gcLocation));
		let pageDomain = document.URL.substring(0,document.URL.indexOf(".com")+4);
		postLogBody = {
			"logText": postLogText,
			"geocaches": [
				gcCode // OK to have a list here
			],
			"OwnerCannotDelete": cannotDeleteCheckboxFlag,
			"logDate": rightNow.toISOString(),
			"logType": postLogType
		}
		let init = {
			method: "POST",
//				mode: "no-cors",
//				credentials: "include",
			headers:{"Content-Type": "application/json"},
			body: JSON.stringify(postLogBody)
		}

		async function doThePost() {
			let response = await fetch(pageDomain+"/bookmarks/mark.aspx",init)
			.catch((err) => {
				alert('the POST failed with: '+err);
				return null;
			});
			return response.json();
		}
		doThePost()
			.then((statusArray) => {
				if (!statusArray) return;
				let alertMessage = "There were errors posting the following logs:\n\n";
				let alertFlag=false;
				statusArray.forEach(element => {
					if (!element.Success) {
						alertMessage+=("     "+element.GeocacheReferenceCode+" - "+element.ErrorMessage+"\n");
						alertFlag=true;
					}
				});
				if (alertFlag) {
					// catch errors where the Reviewer Note type was wrong
					if (alertMessage.indexOf('AfterPublishPostReviewerNote') > 0) { 
						postLogBody.logType = 18;
						init.body = JSON.stringify(postLogBody);
						doThePost().then((statusArray) => {
							if (!statusArray) return;
							let alertMessage = "There were errors posting the following logs:\n\n";
							let alertFlag=false;
							statusArray.forEach(element => {
								if (!element.Success) {
									alertMessage+=("     "+element.GeocacheReferenceCode+" - "+element.ErrorMessage+"\n");
									alertFlag=true;
								}
							});
							if (alertFlag) {
								alert(alertMessage);
								return null;
							}			
							else window.close();		
						})
					}
					else {
						alert(alertMessage);
						return null;
					}
				}
				else window.close();
			})
	}
}

// wait for page to fully load, and then loft the main script functions
let mainIterations = 0; // Note: also used as a hydration failsafe
let SignedInX = document.querySelectorAll('span[class*="username"]')[0];	
let intervalID = setInterval(function() {
	mainIterations++;
	if ( (logPage && hydrated) || (logPage && (mainIterations > 49)) || (bookmarkPage && SignedInX) ) {
		clearInterval(intervalID);
		setTimeout(loaded,100+1000*settingScriptHydrationDelay); //TM configurable delay is intended to allow for NextJS hydration. 
	}
	if (mainIterations > 50) { //also a hydration failsafe at 4.9 seconds
		clearInterval(intervalID);
		console.log('New log thinks this is not a page for templates and bookmarks and is terminating those functions.');
	}
	logPage = document.getElementById(logPageLabel);
    bookmarkPage = document.getElementById(bookmarkPageLabel);
	SignedInX = document.querySelectorAll('span[class*="username"]')[0];	
}, 100);

