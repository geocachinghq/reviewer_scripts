﻿// ==UserScript==
// @name           GCR Review v2
// @description    Enhancements to the GC Reviewer page
// @namespace      https://admin.geocaching.com
// @version        02.71
// @icon           https://i.imgur.com/a94PYHJ.png
// @resource       litmusCSS1 https://admin.geocaching.com/Content/geoadmin?v=xkfBdcXBektkFAxt0aOF42Vbu0JEF-vKdobvSrvKQaI1
// @resource       litmusCSS2 https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/CSS/gcr_Review_Page.txt
//--- ZoneCatcher code start ---
// @require        https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js
//--- ZoneCatcher code end ---
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Page.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Page.user.js
// @include        https://*.geocaching.com/admin/review.aspx*
// @include        https://admin*.geocaching.com/LitmusTest/*
// @include		   */admin/review.aspx*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_getResourceText
// @grant          GM_info
// @grant          GM_log
// @grant          GM_openInTab
// @grant          GM_registerMenuCommand
// @grant          GM_unregisterMenuCommand
// @grant          GM_setClipboard
// @grant          GM_xmlhttpRequest
// @grant          window.close
// @connect        zc.geocaching.com
// @connect        zc-staging.geocaching.com
// ==/UserScript==

/*

Function:
 Enhancements to the GC Reviewer page

*/

/* Change history
 *	02.71	2025-02-18	Auto-open litmus on 'red' results and add associated settings
 *	02.70	2024-12-17	ZC check to use zc-staging when not on www.geocaching.com
 *	02.69	2024-11-08	Add ZoneCatcher check with fuzzy factor
*	02.68	2024-10-31	Resolve script error messing up the RLT map control buttons
 *	02.67	2024-10-08	Fix script error when there is no disable link (for unpublished, disabled)
 *	02.66	2024-07-10	Add a review page settings menu item to blank the HQRegions ThumbsUp list
 *	02.65	2024-05-07	Log region overrides to Tag Manager, and set up temporary separate override storage
 *	02.64	2024-01-09	suppress Update Coordinates item when on native Review Page
 *	02.63	2024-04-02	fix 'logs' breadcrumb
 *	02.62	2024-01-09	revert v2.59.  OpenInTab now fixed in TM5.0.1
 *	02.61	2024-01-06	fix litmusCopy misfiring on the "too close to cache" items
 *	02.60	2023-12-12	begin Tag Manager logging for basic analytics
 *	02.59	2023-12-11	remediate TM5 tab-spawning
*	02.58	2023-11-07	depricate "terraserver" maps link in maps list
 *	02.57	2023-10-10	remove logging flow transition dross, update update coordinates call
 *	02.56	2023-10-03	non-English signedInAs fix, adj native litmus copy for mac users
  *	02.55	2023-09-26	Fix user-configurable markdown link selector (loggedInAs error)
 *	02.54	2023-08-30	Eliminate log flow choice and set permanently to new flow
 *	02.53	2023-07-26	Slight mod to get review page script litmus copy link working again
  *	02.52	2023-07-18	Do not duplicate a native litmus copy link
  *	02.51	2023-05-15	Turn on new/old logging flow toggle
 *	02.50 	2022-11-15  Updates to reflect new log flow
 *  02.49	2023-05-02  Suppress script delete log function when function is provided on native website
 *  02.48	2022-08-04  Vacation warning mod to reflect 100km warning added to native website
 *  02.47	2022-08-02	Bug fix to copy icon on RLT page
 *	02.46	2022-07-18  Correct watch/unwatch and lock/unlock launched by Bookmark Processor
 *	02.45	2022-06-09	Added a search for a CO's nearby finds to help with prox issues
 *	02.44	2022-06-08  Edits to bring reverse geocoding check to HQ native geocoder.  Move v2.39 from script to web.
 *  02.43	2022-06-08	Fixed error where script stripped log skip-by-50 info from form action
 *  02.42   2022-05-15  Added new copy-to-clipboard format 
 *	02.41	2022-02-01	Add additional geonames.org account for load balancing and load diagnostics
 *	02.40	2022-01-25	Add challenge attribute, checker link and title word checks
 *  02.39	2022-01-18	Fixed favicon display and required https
 *  02.38   2021-10-16  Added workaround for geonames.org level=1 bug
 *  02.37   2021-03-17  Added anchor for Waypoints at top of Review Page links
 *  02.36   2020-12-17  Removed old redundand code (script based review queue note)
 *  02.35   2020-06-07  Adding link to PCG Forum for Challenges
 *  02.34   2020-04-02  Bug fix to make Litmus Test icon replace site's favicon on chrome
 *  02.33   2020-02-27  Added link to Wiki anchor links to the breadcrumb line. Made Guidelines, Help Center, and Wiki open in new Tab
 *  02.32   2019-12-20  Adjusted URL of update coordinate link on published listings to point to new logging flow.
 *  02.31   2019-12-09  Bug fix to make "close tab when the green publish button is pressed" work again
 *  02.30   2019-12-06  Added guideline and Help Center anchor links to the breadcrumb line (integrates functionality of GCR Guidelines and KB Links)
 *  02.29   2019-11-07  Removed username from timestamp for private reviewer note and removed script review queue note
 *  02.28   2019-06-25  Updated code to account for public and private reviewer note, and for rebranded icons (same ids)
 *  02.27   2018-10-05  adding different formats for copying (from request in RWH) - srebeelis
 *  02.26   2018-07-18  using new api.geonames.org user name
 *  02.23   2018-05-03  add option to toggle auto-close of holding caches
 *  02.21   2018-03-16  integrating gcr_LitmusConflictCopy functionality
 *  02.20   2018-03-07  fixed Wheelchair icon not being seen by script
 *  02.19   2018-02-12  fixed auto close after selecting publish or hold on review queue
 *  02.18   2018-02-03  added code to close tab when the green publish button is pressed
 *  02.17   2018-02-02  removed zone catcher code, now that Hemlock's Gcr Zone Catcher extension works again
 */


 // ZoneCatcher check global variables
let ZCStatus="";  // changes when each of the two ZC calls complete
let ZCFuzzy, ZCPoint, ZCMenu;  // the list of zones and keywords for each of the two ZC calls and the TM menu item
getZCdata();

// Cancel if popup.
if (window.name == 'gcpop1') { return; }

// Google Analytics setup - a more fault-tolerant approach
function gtagRP(){dataLayer.push(arguments)}
if (typeof dataLayer === 'undefined') dataLayer = [];
function googleTagLogRP(dataAction){ //Method to do a gtag sending an action name
	if (document.URL.indexOf("www") < 0) return;
	try {
		gtagRP('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'ReviewPage',/*'debug_mode':true,*/
			'send_to':'G-GRQE2910DL'});
		console.log('Script '+dataAction+' logged to Tag Manager.');
	}
	catch {console.log('There was an error using gtag.  Logging attempt failed.')}
}
googleTagLogRP('initialize');

// Check session storage to see if auto-close was requested.
fCheckAutoClose();
function fCheckAutoClose() {
	var chkautoclose = sessionStorage.getItem('autoclose');
	if (chkautoclose == 'true') {
		sessionStorage.setItem('autoclose', 'false');
		chkautoclose = sessionStorage.getItem('autoclose');
		sessionStorage.removeItem('autoclose');
		window.close();
		return;
	}
}

var DEBUG=false; // true enables logging
var debugLog = function(){
	if(DEBUG){
		console.log.apply(console, arguments);
	}
};

// Enhancement switches.
// Set value to 0 (zero) to turn off, or 1 (one) to turn on.
var Switch_OwnerNotes = 1;      // Add Owner Notes
var Switch_CacheNotes = 1;      // Add Cache Notes
var Switch_RevArcLink = 1;      // Show Review Note and Archive/Unarchive links
var Switch_CachePage = 1;       // Show cache page link
var Switch_ChangeTitle = 1;     // Change display of cache title
var Switch_ChangeWaypoint = 1;  // Change display of cache waypoint
var Switch_ChangeOwner = 1;     // Change display of owner name
var Switch_NearbyLinks = 1;     // Add links for more nearby caches
var Switch_CacheProximity = 1;  // Show alert for caches closer than 0.1 mile
var Switch_ArcLogs = 1;         // Add links for archived logs to Neaby Caches
var Switch_AddWptSts = 1;       // Show additional waypoints status for nearby caches
var Switch_MoreMaps = 1;        // Add links for more maps.

// The following are for 1-click directions from you home coordinates.
// Actual values are now stored in gm data strings.
var HomeLabel;
var HomeDdLat;
var HomeDdLon;

// Litmus text option indexes.
var iloWatched = 0;
var iloDisabled = 1;
var iloHeld = 2;
var iloPublished = 3;
var iloArchived = 4;

// Get current domain;
var domain = location.protocol + '//' + location.hostname;
var mainPageUrl = document.location + '';

// Pre-acquire needed DOM elements.
var e_CacheDetails_CacheImages   = document.getElementById('ctl00_ContentBody_CacheDetails_CacheImages');
var e_CacheDetails_Container     = document.getElementById('ctl00_ContentBody_CacheDetails_Container');
var e_CacheDetails_Location      = document.getElementById('ctl00_ContentBody_CacheDetails_Location');
var e_CacheDetails_LocationError = document.getElementById('ctl00_ContentBody_CacheDetails_LocError');
var e_CacheDetails_GpxButton	 = document.getElementById('ctl00_ContentBody_CacheDetails_downloadGpxBtn');
var e_CacheDetails_LongDesc      = document.getElementById('ctl00_ContentBody_CacheDetails_LongDesc');
var e_CacheDetails_Name          = document.getElementById('ctl00_ContentBody_CacheDetails_Name');
var e_CacheDetails_Owner         = document.getElementById('ctl00_ContentBody_CacheDetails_Owner');
var e_CacheDetails_PlaceDate     = document.getElementById('ctl00_ContentBody_CacheDetails_PlaceDate');

var e_CacheDetails_Placers       = document.getElementById('ctl00_ContentBody_CacheDetails_Placers');

var e_CacheDetails_RefID         = document.getElementById('ctl00_ContentBody_CacheDetails_RefID');
var e_CacheDetails_ShortDesc     = document.getElementById('ctl00_ContentBody_CacheDetails_ShortDesc');
var e_CacheDetails_Status        = document.getElementById('ctl00_ContentBody_CacheDetails_Status');
var e_CacheDetails_Terrain_Stars = document.getElementById('ctl00_ContentBody_CacheDetails_Terrain_Stars');
var e_CacheDetails_Website       = document.getElementById('ctl00_ContentBody_CacheDetails_Website');
var e_ddAction                   = document.getElementById('ctl00_ContentBody_ddAction'); //null
var e_lnkArchive                 = document.getElementById('ctl00_ContentBody_lnkArchive');
var e_lnkArchived                = document.getElementById('ctl00_ContentBody_lnkArchived');
var e_lnkBackgroundImg           = document.getElementById('ctl00_ContentBody_lnkBackgroundImg');
var e_lnkBookmark                = document.getElementById('ctl00_ContentBody_lnkBookmark');
var e_lnkDisable                 = document.getElementById('ctl00_ContentBody_lnkDisable');
var e_lnkEdit                    = document.getElementById('ctl00_ContentBody_lnkEdit');
var e_lnkListing                 = document.getElementById('ctl00_ContentBody_lnkListing');
var e_lnkLog                     = document.getElementById('ctl00_ContentBody_lnkLog');
var e_lnkReviewNote              = document.getElementById('ctl00_ContentBody_lnkRNote');
var e_MapLinks_MapLinks          = document.getElementById('ctl00_ContentBody_MapLinks_MapLinks');
var e_UserInfo_lbDistance        = document.getElementById('ctl00_ContentBody_UserInfo_lbDistance');
var e_UserInfo_lnkFinds          = document.getElementById('ctl00_ContentBody_UserInfo_lnkFinds');
var e_UserInfo_lnkHides          = document.getElementById('ctl00_ContentBody_UserInfo_lnkHides');
var e_UserInfo_lnkReviewerLogs   = document.getElementById('ctl00_ContentBody_UserInfo_lnkReviewerLogs');
var e_CacheDetails_imgType       = document.getElementById("ctl00_ContentBody_CacheDetails_imgType");
var LitmusLink                   = document.getElementById('ctl00_ContentBody_uxReviewerLitmusLink');
var LitmusIcon                   = document.getElementById('ctl00_ContentBody_uxReviewerLitmusIcon');
var ToolbarIcon1                  = document.querySelector('link[rel="shortcut icon"]');
var ToolbarIcon                  = document.querySelector('link[rel="icon"]');

var e_ZCLink                     = document.getElementById('ZoneGMLink');

var e_btnHold                    = document.getElementById('ctl00_ContentBody_holdButton');
var e_btnWatch                   = document.getElementById('ctl00_ContentBody_watchButton');
let e_btnWatchStatus = false;
if ((!e_btnWatch==null) && (e_btnWatch.firstChild.data.indexOf('Stop') > -1)) e_btnWatchStatus = true;
var e_btnLock                    = document.getElementById('ctl00_ContentBody_lockButton');
let e_btnLockStatus = false;
if ((!e_btnLock==null) && (e_btnLock.firstChild.data.indexOf('Unlock') == 0)) e_btnLockStatus = true;
var e_btnPublish                 = document.getElementById('ctl00_ContentBody_publishButton');
var e_btnHoldAndWatch            = document.getElementById('ctl00_ContentBody_holdAndWatchButton');

var TrashCan =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuNU" +
"mK%2FOAAAACNSURBVDhP1VPBDcAgCGQnd%2BoKzuIKnaVrOIfNxV4xJKBN2kcxBBLkOBBF" +
"vpK0pSbStfsTsQl5z61eB%2F4CYGrlKHcSk2kRgwKsMzMyViB1z7otIYAKrGattuLOQ9sA" +
"GOnDD%2BkrniaBPgHg%2FxCAMwD1RzMY39v67g5wiKjEp8Rlq4yHSx0t1NKfmP2Z1%2BMn" +
"nV0oZlDkZ4QAAAAASUVORK5CYII%3D";

var ThumbsUp =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuND" +
"6NzHYAAAGZSURBVDhPlZO%2FS8NAFMevi5sVJ0fHOgni5uoilc4KbjYiDkJApPQvaNHBGK" +
"eA%2BGNxiKBoqYG2VFwKSq3YoUK1waFYkFbFblr67PfFtFELpoFHkrt7n%2Ff9vrvzCBfP" +
"0%2FUp9T%2BnRWNgSAxOhD0uUn4uKR%2FMUMO8ILx7Tn7JavSaiRDd7fQO%2BKwVOYlqCY" +
"YUz9bdK6hXbji5WdI58rpMUNPVQqnyTtp%2BnNSjApnmIy%2FMaZIlvVX9LRlhGEMeMhxQ" +
"x7DsfZWGZ0%2BoL3BsxeReJ7nl21aAdzm9yzYAulWn%2BF%2F4ZYPE3Hk7FiSJ6oZMH9%2" +
"BNgwJnYDcAg0KABCraAG8wSfGNKAOauS1e6Ezm%2F5YqWEqExzsAWz5AxlqAvf8BVK%2F4" +
"LACCHqEnh8teEvCPgBWoWQ3pFqBLdYzBGpKhIBYdJSEpMRoJXZJvKcWg6ZVFrvDbuy0fcF" +
"hsW8AW%2BvybDIEVfKNB6Db8AuT0jmTsAOS3D1a%2BYBKUjM2rpCgKT6AXkGhXgir7PGAc" +
"867uBQ4NFG0HBQNRGd%2B4oa4A%2Fy36Au5u8600ThC%2BAAAAAElFTkSuQmCC";

var GeneralWarningImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAE" +
"EElEQVR42s2Va2wUVRTH%2F3dmOn1tt8u23b63BW0MQmIgQVJEVOSRSHdJSkRitFb5QqLx" +
"izF8UuojxugXY2JQE8MXXx%2BA2lbUNhIxKG0CJK3YYEu3S9dut%2Fvqe7vdeV3PTMmSZc" +
"VW1MRJbu7dc885v7nnP%2Fcsw3%2F8sP8VYKip%2BagOXnvv1%2B0v%2FOuAod1eXrRxLT" +
"gTEPvVh%2Ft6OlcVuyqna3u8vMLzAGRRAY9FkVIYfBcD2NzdvmL8ig7XvQePiY6iNte2dV" +
"BHRihChCSLCIzMIrGotG3qOv3aPwL4HmniNUe8SP3YA1ZSSRYOHpmEZHfiyi8RbO3%2Bit" +
"0xILDvQKRwy%2FqyPGEKOQ%2FtR86OPZZdOdcD7dQJTCVyEAovhO%2F%2Fpr3izgB7m3lF" +
"6y6kzn0H24cdGXtzLR7kllfhylAcW7pOsb8NCO7Zz%2B3e7ZASIWh%2BP2zHT2cCnn4MUr" +
"kbc7MJ%2BIPz2PZtB1s1YNJ78HVWUvSK89ENUHx%2BGKHxbECrF5KrFjJpMuifRlLV3mjs" +
"OPnqqgChnU3c%2FtRuWoxaX40RmoDt%2FS8yfGYe3gihqh6CqkKXCnAppmLX92fYioCw9%" +
"2FFOuaHGk7%2FJDT4ehB6PQbvci%2BKzv2X4xdcWQKy9G1B1yDpH%2F%2FUYwkne1bwQ8%" +
"2F4lIHKghTsONSLZ3QFjegqCKMMYHYHjYigT0GBPA5gGyAZD1%2BA4nkjE2W0Bkzv2TuQ2" +
"lFfKYhjaYmp52%2BAwfMNw9AVvC%2BAqh6QDv8cXMBCdDTy5MFWXBQi76jzaktrpfKcF6s" +
"994JJkBVsA%2FygcF8YyAeudECvr0wCmGigivU4OB82s3kNz8a4MQCivlNvbnoXu66cguq" +
"2mUbsBmJlGzs4mFL70puW78PZRpL48AdFZS76aBQABRCpTMqHgzHgEh5MzLA2IuuqOweVs" +
"K3imEfrVYbII4IaxDNBpVlTqQ1dhTE4vH9teSKMYor0iA8A1jiLOcHYihmhy6aOWxdkjFm" +
"BScHD7Jy9DO%2F8DIEpW0jTAPEEkBNt7n0PasNkCaAOXMPdcM4Ti0nSJTAAIINBsiv5pII" +
"jnU%2FOMRddU98meB7cKbomaWJx6Gc8G%2BK6RyOOZGtzjgFi9LgtgzrkUNjgzj%2F75uS" +
"iL5pVx2%2FEXoVw4T3dKpuRaNmCMRP7pVpFLSWT3nwLMkU%2Bl%2BmxiAixaVs9t77ZC6e" +
"0lQczy6FkAJBIAtefCtz6AUFJBjW4ftNEhiI6qtAb8FoCTcn0cDBDAUT2W59nuZncJwHzy" +
"JkAzLxANWjMq29LAIAUq5t8BWH4%2BaSUi11EOpJZPICg3ATIV4XJ8xizR8lcUXlMzRjnc" +
"cNlg0LU36O11xojDodMGpbB%2Bm4fRuW7NKtkNgms39jVaW%2F4wEF1SyG4EDi%2FO1v0B" +
"daNJ3oB8YjwAAAAASUVORK5CYII%3D";

var TimeclockSrc =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAE" +
"yElEQVRIx7WVa0xTZxjHyaJzRBlBpqnTxgphQkNtIUBplDrDxRE2Cw0QxTBr1C3i3AYCce" +
"C4jQpaoJWbF1CPgjqug4GXaeJxToYtSECJy8a0SguCQk8R9Qsf%2FnvPqSINGozbTvLL6e" +
"mH3%2FO8%2F%2Ffm4DDDk%2FxdCs5fOA99hwEtZ1tBURTi4uKQlJQEh3%2F7JKXuQl1DHa" +
"xWKyYmJtDd04Pa%2BjoUFhX%2BN0X%2B%2FwJE0NDUiKu%2FX8W5i%2BfR2PoT6psacPJM" +
"FfYXaZCwI4F5K%2FHPF1oEBLq2kUL5wa%2FxQ1YgdictRMo385CezMPeHClKSr7EUaoYx6" +
"soupKqFLyxvOlcs6q%2B6QxTVLgBe5LnoqpiDtovz8bfPbNgPMDHnZtzYLjiiNoT85Cf7Y" +
"SiAgXKj%2BiYssNlqhnlDc2NqprGamTtEeFomSNMf7yD4buzCO9i2DgHD42OhHkEJzy654" +
"yBv1xQQ7lAs9cL2uJ9KNAVvr7Ij401glO1FJOTIcLpY45EOnuKdK5NanyfiF0I8zFy%2Fw" +
"PCQoz089BS5wrtPk%2Fka7IYdb761XFV15yiiwqiceIw2%2BF7MBYv47q1SZ2nSBdw0tH%" +
"2BRYTFuFfqBYtpKZprFqBwfwgyczLpaXKqmpJUHtchL8sJ5j9fStPyMjFqWsIx0r9wUsp%" +
"2Bs1KLWUBwI7iT0XqAOjgf32fuRGpaqsSuQAVVqdXpNqO1zvl5BK5cp6P9H9oVsZj4k7D%" +
"2FW8wfEZaDGfAiCPHbRT7ycsPx7a5ErV2B8iPltGavL3o7XCdzZeUvYGV1Z6STsN%2FWQT" +
"EnZQZF5PcKWB9IcLfXCwd1XkjYmWAfk65UxxSoeXhwh0RgYiPgP49gmV0R1VfbXspZKXlb" +
"H%2FgQfDE25I9Rsz9OVfKwZdsW%2Bw3I7soD%2Bc64XyqckquHLYIBT64A2zl7PLDvqdKx" +
"oQBCIB4Py2A6FIiGKlfEb4q3P0Jy89VMaQGPTJTbtFzZCNgCvQEBkxHZpFJO%2Bnh4FSEI" +
"48OrYRmUo%2F4kD7HrY%2B1HkJGdQZcUitDX426XK%2FtmYeWGtijor61DZ3sUuq7H4mbn" +
"RiKVEz7G%2BMM1hBCY%2B2Q4Vu4ORZTCfg52p%2B%2FW5qsjcO0Sn4glXART5devKTjYIp" +
"3tMejSb8CtG5s46fijUMJaPBn5BLcM3shI80V4RLj9KkpMTpSkpG4l255sJJMvF8ELub4t" +
"mkQg5yKwdRrMSXu7NnPSJyMRhE8xNhyBK2cXITpajuCQEMm0zbZ9x3a6IE%2BKXy8s5XI1" +
"ZgeSrpUcbBFDWww62teTeDaiu0NFCmzFk9HPYD4SjKeWKHS3e0KjXg75ajn9yqOCLC3Bti" +
"82MtShZdBfcbd1ykUQ9jyCcK5TVvp0VEGkkQQlnlmicbtzBZncJQgODmBkMtnrj%2B64%2" +
"BDhV%2FOdKnKwQcEWsQ2G2CDjpOkIk1y0rfWaJJYWV6DUIydJcDLncF37%2BfjMf2ZHKKF" +
"VMbDijUXuDbuXh9g0xhozsaBQYqAjlpA%2F716Lvph%2F09GJoct2wauUKRiwRq9740iGr" +
"QBAaFkorlauQniIil85SXGrmoe2XBbjcwsPpo3xyw3kgZI0QIpGIJgje6uoMkgdJZCtlWq" +
"lUSvv5%2BTE%2BPj4Qi8UMK%2FX29tYKhULJTI5%2FAJbzGAID9%2BU1AAAAAElFTkSuQm" +
"CC";

var SettingsImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHja" +
"nVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjo" +
"CMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89%2BbN%2FrXXPues852zzwfACAyWSDNRNYAM" +
"qUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz%2FSMBAPh%2BPDwrIsAHvgABeNMLCADATZvAMB" +
"yH%2Fw%2FqQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAn" +
"f%2BbTAICd%2BJl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2" +
"ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5" +
"RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA%2Fg88wAAKCRFRHgg%2FP9eM4Ors" +
"7ONo62Dl8t6r8G%2FyJiYuP%2B5c%2BrcEAAAOF0ftH%2BLC%2BzGoA7BoBt%2FqIl7gRo" +
"XgugdfeLZrIPQLUAoOnaV%2FNw%2BH48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl%2FAV%2F" +
"1s%2BX48%2FPf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H%2FLcL%2F%2Fwd0yLESW" +
"K5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s%2BwM%2B3zUAsGo%2BAXuRLahdYwP2Sy" +
"cQWHTA4vcAAPK7b8HUKAgDgGiD4c93%2F%2B8%2F%2FUegJQCAZkmScQAAXkQkLlTKsz%2" +
"FHCAAARKCBKrBBG%2FTBGCzABhzBBdzBC%2FxgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAd" +
"KmAv1EAdNMBRaIaTcA4uwlW4Dj1wD%2FphCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4" +
"IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDL" +
"VDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8%2BQ8cwwOgYBzPEbDAuxsNCsT" +
"gsCZNjy7EirAyrxhqwVqwDu4n1Y8%2BxdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0Ed" +
"oJNwkDhFHCJyKTqEu0JroR%2BcQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpF" +
"TSEtJG0m5SI%2BksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG%2BQh8lsKnWJAcaT4" +
"U%2BIoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6Wt" +
"opXTGmgXaPdpr%2Bh0uhHdlR5Ol9BX0svpR%2BiX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK" +
"%2BYTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI" +
"%2BpXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q%2FpH5Z%2FYkGWcNMw09DpF" +
"GgsV%2FjvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY%2FR27iz2qqaE5QzNKM1ezUvOU" +
"Zj8H45hx%2BJx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aed" +
"pr1Fu1n7gQ5Bx0onXCdHZ4%2FOBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up%2B6Ynr" +
"5egJ5Mb6feeb3n%2Bhx9L%2F1U%2FW36p%2FVHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFD" +
"XcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TD" +
"tMx83MzaLN1pk1mz0x1zLnm%2Beb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVV" +
"pds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw%2B" +
"6TvZN9un2N%2FT0HDYfZDqsdWh1%2Bc7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPL" +
"KcRpnVOb00dnF2e5c4PziIuJS4LLLpc%2BLpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26%2" +
"FuNu5p7ofcn8w0nymeWTNz0MPIQ%2BBR5dE%2FC5%2BVMGvfrH5PQ0%2BBZ7XnIy9jL5FX" +
"rdewt6V3qvdh7xc%2B9j5yn%2BM%2B4zw33jLeWV%2FMN8C3yLfLT8Nvnl%2BF30N%2FI%" +
"2F9k%2F3r%2F0QCngCUBZwOJgUGBWwL7%2BHp8Ib%2BOPzrbZfay2e1BjKC5QRVBj4Ktgu" +
"XBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz" +
"30T6RJZE3ptnMU85ry1KNSo%2Bqi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt%2" +
"F87fOH4p3iC%2BN7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCn" +
"nCHcJnIi%2FRNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG" +
"0yPTq9MYOSkZBxQqohTZO2Z%2Bpn5mZ2y6xlhbL%2BxW6Lty8elQfJa7OQrAVZLQq2Qqbo" +
"VFoo1yoHsmdlV2a%2FzYnKOZarnivN7cyzytuQN5zvn%2F%2FtEsIS4ZK2pYZLVy0dWOa9" +
"rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfe" +
"vc1%2B1dT1gvWd%2B1YfqGnRs%2BFYmKrhTbF5cVf9go3HjlG4dvyr%2BZ3JS0qavEuWTP" +
"ZtJm6ebeLZ5bDpaql%2BaXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO%2FPLi8ZafJzs" +
"07P1SkVPRU%2BlQ27tLdtWHX%2BG7R7ht7vPY07NXbW7z3%2FT7JvttVAVVN1WbVZftJ%2" +
"B7P3P66Jqun4lvttXa1ObXHtxwPSA%2F0HIw6217nU1R3SPVRSj9Yr60cOxx%2B%2B%2Fp" +
"3vdy0NNg1VjZzG4iNwRHnk6fcJ3%2FceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYl" +
"u6T8w%2B0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32" +
"oPb%2B%2B6EHTh0kX%2Fi%2Bc7vDvOXPK4dPKy2%2BUTV7hXmq86X23qdOo8%2FpPTT8e7" +
"nLuarrlca7nuer21e2b36RueN87d9L158Rb%2F1tWeOT3dvfN6b%2FfF9%2FXfFt1%2Bci" +
"f9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v%2B3Njv3H9qwHeg89HcR%2FcGhYPP%2FpH1jw" +
"9DBY%2BZj8uGDYbrnjg%2BOTniP3L96fynQ89kzyaeF%2F6i%2FsuuFxYvfvjV69fO0ZjR" +
"oZfyl5O%2FbXyl%2FerA6xmv28bCxh6%2ByXgzMV70VvvtwXfcdx3vo98PT%2BR8IH8o%2" +
"F2j5sfVT0Kf7kxmTk%2F8EA5jz%2FGMzLdsAAAAEZ0FNQQAAsY58%2B1GTAAAAIGNIUk0A" +
"AHolAACAgwAA%2Bf8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAOLSURBVHjaXNLfT9V1" +
"HMfx5%2Bd7vpxzBAzkN8KBg7Kh4rTmrI4VBgd0Q0BWsYwuakuiUVNTGTWiG7YaBkUtL3Lz" +
"otY0OGHhjwIvAJfB6YywulAUUQEPHNA4gZzD4df33QXrpuc%2F8Npje6lzbW2dusnkLCkt" +
"tQLLAEpTfP5pM4KBGMKRo0fNNdXVrsJ9RSWhhRCBYOCje8N3at89dgyVbrN9fH3wxnttra" +
"2%2Bp57cmRycn8cwDK7%2B6kYAMQzMVktvVVWVw%2Bv18k7V2%2BefeHxb2VwwmPhJY9MD" +
"LTYq%2Bv1NmZk109P%2BJETOokBD8axjB3lOJ2davsstLy93hEIhXC4XubnO7Ozs7QsbNm" +
"a2lZWVvaFpmpAUG3PiVOOJFFdr6wENvgRBBA7VNREfF5cOMDs7i8Oxi%2FVpaRvHJyepqK" +
"jYOTMzs6ibxEAwUMoYbzl9yipihIqLS%2Fyaok5pisGbt4amp6fp6%2BvD6XSSmppCRMRu" +
"BgYGQh6P5zdt6p85fP5ZRDR0PWwBUOfbf%2FggKxA63Cl%2BKrflX7WnpwPQ1dWFxWLB6%" +
"2FVSX1%2F%2Fld%2Fv%2F0sXDJQICgCD1ZQK9F%2BTRTGaszztGIuf0ebquTDmvX%2Bvo6" +
"Mj2e12Xx4aGvoGQA%2B9eAl7kpn5pQV0XfGTBiMBnZdOFpLa3Miix8NpW5q721YbdCR7D%" +
"2FG%2FVOLha2QkWgguLWC1apiAP6%2BbpKz1aY4nr8P%2BxUm6D77FMrTH%2BcZLw1PWYo" +
"1fw9Y%2FphARdAWIgFo1MD4qsj49hq8d3UQVhIf2Hcy35jQ2MPf9uf0Xe3slKcxkymTFGM" +
"61AaCtrMjqYQQmRkTCYuMZdvvYsjeDs9%2B6Bh4Kx3uqa4hvaqQ4N4%2FbgeWV%2B1r4jv" +
"8ImlKr65MjIuGJSQz3TrLpeRt3LlxusWeYn7FNTjTNiPHhz8%2FlENfQwKslpQyOBfq9WG" +
"wAmiEwMWrMrduQws3uMTbn27jb0d0TZVcHVlUKu2%2Byfs4w%2BH1PATF1tbxcvJ%2BRG3" +
"OjAPqjEa4k7EqMGOwaZcseO7c7%2B%2Fqj7SrXf%2BYVHmiw26wQzcoVn0%2BNKCXhhYVs" +
"vniJvGCAW3k5P6InFLyeUHFXtjc9FPPWur%2FXAhEmMOugmQGzAqsVrJEQFsWRqKiZsaIi" +
"maislNciI%2BNVRNZeloa638QcbVOhqTbgMd3EmiXF0rLGsoECzWIC3cyK6VHY8uxEjFIv" +
"TIv8smgYnn8HAOD1i6l3b2VwAAAAAElFTkSuQmCC";

var QueueNoteEditImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAANCAYAAACZ3F9%2FAA" +
"AABGdBTUEAALGPC%2FxhBQAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMU7nOP" +
"kAAAD2SURBVChTjZK7CsJAEEUnlYgviBJIIKWNqaxEJHbWBn9GsAlB0ir4Jels7LSxtIh%" +
"2FYqGF150Nq4l56MKwyWTO3juT1Ugsz%2FPAe9EKw5Acx9H425p0WbeIj0kpg8AzF5zniO" +
"MYlm4B%2Bx3gL6EOqAQZajF0A0ybgOEYG6LEoVJUCun3qSaKH0t02h1cL2cYXQNRFGXBb7" +
"sSQgAcCLcVYdCmD1SmOBN2FASf0KybWahoOEUQ239bVONP9%2BQ2uKcA95PYhVLfrslpV4" +
"JcsJ33krGLnhT0E%2BRTbV2XsDsZSZW0m1KrRZdA5Uqtpv9h2XNOkW8HJ%2F8JNdAXexeD" +
"QlcKybYAAAAASUVORK5CYII%3D";

var MemoryImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuNt" +
"CDrVoAAAIsSURBVDhPjZPdS1NhHMf1f%2BhySHQTXRS76Caz2EVdFEG7CIKKGIhUK3OlS4" +
"01jy%2FppguxInKFx8BGibHaCDWXB4IGwdygmeVLHZxhGdh6x17s0%2B%2BMBNGz6IHnHM" +
"7L9%2FP7%2Fr7P8xQW%2FB0ul4rFsmbpcdV9YmK2IBg8Upj3h0AgysrxW14syuXnIlS4Qz" +
"jLVf4bsFz8%2FRdUNdyjrn%2BGsuNd5pDlDlaKv%2F4Af2uUFpnN%2FignK0ycLAFUVWUk" +
"mcrZPudVOHTYwacFmP8GHVdVZj5Cky%2B62oUBMCrX1SnEhjWGHmocFLEv0M7loMrbL3Cq" +
"RuHle2hsyQMwAvMK4IGIB2IaV66pRAY1KmsVMlK5%2FIzC2Duob84DMGx7xHb%2FkEZmLs" +
"vGTVaKt9mIP9WZkspOt0JyVlyeNwG0SQsLkvbYpJ4TGz2Pv84yOp1lah6eSeXoE514RrJp" +
"MgG0tkUx0q71KJRstxGLp%2Bi9r9ETkTxGdDYX29hf6iL2Cs42mgD8AkiP6%2BzaYyf2OM" +
"W%2BAw5uilgNaxytUrh0W2PrTjvXh3VqGkwAPlnj1HOdMqeLtNjesdtO912Nzj6N0tMKPY" +
"909jpc%2BPpSuOtNAMYmefMZtpTYsBStpaM7TPCOxkWpfCGksW6DlaL1VrqSUKmYAIwdNv" +
"0BJiWw0TlISNoD6SzhVJbIC7iVJifuTOQBuKtD3OhN4G8fRJF19spSeSRtI7Bq6dmwbVQ2" +
"5rET%2FzhUeU9bng9%2FAG8cBABPvbmCAAAAAElFTkSuQmCC";

var Copy2ClipboardSrc =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAfNJREFUeNp0UkFrE0EU" +
"fjszu0maLRZUUI8pHlopFD14qHjrodCLpAdF8ezVsyClFESP%2FoNKwEs8KEiR4k2RgoQe" +
"iihiRaUY0UQTTXY3uzPje9PsZpNsPng7u%2Fve%2B973zYy1cO%2FFb68nZ0KlQWsNBMsC" +
"4PiwOXuXE%2BycYMz8y4Kg5o93VzKTrz815m9Wdh%2B7OXFNcCTJqGE0mfDsQ93E0%2Fd1" +
"8%2B3cfgJLs8dh68bFq34oK1IpkFhLoVCpjgli2UVbgItBK%2BHW0qwhuYQk3VBeb%2FuR" +
"%2FuOFuu2HutuT1UgqoFYRSyk6RxKJ7mvLg4driyYInUBC8%2F5q2lo5tsbin3mcTFHAaH" +
"o9qH1vQa3eMjmS%2F%2FLzryFrj9BaEKlqQjAlGBQwaJ2yObgORzscBgqHrRFJKFU5seA6" +
"trEQHxf506nddtHiYdsfshZhUULw5lvDnH0aEgvK82f6CmxooLUfnQBo0vlTx8wEYfWbWH" +
"9NK2ApwoLNQClujjGtTNjsqOjK3GmYBIY3cRr3gGlrnCAvrL9nN7ano1SC7kYkNfiRNDJL" +
"J4rw6ksTglCaS5S2JvbvrFzGlY9OLa0%2Ff3u4uQo7Bz%2BBLivu%2BEBRYtcCceHBzt5o" +
"M035F0Tmfbl0cqI1si%2ByEsSc4%2BPWkjxJx2ayb8FkLGZZG4H8L8AAgbbegfEkzHUAAA" +
"AASUVORK5CYII%3D";

var CacheHistoryImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
"AABGdBTUEAALGPC%2FxhBQAAAAlwSFlzAAALEgAACxIB0t1%2B%2FAAAAAd0SU1FB9wCGw" +
"8sDRgspSQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACXSURBVDhP" +
"rZBRDoMwDEN7Ev64F7fbBydil8l4QpYSL6AxUekJnNpu1RERp7zeEZBnTjsUy7LFNK37b7" +
"8P7RAIz%2FPBVUk7JDz2rcxZydcgn%2Bx0JUUABXo8Aq7dXwQ8UoAR8tWF%2B4sACrRyya" +
"030MoFt24gcz5dJe4vAijwoMI%2FF1yVuL8IUNhL%2FroBAf%2B6vwggrNM6qj%2FGBx1O" +
"sEsWE1fAAAAAAElFTkSuQmCC";

var ChallengeCheckerImg =
"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
"BmJLR0QA%2FwD%2FAP%2BgvaeTAAAACXBIWXMAAABIAAAASABGyWs%2BAAAACXZwQWcAAA" +
"AYAAAAGAB4TKWmAAAE4klEQVRIx42UXYhdVxmGn3ftfTLnzDmTmcmcGSbJ5KekMU3Qarzw" +
"Rm0KEtraijZS0P5AUaHWm2ilKHihElAvatvUBkXFqwqKWqSVaoKh9QdsBBub1NJY8jcTU9" +
"M587%2FPmfO7Xy%2F2nGlmHBPXzYIN61v7ed7vW2KtdfCsGLxhB%2FA%2BYA9iR5xje24d" +
"o3HEoMy6apWpkdrpJx%2B%2BfPt0f%2FutrcBWYAuQAF8%2F%2BFPOAMSraz94%2Fz49X%" +
"2BIXU%2BjAujIqDtnFXqk3B4UICpFdiCS1vf5vr77nqVeS%2B7iz9bRLA0Uls1VqC3VL7A" +
"beC6Bu4SfuZUMUeNpwmwmDLw18Uc%2BNPsboHlzKSzf0widHYShnL6ZS0%2FZf%2Fi0984" +
"cmFx74qsv5BbVbHZ498rwnL1XqFybpe%2FwonWWCIH5s84ntu8d41%2Ft3cM%2BGf7r9%2" +
"BklentnLgb32u%2FvghUmYaWcUxQhuHbWPlXIcfbXBgZHfWHGBbTvLvD1RWTYSA3xuHwHY" +
"P7Zzo%2B767O24nbg9d1Z3l3%2FG%2Bbf2Opb0owkoxFAIdj5I64J9ckFqBDh%2B5aMeDp" +
"E%2BPPJr3Jiz9I6ZLkEA4m03bQNS6pd%2BT9Io0NNKODsNf5rJ%2FOcDuA3zVZhsQL0OyS" +
"JM5O7gh%2Bf28cGRo4jFFZmuCDlEkdPGLGkn5ZHXTnjT6Aj1Opw6ZTcbkDOMFmCsaG8uwe" +
"ai%2FakPwK5%2BceTPMiEGBWy85gUoCEWkyjvXU9bXbovYPWxv6ZM2F6GvB4JsIYFtJAy1" +
"lg1IyiGFFYrCyiYNoAiUIwjyMezfCnsGOmw79DC5ZhUE03Xz5pQ5MZHywpkOzbR7PM7O%2" +
"Fy9FSEYxCjGAwQwe%2BhJy6tBpcb5e4AcvthnslYd6Yagob%2BiFXFbTUu7aiqwghwiUy5" +
"AR6cYxF3%2FypGZefI0b%2BwPf%2FbgsLSmyZEOtmSki5IBozS7qMlqrCBY%2F%2Fwitu%" +
"2B52aNUhMhKAl%2F7QTg0hdAlidP2Q42UCJMLclJXv0fp79nP%2B53%2Fk5Zn1rlRRpWpX" +
"EqvRhm%2FeERuUEawKefUFoKhLAED%2FFz5NdGmcxsGv0CgNkK%2Bm7N0gyqXAUDEbvlqj" +
"G%2BH1QkYsESx%2FqX7rexSfOETz3gfZGovt5UAIAoxT0Umz3sgMZyGv6ssVBCZEVsh1M3" +
"Dnxl0k33%2FG%2BcPfcbg84cUWTMzYr0yYY290%2FOzfO14SYim2iK6RAd0MYi%2FhUDj8" +
"bavQo%2Fj47zj%2BsUd57kTbQyVpuITLRWljf5Y6Quj%2FyiBGVynqbBqj79GHWHjpJB%2" +
"FameOWXUbKFKUpTCXw14sZRLj%2BoAV3Q6638HTVVO%2B8n3ObbvaVeCeV0x0mqzCZ2JUk" +
"pdFMPLBumnLvNA%2FsecNRFABdS5FEiAlRcF90Wd%2F4lSgXplwuTKl85ZcMF6a4aXDa5U" +
"0zGirMOh83s5kjLGHlmLw87eyFWmuSnXVRCBFHbv3yUkdoeZcCJiwpynRCoLHYZH424c1T" +
"Zzn3%2BgWAU48fpfNfF1w8%2FQ%2Ff%2FJFbUNQLaSd7LC0Wk4YX5moszFZJ5qoszFa9MJ" +
"ss781Ga6nrADgDfOaqxs%2FW4fsYl9iyfrhMqb%2FPtKuqzi%2BSzNfcaXe0DGlqEv%2By" +
"GZe4CIzbjBvGF5tcnEo4%2F9hvSddS9BDw1PxkpW%2Fu7crE1YclxtOU8Zka47M1KoePvV" +
"Pgeus%2FlYo88AJk3s0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTAtMDItMTFUMTU6MzU6MD" +
"QtMDY6MDDflWQeAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDA4LTEyLTE0VDExOjI1OjEwLTA2" +
"OjAwqhRn%2FQAAAABJRU5ErkJggg%3D%3D";

// Text checking function.
String.prototype.startsWith = function(str) {
	return (this.indexOf(str) == 0);
};

String.prototype.contains = function(str) {
	return (this.indexOf(str) >= 0);
};

// this was throwing an error and is unused, so I'm depricating it 2024-10-31
//String.prototype.endsWith = function(str) {
//	return (this.lastIndexOf(str) == (this.length() - str.length()));
//};

// Get currently signed-on geocaching.com profile.
var SignedInAs = getSignedInAs();
if (SignedInAs) {
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
}

// Get Action, if passed.
// Action codes: hold, remhold, lock, unlock, watch, remwatch, publish
var urlAction = UrlParm('action', true);
var urlAutoClose = UrlParm('autoclose', false);
var urlWatchAndHold = UrlParm('holdandwatch', false);

// Remove unnecessary white space.
var PathSearch = "//a[contains(@href, 'queue.aspx')]";
var wsLinks = document.evaluate(PathSearch, document, null,
XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
if(wsLinks.snapshotLength > 0) {
	var wsLink = wsLinks.snapshotItem(0);
	wsLink.parentNode.height = 28;
}

debugLog('----- SignedInAs=', SignedInAs);
debugLog('----- mainPageUrl=', mainPageUrl);
if (mainPageUrl.indexOf('/admin/review.aspx') < 0) {
	debugLog('---- not on review page, exiting');
	var itemCount = addGcConflictCopyToLitmusPage(0);
	itemCount = addGcConflictCopyToLitmusSectionOfReviewPage(itemCount);
	return;
} else {
	debugLog('----- on review page');
	var itemCount = addGcConflictCopyToLitmusSectionOfReviewPage(0);
}

// Get cache data element.
var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
if (!CacheData) {
	CacheData = document.getElementsByClassName('CacheData')[0];
}

// - - - - - - - - - Get Customizable Settings - - - - - - - - - //

// Get values for directions from home.
GetHomeCoordsValues();

// Get Isolate Title setting.
var CustomIsolateTitle = GM_getValue('CustomIsolateTitle_' + SignedInAs, 'On');

// Get Isolate Owner setting.
var CustomIsolateOwner = GM_getValue('CustomIsolateOwner_' + SignedInAs, 'On');

// Get Litmus Test Preview setting.
var CustomLitmusAutomatic = GM_getValue('CustomLitmusAutomatic_' + SignedInAs, 'On');

// Get Litmus Test Browser Title setting.
var CustomLitmusTitle = GM_getValue('CustomizeLitmusTitle_' + SignedInAs, 'On');

// Get Litmus Test Preview Errors Only setting.
var CustomLitmusErrorsOnly = GM_getValue('CustomLitmusErrorsOnly_' + SignedInAs, 'true');

// Get DDMMSS Coordinate Usage Alert setting.
var DDMMSSAlert = GM_getValue('DDMMSSAlert_' + SignedInAs, 'On');

// Hide Zone Catcher & Litmus bars if no error setting.
var HideZCLitBars = GM_getValue('HideZCLitBars_' + SignedInAs, 'On');

// Auto-open Litmus Results on a 'red' result (ie. error)
var openLitmusOnRed = GM_getValue('openLitmusOnRed_' + SignedInAs, 'Off');

// Get Terrain Alert setting.
var CustomTerrainAlert = GM_getValue('CustomTerrainAlert_' + SignedInAs, 'On');

// Get Distance Alert setting.
var CustomDistAlert = GM_getValue('CustomDistAlert_' + SignedInAs, '100') - 0;

// Get Hides Alert setting.
var CustomHidesAlert = GM_getValue('CustomHidesAlert_' + SignedInAs, '3') - 0;

// Get Finds Alert setting.
var CustomFindsAlert = GM_getValue('CustomFindsAlert_' + SignedInAs, '5') - 0;

// Get Coordinate Alert setting.
var CustomCoordinateAlert = GM_getValue('CustomCoordinateAlert_' + SignedInAs, 'On');

// Get Custom Map Zoom setting.
var CustomMapZoom = parseInt(GM_getValue('CustomMapZoom_' + SignedInAs, '0'));

// Get Custom Map Type setting.
var CustomMapType = parseInt(GM_getValue('CustomMapType_' + SignedInAs, '0'));

// Get screen adjustment value.
var ScreenAdjustment = GM_getValue('gmrpScreenAdjustment_' + SignedInAs, '');
if (ScreenAdjustment == '') {
	ScreenAdjustment = 0;
	GM_setValue('gmrpScreenAdjustment_' + SignedInAs, 0);
} else {
	ScreenAdjustment = ScreenAdjustment - 0;
}

// - - - - - - - - - End Customizable Settings - - - - - - - - - //

// Unique ID for cross-script local memory storage.
const memStorId = '955f75d3-1f69-46bc-89db-28e6242f76e7';

// Unique ID to check for cache/user note changes.
const noteChangedGuid = '7086e6f4-d743-4af7-a448-5e6b9a7d1346';

// Unique ID for positioning open browser pages in review mode.
const REV_PAGE_POS_KEY = 'a3148ed3-e28f-4105-a8ab-dcbacbd3b457';

// Constants for cache types.
const TYPE_TRADITIONAL = 2;
const TYPE_MULTI_CACHE = 3;
const TYPE_VIRTUAL     = 4;
const TYPE_LETTERBOX   = 5;
const TYPE_EVENT       = 6;
const TYPE_UNKNOWN     = 8;
const TYPE_WEBCAME     = 11;
const TYPE_CITO        = 13;
const TYPE_EARTHCACHE  = 137;
const TYPE_MEGA_EVENT  = 453;
const TYPE_WHERIGO     = 1858;
const TYPE_COMMUNITY_EVENT = 3653;
const TYPE_BLOCK_PARTY = 4738;
const TYPE_HQ_CACHE = 3773;
const TYPE_HQ_CELEBRATION = 3774;

// Constants for coordinate types.
const CT_UNKNOWN  = '0';
const CT_PHYSICAL = '1';
const CT_VIRTUAL  = '2';

// JSON key for Review Queue Notes
// const JSON_RQN_KEY = '517ba8f0-86f7-46f7-8f0d-d476265f420c';

// Rewrite CSS for publish button to take up less space
GM_addStyle("#ctl00_ContentBody_publishButton { min-width: unset; }");

// Rewrite CSS for page to take up less space.
GM_addStyle("body { line-height: normal !important; }");
GM_addStyle("#table { line-height: 1.75em !important; }");
GM_addStyle("th, td { padding-top: 0.2em !important; padding-right: 0.5em !important; padding-bottom: 0.2em !important; padding-left: 0.5em !important; }   " );


// Add class style for quick buttons. Compatable for both FF 3 & 4.
GM_addStyle(".quickbutton {border:solid 1px rgb(68,142,53); margin-left:12px; " +
			"border-radius:5px; -moz-border-radius:5px; background-color:rgb(239,239,239); " +
			"padding:1px; }");

// Add class style for no-final-waypoint warning.
GM_addStyle(".nfw-warn { color:red; font-size:larger; " +
			"font-weight: bold; border: 2px solid red; " +
			"margin-top: 10px; padding: 6px 18px 6px 18px; " +
			"border-radius: 15px; background-color: rgb(244, 244, 244); display: table }");

// Fix strike-out color. Set to true red.
GM_addStyle(".Warning, .OldWarning, .Validation, .red {color: #ff0000 !important; } " );

// Add border and background color to quote block.
GM_addStyle("blockquote { background-color:#FFFFDB; border:1px solid #4D6180; " +
			"margin:1.2em; padding:0.8em; !important; }");
// To have "old school" style quoting, comment out the above line, and uncomment the line below.
// GM_addStyle("blockquote { background-color:transparent; border-bottom:2px solid #4D6180; border-top:2px solid #4D6180; margin-left:4em; margin-right:4em; padding:0.8em; !important; }");

// Style for cache notes.
GM_addStyle('div.cachenote { border:solid 1px rgb(68, 142, 53); padding:9px; margin-top:6px; ' +
			'font-size:x-small; border-radius:10px; background-color:rgb(255, 255, 160); ');

// Style for dropdown litmus viewer.
GM_addStyle('.gm-litmus-viewer { min-height:0px; height:auto !important; height:0px; } ');

// Style for icon buttons in cache details box.
GM_addStyle(".gm-icon-buttons {margin-right:10px; vertical-align:text-bottom; cursor:pointer;}");

// Style for Challenge Checker image.
GM_addStyle(".CCerImg {margin-right:2px; vertical-align:text-bottom;}");

// Style for Challenge Checker text.
GM_addStyle(".CCerText {margin-left:0px; padding-top:3px; " +
			"font-weight:bold; text-decoration: underline; color:red}");

// Style for trashcan icon buttons in navigation box.
GM_addStyle(".gm-icon-trashcan {margin-left:4px; vertical-align:text-bottom; cursor:pointer;}");

// Style for litmus test auto-log icon.
GM_addStyle(".gm-litmus-log-icon {margin-left:8px; vertical-align:text-bottom; cursor:pointer;}");

// Create class for RQ Notes.
GM_addStyle(".rqn_class { margin-left: 8px; background-color:yellow; }");


// Add event listener to warn of unsaved data before leaving page.
var revNoteHash = '';
if (!urlAction) {
	window.addEventListener("beforeunload", function (e) {
		if (revNoteHash) {
			if (fNoteTextChanged()) {
				(e || window.event).returnValue = ' ';
			}
		}
	});
}

// Add event listener to review note save button, to update note hash value.
var saveButton = document.getElementsByClassName("BtnSave");
var firstSaveButton = saveButton[0]; //save Button of private note
var secondSaveButton = saveButton[1]; //save Button of shared note
firstSaveButton.addEventListener('click', fSaveRevNoteButtonClicked, false);
secondSaveButton.addEventListener('click', fSaveRevNoteButtonClicked, false);

var priv = document.getElementById("private_notes_widget");
var privHeading = priv.getElementsByClassName('WidgetHeader')[0];

// Add timestamp/signature icon to private note.
var imgTimestamp = document.createElement('img');
imgTimestamp.id = 'imgTimestamp';
imgTimestamp.src = TimeclockSrc;
imgTimestamp.classList.add('reviewernotesClass');
imgTimestamp.title = 'Insert Timestamp & Signature';
insertAheadOf(imgTimestamp, firstSaveButton);
imgTimestamp.addEventListener('click', fInsertTimestamp, false);

// Add timestamp/signature icon to public note.
var imgPubTimestamp = document.createElement('img');
imgPubTimestamp.id = 'imgPubTimestamp';
imgPubTimestamp.src = TimeclockSrc;
imgPubTimestamp.classList.add('reviewerpublicnotesClass');
imgPubTimestamp.title = 'Insert Timestamp & Signature';
insertAheadOf(imgPubTimestamp, secondSaveButton);
imgPubTimestamp.addEventListener('click', fInsertTimestampPub, false);

// Remove extra line before owner note link.
e_UserInfo_lnkReviewerLogs.parentNode.style.margin = '0px';

// If Premium Member cache, add PM icon under the cache type icon.
if (eval(CacheData.getAttribute('data-ispremiummemberonly'))) {
	var e_imgType = document.getElementById('ctl00_ContentBody_CacheDetails_imgType');
	e_imgType.parentNode.appendChild(document.createElement('br'));
	var pmImg = document.createElement('img');
	pmImg.src = '/images/small_profile.gif';
	e_imgType.parentNode.appendChild(pmImg);
}

// Get the GUID for the cache, if needed.
var CacheGuid = CacheData.getAttribute('data-cacheguid');

// Get cache waypoint id.
var WptID = CacheData.getAttribute('data-gccode');

// Get decimal-degree coordinate values.
var CoordDdLat = CacheData.getAttribute('data-latitude') - 0;
var CoordDdLon = CacheData.getAttribute('data-longitude') - 0;
var coordsDecMinutes = formatCoordsDecMinutes(CoordDdLat, 'N', 'S') + '  ' + formatCoordsDecMinutes(CoordDdLon, 'E', 'W');
debugLog("coordsDecMinutes", coordsDecMinutes);

// Get cache type.
var cd_cachetypeid = CacheData.getAttribute('data-cachetypeid');

// Get difficulty rating.
var cd_difficulty = CacheData.getAttribute('data-difficulty') - 0;

// Get Cache Title.
var CacheName = e_CacheDetails_Name.firstChild;
while (CacheName.nodeName != '#text') {
	CacheName = CacheName.firstChild;
}
CacheName = CacheName.data;
var CacheName = CacheData.getAttribute('data-cachename');

// Get owner name & guid.
var OwnerName = CacheData.getAttribute('data-ownerusername');
var OwnerGuid = CacheData.getAttribute('data-ownerguid');

// Get coordinates for this cache.
var e_CacheDetails_Coords = document.getElementById("ctl00_ContentBody_CacheDetails_Coords");
var txCoordsNode = e_CacheDetails_Coords.firstChild;
while (txCoordsNode.nodeName != '#text') {
	txCoordsNode = txCoordsNode.firstChild;
}
txCoords = txCoordsNode.data;

// Get type of posted coordinates.
var postedCoordType = CacheData.getAttribute('data-postedcoordinatetype');


// Show warning if coordinate type has not been set.
if ((cd_cachetypeid == TYPE_MULTI_CACHE || cd_cachetypeid == TYPE_LETTERBOX || cd_cachetypeid == TYPE_UNKNOWN) &&
	(postedCoordType == CT_UNKNOWN)) {
	fShowAlert('Posted Coordinates Type Not Set');
}

// If it appears that DD MM SS coordinate data was entered.
if (DDMMSSAlert == 'On') {
	if ((cd_cachetypeid == TYPE_TRADITIONAL || cd_cachetypeid == TYPE_MULTI_CACHE || cd_cachetypeid == TYPE_LETTERBOX ||
		 cd_cachetypeid == TYPE_UNKNOWN) && postedCoordType == CT_PHYSICAL) {
		var dmsCheckerLat = Math.abs(CoordDdLat * 3600);
		dmsCheckerLat -= parseInt(dmsCheckerLat);
		dmsCheckerLat = roundNumber(dmsCheckerLat, 2);
		var dmsCheckerLon = Math.abs(CoordDdLon * 3600);
		dmsCheckerLon -= parseInt(dmsCheckerLon);
		dmsCheckerLon = roundNumber(dmsCheckerLon, 2);
		if ((dmsCheckerLat <= .02 || dmsCheckerLat >= .98)  &&
			(dmsCheckerLon <= .02 || dmsCheckerLon >= .98)) {
			fShowAlert('Possible Use of DD MM SS Coordinates');
		}
	}
}

// Get rid of degree symbol.
var corDecMinX = txCoords.replace(/\xB0/g,'');

// Remove space after hemisphere code.
txCoords = txCoords.replace(/N /g, 'N');
txCoords = txCoords.replace(/S /g, 'S');
txCoords = txCoords.replace(/E /g, 'E');
txCoords = txCoords.replace(/W /g, 'W');
txCoordsNode.data = txCoords;

// Remove extra line above cache title.
e_CacheDetails_Name.parentNode.style.marginTop = '0px';

// Remove extra line above profile name.
var e_lbUsername = document.getElementById("ctl00_ContentBody_UserInfo_lbUsername");
e_lbUsername.parentNode.parentNode.style.marginTop = '0px';

// Code for cache type.
var CacheTypeCode = CacheData.getAttribute('data-cachetypeid');
var CacheTypeName = fCacheCode2Name(CacheTypeCode);
var CacheType1 = CacheTypeName.substring(0,1);
if (CacheTypeName.toUpperCase() == 'EARTHCACHE') {
	CacheType1 = 'EC';
} else if (CacheTypeName.toUpperCase() == 'MEGA-EVENT CACHE') {
	CacheType1 = 'E';
}

// Code for cache size.
var CacheSize1 = e_CacheDetails_Container.innerHTML;
CacheSize1 = CacheSize1.substr(CacheSize1.indexOf('(') + 1, 1);

// If no size chosen, highlight.
if (e_CacheDetails_Container.firstChild.data.trim() == '(Not chosen)' ||
	e_CacheDetails_Container.firstChild.data.trim() == '(Other)') {
	if ((CacheType1 != 'E') && (CacheType1 != 'C')) {
		e_CacheDetails_Container.style.backgroundColor = 'rgb(255, 191, 194)';
	}
}

// Remove background image.
document.body.background = '';

// Change links in navigation boxes to open in tabs.
var navDivs = document.evaluate("//div[@class = 'WidgetBody']",
document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
// Loop through navigation boxes.
var ni = navDivs.snapshotLength;
for (var i = 0; i < ni; i++) {
	var navDiv = navDivs.snapshotItem(i);
	var navLinks = document.evaluate(".//a[@href]", navDiv, null,
	XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	// Loop through list.
	var nj = navLinks.snapshotLength;
	for (var j = 0; j < nj; j++) {
		var navLink = navLinks.snapshotItem(j);
		navLink.target = "_blank";
	}
}

// Change owner link to open in new tab.
e_CacheDetails_Owner.target = '_blank';

// Change post log link to open in new tab.
e_lnkLog.target = '_blank';

// Change review log link to open in new tab.
e_lnkReviewNote.target = '_blank';

// Change enable/disbale link to open in new tab.
if (e_lnkDisable) e_lnkDisable.target = '_blank';

// Change archive link to open in new tab.
e_lnkArchive.target = '_blank';

// Change bookmark link to open in new tab.
e_lnkBookmark.target = '_blank';

// Change view archived logs link to open in new tab.
e_lnkArchived.target = '_blank';

// Change view cache listing link to open in new tab.
e_lnkListing.target = '_blank';

// Get cache status.
var bPublished = false;
var bDisabled = false;
var bArchived = false;
var bHeld = false;
var bWatched = false;

if (e_CacheDetails_Status) {
	var sCacheStatus = e_CacheDetails_Status.firstChild.data;
	var bArchived = (sCacheStatus.contains('Archived'));
	var bDisabled = (sCacheStatus.contains('Inactive'));
	var bPublished = (!(sCacheStatus.contains('Not Published')));
} else {
	bPublished = true;
}
if (e_btnHold && e_btnHold.value.contains('Release')) { bHeld = true; }
if (e_btnWatch && e_btnWatch.classList.contains('selected')) { bWatched = true; }

// GeoNames Error messages.
var gnErr = new Array();
gnErr[10] = 'Authorization Exception';
gnErr[11] = 'Record Does Not Exist';
gnErr[12] = 'Other Error';
gnErr[13] = 'Database Timeout';
gnErr[14] = 'Invalid Parameter';
gnErr[15] = 'No Result Found';
gnErr[16] = 'Duplicate Exception';
gnErr[17] = 'Postal Code Not Found';
gnErr[18] = 'Daily Limit Of Credits Exceeded';
gnErr[19] = 'Hourly Limit Of Credits Exceeded';
gnErr[20] = 'Weekly Limit Of Credits Exceeded';

// If Puzzle or Multi, and no Final waypoint, display error.
var finalWpErr = false;
fVerifyFinalWaypoint();

// If a Related Website was entered, hightlight and change title to URL.
if (e_CacheDetails_Website.href) {
	e_CacheDetails_Website.title = e_CacheDetails_Website.href;
	e_CacheDetails_Website.style.backgroundColor = 'yellow';
	var rwsSpan = document.createElement('span');
	rwsSpan.appendChild(document.createTextNode(e_CacheDetails_Website.href));
	rwsSpan.style.fontSize = 'xx-small';
	rwsSpan.style.marginLeft = '5px';
	insertAfter(rwsSpan, e_CacheDetails_Website);
} else {
	e_CacheDetails_Website.style.textDecoration = '';
	e_CacheDetails_Website.firstChild.data = '(none)';
}

// If published, add Update Coordinates link next to Edit Listing.  Could send other template parameters
var lnkEditDD = e_lnkEdit.parentNode;
if (bPublished && (lnkEditDD.childElementCount < 3)) { // don't need to add link when it comes natively
	lnkEditDD.appendChild(document.createTextNode(' | '));
	var lnkUpdateCoords = document.createElement("A");
	lnkUpdateCoords.href = '../live/geocache/' +
						   WptID +
						   '/log?logType=47'; /*+ 
						   '&owner='   + encodeURIComponent(OwnerName) +
						   '&title='   + encodeURIComponent(CacheName.trim()) +
						   '&wptid='   + encodeURIComponent(WptID) +
						   '&coords='  + encodeURIComponent(txCoords); */
	lnkUpdateCoords.target = "_blank";
	lnkUpdateCoords.appendChild(document.createTextNode('Update Coordinates'));
	lnkEditDD.appendChild(lnkUpdateCoords);
}

// Use unique and representitive icons in the menu panel.

var imgHist = fReplaceNavIcon("ctl00_ContentBody_lnkCacheHistory", CacheHistoryImg);
imgHist.title = 'TARDIS, Type 40, Mark 1\n' +
				'Condition: Defective Chameleon Circuit\n' +
				'Current Status: Missing, Believed Stolen';

// Remove class from cache page link, to remove warning color.
document.getElementById("ctl00_ContentBody_lnkListing").setAttribute('class', '');

// Remove Upload Image link from Nav box, since it's not authorized.
var e_lnkUploadImgs = document.getElementById("ctl00_ContentBody_lnkUploadImgs");
if (e_lnkUploadImgs) {
	var upimgDD = e_lnkUploadImgs.parentNode;
	var upimgDT = upimgDD.previousSibling.previousSibling;
	removeNode(upimgDT);
	removeNode(upimgDD);
}

// Add text nodes for litmus warning & error totals.
if (LitmusLink) {
	var spanLitErrTot = document.createElement("SPAN");
	spanLitErrTot.id = 'spanLitErrTot';
	spanLitErrTot.style.marginLeft = '4px';
	spanLitErrTot.style.fontWeight = 'bold';
	spanLitErrTot.title = 'Number of Errors found.';
	spanLitErrTot.setAttribute('class', 'LitmusBodyError');
	var txtLitErrTot = document.createTextNode(' ');
	spanLitErrTot.appendChild(txtLitErrTot);
	insertAfter(spanLitErrTot, LitmusLink);

	var spanLitSolidus = document.createElement("SPAN");
	spanLitSolidus.id = 'spanLitSolidus';
	var txtLitSolidus = document.createTextNode(' ');
	spanLitSolidus.appendChild(txtLitSolidus);
	insertAfter(spanLitSolidus, document.getElementById("spanLitErrTot"));

	var spanLitWrnTot = document.createElement("SPAN");
	spanLitWrnTot.id = 'spanLitWrnTot';
	spanLitWrnTot.style.fontWeight = 'bold';
	spanLitWrnTot.title = 'Number of Warnings found.';
	spanLitWrnTot.setAttribute('class', 'LitmusBodyWarning');
	var txtLitWrnTot = document.createTextNode(' ');
	spanLitWrnTot.appendChild(txtLitWrnTot);
	insertAfter(spanLitWrnTot, document.getElementById("spanLitSolidus"));
}

// Get litmus error counts, and set icons.
var litCount = document.getElementById('ctl00_ContentBody_uxReviewerLitmusCount');
if (litCount) {
	var litErrCount = litCount.childNodes[0].firstChild.data - 0;
	var litWarnCount = litCount.childNodes[2].firstChild.data - 0;
	var litTotalCount = litErrCount + litWarnCount;
}
if (CustomLitmusTitle == 'On')   {
	ToolbarIcon1.href = LitmusIcon.src;
	ToolbarIcon.href = LitmusIcon.src;
}

// Add useful anchor links (Guidelines, Help Center, Wiki, map & logs) to the breadcrumb line.
var e_Breadcrumbs = document.getElementById("ctl00_Breadcrumbs");
if (e_Breadcrumbs) {
	var aGuidelinesSpan = document.createElement("span");
	aGuidelinesSpan.style.marginLeft = '20px';
	var aGuidelinesLink = document.createElement("a");
	aGuidelinesLink.href = "https://www.geocaching.com/play/guidelines";
	aGuidelinesLink.setAttribute('target', '_blank');
	aGuidelinesLink.appendChild(document.createTextNode("Guidelines"));
	aGuidelinesSpan.appendChild(aGuidelinesLink);
	e_Breadcrumbs.appendChild(aGuidelinesSpan);

	var aHelpCenterSpan = document.createElement("span");
	aHelpCenterSpan.style.marginLeft = '10px';
	var aHelpCenterLink = document.createElement("a");
	aHelpCenterLink.href = "https://www.geocaching.com/help/";
	aHelpCenterLink.setAttribute('target', '_blank');
	aHelpCenterLink.appendChild(document.createTextNode("Help Center"));
	aHelpCenterSpan.appendChild(aHelpCenterLink);
	e_Breadcrumbs.appendChild(aHelpCenterSpan);

	var aWikiSpan = document.createElement("span");
	aWikiSpan.style.marginLeft = '10px';
	var aWikiLink = document.createElement("a");
	aWikiLink.href = "https://wiki.groundspeak.com/display/VOL/Home";
	aWikiLink.setAttribute('target', '_blank');
	aWikiLink.appendChild(document.createTextNode("Wiki"));
	aWikiSpan.appendChild(aWikiLink);
	e_Breadcrumbs.appendChild(aWikiSpan);

	var aMapSpan = document.createElement("span");
	aMapSpan.style.marginLeft = '10px';
	var aMapLink = document.createElement("a");
	aMapLink.href = "#map_canvas";
	aMapLink.appendChild(document.createTextNode("Map"));
	aMapSpan.appendChild(aMapLink);
	e_Breadcrumbs.appendChild(aMapSpan);

	var aWptSpan = document.createElement("span");
	aWptSpan.style.marginLeft = '10px';
	var aWptLink = document.createElement("a");
	aWptLink.href = "#ctl00_ContentBody_Waypoints";
	aWptLink.appendChild(document.createTextNode("Waypoints"));
	aWptSpan.appendChild(aWptLink);
	e_Breadcrumbs.appendChild(aWptSpan);

	var aLogSpan = document.createElement("span");
	aLogSpan.style.marginLeft = '10px';
	var aLogLink = document.createElement("a");
	aLogLink.href = "#log_table";
	aLogLink.appendChild(document.createTextNode("Logs"));
	aLogSpan.appendChild(aLogLink);
	e_Breadcrumbs.appendChild(aLogSpan);
}

// Set Review Log type code.
var ReviewLogType = '18';
if (bPublished) { ReviewLogType = '68'; }

// Change wording for User notes, and add a Post User Note option.
var e_lnkReviewerLogs = document.getElementById("ctl00_ContentBody_UserInfo_lnkReviewerLogs");
if (e_lnkReviewerLogs) {
	var revLogText = e_lnkReviewerLogs.firstChild.data;
	var numLogTxt = revLogText.replace(/[^0-9]/g,"");
	if (numLogTxt) {
		e_lnkReviewerLogs.firstChild.data = 'Work User Notes (' + numLogTxt + ')';
		var aPostUserNote = document.createElement('a');
		aPostUserNote.appendChild(document.createTextNode('Post Note'));
		aPostUserNote.style.marginLeft = '12px';
		aPostUserNote.href = '/admin/userlog.aspx?guid=' + OwnerGuid + '&LogType=54';
		aPostUserNote.target = '_blank';
		insertAfter(aPostUserNote, e_lnkReviewerLogs);
	} else {
		e_lnkReviewerLogs.firstChild.data = 'Post User Note';
		e_lnkReviewerLogs.href = '/admin/userlog.aspx?guid=' + OwnerGuid + '&LogType=54';
	}
}

// Remove event days calculation until rendered correctly.
var e_DaysAway = document.getElementById("ctl00_ContentBody_CacheDetails_DaysAway");
if (e_DaysAway) {
	removeNode(e_DaysAway);
}

// If Event or CITO type cache, highlight date if less than 14  or more than 90 days from today.
if ((CacheType1 == 'E' || CacheType1 == 'C') && !bPublished) {
	// Get cache date, time set to 2am, to compensate for DST change over.
	var placeDate = CacheData.getAttribute('data-dateplaced').toString();
	var placeYYYY = placeDate.substr(0,4) - 0;
	var placeMM   = placeDate.substr(4,2) - 0;
	var placeDD   = placeDate.substr(6,2) - 0;

	placeDate = placeYYYY + '/' + placeMM + '/' + placeDD + ' 02:00:00';
	var cDate = Date.parse(placeDate);

	// Get day of the week for placed date.
	var dotw = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var d = new Date();
	d.setFullYear(placeYYYY, placeMM - 1, placeDD);
	placeDotW = dotw[d.getDay()];

	// Get local date, time set to midnight.
	var nDate = new Date();
	nDate.setHours(0,0,0,0);

	// Calculate days difference.
	var difDays = Math.floor((cDate - nDate) / 86400000);

	// If less than 2 weeks, show alert.
	if (difDays == 1) { var plural = ''; } else { plural = 's'; }
	var newDateTitle = `${placeDotW}, ${difDays.toString()} day${plural} away.`;
	if (difDays > 90) {
		var okDate = new Date(cDate);
		okDate = addDays(d, -90);
		newDateTitle += `\nOK Date: ${okDate.getFullYear()}-${fPad(okDate.getMonth() + 1,2)}-${fPad(okDate.getDate(),2)}`;
	}
	e_CacheDetails_PlaceDate.title = newDateTitle;
	if ((difDays < 14) || (difDays > 90)) {
		e_CacheDetails_PlaceDate.style.backgroundColor = 'yellow';
		var emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_CacheDetails_PlaceDate);
		if (difDays >= 0) {
			emDateDif.appendChild(document.createTextNode(' \u2190' +
														  difDays.toString() + ' day' + plural + ' away'));
		} else {
			emDateDif.appendChild(document.createTextNode(' \u2190' +
														  'date has passed'));
		}
	}
}

// Check if posted coordinates match selected location.
// handle a location error, if any
if (!(e_CacheDetails_LocationError.firstChild == null)) {
	// the override string is the location/reverseGeocodeLocation pair
	var override = e_CacheDetails_Location.firstChild.data + e_CacheDetails_LocationError.firstChild.data;

	// Convert saved list of acceptible place combinations to current format, if necessary
	fConvertRegionOverride();

	// If script settings have reverse geocoding off, suppress native error functionality
	if (!(CustomCoordinateAlert == 'On')) {
		e_CacheDetails_Location.style.color = 'black';
		e_CacheDetails_LocationError.style.visibility = 'hidden';
	}
	else {
		if (fCheckRegionOverride()) {
			e_CacheDetails_Location.style.color = 'green';
			e_CacheDetails_LocationError.style.visibility = 'hidden';
		}
		else {
			// add the "thumbs up" button to accept the location mismatch
			var addRgnOvr = document.createElement('a');
			addRgnOvr.href = 'javascript:void(0)';
			var imgRgnOvr = document.createElement('img');
			imgRgnOvr.src = ThumbsUp;
			imgRgnOvr.title = 'Accept as a match';
			imgRgnOvr.align = 'absbottom';
			imgRgnOvr.style.marginLeft = '3px';
			addRgnOvr.appendChild(imgRgnOvr);
			insertAfter(addRgnOvr, e_CacheDetails_LocationError);
			addRgnOvr.addEventListener('click', fAddRegionOverride, false);
		}
	}
}

// Add enhancements to Navagation box.
// Locate navagation box table.
var PathSearch = "//img[contains(@src, 'view_cache_listing.png')]";
var gifBhandList = document.evaluate(PathSearch, document, null,
XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,    null);
if (gifBhandList.snapshotLength == 1) {
	var gifBhand = gifBhandList.snapshotItem(0);
	var nvxDiv = fUpGen(gifBhand, 5);
	h3RevOpt = nvxDiv.childNodes[1].childNodes[1];
}

// Shorten link to email owner.
document.getElementById('ctl00_ContentBody_lnkEmail').firstChild.data = 'Email Owner';

// Remove background image viewer if no background image specified.
if (e_lnkBackgroundImg) {
	// Move up DOM to find table.
	var div_lnkBackgroundImg = fUpGenToType(e_lnkBackgroundImg, 'DIV');
	if (!e_lnkBackgroundImg.href) {
		removeNode(div_lnkBackgroundImg.parentNode);
	}
}

// Create Settings icon button.
var lnkOpenSettings = document.createElement('a');
lnkOpenSettings.id = 'lnkOpenSettings';
lnkOpenSettings.href = 'javascript:void(0)';
lnkOpenSettings.title = 'Open Settings';
var imgOpenSettings = document.createElement('img');
imgOpenSettings.src = SettingsImg;
imgOpenSettings.classList.add('gm-icon-buttons');
lnkOpenSettings.appendChild(imgOpenSettings);
lnkOpenSettings.addEventListener("click", fShowSettings, true);

// Create Memory Link icon button.
var lnkMemoryFilter = document.createElement('a');
lnkMemoryFilter.id = 'lnkMemoryFilter';
lnkMemoryFilter.href = 'javascript:void(0)';
lnkMemoryFilter.title = 'Save this cache to Memory';
var imgMemoryFilter = document.createElement('img');
imgMemoryFilter.src = MemoryImg;
imgMemoryFilter.classList.add('gm-icon-buttons');
lnkMemoryFilter.appendChild(imgMemoryFilter);
lnkMemoryFilter.addEventListener("click", fAddToMemory, true);

// Create Copy2Clip icon button.
var lnkCopy2ClipLink = document.createElement('a');
lnkCopy2ClipLink.id = "lnkCopy2ClipLink";
if (GM_getValue('NewCopyMode_' + SignedInAs, 'Off') == 'Off') {
	lnkCopy2ClipLink.setAttribute('copytype', 1);
	lnkCopy2ClipLink.title = 'Copy formatted cache name & ID to clipboard.\n';
	lnkCopy2ClipLink.title +=
	'SHIFT: Copy Latitude in -DD.ddddd format.\n' +
	'CLRL: Copy Longitude in -DD.ddddd format.\n' +
	'SHIFT + CTRL: Copy Coordinates in -DD.ddddd,-DD.ddddd format.';
} else {
	if (GM_getValue('CopyNameMdLink_' + SignedInAs, 'Off') == 'Off') {
		lnkCopy2ClipLink.title = "Click: Copy GC-Code and Cache Name to clipboard"
		lnkCopy2ClipLink.title += "\r\nCtrl-Click: Copy GC-Code and Cache Name as Markdown Link";
		lnkCopy2ClipLink.title += "\r\nShift-Click: Copy GC-Code and Cache Name to clipboard, add coordinates in DDD MM.MMM format on new line";
		lnkCopy2ClipLink.title += "\r\nCtrl-Shift-Click: Copy GC-Code and Cache Name as Markdown link to clipboard, add coordinates in DDD MM.MMM format on new line";
		lnkCopy2ClipLink.title += "\r\nAlt-Shift: Copy GC-Code and Cache Name to clipboard, ready for use in email";
		lnkCopy2ClipLink.title += "\r\nAlt-Click: Copy only coordinates in DDD MM.MMM format to clipboard";
	} else {
		lnkCopy2ClipLink.title = "Click: Copy GC-Code and Cache Name to clipboard as Markdown link";
		lnkCopy2ClipLink.title += "\r\nShift-Click: Copy GC-Code and Cache Name as Markdown link to clipboard, add coordinates in DDD MM.MMM format on new line";
		lnkCopy2ClipLink.title += "\r\nAlt-Shift: Copy GC-Code and Cache Name to clipboard, ready for use in email";
		lnkCopy2ClipLink.title += "\r\nAlt-Click: Copy only coordinates in DDD MM.MMM format to clipboard";
	}
}
var imgCopy2ClipLink = document.createElement('img');
imgCopy2ClipLink.src = Copy2ClipboardSrc;
imgCopy2ClipLink.classList.add('gm-icon-buttons');
lnkCopy2ClipLink.appendChild(imgCopy2ClipLink);
lnkCopy2ClipLink.addEventListener('mousedown', fCopy2Clipboard, true);

// Put all links and text into a Span element.
PageLinkSpan = document.getElementById("PageLinkSpan");
if (!PageLinkSpan) {
	PageLinkSpan = document.createElement("span");
	PageLinkSpan.id = 'PageLinkSpan';
	$(PageLinkSpan).insertBefore(e_CacheDetails_GpxButton);
}

// Add elements to span.
PageLinkSpan.appendChild(lnkOpenSettings);
// PageLinkSpan.appendChild(lnkQueueNoteEdit);
PageLinkSpan.appendChild(lnkMemoryFilter);
PageLinkSpan.appendChild(lnkCopy2ClipLink);
PageLinkSpan.appendChild(document.createElement('br'));


// tests associated with Challenge caches
var PathSearch = "//div[contains(@class, 'WidgetBody')]/img[contains(@src, 'challengecache-yes')]";
let C_Icon = document.evaluate(PathSearch, document, null, //test for challenge attribute
	XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
let C_Title = (e_CacheDetails_Name.innerText.search(/challenge/i) >= 0);
PathSearch = "//a[contains(@href, 'project-gc.com/Challenges/')]";
var ccLink = document.evaluate(PathSearch, e_CacheDetails_LongDesc, null, //test for challenge checker link
XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
if (ccLink) { 

	var lnkCCer = document.createElement('a');
	lnkCCer.id = 'lnkCCer';
	lnkCCer.href = 'javascript:void(0)';
	lnkCCer.title = 'Run Challenge Checkers in New Tabs';
	var imgCCer = document.createElement('img');
	imgCCer.src = ChallengeCheckerImg;
	imgCCer.classList.add('gm-icon-buttons');
	imgCCer.style.paddingTop = '6px';
	imgCCer.style.verticalAlign = 'top';
	lnkCCer.appendChild(imgCCer);
	PageLinkSpan.appendChild(document.createElement('br'));
	PageLinkSpan.appendChild(lnkCCer);
	var spanCCer = document.createElement('span');
	spanCCer.id = 'spanCCer';
	spanCCer.appendChild(document.createTextNode('Run Challenge Checkers'));
	spanCCer.classList.add('CCerText');
	lnkCCer.appendChild(spanCCer);
	var dftCcProfile = SignedInAs;
	var AltCCerProfile = GM_getValue('AltCCerProfile_' + SignedInAs, '').trim();
	if (AltCCerProfile) { dftCcProfile = AltCCerProfile; }
	lnkCCer.setAttribute('ccUrl1', ccLink.href +
								   `?u=${encodeURIComponent(window.btoa(OwnerName))}&wpid=${WptID}`);
	lnkCCer.setAttribute('ccUrl2', ccLink.href +
								   `?u=${encodeURIComponent(window.btoa(dftCcProfile))}`);
	lnkCCer.addEventListener('click', fRunChallengeChecker);
	PageLinkSpan.appendChild(document.createTextNode(' | '));
	var lnkPCGForum = document.createElement('a');
	lnkPCGForum.id = 'lnkPCGForum';
	lnkPCGForum.href = 'https://project-gc.com/forum/search?forum_id=8&phorum_page=search&search=' + WptID + '&match_type=ALL&author=&match_forum%5B%5D=ALL&match_threads=0&match_dates=365';
	lnkPCGForum.title = 'Project-GC Forum';
	lnkPCGForum.target = '_blank';
	var spanPCGForum = document.createElement('span');
	spanPCGForum.id = 'spanPCGForum';
	spanPCGForum.appendChild(document.createTextNode('Project-GC Forum search'));
	spanPCGForum.classList.add('CCerText');
	lnkPCGForum.appendChild(spanPCGForum);
	PageLinkSpan.appendChild(lnkPCGForum);
	if (!C_Icon) fShowAlert('No attribute for challenges'); 
	if (!C_Title) fShowAlert('Title does not contain the word challenge');
	if (ccLink.href.toUpperCase().indexOf(WptID)==-1) {fShowAlert('Challenge checker link is for a different cache')};
} 
else { //no challenge checker, but attribute or title
	if (C_Icon) {
		if (C_Title) fShowAlert('Challenge attribute and title, but no checker link');
		else fShowAlert('Challenge attribute, but no checker link nor challenge in title');
	}
	else if (C_Title) fShowAlert('Challenge in title but no checker link or attribute');
}


function fRunChallengeChecker() {
/*todo	setClickLink(this.getAttribute('ccUrl1'),'ccUrl1');
	setClickLink(this.getAttribute('ccUrl2'),'ccUrl2');
	document.getElementById('ccUrl1').click();
	document.getElementById('ccUrl2').click();*/
	GM_openInTab(this.getAttribute('ccUrl1'));  //remediating TM5 tab spawning
	GM_openInTab(this.getAttribute('ccUrl2'));
}

// Fade/Flash Clicked Icons. Can handle any number of icons simultaneously.
var t2 = [], i2 = [];  // Set at global level.
function fFadeClickedIcon(imgToFade) {
	imgToFade.style.opacity = 0.2;
	t2.unshift(0);
	i2.unshift(imgToFade);
	t2[0] = window.setTimeout(
	function() {
		window.clearTimeout(t2.pop());
		i2.pop().style.opacity = 1;
	}
	, 650);
}

function fAddToMemory() {
	fFadeClickedIcon(imgMemoryFilter);
	// Get current storage value, and convert to array.
	var curStore = window.localStorage.getItem(memStorId);
	if (curStore) {
		var aCurStore = curStore.split(',');
	} else {
		aCurStore = new Array();
	}
	if (aCurStore.indexOf(WptID) == -1) {
		aCurStore.push(WptID);
	}
	// Save array back to storage.
	curStore = aCurStore.join(',');
	window.localStorage.setItem(memStorId, curStore);
}

// Copy formatted cache name and waypoint to clipboard.
function fCopy2Clipboard(e) {
	fFadeClickedIcon(imgCopy2ClipLink);

	if (GM_getValue('NewCopyMode_' + SignedInAs, 'Off') == 'Off') {
		if (e.shiftKey && e.ctrlKey) {
			GM_setClipboard(CoordDdLat.toString() + ',' + CoordDdLon.toString());
		} else if (e.shiftKey) {
			GM_setClipboard(CoordDdLat);
		} else if (e.ctrlKey) {
			GM_setClipboard(CoordDdLon);
		} else {
			if (this.getAttribute('copytype') == 1) {
				GM_setClipboard(`"${CacheName}" (${WptID})`);
			} else {
				GM_setClipboard(`["${CacheName}" (${WptID})](http://coord.info/${WptID})`);
			}
		}
	} else {
		if (GM_getValue('CopyNameMdLink_' + SignedInAs, 'Off') == 'Off') {
			if (e.shiftKey && e.ctrlKey) {
				GM_setClipboard(`[${WptID}: ${CacheName}](https://coord.info/${WptID})\r\n${coordsDecMinutes}`);
			} else if (e.shiftKey && e.altKey) {
				GM_setClipboard(`${WptID}: \'${CacheName}\' (https://coord.info/${WptID})`);
			} else if (e.shiftKey) {
				GM_setClipboard(`${WptID}: ${CacheName}\r\n${coordsDecMinutes}`);
			} else if (e.ctrlKey) {
				GM_setClipboard(`[${WptID}: ${CacheName}](https://coord.info/${WptID})`);
			} else if (e.altKey) {
				GM_setClipboard(`${coordsDecMinutes}`);
			} else {
				GM_setClipboard(`${WptID}: ${CacheName}`);
			}
		} else {
			if (e.shiftKey && e.altKey) {
				GM_setClipboard(`${WptID}: \'${CacheName}\' (https://coord.info/${WptID})`);
			} else if (e.shiftKey) {
				GM_setClipboard(`[${WptID}: ${CacheName}](https://coord.info/${WptID})\r\n${coordsDecMinutes}`);
			} else if (e.altKey) {
				GM_setClipboard(`${coordsDecMinutes}`);
			} else {
				GM_setClipboard(`[${WptID}: ${CacheName}](https://coord.info/${WptID})`);
			}
		}
	}
}

// Copy formatted cache name and waypoint to clipboard.
function fCopyX2Clipboard(e) {
	fFadeClickedIcon(this.firstChild);
	GM_setClipboard(this.getAttribute('info'));
}

// Update Review Queue Text.
//fUpdateRqnScreenText();

// Enlarge and color cache waypoint.    CacheDetails_WptRef
var e_CacheDetails_WptRef = document.getElementById("ctl00_ContentBody_CacheDetails_WptRef");
e_CacheDetails_WptRef.style.fontWeight = "bold";
e_CacheDetails_WptRef.style.fontSize = "larger";

// Enlarge and color cache placer.  CacheDetails_Placers
if (Switch_ChangeOwner) {
	if (e_CacheDetails_Placers.firstChild.data.trim() ==
		e_CacheDetails_Owner.firstChild.data.trim()) {
		var NameColor = "blue";
	} else {
		NameColor = "red";
	}
	e_CacheDetails_Placers.style.color = NameColor;
}

// Remove misfunctioning wheelchair link.
var e_HandicapAccess = document.getElementById("ctl00_ContentBody_CacheDetails_HandicapAccess");
if (e_HandicapAccess) {
	removeNode(e_HandicapAccess);
}

// Show alert if Terrain rating=1, and no wheelchair icon.  CacheDetails_Terrain_Stars
if (CustomTerrainAlert == 'On') {
	if (CacheTypeName != 'Event Cache' &&
		CacheTypeName != 'Cache In Trash Out Event' ) {
		if (e_CacheDetails_Terrain_Stars.alt == "1 out of 5") {
			// Check for wheelchair icon.
			WC_Icon = (document.body.innerHTML.search(/wheelchair-yes.\png/g) >= 0);
			if (! WC_Icon) {
				var TerRatingSpan = document.createElement("span");
				TerRatingSpan.id = 'Handicap_Notification_Link';
				var TerRatingHndi = document.createElement("a");
				TerRatingHndi.target = '_blank';
				TerRatingHndi.title = 'Create Handicap notice log';
				TerRatingHndi.style.color = 'red';
				TerRatingHndi.style.marginLeft = '6px';
				TerRatingHndi.style.fontWeight = 'bold';
				TerRatingHndi.style.fontStyle = 'italic';
				tempHref = domain + "/live/geocache/"+ WptID + "/log" + "?";
				TerRatingHndi.href = tempHref +
									 'LogType=hndi&auto=y&close=y' +
									 '&owner='   + encodeURIComponent(OwnerName) +
									 '&title='   + encodeURIComponent(CacheName.trim()) +
									 '&wptid='   + encodeURIComponent(WptID) +
									 '&coords='  + encodeURIComponent(txCoords);
				TerRatingHndi.appendChild(document.createTextNode('\u2190Handicap Accessible?'));
				TerRatingSpan.appendChild(TerRatingHndi);
				insertAfter(TerRatingSpan, e_CacheDetails_Terrain_Stars);
				var HcaNote = 1;
			}
		}
	}
}

// Hides Warning.
if (CustomHidesAlert >= 0) {
	var HidesCount = e_UserInfo_lnkHides.innerHTML + "";
	HidesCount = parseInt(HidesCount.replace(/[^0-9]/g,""));
	if (HidesCount <= CustomHidesAlert) {
		e_UserInfo_lnkHides.style.backgroundColor = 'yellow';
	} else {
		e_UserInfo_lnkHides.style.backgroundColor = '';
	}
}

// Finds Warning.
if (CustomFindsAlert >= 0) {
	var FindsCount = e_UserInfo_lnkFinds.innerHTML + "";
	FindsCount = parseInt(FindsCount.replace(/[^0-9]/g,""));
	if (FindsCount <= CustomFindsAlert) {
		e_UserInfo_lnkFinds.style.backgroundColor = 'yellow';
	} else {
		e_UserInfo_lnkFinds.style.backgroundColor = '';
	}
}

// Distance Warning.    UserInfo_lbDistance
if (CustomDistAlert > 0) {
	DistVal = e_UserInfo_lbDistance.firstChild.data;
	if (DistVal.search(/Unknown/gi) >= 0) {
		DistVal = CustomDistAlert;

		var aHomeNote = document.createElement("a");
		aHomeNote.href = "javascript:void(0)";
		aHomeNote.title = "Home Coordinates Request Note";
		aHomeNote.appendChild(document.createTextNode('home\u00A0\u266A'));
		insertAfter(document.createTextNode(']'), e_UserInfo_lbDistance);
		insertAfter(aHomeNote, e_UserInfo_lbDistance);
		insertAfter(document.createTextNode('\u00A0['), e_UserInfo_lbDistance);
		aHomeNote.addEventListener("click", fHomeNote, true);
	} else {
		// Determine if measurement in feet.
		if (DistVal.search(/ft/g) >= 0) {var DistUnits = 'ft';}
		DistVal = parseInt(DistVal.replace(/[^0-9.]/g,""));
		// If measurement in feet, the convert to miles.
		if (DistUnits == 'ft') {DistVal /= 5280;}
		if (DistVal >= CustomDistAlert) {
			var dnt = e_UserInfo_lbDistance.firstChild.data.replace(/Distance: /g,"");
			e_UserInfo_lbDistance.firstChild.data = dnt;
			var aVacaNote = document.createElement("a");
			aVacaNote.href = "javascript:void(0)";
			aVacaNote.title = "Vacation Cache Note";
			aVacaNote.setAttribute('DistVal', DistVal);
			aVacaNote.appendChild(document.createTextNode('vaca\u00A0\u266A'));
			insertAfter(document.createTextNode(']'), e_UserInfo_lbDistance);
			insertAfter(aVacaNote, e_UserInfo_lbDistance);
			insertAfter(document.createTextNode('\u00A0['), e_UserInfo_lbDistance);
			aVacaNote.addEventListener("click", fVacaNote, true);
		}
	}
	if (DistVal >= CustomDistAlert) {
		e_UserInfo_lbDistance.style.color = 'red';
		e_UserInfo_lbDistance.style.fontWeight = "bold";
	}
	else {
//		e_UserInfo_lbDistance.style.color = 'black';
		e_UserInfo_lbDistance.className = '';
		e_UserInfo_lbDistance.style.fontWeight = "normal";
	}
}

// Cache Proximity Warning and Archived Logs links.
if (Switch_CacheProximity || Switch_ArcLogs) {

	// Add guid data to this cache's archived logs link.
	e_lnkArchived.href += '&WptId=' + CacheGuid;

	// Get this cache type.
	var tcType = CacheTypeName;

	// Get list of nearby cache rows.
	var PathSearch = "//a[@href='/about/cache_types.aspx']";
	var ncLinks = document.evaluate(PathSearch, document, null,
	XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	var maxR = ncLinks.snapshotLength;

	// Get link to table.
	if (maxR) {
		var ncLink = ncLinks.snapshotItem(0);
		var ncTable = fUpGenToType(ncLink, 'TABLE');
	}

	// Loop through list.
	var TooClose = false;
	for (var i = 0; i < maxR; i++) {
		var ncLink = ncLinks.snapshotItem(i);
		var ncRow = fUpGenToType(ncLink, 'TR');

		// If nearby cache is PM, move icon to first cell.
		if (ncRow.cells[1].childNodes.length > 1) {
			var pmIcon = ncRow.cells[1].removeChild(ncRow.cells[1].lastChild);
			ncRow.cells[0].appendChild(pmIcon);
		}

		// If "new" icon, move to second line in cell.
		if (ncRow.cells[3].childNodes.length > 1) {
			insertAfter(document.createElement('br'), ncRow.cells[3].firstChild);
		}

		// Apply no-wrap to selected cells.
		ncRow.cells[3].style.whiteSpace = 'nowrap';
		ncRow.cells[6].style.whiteSpace = 'nowrap';
		ncRow.cells[7].style.whiteSpace = 'nowrap';

		// Get type of nearby cache.
		var ncType = ncLink.firstChild.alt;

		// Determine nearby cache status;
		var txStatusNode = ncRow.cells[5].firstChild;
		while (txStatusNode.nodeName != 'A') {
			txStatusNode = txStatusNode.nextSibling;
		}
		var txStatusNodeA = txStatusNode;

		var ncStatus = 'Active';
		if (txStatusNodeA.childNodes.length == 1) {
			// Nearby cache has been published.
			if (txStatusNodeA.firstChild) {
				var zTest = txStatusNodeA.firstChild;
				if (zTest.nodeName == 'SPAN') {
					if (zTest.getAttribute('class') == 'Strike') {
						ncStatus = 'Disabled';
					}
					if (zTest.getAttribute('class') == 'OldWarning Strike') {
						ncStatus = 'Archived';
					}
				}
			}
		} else {
			// Nearby cache has not been published.
			if (txStatusNodeA.lastChild) {
				var zTest = txStatusNodeA.lastChild;
				if (zTest.nodeName == 'STRONG') {
					ncStatus = 'Unpublished';
				}
			}
		}
		// Determine nearby cache waypoint ID.
		var txWptNode = txStatusNodeA.nextSibling;
		var RegEx1 = new RegExp('.*[(](.*?)[)]$');
		var RegRslt = RegEx1.exec(txWptNode.data.trim());
		if (RegRslt) {
			ncWptID = RegRslt[1];

			// Check proximity, if this is not virtual, event or CITO.
			if (Switch_CacheProximity) {
				if ((ncType != 'Event Cache') &&
					(ncType != 'Mega-Event Cache') &&
					(ncType != 'Virtual Cache') &&
					(ncType != 'Webcam Cache') &&
					(ncType != 'Earthcache') &&
					(ncType != 'Cache In Trash Out Event')) {
					if (ncStatus != 'Archived' && postedCoordType == CT_PHYSICAL) {


						var ncDist = ncRow.cells[4].firstChild.data.trim();

						// If too close.
						cellClass = ncRow.cells[4].getAttribute('class', '');
						if (cellClass == 'AdminAlert') {
							var ncDir = ncRow.cells[4].lastChild.data;
							ncRow.cells[4].style.backgroundColor = '#ffff88';
							TooClose = true;

							// Create proximity note link.
							// Get measurement system.
							ncDistSys = ncDist.replace(/[0-9.]/g,'');

							// Get raw numeric value.
							ncDist = ncDist.replace(/[^0-9.]/g,'');
							if (ncDist == '') { ncDist = 0; }
							ncDist = ncDist - 0;
							if (ncDist > 0) {
								// If expressed in feet, round to nearest 5', convert to meters.
								if (ncDistSys == 'ft') {
									var ncDistFt = (ncDist + 2.5) / 5;
									ncDistFt = Math.floor(ncDistFt) * 5;
									var ncDistM = Math.floor(ncDist * 0.3048 + .5);
								}

								// If expressed in kilometers, convert to feet, round to nearest 5'.
								if (ncDistSys == 'km') {
									var ncDistM = Math.floor(ncDist * 1000 + .5);
									var ncDistFt = Math.floor(ncDistM / 0.3048 + .5);
									var ncDistFt = (ncDistFt + 2.5) / 5;
									ncDistFt = Math.floor(ncDistFt) * 5;
								}
								if (ncDistSys == 'm') {
									var ncDistM = Math.floor(ncDist + .5);
									var ncDistFt = Math.floor(ncDistM / 0.3048 + .5);
									var ncDistFt = (ncDistFt + 2.5) / 5;
									ncDistFt = Math.floor(ncDistFt) * 5;
								}
							} else {
								ncDistFt = 0;
								ncDistM = 0;
							}

							var ncTitle = txStatusNode.lastChild;
							while (ncTitle.nodeName != '#text') {
								ncTitle = ncTitle.lastChild;
							}
							ncTitle = ncTitle.data;
							var aProxNote = document.createElement("a");
							aProxNote.setAttribute('ncWptId', ncWptID);
							aProxNote.setAttribute('ncTitle', ncTitle);
							aProxNote.setAttribute('ncDistFt', ncDistFt);
							aProxNote.setAttribute('ncDistM', ncDistM);
							aProxNote.setAttribute('ncDir', ncDir);
							aProxNote.href = "javascript:void(0)";
							aProxNote.title = "Proximity Note";
							aProxNote.appendChild(document.createTextNode('prox\u00A0\u266A'));
							var xBR = ncRow.cells[7].childNodes[1];
							insertAheadOf(document.createTextNode('\u00A0['), xBR);
							insertAheadOf(aProxNote, xBR);
							insertAheadOf(document.createTextNode(']'), xBR);
							aProxNote.addEventListener("click", fProxNote, true);
						}
					}
				}
			}
		}
	}
}

// set up TM5.0.0 Firefox tab-spawning work around
let scriptDiv = document.createElement('div');
scriptDiv.setAttribute('hidden',true);
scriptDiv.setAttribute('id','scriptDiv');
document.querySelector('body').appendChild(scriptDiv);

// set up proximity check URL - search for COs nearby finds
var proxURL = domain + "\/play\/results\/" +
	"?st=" + WptID +
	"&fb=" + encodeURI(OwnerName) +
	"&ot=geocache" +		// types of cache to include
//	"&sa=1" +				// nothing=unarchived, 1=both, 2=archived
	"&asc=true" +			// sort order true=ascending
	"&sort=geocacheName" + 	// sort column.  Good alternative is =distance
	"&r=5" + 				// radius in miles
	"&su=1";  				// include published and unpublished caches.  Comment out for published only.

var proxHook = $('small:contains("Not including this listing")')[0];
proxHook.innerText=proxHook.innerText+"  ";
testHook = proxHook.lastChild;
var proxSearch = document.createElement("button");
proxSearch.type = "button";
proxSearch.href = "javascript:void(0)";
proxSearch.title = "Search for CO's nearby finds";
proxSearch.appendChild(document.createTextNode('Search for CO\'s Nearby Finds'));
insertAfter(proxSearch,proxHook.lastChild);

/* todo TM5 work around
setClickLink(proxURL,"nearbyURL");
function setClickLink (targetUrl, targetID)  {
	if (!document.getElementById(targetID)) {
		let searchLink = document.createElement('a');
		searchLink.setAttribute('href',targetUrl);
		searchLink.setAttribute('target','_blank');
		searchLink.setAttribute('id',targetID);
		scriptDiv.appendChild(searchLink);
	}
	else {
		document.getElementById(targetID).href = targetUrl;
	}
}*/

proxSearch.addEventListener("click", fLaunchCOSearch);

function fLaunchCOSearch(event) {
//todo	document.getElementById('nearbyURL').click(); //tm5 remediation
	GM_openInTab(proxURL,"_blank");
}

// Enlarge map.
var xmap = document.getElementById("map");
if (xmap) {
	xmap.style.height = '300px';
	xmap.style.width = '260px';
}

// Get settings for Map enhancements.
var cRadList = GM_getValue('CustomRadius_' + SignedInAs, '').trim();
if (!cRadList) { cRadList = '528'; }
var rRadList = GM_getValue('CustomRRRadius_' + SignedInAs, '').trim();
if (!rRadList) { rRadList = '150'; }

/* depricate as useless 11/07/23
// GC Terraserver and Topo Maps
var gqMapLi = document.createElement("li");
var gqMapA = document.createElement("a");
gqMapLi.appendChild(gqMapA);
gqMapA.appendChild(document.createTextNode('GC Terraserver Maps'));
gqMapA.href = 'http://www.geocachingadmin.com/maps/terra.php?type=1&zoom=12' +
			  '&width=800&height=500&autoinc=yes&mkr1=210&center=' +
			  CoordDdLat + ',' + CoordDdLon + '&p=209:' + CoordDdLon + ':' +
			  CoordDdLat;
gqMapA.target = '_blank';
e_MapLinks_MapLinks.insertBefore(gqMapLi, e_MapLinks_MapLinks.childNodes[4]);

gqMapLi.appendChild(document.createTextNode(' \u00A0 ('));
var gqMapA = document.createElement("a");
gqMapLi.appendChild(gqMapA);
gqMapA.appendChild(document.createTextNode('Topo'));
gqMapLi.appendChild(document.createTextNode(')'));
gqMapA.href = 'http://www.geocachingadmin.com/maps/terra.php?type=2&zoom=12' +
			  '&width=800&height=500&autoinc=yes&mkr1=210&center=' +
			  CoordDdLat + ',' + CoordDdLon + '&p=209:' + CoordDdLon + ':' +
			  CoordDdLat;
gqMapA.target = '_blank';
*/
// Directions from home.
if (HomeLabel != '') {
	var gqMapLi = document.createElement("li");
	var gqMapA = document.createElement("a");
	gqMapLi.appendChild(gqMapA);
	gqMapA.appendChild(document.createTextNode('Directions from ' + HomeLabel));
	gqMapA.href = 'http://maps.google.com/maps?q=' + HomeDdLat + ',' + HomeDdLon +
				  '+(' + encodeURI(HomeLabel) + ')+to+' + CoordDdLat + ',' + CoordDdLon +
				  '+(' + WptID +')';
	gqMapA.target = '_blank';
	e_MapLinks_MapLinks.insertBefore(gqMapLi, e_MapLinks_MapLinks.lastChild.nextSibling);
}

// If any pictures loaded to page, add link to gallery.
if (e_CacheDetails_CacheImages) {
	var aGalleryLink = document.createElement('a');
	aGalleryLink.href = domain + "/seek/gallery.aspx?guid=" + CacheGuid;
	aGalleryLink.target = '_blank';
	aGalleryLink.title = 'Show Cache Gallery';
	aGalleryLink.style.fontWeight = 'bold';
	aGalleryLink.appendChild(document.createTextNode('Visit the Gallery'));
	e_CacheDetails_CacheImages.insertBefore(aGalleryLink, e_CacheDetails_CacheImages.firstChild);
	aGalleryLink.parentNode.insertBefore(document.createElement('br'), aGalleryLink.nextSibling);
}

// Script detection.
var sdScripts = 0;
var ldScripts = 0;
var bScriptFound = false;

// Get count of short description script nodes.
if (e_CacheDetails_ShortDesc) {
	var PathSearch = ".//script";
	var scriptNodes = document.evaluate(
	PathSearch,
	e_CacheDetails_ShortDesc,
	null,
	XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
	null);
	sdScripts = scriptNodes.snapshotLength;
}

// Get count of long description script nodes.
if (e_CacheDetails_LongDesc) {
	var PathSearch = ".//script";
	var scriptNodes = document.evaluate(
	PathSearch,
	e_CacheDetails_LongDesc,
	null,
	XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
	null);
	ldScripts = scriptNodes.snapshotLength;
}
bScriptFound = ((sdScripts + ldScripts) > 0);

// If no nodes, check for "javascript" in text.
if (!bScriptFound) {
	var allDesc = '';
	if (e_CacheDetails_ShortDesc) {
		var allDesc = e_CacheDetails_ShortDesc.innerHTML.toString() + ' ';
	}
	if (e_CacheDetails_LongDesc) {
		var allDesc = allDesc + e_CacheDetails_LongDesc.innerHTML.toString();
	}
	var RegEx1 = new RegExp('javascript', 'i');
	if (allDesc.match(RegEx1)) { bScriptFound = true; }
}

// If any script nodes found, display warning image.
if (bScriptFound) {
	fShowAlert('Possible Javascript On Page');
}

// remove script actions from form action, but leave pagination stuff in URL
var myAspForm = document.getElementById("aspnetForm");
if ( !(myAspForm.action.indexOf("skip") > -1) ) myAspForm.action = './review.aspx?guid=' + CacheGuid;

// Locked page detection.
var cd_islocked = eval(CacheData.getAttribute('data-islocked'));
if (cd_islocked) {
	//fShowAlert('Cache Page is Locked');
}

// If Event, and Difficulty rating > 1.
if (cd_cachetypeid == TYPE_EVENT && cd_difficulty > 1) {
	fShowAlert('Event Difficulty Greater Than 1');
}

// If the native log delete update is running, don't insert log delete elements
/*let firstLogTrashcanID ="ctl00_ContentBody_CacheLogs_dlLogHistory_ctl01_trashcan";
if (!(document.getElementById(firstLogTrashcanID))) {

	// Add delete log link to each log.
	// Find log table.
	e_ncTbl = document.getElementById("ncTbl");
	if (!e_ncTbl) {
		// If no nearby cache log, use nearby cache link as starting point.
		e_ncTbl = document.getElementById("ctl00_ContentBody_lnkNearbyCaches");
	}

	// Move down siblings until the next table is found.
	if (e_ncTbl) {
		var logTable = e_ncTbl.nextSibling;
		while (logTable.nodeName != 'TABLE') {
			logTable = logTable.nextSibling;
			if (!logTable) {
				GM_log('ERROR: No starting point to locate Log Table');
				break;
			}
		}
	}

	// If table is found, find log links, and add delete links.
	if (logTable) {
		var PathSearch = ".//a[contains(@href, '/log.aspx?LUID=')]";
		var aViewLogList = document.evaluate(PathSearch, logTable, null,
		XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,    null);
		var ic = aViewLogList.snapshotLength;
		for (var i = 0; i < ic; i++) {
			var aViewLog = aViewLogList.snapshotItem(i);

			if (aViewLog.parentNode.cellIndex == 4) {
				aViewLog.target = '_blank';
				aViewLog.parentNode.style.whiteSpace = 'nowrap';
				var aDelLog = document.createElement("a");
				aDelLog.href = aViewLog.href + '&delete=y&close=y';
				aDelLog.target = '_blank';
				aDelLog.title = 'Delete Log';
				aDelLog.style.marginLeft = '16px';
				aDelLog.appendChild(document.createElement("img"));
				aDelLog.firstChild.src = TrashCan;
				aDelLog.firstChild.style.verticalAlign = 'text-bottom';
				aViewLog.parentNode.appendChild(aDelLog);
				aDelLog.addEventListener("click", fFadeOutLogText, true);
			} else { GM_log('ERROR: View Log link not in cellIndex 3.'); }
		}
	} else { GM_log('ERROR: Unable to locate Log Table.'); }
}*/

// If url action passed.
if (urlAction) {

	// Action is Hold when not held.
	if (urlAction === 'hold') {

		if (urlAutoClose === 'yes') {
			sessionStorage.setItem('autoclose', 'true');
		}
		else if (urlAutoClose === 'no') {
			sessionStorage.setItem('autoclose', 'false');
		}
		else {
			sessionStorage.setItem('autoclose', 'false');
		}

		if(!bHeld) {
			if (urlWatchAndHold === 'yes') {
				e_btnHoldAndWatch.click();
			}
			else {
				e_btnHold.click();
			}
		} else {
			fCheckAutoClose();
		}
		// Action is remhold when held.
	} else if (urlAction === 'remhold') {
		if (urlAutoClose === 'yes') {
			sessionStorage.setItem('autoclose', 'true');
		}
		else if (urlAutoClose === 'no') {
			sessionStorage.setItem('autoclose', 'false');
		}
		else {
			sessionStorage.setItem('autoclose', 'false');
		}

		if(bHeld) {
			e_btnHold.click();
		} else {
			fCheckAutoClose();
		}
		// Action is Publish and cache is held.
	} else if (urlAction === 'publish') {
		if(bHeld) {
			e_btnHold.click(); //clicking the hold link refreshes the page
		} else {
			sessionStorage.setItem('autoclose', 'true');
			e_btnPublish.click();
		}

		// Action is Lock or unlock.
	} else if ( ((urlAction === 'unlock')&&e_btnLockStatus) || ((urlAction ==='lock')&&(!e_btnLockStatus)) ) {
		e_btnLock.click();
	
	// Action is Watch.
	} else if ( ((urlAction === 'remwatch')&&e_btnWatchStatus) || ((urlAction ==='watch')&&(!e_btnWatchStatus)) ) {
		e_btnWatch.click();
	}
}

// Add anchor link to position page to logs.
// Skip if already assigned by another script.
if (!document.getElementById('log_table')) {
	var logTable = document.querySelector('table[class="Table geocache-logs"]');
	if (logTable) logTable.id = 'log_table';
}


// Monitor for storage change.
window.addEventListener('storage', fStorageChanged, false);

// Set map type and zoom level. //
fAdjustMap(CustomMapZoom, CustomMapType);

// Expand the litmus test results area when there is a "red" warning 
if (openLitmusOnRed == "On") {
	let litmusErrorCountSpan = document.getElementsByClassName('litmus-error-count')[0];
	if (litmusErrorCountSpan) {
		if (litmusErrorCountSpan.innerText != "0") {
			let buttonLink = document.getElementsByClassName('btn-link expand-collapse-btn')[0];
			if (buttonLink) {
				if (buttonLink.innerText.indexOf('Expand') > -1) 
					setTimeout(()=>{
						buttonLink.click();
						console.log('Review Page script attempted to expand the Litmus area.');
					},250);
			}
		}
	}
}

// Round a number to a specific number of decimal places.
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function fShowSettings() {

	fCreateSettingsDiv('Review Page Settings');
	var divSet = document.getElementById('gm_divSet');

	// Auto-run Litmus.
	var pAutoRunLitmus = document.createElement('p');
	pAutoRunLitmus.setAttribute('class', 'SettingElement');
	var cbAutoRunLitmus = document.createElement('input');
	cbAutoRunLitmus.id = 'cbAutoRunLitmus';
	cbAutoRunLitmus.type = 'checkbox';
	cbAutoRunLitmus.style.marginRight = '6px';
	var labelAutoRunLitmus = document.createElement('label');
	labelAutoRunLitmus.setAttribute('for', 'cbAutoRunLitmus');
	labelAutoRunLitmus.appendChild(document.createTextNode('Auto-run Litmus Test, when conditions met.'));
	var CustomLitmusAutomatic = GM_getValue('CustomLitmusAutomatic_' + SignedInAs, 'On');
	cbAutoRunLitmus.checked = (CustomLitmusAutomatic == 'On');
	pAutoRunLitmus.appendChild(cbAutoRunLitmus);
	pAutoRunLitmus.appendChild(labelAutoRunLitmus);
	divSet.appendChild(pAutoRunLitmus);

	// Litmus Viewport - show errors only.
	var pLitmusErrorsOnly = document.createElement('p');
	pLitmusErrorsOnly.setAttribute('class', 'SettingElement');
	var cbLitmusErrorsOnly = document.createElement('input');
	cbLitmusErrorsOnly.id = 'cbLitmusErrorsOnly';
	cbLitmusErrorsOnly.type = 'checkbox';
	cbLitmusErrorsOnly.style.marginRight = '6px';
	var labelLitmusErrorsOnly = document.createElement('label');
	labelLitmusErrorsOnly.setAttribute('for', 'cbLitmusErrorsOnly');
	labelLitmusErrorsOnly.appendChild(document.createTextNode('Show only errors, in the Litmus viewport.'));
	var CustomLitmusErrorsOnly = GM_getValue('CustomLitmusErrorsOnly_' + SignedInAs, 'true');
	cbLitmusErrorsOnly.checked = (CustomLitmusErrorsOnly == 'true');
	pLitmusErrorsOnly.appendChild(cbLitmusErrorsOnly);
	pLitmusErrorsOnly.appendChild(labelLitmusErrorsOnly);
	divSet.appendChild(pLitmusErrorsOnly);

	// Customize Distance from Home Alert.
	var pCustomDistAlert = document.createElement('p');
	pCustomDistAlert.setAttribute('class', 'SettingElement');
	var txtCustomDistAlert = document.createElement('input');
	txtCustomDistAlert.id = 'txtCustomDistAlert';
	txtCustomDistAlert.maxLength = 3;
	txtCustomDistAlert.style.width = '3em';
	txtCustomDistAlert.style.marginRight = '6px';
	txtCustomDistAlert.title = 'If the distance between the owner\'s home coordinates and the cache ' +
							   'are at or beyond this value, the distance will be highlighted in yellow.';
	var labelCustomDistAlert = document.createElement('label');
	labelCustomDistAlert.setAttribute('for', 'txtCustomDistAlert');
	labelCustomDistAlert.appendChild(document.createTextNode('Number of miles from home to trigger a Distance Alert.'));
	labelCustomDistAlert.title = txtCustomDistAlert.title;
	var CustomDistAlert = parseInt(GM_getValue('CustomDistAlert_' + SignedInAs, '100'));
	txtCustomDistAlert.value = CustomDistAlert;
	pCustomDistAlert.appendChild(txtCustomDistAlert);
	pCustomDistAlert.appendChild(labelCustomDistAlert);
	txtCustomDistAlert.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pCustomDistAlert);

	// Customize Hides Owned Alert.
	var pCustomHidesAlert = document.createElement('p');
	pCustomHidesAlert.setAttribute('class', 'SettingElement');
	var txtCustomHidesAlert = document.createElement('input');
	txtCustomHidesAlert.id = 'txtCustomHidesAlert';
	txtCustomHidesAlert.maxLength = 3;
	txtCustomHidesAlert.style.width = '3em';
	txtCustomHidesAlert.style.marginRight = '6px';
	txtCustomHidesAlert.title = 'If the submitter owns this number or fewer caches, that number will ' +
								'be highlighted in yellow.';
	var labelCustomHidesAlert = document.createElement('label');
	labelCustomHidesAlert.setAttribute('for', 'txtCustomHidesAlert');
	labelCustomHidesAlert.appendChild(document.createTextNode('Number of owner hides at or below which an Alert is shown. Zero disables.'));
	labelCustomHidesAlert.title = txtCustomHidesAlert.title;
	var CustomHidesAlert = parseInt(GM_getValue('CustomHidesAlert_' + SignedInAs, '3'));
	txtCustomHidesAlert.value = CustomHidesAlert;
	pCustomHidesAlert.appendChild(txtCustomHidesAlert);
	pCustomHidesAlert.appendChild(labelCustomHidesAlert);
	txtCustomHidesAlert.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pCustomHidesAlert);

	// Customize Finds Alert.
	var pCustomFindsAlert = document.createElement('p');
	pCustomFindsAlert.setAttribute('class', 'SettingElement');
	var txtCustomFindsAlert = document.createElement('input');
	txtCustomFindsAlert.id = 'txtCustomFindsAlert';
	txtCustomFindsAlert.maxLength = 3;
	txtCustomFindsAlert.style.width = '3em';
	txtCustomFindsAlert.style.marginRight = '6px';
	txtCustomFindsAlert.title = 'If the submitter has found this number or fewer caches, that number will ' +
								'be highlighted in yellow.';
	var labelCustomFindsAlert = document.createElement('label');
	labelCustomFindsAlert.setAttribute('for', 'txtCustomFindsAlert');
	labelCustomFindsAlert.appendChild(document.createTextNode('Number of owner Finds at or below which an Alert is shown. Zero disables.'));
	labelCustomFindsAlert.title = txtCustomFindsAlert.title;
	var CustomFindsAlert = parseInt(GM_getValue('CustomFindsAlert_' + SignedInAs, '5'));
	txtCustomFindsAlert.value = CustomFindsAlert;
	pCustomFindsAlert.appendChild(txtCustomFindsAlert);
	pCustomFindsAlert.appendChild(labelCustomFindsAlert);
	txtCustomFindsAlert.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pCustomFindsAlert);

	// Use GeoLocation Verification.
	var pCoordinateAlert = document.createElement('p');
	pCoordinateAlert.setAttribute('class', 'SettingElement');
	var cbCoordinateAlert = document.createElement('input');
	cbCoordinateAlert.id = 'cbCoordinateAlert';
	cbCoordinateAlert.type = 'checkbox';
	cbCoordinateAlert.style.marginRight = '6px';
	cbCoordinateAlert.title = 'Use external site to confirm that coordinates are within the ' +
							  'chosen location.';
	var labelCoordinateAlert = document.createElement('label');
	labelCoordinateAlert.setAttribute('for', 'cbCoordinateAlert');
	labelCoordinateAlert.title = cbCoordinateAlert.title;
	labelCoordinateAlert.appendChild(document.createTextNode('Use GeoLocation to validate coordinates.'));
	var CustomCoordinateAlert = GM_getValue('CustomCoordinateAlert_' + SignedInAs, 'On');
	cbCoordinateAlert.checked = (CustomCoordinateAlert == 'On');
	pCoordinateAlert.appendChild(cbCoordinateAlert);
	pCoordinateAlert.appendChild(labelCoordinateAlert);
	divSet.appendChild(pCoordinateAlert);

	// Set Starting Map Type.
	var CustomStartingMapType = GM_getValue('CustomMapType_' + SignedInAs, '0');
	var pStartingMapType = document.createElement('p');
	pStartingMapType.setAttribute('class', 'SettingElement');
	var selStartingMapType = document.createElement('select');
	selStartingMapType.id = 'selStartingMapType';
	selStartingMapType.style.marginRight = '6px';
	selStartingMapType.title = 'Set Starting Map Type';
	var arMapSettingTypes = ['No setting', 'Map Display', 'Satellite Display',
							 'Satellite + Labels Display', 'Map + Terrain Display'];
	for (var i in arMapSettingTypes) {
		var opStartingMapType = document.createElement('option');
		opStartingMapType.value = i;
		opStartingMapType.appendChild(document.createTextNode(arMapSettingTypes[i]));
		opStartingMapType.selected = (parseInt(CustomStartingMapType) == i);
		selStartingMapType.appendChild(opStartingMapType);
	}
	var labelStartingMapType = document.createElement('label');
	labelStartingMapType.setAttribute('for', 'selStartingMapType');
	labelStartingMapType.title = selStartingMapType.title;
	labelStartingMapType.appendChild(document.createTextNode('Initial Google Map type shown on page.'));
	pStartingMapType.appendChild(selStartingMapType);
	pStartingMapType.appendChild(labelStartingMapType);
	divSet.appendChild(pStartingMapType);

	// Set Map Zoom Adjustment.
	var CustomMapZoom = GM_getValue('CustomMapZoom_' + SignedInAs, '0') - 0;
	var pCustomMapZoom = document.createElement('p');
	pCustomMapZoom.setAttribute('class', 'SettingElement');
	var selCustomMapZoom = document.createElement('select');
	selCustomMapZoom.id = 'selCustomMapZoom';
	selCustomMapZoom.style.marginRight = '6px';
	selCustomMapZoom.title = 'Select a positive or negative adjustment number ' +
							 'indicating the number of zoom levels to change from the default. ' +
							 'A positive number zooms in. ' +
							 'A negative number zooms out. ' +
							 'NOTE: This zoom adjustment only occurs on caches with ' +
							 'no Additional Waypoints. Also, there is no zoom ' +
							 'adjustment if your Custom Map Type is set to \'No setting\'.';
	for (var i = 5; i >= -14; i--) {
		var opCustomMapZoom = document.createElement('option');
		var j = i.toString();
		if (i > 0) j = '+' + j;
		opCustomMapZoom.value = i;
		opCustomMapZoom.appendChild(document.createTextNode(j));
		opCustomMapZoom.selected = (CustomMapZoom == i);
		selCustomMapZoom.appendChild(opCustomMapZoom);
	}
	var labelCustomMapZoom = document.createElement('label');
	labelCustomMapZoom.setAttribute('for', 'selCustomMapZoom');
	labelCustomMapZoom.title = selCustomMapZoom.title;
	labelCustomMapZoom.appendChild(document.createTextNode('Google Map zoom adjustment.'));
	pCustomMapZoom.appendChild(selCustomMapZoom);
	pCustomMapZoom.appendChild(labelCustomMapZoom);
	divSet.appendChild(pCustomMapZoom);

	// Alternate Profile Name for Challenge Checker.
	var pAltCCerProfile = document.createElement('p');
	pAltCCerProfile.setAttribute('class', 'SettingElement');
	var txtAltCCerProfile = document.createElement('input');
	txtAltCCerProfile.id = 'txtAltCCerProfile';
	txtAltCCerProfile.style.width = '15em';
	txtAltCCerProfile.style.marginRight = '6px';
	txtAltCCerProfile.title = 'Altername Profile for Challenge Checker.';
	var labelAltCCerProfile = document.createElement('label');
	labelAltCCerProfile.setAttribute('for', 'txtAltCCerProfile');
	labelAltCCerProfile.appendChild(document.createTextNode('Alternate Profile for Challenge Checker (leave blank to use your Reviewer profile).'));
	labelAltCCerProfile.title = txtAltCCerProfile.title;
	var AltCCerProfile = GM_getValue('AltCCerProfile_' + SignedInAs, '');
	txtAltCCerProfile.value = AltCCerProfile;
	pAltCCerProfile.appendChild(txtAltCCerProfile);
	pAltCCerProfile.appendChild(labelAltCCerProfile);
	txtAltCCerProfile.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pAltCCerProfile);

	// Auto close tab on publish.
	var pCustomCloseOnPublish = document.createElement('p');
	pCustomCloseOnPublish.setAttribute('class', 'SettingElement');
	var cbCustomCloseOnPublish = document.createElement('input');
	cbCustomCloseOnPublish.id = 'cbCustomCloseOnPublish';
	cbCustomCloseOnPublish.type = 'checkbox';
	cbCustomCloseOnPublish.style.marginRight = '6px';
	cbCustomCloseOnPublish.title = 'Automatically close tab when published via green button.';
	var labelCustomCloseOnPublish = document.createElement('label');
	labelCustomCloseOnPublish.setAttribute('for', 'cbCustomCloseOnPublish');
	labelCustomCloseOnPublish.title = cbCustomCloseOnPublish.title;
	labelCustomCloseOnPublish.appendChild(document.createTextNode('Automatically close tab when ' +
																  'published via green button.'));
	var CustomCloseOnPublish = GM_getValue('CustomCloseOnPublish_' + SignedInAs, 'On');
	cbCustomCloseOnPublish.checked = (CustomCloseOnPublish == 'On');
	pCustomCloseOnPublish.appendChild(cbCustomCloseOnPublish);
	pCustomCloseOnPublish.appendChild(labelCustomCloseOnPublish);
	divSet.appendChild(pCustomCloseOnPublish);

	// Put Litmus results in browser title.
	var pCustomizeLitmusTitle = document.createElement('p');
	pCustomizeLitmusTitle.setAttribute('class', 'SettingElement');
	var cbCustomizeLitmusTitle = document.createElement('input');
	cbCustomizeLitmusTitle.id = 'cbCustomizeLitmusTitle';
	cbCustomizeLitmusTitle.type = 'checkbox';
	cbCustomizeLitmusTitle.style.marginRight = '6px';
	cbCustomizeLitmusTitle.title = 'Show Litmus results icon in the browser title bar.';
	var labelCustomizeLitmusTitle = document.createElement('label');
	labelCustomizeLitmusTitle.setAttribute('for', 'cbCustomizeLitmusTitle');
	labelCustomizeLitmusTitle.title = cbCustomizeLitmusTitle.title;
	labelCustomizeLitmusTitle.appendChild(document.createTextNode('Show Litmus results icon ' +
																  'in the browser title bar.'));
	var CustomizeLitmusTitle = GM_getValue('CustomizeLitmusTitle_' + SignedInAs, 'On');
	cbCustomizeLitmusTitle.checked = (CustomizeLitmusTitle == 'On');
	pCustomizeLitmusTitle.appendChild(cbCustomizeLitmusTitle);
	pCustomizeLitmusTitle.appendChild(labelCustomizeLitmusTitle);
	divSet.appendChild(pCustomizeLitmusTitle);

	// Show handicap/terrain alert.
	var pCustomTerrainAlert = document.createElement('p');
	pCustomTerrainAlert.setAttribute('class', 'SettingElement');
	var cbCustomTerrainAlert = document.createElement('input');
	cbCustomTerrainAlert.id = 'cbCustomTerrainAlert';
	cbCustomTerrainAlert.type = 'checkbox';
	cbCustomTerrainAlert.style.marginRight = '6px';
	cbCustomTerrainAlert.title = 'Show alert when the Terrain Rating is set ' +
								 'to 1 and no Handicapped attribute has been selected.';
	var labelCustomTerrainAlert = document.createElement('label');
	labelCustomTerrainAlert.setAttribute('for', 'cbCustomTerrainAlert');
	labelCustomTerrainAlert.title = cbCustomTerrainAlert.title;
	labelCustomTerrainAlert.appendChild(document.createTextNode('Show alert when the Terrain Rating ' +
																'is set to 1 and no Handicapped attribute has been selected.'));
	var CustomTerrainAlert = GM_getValue('CustomTerrainAlert_' + SignedInAs, 'On');
	cbCustomTerrainAlert.checked = (CustomTerrainAlert == 'On');
	pCustomTerrainAlert.appendChild(cbCustomTerrainAlert);
	pCustomTerrainAlert.appendChild(labelCustomTerrainAlert);
	divSet.appendChild(pCustomTerrainAlert);

	// Show DD MM SS Usge alert.
	var pDDMMSSAlert = document.createElement('p');
	pDDMMSSAlert.setAttribute('class', 'SettingElement');
	var cbDDMMSSAlert = document.createElement('input');
	cbDDMMSSAlert.id = 'cbDDMMSSAlert';
	cbDDMMSSAlert.type = 'checkbox';
	cbDDMMSSAlert.style.marginRight = '6px';
	cbDDMMSSAlert.title = 'Show alert when DDMMSS Coordinates Usage detected.';
	var labelDDMMSSAlert = document.createElement('label');
	labelDDMMSSAlert.setAttribute('for', 'cbDDMMSSAlert');
	labelDDMMSSAlert.title = cbDDMMSSAlert.title;
	labelDDMMSSAlert.appendChild(document.createTextNode('Show alert when DDMMSS Coordinates Usage detected.'));
	var DDMMSSAlert = GM_getValue('DDMMSSAlert_' + SignedInAs, 'On');
	cbDDMMSSAlert.checked = (DDMMSSAlert == 'On');
	pDDMMSSAlert.appendChild(cbDDMMSSAlert);
	pDDMMSSAlert.appendChild(labelDDMMSSAlert);
	divSet.appendChild(pDDMMSSAlert);

	// Copy icon option to create markdown link instead of plain text.
	var pCopyNameMdLink = document.createElement('p');
	pCopyNameMdLink.setAttribute('class', 'SettingElement');
	var cbCopyNameMdLink = document.createElement('input');
	cbCopyNameMdLink.id = 'cbCopyNameMdLink';
	cbCopyNameMdLink.type = 'checkbox';
	cbCopyNameMdLink.style.marginRight = '6px';
	cbCopyNameMdLink.title = 'Use Markdown Link instead of Plain Text when clicking Litmus Copy icon.';
	var labelCopyNameMdLink = document.createElement('label');
	labelCopyNameMdLink.setAttribute('for', 'cbCopyNameMdLink');
	labelCopyNameMdLink.title = cbCopyNameMdLink.title;
	labelCopyNameMdLink.appendChild(document.createTextNode('Use Markdown Link instead of Plain Text when clicking Copy icon.'));
	var CopyNameMdLink = GM_getValue('CopyNameMdLink_' + SignedInAs, 'On');
	cbCopyNameMdLink.checked = (CopyNameMdLink == 'On');
	pCopyNameMdLink.appendChild(cbCopyNameMdLink);
	pCopyNameMdLink.appendChild(labelCopyNameMdLink);
	divSet.appendChild(pCopyNameMdLink);

	// Copy mode of normal cache copy
	var pNewCopyMode = document.createElement('p');
	pNewCopyMode.setAttribute('class', 'SettingElement');
	var cbNewCopyMode = document.createElement('input');
	cbNewCopyMode.id = 'cbNewCopyMode';
	cbNewCopyMode.type = 'checkbox';
	cbNewCopyMode.style.marginRight = '6px';
	cbNewCopyMode.title = 'Old copy mode: Choice to copy coordinates in various forms.\r\nNew copy mode: choice to copy gc code/cache name as text/markdown and include coordinates in DDD MM.MMM.';
	var labelNewCopyMode = document.createElement('label');
	labelNewCopyMode.setAttribute('for', 'cbNewCopyMode');
	labelNewCopyMode.title = cbNewCopyMode.title;
	labelNewCopyMode.appendChild(document.createTextNode('Use new copy mode (GC-Code, Name, DDD MM.MMM) instead of old copy mode (DD.DDDD in various forms).'));
	var NewCopyMode = GM_getValue('NewCopyMode_' + SignedInAs, 'Off');
	cbNewCopyMode.checked = (NewCopyMode == 'On');
	pNewCopyMode.appendChild(cbNewCopyMode);
	pNewCopyMode.appendChild(labelNewCopyMode);
	divSet.appendChild(pNewCopyMode);

	// Hide ZC and Lit bars if no errors.
	var pHideZCLitBars = document.createElement('p');
	pHideZCLitBars.setAttribute('class', 'SettingElement');
	var cbHideZCLitBars = document.createElement('input');
	cbHideZCLitBars.id = 'cbHideZCLitBars';
	cbHideZCLitBars.type = 'checkbox';
	cbHideZCLitBars.style.marginRight = '6px';
	cbHideZCLitBars.title = 'Hide Zone Catcher and Litmus bars when there are no messages.';
	var labelHideZCLitBars = document.createElement('label');
	labelHideZCLitBars.setAttribute('for', 'cbHideZCLitBars');
	labelHideZCLitBars.title = cbHideZCLitBars.title;
	labelHideZCLitBars.appendChild(document.createTextNode('Hide Zone Catcher and Litmus bars when there are no messages.'));
	var HideZCLitBars = GM_getValue('HideZCLitBars_' + SignedInAs, 'On');
	cbHideZCLitBars.checked = (HideZCLitBars == 'On');
	pHideZCLitBars.appendChild(cbHideZCLitBars);
	pHideZCLitBars.appendChild(labelHideZCLitBars);
	divSet.appendChild(pHideZCLitBars);

	// Auto-open Litmus Results on 'red' error
	var pOpenLitmusOnRed = document.createElement('p');
	pOpenLitmusOnRed.setAttribute('class', 'SettingElement');
	var cbOpenLitmusOnRed = document.createElement('input');
	cbOpenLitmusOnRed.id = 'cbOpenLitmusOnRed';
	cbOpenLitmusOnRed.type = 'checkbox';
	cbOpenLitmusOnRed.style.marginRight = '6px';
	cbOpenLitmusOnRed.title = 'Auto-expand ZoneCatcher results when there is a red (error) result.';
	var labelOpenLitmusOnRed = document.createElement('label');
	labelOpenLitmusOnRed.setAttribute('for', 'cbOpenLitmusOnRed');
	labelOpenLitmusOnRed.title = cbOpenLitmusOnRed.title;
	labelOpenLitmusOnRed.appendChild(document.createTextNode('Auto-expand ZoneCatcher results when there is a red (error) result.'));
	var openLitmusOnRed = GM_getValue('openLitmusOnRed_' + SignedInAs, 'Off');
	cbOpenLitmusOnRed.checked = (openLitmusOnRed == 'On');
	pOpenLitmusOnRed.appendChild(cbOpenLitmusOnRed);
	pOpenLitmusOnRed.appendChild(labelOpenLitmusOnRed);
	divSet.appendChild(pOpenLitmusOnRed);

	// Auto submit & Close Proximity log.
	var pSubmitCloseProximityLog = document.createElement('p');
	pSubmitCloseProximityLog.setAttribute('class', 'SettingElement');
	var cbSubmitCloseProximityLog = document.createElement('input');
	cbSubmitCloseProximityLog.id = 'cbSubmitCloseProximityLog';
	cbSubmitCloseProximityLog.type = 'checkbox';
	cbSubmitCloseProximityLog.style.marginRight = '6px';
	cbSubmitCloseProximityLog.title = 'Auto-Submit and Close Proximity log.';
	var labelSubmitCloseProximityLog = document.createElement('label');
	labelSubmitCloseProximityLog.setAttribute('for', 'cbSubmitCloseProximityLog');
	labelSubmitCloseProximityLog.title = cbSubmitCloseProximityLog.title;
	labelSubmitCloseProximityLog.appendChild(document.createTextNode('Auto-submit and close ' +
																	 'Proximity log.'));
	cbSubmitCloseProximityLog.checked = GM_getValue('SubmitCloseProximityLog_' + SignedInAs, true);
	pSubmitCloseProximityLog.appendChild(cbSubmitCloseProximityLog);
	pSubmitCloseProximityLog.appendChild(labelSubmitCloseProximityLog);
	divSet.appendChild(pSubmitCloseProximityLog);

	// Auto submit & Close Vacation log.
	var pSubmitCloseVacationLog = document.createElement('p');
	pSubmitCloseVacationLog.setAttribute('class', 'SettingElement');
	var cbSubmitCloseVacationLog = document.createElement('input');
	cbSubmitCloseVacationLog.id = 'cbSubmitCloseVacationLog';
	cbSubmitCloseVacationLog.type = 'checkbox';
	cbSubmitCloseVacationLog.style.marginRight = '6px';
	cbSubmitCloseVacationLog.title = 'Auto-Submit and Close Vacation log.';
	var labelSubmitCloseVacationLog = document.createElement('label');
	labelSubmitCloseVacationLog.setAttribute('for', 'cbSubmitCloseVacationLog');
	labelSubmitCloseVacationLog.title = cbSubmitCloseVacationLog.title;
	labelSubmitCloseVacationLog.appendChild(document.createTextNode('Auto-submit and close ' +
																	'Vacation log.'));
	cbSubmitCloseVacationLog.checked = GM_getValue('SubmitCloseVacationLog_' + SignedInAs, true);
	pSubmitCloseVacationLog.appendChild(cbSubmitCloseVacationLog);
	pSubmitCloseVacationLog.appendChild(labelSubmitCloseVacationLog);
	divSet.appendChild(pSubmitCloseVacationLog);

	// Auto Resize Images Wider Than 600 Pixels.
	var pResizeLargeImages = document.createElement('p');
	pResizeLargeImages.setAttribute('class', 'SettingElement');
	var cbResizeLargeImages = document.createElement('input');
	cbResizeLargeImages.id = 'cbResizeLargeImages';
	cbResizeLargeImages.type = 'checkbox';
	cbResizeLargeImages.style.marginRight = '6px';
	cbResizeLargeImages.title = 'Auto-Resize Large Images In Descriptions.';
	var labelResizeLargeImages = document.createElement('label');
	labelResizeLargeImages.setAttribute('for', 'cbResizeLargeImages');
	labelResizeLargeImages.title = cbResizeLargeImages.title;
	labelResizeLargeImages.appendChild(document.createTextNode('Auto-Resize large images in ' +
															   'descriptions.'));
	cbResizeLargeImages.checked = GM_getValue('ResizeLargeImages_' + SignedInAs, true);
	pResizeLargeImages.appendChild(cbResizeLargeImages);
	pResizeLargeImages.appendChild(labelResizeLargeImages);
	divSet.appendChild(pResizeLargeImages);

	// Customize Text for Auto Clean-Up.
	var pAutoCleanUpText = document.createElement('p');
	pAutoCleanUpText.setAttribute('class', 'SettingElement');
	var txtAutoCleanUpText = document.createElement('input');
	txtAutoCleanUpText.id = 'txtAutoCleanUpText';
	txtAutoCleanUpText.style.width = '15em';
	txtAutoCleanUpText.style.marginRight = '6px';
	txtAutoCleanUpText.title = 'Text for Auto Clean Up Archive Log';
	var labelAutoCleanUpText = document.createElement('label');
	labelAutoCleanUpText.setAttribute('for', 'txtAutoCleanUpText');
	labelAutoCleanUpText.appendChild(document.createTextNode('Text for Auto Clean Up Archive Log.'));
	labelAutoCleanUpText.title = txtAutoCleanUpText.title;
	var AutoCleanUpText = GM_getValue('AutoCleanUpText_' + SignedInAs, 'Database clean up.');
	txtAutoCleanUpText.value = AutoCleanUpText;
	pAutoCleanUpText.appendChild(txtAutoCleanUpText);
	pAutoCleanUpText.appendChild(labelAutoCleanUpText);
	txtAutoCleanUpText.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pAutoCleanUpText);

	// Customize Text for Auto Clean-Up - Shifted.
	var pAutoCleanUpTextShift = document.createElement('p');
	pAutoCleanUpTextShift.setAttribute('class', 'SettingElement');
	var txtAutoCleanUpTextShift = document.createElement('input');
	txtAutoCleanUpTextShift.id = 'txtAutoCleanUpTextShift';
	txtAutoCleanUpTextShift.style.width = '15em';
	txtAutoCleanUpTextShift.style.marginRight = '6px';
	txtAutoCleanUpTextShift.title = 'Text for Auto Clean Up Archive Log';
	var labelAutoCleanUpTextShift = document.createElement('label');
	labelAutoCleanUpTextShift.setAttribute('for', 'txtAutoCleanUpTextShift');
	labelAutoCleanUpTextShift.appendChild(document.createTextNode('Alternate Auto Clean Up Log Text (Shift + click).'));
	labelAutoCleanUpTextShift.title = txtAutoCleanUpTextShift.title;
	var AutoCleanUpTextShift = GM_getValue('AutoCleanUpTextShift_' + SignedInAs, 'Duplicate submission. Archived.');
	txtAutoCleanUpTextShift.value = AutoCleanUpTextShift;
	pAutoCleanUpTextShift.appendChild(txtAutoCleanUpTextShift);
	pAutoCleanUpTextShift.appendChild(labelAutoCleanUpTextShift);
	txtAutoCleanUpTextShift.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pAutoCleanUpTextShift);

	// Highlight options for Multi-cache posted coordinates.
	var CoordType_3_Highlight = GM_getValue('CoordType_3_Highlight_' + SignedInAs, '1');
	var pCoordType_3_Highlight = document.createElement('p');
	pCoordType_3_Highlight.setAttribute('class', 'SettingElement');
	var selCoordType_3_Highlight = document.createElement('select');
	selCoordType_3_Highlight.id = 'selCoordType_3_Highlight';
	selCoordType_3_Highlight.style.marginRight = '6px';
	selCoordType_3_Highlight.title = 'Set Alert Option';
	var arHighlight_3_Options = ['No Alert', 'Physical', 'Virtual'];
	var ic = arHighlight_3_Options.length;
	for (var i = 0; i < ic; i++) {
		var opCoordType_3_Highlight = document.createElement('option');
		opCoordType_3_Highlight.value = i;
		opCoordType_3_Highlight.appendChild(document.createTextNode(arHighlight_3_Options[i]));
		opCoordType_3_Highlight.selected = (parseInt(CoordType_3_Highlight) == i);
		selCoordType_3_Highlight.appendChild(opCoordType_3_Highlight);
	}
	var labelCoordType_3_Highlight = document.createElement('label');
	labelCoordType_3_Highlight.setAttribute('for', 'selCoordType_3_Highlight');
	labelCoordType_3_Highlight.title = selCoordType_3_Highlight.title;
	labelCoordType_3_Highlight.appendChild(document.createTextNode('Set posted coordinate type to show alert for Multi-caches.'));
	pCoordType_3_Highlight.appendChild(selCoordType_3_Highlight);
	pCoordType_3_Highlight.appendChild(labelCoordType_3_Highlight);
	divSet.appendChild(pCoordType_3_Highlight);

	// Highlight options for Letterbox Hybrid posted coordinates.
	var CoordType_5_Highlight = GM_getValue('CoordType_5_Highlight_' + SignedInAs, '1');
	var pCoordType_5_Highlight = document.createElement('p');
	pCoordType_5_Highlight.setAttribute('class', 'SettingElement');
	var selCoordType_5_Highlight = document.createElement('select');
	selCoordType_5_Highlight.id = 'selCoordType_5_Highlight';
	selCoordType_5_Highlight.style.marginRight = '6px';
	selCoordType_5_Highlight.title = 'Set Alert Option';
	var arHighlight_5_Options = ['No Alert', 'Physical', 'Virtual'];
	for (var i in arHighlight_5_Options) {
		var opCoordType_5_Highlight = document.createElement('option');
		opCoordType_5_Highlight.value = i;
		opCoordType_5_Highlight.appendChild(document.createTextNode(arHighlight_5_Options[i]));
		opCoordType_5_Highlight.selected = (parseInt(CoordType_5_Highlight) == i);
		selCoordType_5_Highlight.appendChild(opCoordType_5_Highlight);
	}
	var labelCoordType_5_Highlight = document.createElement('label');
	labelCoordType_5_Highlight.setAttribute('for', 'selCoordType_5_Highlight');
	labelCoordType_5_Highlight.title = selCoordType_5_Highlight.title;
	labelCoordType_5_Highlight.appendChild(document.createTextNode('Set posted coordinate type to show alert for Letterbox Hybrid.'));
	pCoordType_5_Highlight.appendChild(selCoordType_5_Highlight);
	pCoordType_5_Highlight.appendChild(labelCoordType_5_Highlight);
	divSet.appendChild(pCoordType_5_Highlight);

	// Highlight options for Mystery Cache posted coordinates.
	var CoordType_8_Highlight = GM_getValue('CoordType_8_Highlight_' + SignedInAs, '1');
	var pCoordType_8_Highlight = document.createElement('p');
	pCoordType_8_Highlight.setAttribute('class', 'SettingElement');
	var selCoordType_8_Highlight = document.createElement('select');
	selCoordType_8_Highlight.id = 'selCoordType_8_Highlight';
	selCoordType_8_Highlight.style.marginRight = '6px';
	selCoordType_8_Highlight.title = 'Set Alert Option';
	var arHighlight_8_Options = ['No Alert', 'Physical', 'Virtual'];
	for (var i in arHighlight_8_Options) {
		var opCoordType_8_Highlight = document.createElement('option');
		opCoordType_8_Highlight.value = i;
		opCoordType_8_Highlight.appendChild(document.createTextNode(arHighlight_8_Options[i]));
		opCoordType_8_Highlight.selected = (parseInt(CoordType_8_Highlight) == i);
		selCoordType_8_Highlight.appendChild(opCoordType_8_Highlight);
	}
	var labelCoordType_8_Highlight = document.createElement('label');
	labelCoordType_8_Highlight.setAttribute('for', 'selCoordType_8_Highlight');
	labelCoordType_8_Highlight.title = selCoordType_8_Highlight.title;
	labelCoordType_8_Highlight.appendChild(document.createTextNode('Set posted coordinate type to show alert for Mystery Cache.'));
	pCoordType_8_Highlight.appendChild(selCoordType_8_Highlight);
	pCoordType_8_Highlight.appendChild(labelCoordType_8_Highlight);
	divSet.appendChild(pCoordType_8_Highlight);

	// Create Save/Cancel Buttons.
	var ds_ButtonsP = document.createElement('div');
	ds_ButtonsP.setAttribute('class', 'SettingButtons');
	var ds_SaveButton = document.createElement('button');
	ds_SaveButton = document.createElement('button');
	ds_SaveButton.appendChild(document.createTextNode("Save"));
	ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
	var ds_CancelButton = document.createElement('button');
	ds_CancelButton.style.marginLeft = '6px';
	ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
	ds_CancelButton.appendChild(document.createTextNode("Cancel"));
	ds_ButtonsP.appendChild(ds_SaveButton);
	ds_ButtonsP.appendChild(ds_CancelButton);
	divSet.appendChild(ds_ButtonsP);

	fOpenSettingsDiv();
}

// Create Greasemonkey Tools menu option to allow user to change settings.
GM_registerMenuCommand('Review Page Settings', fShowSettings);
GM_registerMenuCommand('Change "Directions from Home" Settings...', SetHomeCoordsValues);

// Add menu options to customize radius selections.
GM_registerMenuCommand('Customize Map Radius Settings...', CustomizeRadius);
GM_registerMenuCommand('Customize Map (RR) Radius Settings...', CustomizeRRRadius);
GM_registerMenuCommand('Reset Thumbs-up List', fResetThumbsUp);

function fSaveButtonClicked() {
	// Save Auto-run Litmus.
	var cbAutoRunLitmus = document.getElementById('cbAutoRunLitmus');
	GM_setValue('CustomLitmusAutomatic_' + SignedInAs, (cbAutoRunLitmus.checked)?'On':'Off');

	// Save Litmus Errors Only.
	var cbLitmusErrorsOnly = document.getElementById('cbLitmusErrorsOnly');
	GM_setValue('CustomLitmusErrorsOnly_' + SignedInAs, (cbLitmusErrorsOnly.checked)?'true':'false');

	// Save Distance from Home Alert.
	var txtCustomDistAlert = document.getElementById('txtCustomDistAlert');
	GM_setValue('CustomDistAlert_' + SignedInAs, parseInt(txtCustomDistAlert.value).toString());

	// Save Hides Alert.
	var txtCustomHidesAlert = document.getElementById('txtCustomHidesAlert');
	GM_setValue('CustomHidesAlert_' + SignedInAs, parseInt(txtCustomHidesAlert.value).toString());

	// Save Finds Alert.
	var txtCustomFindsAlert = document.getElementById('txtCustomFindsAlert');
	GM_setValue('CustomFindsAlert_' + SignedInAs, parseInt(txtCustomFindsAlert.value).toString());

	// Save GeoLocation Validation.
	var cbCoordinateAlert = document.getElementById('cbCoordinateAlert');
	GM_setValue('CustomCoordinateAlert_' + SignedInAs, (cbCoordinateAlert.checked)?'On':'Off');

	// Save Map Type.
	var selStartingMapType = document.getElementById('selStartingMapType');
	GM_setValue('CustomMapType_' + SignedInAs, parseInt(selStartingMapType.value));

	// Save Map Zoom.
	var selCustomMapZoom = document.getElementById('selCustomMapZoom');
	GM_setValue('CustomMapZoom_' + SignedInAs, selCustomMapZoom.value - 0);

	// Save Alternate Challenge Checker Profile name.
	var txtAltCCerProfile = document.getElementById('txtAltCCerProfile');
	GM_setValue('AltCCerProfile_' + SignedInAs, txtAltCCerProfile.value.trim());

	// Close tab on publish.
	var cbCustomCloseOnPublish = document.getElementById('cbCustomCloseOnPublish');
	GM_setValue('CustomCloseOnPublish_' + SignedInAs, (cbCustomCloseOnPublish.checked)?'On':'Off');

	// Show Litmus Results icon in browser title.
	var cbCustomizeLitmusTitle = document.getElementById('cbCustomizeLitmusTitle');
	GM_setValue('CustomizeLitmusTitle_' + SignedInAs, (cbCustomizeLitmusTitle.checked)?'On':'Off');

	// Show DDMMSS Coordinate Usage alert.
	var cbDDMMSSAlert = document.getElementById('cbDDMMSSAlert');
	GM_setValue('DDMMSSAlert_' + SignedInAs, (cbDDMMSSAlert.checked)?'On':'Off');

	// Copy cache name as markdown link instead of plain text.
	var cbCopyNameMdLink = document.getElementById('cbCopyNameMdLink');
	GM_setValue('CopyNameMdLink_' + SignedInAs, (cbCopyNameMdLink.checked)?'On':'Off');

	// Copy mode of normal cache copy
	var cbNewCopyMode = document.getElementById('cbNewCopyMode');
	GM_setValue('NewCopyMode_' + SignedInAs, (cbNewCopyMode.checked)?'On':'Off');

	// Hide Zone Catcher and Litmus bars when there are no messages.
	var cbHideZCLitBars = document.getElementById('cbHideZCLitBars');
	GM_setValue('HideZCLitBars_' + SignedInAs, (cbHideZCLitBars.checked)?'On':'Off');

	// insert openLitmusOnRed here
	var cbOpenLitmusOnRed = document.getElementById('cbOpenLitmusOnRed');
	GM_setValue('openLitmusOnRed_' + SignedInAs, (cbOpenLitmusOnRed.checked)?'On':'Off');

	// Show Terrain/Handicap alert.
	var cbCustomTerrainAlert = document.getElementById('cbCustomTerrainAlert');
	GM_setValue('CustomTerrainAlert_' + SignedInAs, (cbCustomTerrainAlert.checked)?'On':'Off');

	// Close/Submit Proximity log.
	var cbSubmitCloseProximityLog = document.getElementById('cbSubmitCloseProximityLog');
	GM_setValue('SubmitCloseProximityLog_' + SignedInAs, cbSubmitCloseProximityLog.checked);

	// Close/Submit Vacation log.
	var cbSubmitCloseVacationLog = document.getElementById('cbSubmitCloseVacationLog');
	GM_setValue('SubmitCloseVacationLog_' + SignedInAs, cbSubmitCloseVacationLog.checked);

	// Resize Large Images.
	var cbResizeLargeImages = document.getElementById('cbResizeLargeImages');
	GM_setValue('ResizeLargeImages_' + SignedInAs, cbResizeLargeImages.checked);

	// Save Auto Cleanup Text.
	var txtAutoCleanUpText = document.getElementById('txtAutoCleanUpText');
	GM_setValue('AutoCleanUpText_' + SignedInAs, txtAutoCleanUpText.value.trim());

	// Save Auto Cleanup Text - Shifted.
	var txtAutoCleanUpTextShift = document.getElementById('txtAutoCleanUpTextShift');
	GM_setValue('AutoCleanUpTextShift__' + SignedInAs, txtAutoCleanUpTextShift.value.trim());

	// Save Alert Setting for Multi-cache.
	var selCoordType_3_Highlight = document.getElementById('selCoordType_3_Highlight');
	GM_setValue('CoordType_3_Highlight_' + SignedInAs, parseInt(selCoordType_3_Highlight.value));

	// Save Alert Setting for Letterbox Hybrid.
	var selCoordType_5_Highlight = document.getElementById('selCoordType_5_Highlight');
	GM_setValue('CoordType_5_Highlight_' + SignedInAs, parseInt(selCoordType_5_Highlight.value));

	// Save Alert Setting for Mystery Caches.
	var selCoordType_8_Highlight = document.getElementById('selCoordType_8_Highlight');
	GM_setValue('CoordType_8_Highlight_' + SignedInAs, parseInt(selCoordType_8_Highlight.value));

	fCloseSettingsDiv();
}

function fCancelButtonClicked() {
	var resp = confirm('Any changes will be lost. Close Settings?');
	if (resp) {
		fCloseSettingsDiv();
	}
}

function fSelectAllText() {
	this.select();
}

// Close the tab if th egreen Publish button is pressed (depending on options)
if(GM_getValue('CustomCloseOnPublish_' + SignedInAs, 'On') == 'On') {
	if (e_btnPublish) {
		e_btnPublish.addEventListener("click", function(){sessionStorage.setItem('autoclose', 'true');});
	}
}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

// Return true or false, if private note changed since last save.
function fNoteTextChanged() {
	var privTextarea = document.getElementById("private_notes");
	var chkRevNoteHash = calcSHA1(privTextarea.value.trim());
	return (chkRevNoteHash != revNoteHash);
}

// When private note saved, update note hash value.
function fSaveRevNoteButtonClicked() {
	var privTextarea = document.getElementById("private_notes");
	revNoteHash = calcSHA1(privTextarea.value.trim());
}

// Insert time stamp when icon clicked.
function fInsertTimestamp() {
	var imgTimestamp = document.getElementById("imgTimestamp");
	if (imgTimestamp.classList.contains('timestampDisabled')) { return; }
	var timeDateSig = ' ~~' + ' ' + fTimeStamp() + ' ';
	var privTextarea = document.getElementById("private_notes");
	if (privTextarea) {
		ReplaceSelText(privTextarea, timeDateSig, true);
	}
}

// Insert time stamp when icon clicked.
function fInsertTimestampPub() {
	var imgPubTimestamp = document.getElementById("imgPubTimestamp");
	if (imgPubTimestamp.classList.contains('timestampDisabled')) { return; }
	var timeDateSig = ' ~~' + SignedInAs + ' ' + fTimeStamp() + ' ';
	var pubTextarea = document.getElementById("shared_notes");
	if (pubTextarea) {
		ReplaceSelText(pubTextarea, timeDateSig, true);
	}
}

// Replace selected text. Pass text object, new text, and t/f append to end
// if no selection. Default (false) is to append to start.
function ReplaceSelText(obj, newText, appendToEnd) {
	var len = obj.value.length;
	var start = obj.selectionStart;
	var end = obj.selectionEnd;
	if (appendToEnd) {
		if (start == 0 && end == 0) {
			start = len - 1;
			end = start;
		}
	}
	obj.value = obj.value.substring(0,start) + newText +
				obj.value.substring(end, len);
}

// Processing storage change.
function fStorageChanged(e) {
	if ((e.key == REV_PAGE_POS_KEY) && e.newValue.length) {
		if (e.newValue == '~publish~') {
			e_btnPublish.click();
		} else {
			var pageUrl = location.href;
			if (location.hash.length) {
				var RegEx1 = new RegExp(location.hash + '$', 'gi');
				pageUrl = pageUrl.replace(RegEx1, '');
			}
			pageUrl += e.newValue;
			location.replace(pageUrl);
		}
	}
}

// Resize images > 600px.
function fResizeImages() {
	fResizeIn('ctl00_ContentBody_CacheDetails_ShortDesc');
	fResizeIn('ctl00_ContentBody_CacheDetails_LongDesc');
	function fResizeIn(domId) {
		var domElem = document.getElementById(domId);
		if (domElem) {
			// Get list of images.
			var imgs = document.evaluate(".//img", domElem, null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
			// Loop through list.
			var ic = imgs.snapshotLength;
			for (var i = 0; i < ic; i++) {
				var pix = imgs.snapshotItem(i);
				var pixWid = pix.width - 0 ;
				if (pixWid > 600) {
					pix.removeAttribute('height');
					pix.removeAttribute('width');
					pix.classList.add("gm-img-resizer");
				}
			}
		}
	}
}

// Replace Navigation Icons.
function fReplaceNavIcon(id, imgsrc) {
	var imgx = document.getElementById(id);
	if (imgx) {
		while (imgx.nodeName != 'IMG') { imgx = imgx.previousSibling; }
		imgx.src = imgsrc;
		imgx.style.verticalAlign = 'text-bottom';
		return imgx;
	}
}

// Customize Radius Settings
function CustomizeRadius() {
	var xRadList = GM_getValue('CustomRadius_' + SignedInAs, '');
	var xRtnVar = prompt('Enter radius codes. Separate each code group with a semicolon.\n' +
						 'You must enter distance and unit, and may default the rest.\n\n' +
						 'Set to blank to restore original default values.', xRadList);
	if (xRtnVar == null) { return; }
	xRtnVar = xRtnVar.replace(/\s/g,'');
	GM_setValue('CustomRadius_' + SignedInAs, xRtnVar);
	alert('You will need to refresh any open pages for the change \n' +
		  'to take effect.');
}

// Customize (RR) Radius Settings
function CustomizeRRRadius() {
	var xRadList = GM_getValue('CustomRRRadius_' + SignedInAs, '');
	var xRtnVar = prompt('Enter radius codes. Separate each code group with a semicolon.\n' +
						 'You must enter distance and unit, and may default the rest.\n\n' +
						 'Set to blank to restore original default values.', xRadList);
	if (xRtnVar == null) { return; }
	xRtnVar = xRtnVar.replace(/\s/g,'');
	GM_setValue('CustomRRRadius_' + SignedInAs, xRtnVar);
	alert('You will need to refresh any open pages for the change \n' +
		  'to take effect.');
}

// Set Home Coordinates for auto driving directions.
function GetHomeCoordsValues() {
	HomeLabel = GM_getValue('HomeLabel', '').trim();
	if (HomeLabel == '') {
		HomeDdLat = null;
		HomeDdLon = null;
		return;
	}
	HomeDdLat = GM_getValue('HomeDdLat', null);
	HomeDdLon = GM_getValue('HomeDdLon', null);
}

function SetHomeCoordsValues() {

	HomeLabel = GM_getValue('HomeLabel', '');
	if (HomeLabel == null || HomeLabel == '') {HomeLabel = 'My Home';}
	var RtnVar = prompt('Enter label for starting point.\nDirections From...\n\n' +
						'(Leave blank to turn off)', HomeLabel);
	if (RtnVar == null) { return; }
	HomeLabel = RtnVar;
	GM_setValue('HomeLabel', HomeLabel);
	if (RtnVar == '') {
		alert('Option will be turned off');
		HomeLable = '';
		return;
	}

	if (HomeDdLat == null) {HomeDdLat = 'N 00 00.000';}
	var PromptText = 'Enter the LATITUDE for ' + HomeLabel + '.\nCoordinates may be in signed ' +
					 'decimal format,\nor degree decimal-minutes format.';
	var RtnVar = prompt(PromptText, HomeDdLat);
	if ((RtnVar == '') || (RtnVar == null)) {
		alert('Operation canceled, or entry blank.\nSelect menu option again to restart');
		return;
	} else {
		HomeDdLat = RtnVar;
		GM_setValue('HomeDdLat', HomeDdLat);
	}

	if (HomeDdLon == null) {HomeDdLon = 'W 000 00.000';}
	var PromptText = 'Enter the LONGITUDE for ' + HomeLabel + '.\nCoordinates must be in ' +
					 'the same format\nused for the LATITUDE.';
	var RtnVar = prompt(PromptText, HomeDdLon);
	if ((RtnVar == '') || (RtnVar == null)) {
		alert('Operation canceled, or entry blank.\nSelect menu option again to restart');
		return;
	} else {
		HomeDdLon = RtnVar;
		GM_setValue('HomeDdLon', HomeDdLon);
	}

}

// Refresh Owner Note Data.
function RefreshOwnerNoteData() {
	GM_xmlhttpRequest( {
		method:"GET",
		url:onReqUrl,
		onload:function(result) {
			NewOwnerNote = result.responseText;
			// Update owner note text.
			if (NewOwnerNote.substring(0,1) == "_") {
				e_OwnerNoteTextArea.value = NewOwnerNote.substring(1);
			} else if (NewOwnerNote == "%") {
				e_OwnerNoteTextArea.value = '';
			}
		}
	});

}

// Convert Waypoint to ID number.
function WptToID(Wpt) {
	var BASE_GC = "0123456789ABCDEFGHJKMNPQRTVWXYZ";
	var BASE_31 = "0123456789ABCDEFGHIJKLMNOPQRSTU";
	var WptWork, Wpt31 = '', idNum;
	// Strip off leading 'GC'.
	WptWork = Wpt.replace(/^GC/,'');
	// If old-style base-16 value.
	if ((WptWork.length <= 4) && (WptWork.search(/^(\d|[A-F]).*/)) >= 0) {
		idNum = parseInt(WptWork, 16);
		// If new-style base-31 value.
	} else {
		for (var i = 0; i < WptWork.length; i++) {
			Wpt31 += BASE_31.substr(BASE_GC.indexOf(WptWork.substr(i, 1)), 1);
		}
		idNum = parseInt(Wpt31, 31) - 411120;
	}
	return idNum;
}

// Returns a URL parameter.
//  ParmName - Parameter name to look for.
//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
//  UrlString - (optional) String to search. If omitted, document URL is used.
function UrlParm(ParmName, IgnoreCase, UrlString) {
	var RegRslt, sc = '', RtnVal = '';
	if (IgnoreCase) {sc = 'i';}
	if(UrlString) {
		var PageUrl = UrlString;
	} else {
		PageUrl = document.location + '';
	}
	var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
	var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
	RegRslt = RegEx1.exec(ParmString);
	if (RegRslt) {RtnVal = RegRslt[2];}
	return RtnVal;
}

// Function to add days to a given date object. Returns a date object.
function addDays(startDate, numberOfDays) {
	var returnDate = new Date(
	startDate.getFullYear(),
	startDate.getMonth(),
	startDate.getDate() + numberOfDays,
	startDate.getHours(),
	startDate.getMinutes(),
	startDate.getSeconds()
	);
	return returnDate;
}

// If Puzzle, Multi, or Wherigo, varify Final Waypoint.
function fVerifyFinalWaypoint() {
	var finalWpFound = 0;
	var cd_cachetypeid = CacheData.getAttribute('data-cachetypeid');
	if (cd_cachetypeid == TYPE_MULTI_CACHE || cd_cachetypeid == TYPE_UNKNOWN ||
		cd_cachetypeid == TYPE_WHERIGO || cd_cachetypeid == TYPE_LETTERBOX) {
		var cd_wp_count = CacheData.getAttribute('data-wp_count') - 0;
		for (var i = 0; i < cd_wp_count; i++) {
			var wpType = CacheData.getAttribute('data-wp_' + i.toString() + '_type');
			if (wpType == '220') {
				var finalLat = CacheData.getAttribute('data-wp_' + i.toString() + '_latitude') - 0;
				var finalLon = CacheData.getAttribute('data-wp_' + i.toString() + '_longitude') - 0;
				finalWpFound++;
			}
		}
		if (!finalWpFound) {
			fShowAlert('No Final Waypoint Entered');
			finalWpErr = true;
		} else if (finalWpFound > 1) {
			fShowAlert('Multiple Final Waypoints');
			finalWpErr = true;
		} else if (cd_cachetypeid == TYPE_UNKNOWN) {
			if (fDistCalc(CoordDdLat, CoordDdLon, finalLat, finalLon, 'mi') >= 2.11) {
				fShowAlert('Final Too Far From Posted Coordinates');
			}
		}
	}
}

// Show alert warning.
function fShowAlert(alertText) {
	var cdl = document.getElementsByClassName('CacheDetailsList')[0];
	var divNFP = document.getElementById('divNFP');
	if (!divNFP) {
		divNFP = document.createElement('div');
		divNFP.id = 'divNFP';
		insertAheadOf(divNFP, cdl);
		divNFP.classList.add('nfw-warn');
		divNFP.classList.add('dropshadow_se');
	} else {
		divNFP.appendChild(document.createElement('br'));
	}
	var imgNFP = document.createElement('img');
	imgNFP.src = GeneralWarningImg;
	imgNFP.style.marginRight = '12px';
	divNFP.appendChild(imgNFP);
	divNFP.appendChild(document.createTextNode(alertText));
}

// Great Circle distance calculation. Distance returned in specified units.
function fDistCalc(lat1, lon1, lat2, lon2, units) {
	switch(units) {
	case 'mi':     var erad = 3956.665;        break;
	case 'ft':     var erad = 20891191.2;      break;
	case 'm':      var erad = 6367635.08;      break;
	case 'km':     var erad = 6367.63508;      break;
	}
	// Convert to radians.
	lat1 = lat1 / 180 * Math.PI;
	lat2 = lat2 / 180 * Math.PI;
	var lonDelta = (lon2 - lon1) / 180 * Math.PI;

	// Base calculation.
	var gcWork = Math.acos(
	(Math.sin(lat1) * Math.sin(lat2)) +
	(Math.cos(lat1) * Math.cos(lat2) * Math.cos(lonDelta))
	);
	var dist = erad * gcWork;
	return dist;
}

// convert old acceptable locations to new format
// old format is 'cspRegion, cspCountry=geonamesRegion, geonamesCountry'
// new format is 'cspRegion, cspCountry<<< peliasRegion, peliasCountry'
function fConvertRegionOverride(){
	var rgnPairs = GM_getValue('RegionOverride', '');
	rgnPairs = rgnPairs.replace(/=/g,"<<<< ");
	GM_setValue('RegionOverride', rgnPairs);
}

// Is reverse geocoding check overridden for this location? 
function fCheckRegionOverride() {
	var rgnPairs = GM_getValue('RegionOverrideHQRegions', '');
	return (!(rgnPairs.indexOf(override)<0));
}

// Add a feature to reset the Thumbs Up list
function fResetThumbsUp() {
	if (confirm('Are you sure you want to remove all prior "thumbs up" regions?'))
		GM_setValue('RegionOverrideHQRegions', '');
}

// Add this location/reverseGeolocation pair to the list of overrides, if necessary
function fAddRegionOverride() {
	if (fCheckRegionOverride()) {
		alert('This has already been added to the exceptions list.');
	}
	else {
		var resp = confirm('Consider ' + e_CacheDetails_Location.firstChild.data + ' an acceptable alternative for\n' +
						   e_CacheDetails_LocationError.firstChild.data + ' from now on?');
		if (!resp) {
			alert('Action canceled');
			return;
		}
		var rgnPairs = GM_getValue('RegionOverrideHQRegions', '');
		rgnPairs += "?" + override;
		GM_setValue('RegionOverrideHQRegions', rgnPairs);
		googleTagLogRP('Override-'+override);
		// make alert go away
		e_CacheDetails_Location.style.color = 'green';
		e_CacheDetails_LocationError.style.visibility = 'hidden';
		addRgnOvr.style.visibility = 'hidden';
	}
}

// Set drop-down to Hold/Remove Hold, and click button.
function fHoldCache() {
	e_btnHold.click();
}

// Set drop-down to Hold/Remove Hold, and click button.
function fWatchCache() {
	e_btnWatch.click();
}

// Set drop-down to Publish, and click button.
function fPublishCache() {
	e_btnPublish.click();
	if (GM_getValue('CustomCloseOnPublish_' + SignedInAs, 'On') == 'On') {
		sessionStorage.setItem('autoclose', 'true');
	}
}

// Convert all non alphameric text to HTML entities.
function toFullEntity(aa) {
	var i = 0;
	var bb = '';
	var Xmatch = new RegExp('[a-z 0-9]', 'i');
	for (i=0; i<aa.length; i++) {
		if (aa.charAt(i).match(Xmatch)) {
			bb += aa.charAt(i);
		} else {
			bb += '&#' + aa.charCodeAt(i) + ';';
		}
	}
	return bb;
}

// Process click of Prox# link.
function fProxNote() {
	var SubmitCloseProximityLog = GM_getValue('SubmitCloseProximityLog_' + SignedInAs, true);
	var tWptId  = this.getAttribute('ncWptId');
	var tTitle  = this.getAttribute('ncTitle');
	var tDistFt = this.getAttribute('ncDistFt');
	var tDistM  = this.getAttribute('ncDistM');
	var tDir    = this.getAttribute('ncDir');

	var autoUrl = domain + "/live/geocache/"+ WptID + "/log" + "?LogType=prox" +
				  '&owner='    + encodeURIComponent(OwnerName) +
				  '&title='    + encodeURIComponent(CacheName.trim()) +
				  '&wptid='    + encodeURIComponent(WptID) +
				  '&coords='   + encodeURIComponent(txCoords) +
				  '&nctitle='  + encodeURIComponent(tTitle.trim()) +
				  '&ncwptid='  + encodeURIComponent(tWptId) +
				  '&ncdistft=' + encodeURIComponent(tDistFt) +
				  '&ncdistm='  + encodeURIComponent(tDistM) +
				  '&ncdir='    + encodeURIComponent(tDir);
	if (SubmitCloseProximityLog) { autoUrl += '&auto=y&close=y'; }
	GM_openInTab(autoUrl);  //remediate tm5 tab-spawning
//todo	setClickLink(autoUrl,'proxURL');
//	document.getElementById('proxURL').click();
}

// Process click of Home# link.
function fHomeNote() {
	var autoUrl = domain + "/live/geocache/"+ WptID + "/log" + "?LogType=home" +
				  '&owner='    + encodeURIComponent(OwnerName) +
				  '&title='    + encodeURIComponent(CacheName.trim()) +
				  '&wptid='    + encodeURIComponent(WptID) +
				  '&coords='   + encodeURIComponent(txCoords);
	GM_openInTab(autoUrl);//remediate tm5 tab-spawning
//todo	setClickLink(autoUrl,'homeURL');
//	document.getElementById('homeURL').click();
}

// Process click of Vaca# link.
function fVacaNote() {
	var SubmitCloseVacationLog = GM_getValue('SubmitCloseVacationLog_' + SignedInAs, true);
	var vDist = this.getAttribute('DistVal', 0);
	var vDist = vDist - 0;

	var vDistMi = (vDist + 2.5) / 5;
	vDistMi = Math.floor(vDistMi) * 5;

	var vDistKm = (vDist * 1.609344 + 2.5) / 5;
	vDistKm = Math.floor(vDistKm) * 5;

	var autoUrl = domain + "/live/geocache/"+ WptID + "/log" + "?LogType=vaca" +
				  '&owner='    + encodeURIComponent(OwnerName) +
				  '&title='    + encodeURIComponent(CacheName.trim()) +
				  '&wptid='    + encodeURIComponent(WptID) +
				  '&coords='   + encodeURIComponent(txCoords) +
				  '&vdistmi='  + encodeURIComponent(vDistMi) +
				  '&vdistkm='  + encodeURIComponent(vDistKm);
	if (SubmitCloseVacationLog) { autoUrl += '&auto=y&close=y'; }
	console.log(autoUrl);
	GM_openInTab(autoUrl);  //remediate tm5 tab-spawning
	//todo setClickLink(autoUrl,'vacaURL');
	//document.getElementById('vacaURL').click();
}

// Process click of Litmus Error link.  This function is never called and appears unimplemented
function fLitmusErrorNote_1() {
	var eRow       = fUpGenToType(this, 'TR');
	var tWptId     = eRow.cells[2].firstChild.firstChild.data.trim();
	var tTitle     = eRow.cells[3].firstChild;
	while (tTitle.nodeName != '#text') { tTitle = tTitle.firstChild; }
	tTitle         = tTitle.data.trim();
	var tCtype     = eRow.cells[4].firstChild.alt.trim();
	var tDistRaw   = eRow.cells[1].lastChild.data.trim();
	if (tDistRaw != 'Here') {
		var tDist      = tDistRaw.replace(/\D/g,'') - 0;

		var tDistFt    = (tDist + 2.5) / 5;
		tDistFt        = Math.floor(tDistFt) * 5;

		var tDistFtFuz = (fFuzzy(55, 80, tDist) + 2.5) / 5;
		tDistFtFuz     = 'about ' + Math.floor(tDistFtFuz) * 5 + ' feet';
		tDistFtFuz     = 'about ' + tDistFtFuz.toString() + ' feet';

		var tDistM     = Math.floor(tDist * 0.3048 + .5);
		var tDistMFuz  = Math.floor((tDistFtFuz * 0.3048 + 2.5) / 5 );
		tDistMFuz      = Math.floor(tDistMFuz) * 5;
		tDistMFuz      = 'about ' + tDistMFuz.toString() + 'm';
		var tDir       = eRow.cells[1].firstChild.alt.trim();
	} else {
		tDist = '0';
		tDistFtFuz = 'less than 75 feet';
		tDistMFuz = 'less than 50m';
		tDir = '';
	}

	var resp = prompt('Distance value:', tDistFtFuz);
	if (resp != null && resp != "") { tDistFtFuz = resp; }

	var autoUrl = domain + "/live/geocache/"+ WptID + "/log" + "?LogType=lit1" +
				  '&owner='       + encodeURIComponent(OwnerName) +
				  '&title='       + encodeURIComponent(CacheName.trim()) +
				  '&wptid='       + encodeURIComponent(WptID) +
				  '&coords='      + encodeURIComponent(txCoords) +
				  '&nctitle='     + encodeURIComponent(tTitle.trim()) +
				  '&nctype='      + encodeURIComponent(tCtype) +
				  '&ncwptid='     + encodeURIComponent(tWptId) +
				  '&ncdistft='    + encodeURIComponent(tDistFt) +
				  '&ncdistftfuz=' + encodeURIComponent(tDistFtFuz) +
				  '&ncdistm='     + encodeURIComponent(tDistM) +
				  '&ncdistmfuz='  + encodeURIComponent(tDistMFuz) +
				  '&ncdir='       + encodeURIComponent(tDir);
	GM_openInTab(autoUrl); //remediate tm5 tab-spawning
//todo	setClickLink(autoUrl,'litmusURL');
//	document.getElementById('litmusURL').click();
}

// Fuzzy-ize a numeric value. Pass low percent, high percent, and value to fuzz.
// Exmple: fFuzzy(60, 85, 123) will return a random whole value between 60% and 85% of 123.
function fFuzzy(lo, hi, num) {
	var delta = Math.round(hi - lo);
	var r = Math.floor(Math.random() * (delta + 1));
	var fuzzy = Math.round(num * (lo + r) / 100);
	return fuzzy;
}

function fAdjustMap(j, k) {
	// Value for j = +/- zoom level.
	// Values for k (map type):
	// 0 - No setting (uses last map type, or Terrain if no last type). No zooming performed.
	// 1 - Map Display.
	// 2 - Satellite Display.
	// 3 - Hybrid Display.
	// 4 - Terrain Display.

	// Find map canvas.
	var mapcanvas = document.getElementById("map_canvas");
	if (mapcanvas) {
		if (k != 0) {

			function fAdjustMapType() {
				// If Map or Terrain type.
				if (k == 1 || k == 4) {
					var mapMapDiv = document.evaluate(".//div[(text() = 'Map')]",
					mapcanvas, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
					if (mapMapDiv) {
						clearInterval(m1);
						var evObjm1 = document.createEvent('MouseEvents');
						evObjm1.initEvent('mouseup', true, true);
						mapMapDiv.dispatchEvent(evObjm1);

						var mapTerrainLbl = document.evaluate(".//label[(text() = 'Terrain')]",
						mapcanvas, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
						if (mapTerrainLbl) {
							var mapTerrainChx = mapTerrainLbl.previousSibling.firstChild;
							var mapTerrainChxed = (mapTerrainChx.style.display != 'none');
							if ((mapTerrainChxed && k == 1) ||
								(!mapTerrainChxed && k == 4)) {
								var evObjm1 = document.createEvent('MouseEvents');
								evObjm1.initEvent('click', true, true);
								mapTerrainChx.dispatchEvent(evObjm1);
							}
						}
						fAdjustMapZoom(j);
					}
				}

				// If Satellite or Hybrid type.
				if (k == 2 || k == 3) {
					var mapSatDiv = document.evaluate(".//div[(text() = 'Satellite')]",
					mapcanvas, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
					if (mapSatDiv) {
						clearInterval(m1);
						var evObjm1 = document.createEvent('MouseEvents');
						evObjm1.initEvent('mouseup', true, true);
						mapSatDiv.dispatchEvent(evObjm1);

						var mapHybridLbl = document.evaluate(".//label[(text() = 'Labels')]",
						mapcanvas, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
						if (mapHybridLbl) {
							var  mapHybridChx = mapHybridLbl.parentNode.firstChild;
							if ((mapHybridChx.checked && k == 2) ||
								(!mapHybridChx.checked && k == 3)) {
								var evObjm1 = document.createEvent('MouseEvents');
								evObjm1.initEvent('click', true, true);
								mapHybridChx.dispatchEvent(evObjm1);
							}
						}
						fAdjustMapZoom(j);
					}
				}
			}

			var m1 = setInterval(fAdjustMapType, 200);
		}

		// Change map zoom level if no additional waypoints.
		function fAdjustMapZoom(j) {
			if (j != 0) {
				if (!document.getElementById("ctl00_ContentBody_Waypoints_btnDelete")) {
					if (j >= 0) {
						sZoomTitle = 'Zoom in';
					} else {
						sZoomTitle = 'Zoom out';
					}
					j = Math.floor(Math.abs(j));
					var i = 0;
					var evObj = document.createEvent('MouseEvents');
					function fAdjustMap_FindCtrl() {
						var ctrlX = document.evaluate(".//div[@title='" + sZoomTitle + "']",
						mapcanvas, null, XPathResult.FIRST_ORDERED_NODE_TYPE,
						null).singleNodeValue;
						if (ctrlX) {
							clearInterval(t6);
							function fAdjustMap_ClickCtrl() {
								if (i < j) {
									evObj.initEvent('click', true, true);
									ctrlX.dispatchEvent(evObj);
									i++;
								} else {
									clearInterval(t7);
								}
							}
							var t7 = setInterval(fAdjustMap_ClickCtrl, 150);
						}
					}
					var t6 = setInterval(fAdjustMap_FindCtrl, 200);
				}
			}
		}
	}
}

// Return number of regular notes posted to cache.
function fNoteCount() {
	var PathSearch = "//img[@src='/images/logtypes/4.png' and @alt='Write note']";
	var notesQry = document.evaluate(PathSearch, document, null,
	XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	var nc = notesQry.snapshotLength;
	return nc;
}

// Fade-out log's rows that have been deleted via the delete-log link.
function fFadeOutLogText() {
	// Note: rowIndex returns the 1-based row position, not a true 0-based index.
	// When used as a "rows" index, it's actually 1 more than the current row.
	var curRow = fUpGenToType(this, 'TR');
	if (curRow) {
		curRow.style.opacity = '0.25';
		curRow.parentNode.rows[curRow.rowIndex].style.opacity = '0.25';
	}
}

// Convert numeric cache type code to cache type name.
function fCacheCode2Name(cCode) {
	var cCode;
	var cName;
	var cCodeD = cCode - 0;
	switch(cCodeD) {
	case TYPE_TRADITIONAL : cName = 'Traditional Cache';        break;
	case TYPE_MULTI_CACHE : cName = 'Multi-cache';              break;
	case TYPE_VIRTUAL     : cName = 'Virtual Cache';            break;
	case TYPE_LETTERBOX   : cName = 'Letterbox Hybrid';         break;
	case TYPE_EVENT       : cName = 'Event Cache';              break;
	case TYPE_UNKNOWN     : cName = 'Unknown Cache';            break;
	case TYPE_WEBCAME     : cName = 'Webcam Cache';             break;
	case TYPE_CITO        : cName = 'Cache In Trash Out Event'; break;
	case TYPE_EARTHCACHE  : cName = 'Earthcache';               break;
	case TYPE_MEGA_EVENT  : cName = 'Mega-Event Cache';         break;
	case TYPE_WHERIGO     : cName = 'Wherigo Cache';            break;
	case TYPE_COMMUNITY_EVENT : cName = 'Community Celebration Event'; break;
	case TYPE_BLOCK_PARTY : cName = 'Geocaching HQ Block Party'; break;
	case TYPE_HQ_CACHE : cName = 'Geocaching HQ Cache'; break;
	case TYPE_HQ_CELEBRATION : cName = 'Geocaching HQ Celebration'; break;
	default: cName = 'Cache Type Undefined in Script'; break;
	}
	return cName;
}

// Returns a local time in YYYY-MM-DD HH:MM:SS (24 hour) style date/time stamp.
// NOTE: Requires the fPad function.
function fTimeStamp() {
	var d = new Date();
	return d.getFullYear() + '-' +
		   fPad(d.getMonth() + 1, 2) + '-' +
		   fPad(d.getDate(), 2) + ' ' +
		   fPad(d.getHours(), 2) + ':' +
		   fPad(d.getMinutes(), 2) + ':' +
		   fPad(d.getSeconds(), 2);
}

// Pad a number with leading zeros.
function fPad(number, length) {
	var str = "" + number;
	while(str.length < length) {
		str = '0' + str;
	}
	return str;
}

// Removes accents, spaces, non-alpha characters, and converts to lower case.
function accentsTidy(s) {
	var r = s.toLowerCase();
	r = r.replace(new RegExp("\\s", 'g'),"");

	// Latin-1 Supplement Unicode conversion.
	// Lower case conversion works on all Latin-1 characters; no need to convert upper case.
	r = r.replace(new RegExp("[\\u00E0-\\u00E5]"       , 'g'),"a");
	r = r.replace(new RegExp("[\\u00E6]"               , 'g'),"ae");
	r = r.replace(new RegExp("[\\u00E7]"               , 'g'),"c");
	r = r.replace(new RegExp("[\\u00E8-\\u00EB]"       , 'g'),"e");
	r = r.replace(new RegExp("[\\u00EC-\\u00EF]"       , 'g'),"i");
	r = r.replace(new RegExp("[\\u00F1]"               , 'g'),"n");
	r = r.replace(new RegExp("[\\u00F2-\\u00F6\\u00F8]", 'g'),"o");
	r = r.replace(new RegExp("[\\u00F9-\\u00FC]"       , 'g'),"u");
	r = r.replace(new RegExp("[\\u00FD\\u00FF]"        , 'g'),"y");

	// Latin Extended-A Unicode conversion, upper & lower case.
	r = r.replace(new RegExp("[\\u0100-\\u0105]", 'g'),"a");
	r = r.replace(new RegExp("[\\u0106-\\u010D]", 'g'),"c");
	r = r.replace(new RegExp("[\\u010E-\\u0111]", 'g'),"d");
	r = r.replace(new RegExp("[\\u0112-\\u011B]", 'g'),"e");
	r = r.replace(new RegExp("[\\u011C-\\u0123]", 'g'),"g");
	r = r.replace(new RegExp("[\\u0124-\\u0127]", 'g'),"h");
	r = r.replace(new RegExp("[\\u0128-\\u0131]", 'g'),"i");
	r = r.replace(new RegExp("[\\u0132-\\u0133]", 'g'),"ij");
	r = r.replace(new RegExp("[\\u0134-\\u0135]", 'g'),"j");
	r = r.replace(new RegExp("[\\u0136-\\u0138]", 'g'),"k");
	r = r.replace(new RegExp("[\\u0139-\\u0142]", 'g'),"l");
	r = r.replace(new RegExp("[\\u0143-\\u014B]", 'g'),"n");
	r = r.replace(new RegExp("[\\u014C-\\u0151]", 'g'),"o");
	r = r.replace(new RegExp("[\\u0152-\\u0153]", 'g'),"oe");
	r = r.replace(new RegExp("[\\u0154-\\u0159]", 'g'),"r");
	r = r.replace(new RegExp("[\\u015A-\\u0161]", 'g'),"s");
	r = r.replace(new RegExp("[\\u0162-\\u0167]", 'g'),"t");
	r = r.replace(new RegExp("[\\u0168-\\u0173]", 'g'),"u");
	r = r.replace(new RegExp("[\\u0174-\\u0175]", 'g'),"w");
	r = r.replace(new RegExp("[\\u0176-\\u0178]", 'g'),"y");
	r = r.replace(new RegExp("[\\u0179-\\u017E]", 'g'),"z");

	// Remove all remaining non-alpha characters.
	r = r.replace(new RegExp("\\W", 'g'),"");
	return r;
}

// Test for numeric-only data.
function isNumeric(value) {
	if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) {
		return false;
	} else {
		return true;
	}
}

// Test for integer only data.
function isInteger(value) {
	if (value == null || !value.toString().match(/^[+-]?\d*$/)) {
		return false;
	} else {
		return true;
	}
}

// Test for positive numeric-only data.
function isPositiveNumeric(value) {
	if (value == null || !value.toString().match(/^\d*\.?\d*$/)) {
		return false;
	} else {
		return true;
	}
}

// Remove element and everything below it.
function removeNode(element) {
	element.parentNode.removeChild(element);
}

// Insert element after an existing element.
function insertAfter(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
}

// Insert element ahead of an existing element.
function insertAheadOf(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement);
}

// Move up the DOM tree a specific number of generations.
function fUpGen(gNode, g) {
	var gNode;
	var g;
	for (var i = 0; i < g; i++) {
		gNode = gNode.parentNode;
		if (gNode.nodeName == 'undefined') {
			gNode = null;
			break;
		}
	}
	return gNode;
}

// Move up the DOM tree until a specific DOM type is reached.
function fUpGenToType(gNode, gType) {
	var gNode;
	var gType = gType.toUpperCase();
	while (gNode.nodeName.toUpperCase() != gType) {
		gNode = gNode.parentNode;
		if (!gNode) {
			throw false;
			break;
		}
	}
	return gNode;
}

// Post warning if note logs are present on the cache and the cache is un-published.
var NumNoteLogs = fNoteCount();
if (NumNoteLogs > 0 && !bPublished) {
	var AlertText = NumNoteLogs + ' note log';
	if (NumNoteLogs > 1) {
		AlertText = AlertText + 's are';
	}
	else {
		AlertText = AlertText + ' is';
	}
	fShowAlert(AlertText + ' posted to the cache');
}

//********************************************************************************//
//                                                                                //
//                        Settings Interface Functions                            //
//                                                                                //
//********************************************************************************//

function fCreateSettingsDiv(sTitle) {

	// If div already exists, reposition browser, and show alert.
	var divSet = document.getElementById("gm_divSet");
	if (divSet) {
		var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
		window.scrollTo(window.pageXOffset, YOffsetVal);
		alert('Edit Setting interface already on screen.');
		return;
	}

	// Set styles for titles and elements.
	GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 6px; !important; } ' );
	GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
	GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
	GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );

	// Create blackout div.
	document.body.setAttribute('style', 'height:100%;');
	var divBlackout = document.createElement('div');
	divBlackout.id = 'divBlackout';
	divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
									  'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
									  'height: 100%; width: 100%; display: block;');

	// Create div.
	divSet = document.createElement('div');
	divSet.id = 'gm_divSet';
	divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
								 'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
	popwidth = parseInt(window.innerWidth * .7);
	divSet.style.width = popwidth + 'px';

	divSet.setAttribute('winTopPos', document.documentElement.scrollTop);  // snapback code

	// Create heading.
	var ds_Heading = document.createElement('div');
	ds_Heading.setAttribute('class', 'SettingTitle');
	ds_Heading.appendChild(document.createTextNode(sTitle));
	divSet.appendChild(ds_Heading);

	var ds_Version = document.createElement('div');
	ds_Version.setAttribute('class', 'SettingVersionTitle');
	ds_Version.appendChild(document.createTextNode('Script Version ' + GM_info.script.version));
	divSet.appendChild(ds_Version);

	// Add div to page.
	var toppos =  parseInt(window.pageYOffset +  60);
	var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
	divSet.style.top = toppos + 'px';
	divSet.style.left = leftpos + 'px';
	divSet.setAttribute('YOffsetVal', window.pageYOffset);

	// Add blackout and setting divs.
	document.body.appendChild(divBlackout);
	document.body.appendChild(divSet);
	window.addEventListener('resize', fSetLeftPos, true);

}

// Resize/reposition on window resizing.
function fSetLeftPos() {
	var divSet = document.getElementById('gm_divSet');
	if (divSet) {
		var popwidth = parseInt(window.innerWidth * .5);
		divSet.style.width = popwidth + 'px';
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.left = leftpos + 'px';
	}
}

function fOpenSettingsDiv() {
	// Add blackout and setting divs.
	var divBlackout = document.getElementById('divBlackout');
	var divSet = document.getElementById('gm_divSet');
	divSet.style.visibility = 'visible';
	divBlackout.style.visibility = 'visible';
	var op = 0;
	var si = window.setInterval(fShowBlackout, 40);
	// Function to fade-in blackout div.
	function fShowBlackout() {
		op = op + .05;
		divBlackout.style.opacity = op;
		if (op >= .75) {
			window.clearInterval(si);
		}
	}
}

function fCloseSettingsDiv() {
	var divBlackout = document.getElementById('divBlackout');
	var divSet = document.getElementById('gm_divSet');
	var winTopPos = divSet.getAttribute('winTopPos') - 0;  // snapback code
	removeNode(divSet);
	removeNode(divBlackout);
	window.removeEventListener('resize', fSetLeftPos, true);
	document.documentElement.scrollTop = winTopPos;  // snapback code
}

//-----------------------------------------------------------------------------
function getSignedInAs() {
	if      ($('.user-name').text().length > 0) {
		var result = $('.user-name').text();
		//alert ("1->"+result);
	}
	else if   ($('.li-user-info').text().length > 0) {
		var result = $('.li-user-info > span:first-child').text();
		//alert ("2->"+result);
	}
	else if   ($('#ctl00_LoginUrl').prev().text().length > 0) {
		var result = $('#ctl00_LoginUrl').prev().text();
		//alert ("3->"+result);
	}
	else if   ($('.SignedInProfileLink').text().length > 0) {
		var result = $('.SignedInProfileLink > .li-user-info > span:first-child').text();
		//alert ("4->"+result);
	}
//	else if   ($('#ctl00_LogoutUrl')) {
//		var result = $('#ctl00_LogoutUrl').parent().find("a").text();
//		alert ("5->"+result);
//	}
	else if (document.getElementById('ctl00_LogoutUrl')) {
		result = document.getElementById('ctl00_LogoutUrl').parentElement.parentElement.querySelectorAll('a')[0].innerText;
	}
	else {
		targetElement = document.getElementsByClassName("SignedInText")[0];
		if (targetElement) {
			result = targetElement.querySelector("a").innerText;
		}
		else result = "not logged in";
		/*		//alert("ELSE");
		//alert ("Could not determine signedInAs, please make sure you are logged in and verify that the dom elements ctl00_LoginUrl or SignedInProfileLink are present.");
		var SignedInLink = document.evaluate(
		"//a[contains(@class, 'SignedInProfileLink')]",
		document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
		).singleNodeValue;
		if (SignedInLink) {
			result = SignedInLink.firstChild.data;
			result = result.replace(/<\&amp;>/g, '&');
		}
		return;
	*/	}
	console.log('user signed in as: '+result);
	return result;
}

//-----------------------------------------------------------------------------
function addGcConflictCopyToLitmusPage(itemCountOffset) {
	var elements = document.getElementsByClassName("ResultContainer");

	//    <div class=ResultContainer>
	//      <ul class=unstyled>
	//        <li>
	//          <a href=...seek/cahche_details>FNxxxx</a>
	//           -
	//          <a href=...seek/cache_details>Final Location</a>
	//          Waypoint too close to
	//          <a href=...seek/cache_details>Final Location</a>
	//          Waypoint <
	//          <img src=... icon>
	//           o
	//          <a class="" href="....seek/cache_details">Cache Name</a>
	//           o
	//          <a href=="...admin/review...">GC-Code</a>
	//          -
	//          <img src=... direction>
	//          xx.x m NW
	//        </li>
	//      </ul>
	//    <div>
	var itemCount = itemCountOffset;

	for (var element of elements) {
		debugLog(" - element=", element);
		if ("Errors" == element.parentNode.className ) {
			debugLog("   --> parsing error results for distance conflicts");
			if (element.childNodes.length == 3) {
				var ul = element.childNodes[1];
				if ("UL" == ul.tagName && "unstyled" == ul.className) {
					for (var item of ul.childNodes) {
						if ("LI" == item.tagName) {
							debugLog("       - item=", item);

							// states of i: 0=waiting for "Waypoint <"
							//              1=Cache Name contained in 3 parts
							//              3=this contains cache name
							//              5=this part contains GC code
							var i = 0;
							var cacheName = null;
							var gcCode = null;
							var base = null;
							for (var part of item.childNodes) {
								debugLog("         - part=", part);
								if (" Waypoint < " == part.nodeValue ||
									" too close to cache " == part.nodeValue ||
									" Waypoint too close to cache " == part.nodeValue) {
									if (0 == i) {
										debugLog("           - Waypoint > string detected, next part is cache name");
										i=1;
									} else {
										debugLog("           - Waypoint > string detected in unexpected state i=", i);
									}
								} else if (3 == i) {
									if ("A" == part.nodeName && part.href.includes("www.geocaching.com/seek/cache_details")) {
										i = i+1;
										cacheName = part.innerText;
										debugLog("           - detected cache name: ", cacheName);
									} else {
										debugLog("           - cache name not detected with index i=", i);
									}
								} else if (5 == i) {
									if ("A" == part.nodeName && part.href.includes("www.geocaching.com/admin/review")) {
										i = 0;
										gcCode = part.innerText;
										debugLog("           - detected gc code: ", gcCode);
										base = part;
									} else {
										debugLog("           - gc code not detected with index i=", i);
									}
								} else if (0 != i) {
									i = i+1;
								}
							}

							if (cacheName != null && gcCode != null) {
								debugLog("         -> detected ["+gcCode+"] ["+cacheName+"], base=", base);
								cacheName = cacheName.replace(gcCode+' ', '');
								cacheName = cacheName.replace(gcCode, '');
								debugLog("         -> cleaned up cache name ["+gcCode+"] ["+cacheName+"], base=", base);

								itemCount = itemCount + 1;
								addLinkButton(base, itemCount, cacheName, gcCode);
							} else {
								debugLog("         -> failed to extract cache information");
							}
						}
					}
				} else {
					debugLog("       did not find UL element", ul);
				}
			} else {
				debugLog("       did not find 3 childNodes while looking for UL element");
			}
		} else if ("Warnings" == element.parentNode.className) {
			debugLog("   --> not interested in warning results");
		} else if ("Pass" == element.parentNode.className) {
			debugLog("   --> not interested in pass results");
		} else {
			debugLog("   --> unexpected parent class", element.parentNode.className);
		}
	}
	return itemCount;
}

//-----------------------------------------------------------------------------
function addGcConflictCopyToLitmusSectionOfReviewPage(itemCountOffset) {
	/* children of <li> item
		proximity conflict with listing coords:    0:[icon] 1:[link to cache under review] "too close to" 2:[icon] 3:[link to conflicting cache] " - " 4:[directional image] "distance and bearing"
		proximity conflict with waypoint of cache: 0:[icon] 1:[link to cache under review] "too close to" 2:[waypoint code link] 3:[waypoint type link] "waypoint" 4:[icon] 5:[link to conflicting cache] " - "6:[directional image] "distance and bearing"
		*/
	var elements = document.querySelector('#ctl00_ContentBody_rltErrorListViewContainer ul');
	debugLog(' - elements=', elements);

	var itemCount = itemCountOffset;
	if (elements) {
		if (elements.querySelectorAll('img[name="litmusCopy"]')[0]) {
			console.log('detected a native litmusCopy feature');
			//if native litmus link detected, do not insert a duplicate
			if (GM_getValue('CopyNameMdLink_' + SignedInAs, 'Off') == 'Off') {
				console.log('The copy preference is in the default setting - no button added.');
				return;
			}
		}	
		for(var element of elements.children) {
			debugLog('   - element=', element);
			var nativeCopyImg = element.querySelector('[name=litmusCopy]');
			if (nativeCopyImg) {
				var markdownData = nativeCopyImg.getAttribute('nameattr');
				addLinkButton(nativeCopyImg,itemCount,
					markdownData.substring(markdownData.indexOf(':')+2),
					markdownData.substring(0,markdownData.indexOf(':'))
				);
				itemCount++;
			}
			else console.log('there was a problem setting up a litmus copy link');
/*			var index = null;
			if (element.children && element.children.length > 5) {
				// waypoint case - was 5
				index = 5;
			} else if (element.children && element.children.length > 3) {
				// listing coord case - was 3
				index = 3;
			}
			if (index) {
				var link = element.children[index];
				var text = link.innerText;
				debugLog('     - link', link);
				debugLog('     - text', text);
				var gcCode = '';
				var cacheName = text;

				// try to split name into code and name
				var parts = text.split(' ', 1);
				debugLog('     - parts', parts);
				if (parts.length > 0) {
					if (parts[0].startsWith('GC')) {
						gcCode = parts[0];
						cacheName = cacheName.replace(gcCode+' ', '');
						cacheName = cacheName.replace(gcCode, '');

						debugLog('     - gcCode', gcCode);
						debugLog('     - cacheName', cacheName);
					}
				}

				itemCount = itemCount + 1;
				addLinkButton(link, itemCount, cacheName, gcCode);

			} else {
				debugLog('     - Could not determine case');
			}*/
		}
	}

	return itemCount;
}

//-----------------------------------------------------------------------------
function addLinkButton(base, itemCount, cacheName, gcCode) {
	var imgLink = document.createElement('img');
	imgLink.id = "imgLitmusLink"+itemCount;
	imgLink.src = Copy2ClipboardSrc;
	imgLink.style.margin = "0px 0px 0px 10px";
	imgLink.style.cursor = "pointer";
	imgLink.addEventListener('click', copyData, false);

	if (GM_getValue('CopyNameMdLink_' + SignedInAs, 'Off') == 'Off') {
		imgLink.title = "Click: Copy GC-Code and Cache Name to clipboard\r\nCtrl-Click: Copy GC-Code and Cache Name as Markdown Link";
	} else {
		imgLink.title = "Copy GC-Code and Cache Name to clipboard as Markdown link";
	}
	if (gcCode && gcCode != '') {
		imgLink.value = gcCode + ": "+cacheName;
		imgLink.data = {code: gcCode, name: cacheName};
	} else {
		imgLink.value = cacheName;
		imgLink.data = null;
	}

	insertAfter(imgLink, base);
	debugLog("            - added imgLink", imgLink);
}

//-----------------------------------------------------------------------------
function copyData(arg) {
	var result = arg.target.value;
	if (result != null) {
		if (GM_getValue('CopyNameMdLink_' + SignedInAs, 'Off') == 'On' || arg.ctrlKey && arg.target.data && arg.target.data.code) {
			result = '['+arg.target.value+'](http://coord.info/'+arg.target.data.code+')';
		}
		GM_setClipboard(result);
		debugLog("Returning cache information: ", result);
	} else {
		debugLog("Nothing to return", result);
	}
}

//-----------------------------------------------------------------------------
function formatCoordsDecMinutes(decDegrees, posPrefix, negPrefix) {
	var leadZero = '';
	var prefix = posPrefix;
	if (decDegrees < 0) {
		prefix = negPrefix;
		decDegrees = -decDegrees;
	}
	var degrees = Math.floor(decDegrees);
	var decMinutes = decDegrees - degrees;
	decMinutes *= 60;
	decMinutes = roundNumber(decMinutes, 3);
	if (decMinutes < 10) {
		leadZero = '0';
	}
	decMinutes = `${leadZero}${decMinutes}`;
	if (decMinutes.length<6) {
		decMinutes += '0';
	}

	return `${prefix}${degrees}° ${decMinutes}`;
}


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Begin SHA-1 Implementation
/*
 * A JavaScript implementation of the Secure Hash Algorithm, SHA-1, as defined
 * in FIPS PUB 180-1
 * Copyright (C) Paul Johnston 2000.
 * See http://pajhome.org.uk/site/legal.html for details.
 */

/*
 * Convert a 32-bit number to a hex string with ms-byte first
 */

var hex_chr = "0123456789abcdef";

function hex(num) {
  var str = "";
  for(var j = 7; j >= 0; j--)
    str += hex_chr.charAt((num >> (j * 4)) & 0x0F);

  return str;
}

/*
 * Convert a string to a sequence of 16-word blocks, stored as an array.
 * Append padding bits and the length, as described in the SHA1 standard.
 */
function str2blks_SHA1(str) {
  var nblk = ((str.length + 8) >> 6) + 1;
  var blks = new Array(nblk * 16);
  for(var i = 0; i < nblk * 16; i++) blks[i] = 0;
  for(i = 0; i < str.length; i++)
    blks[i >> 2] |= str.charCodeAt(i) << (24 - (i % 4) * 8);

  blks[i >> 2] |= 0x80 << (24 - (i % 4) * 8);
  blks[nblk * 16 - 1] = str.length * 8;

  return blks;
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function add(x, y) {
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);

  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left
 */
function rol(num, cnt) {
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Perform the appropriate triplet combination function for the current
 * iteration
 */
function ft(t, b, c, d) {
  if(t < 20) return (b & c) | ((~b) & d);
  if(t < 40) return b ^ c ^ d;
  if(t < 60) return (b & c) | (b & d) | (c & d);

  return b ^ c ^ d;
}

/*
 * Determine the appropriate additive constant for the current iteration
 */
function kt(t) {
  return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
         (t < 60) ? -1894007588 : -899497514;
}

/*
 * Take a string and return the hex representation of its SHA-1.
 */
function calcSHA1(str) {
  var x = str2blks_SHA1(str);
  var w = new Array(80);

  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;
  var e = -1009589776;

  for(var i = 0; i < x.length; i += 16) {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;
    var olde = e;

    for(var j = 0; j < 80; j++) {
      if(j < 16) w[j] = x[i + j];
      else w[j] = rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
      t = add(add(rol(a, 5), ft(j, b, c, d)), add(add(e, w[j]), kt(j)));
      e = d;
      d = c;
      c = rol(b, 30);
      b = a;
      a = t;
    }

    a = add(a, olda);
    b = add(b, oldb);
    c = add(c, oldc);
    d = add(d, oldd);
    e = add(e, olde);
  }

  return hex(a) + hex(b) + hex(c) + hex(d) + hex(e);
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End SHA1 Implementation

function getZCdata(){
	let fuzzyFactor = 1*GM_getValue("ZCFuzzyFactor", -1);
//	if (!(fuzzyFactor <= 5000 && fuzzyFactor >= -5000) || fuzzyFactor == null) fuzzyFactor = -1;
	if (fuzzyFactor < 0) {
		GM_setValue("ZCFuzzyFactor",-1);
		console.log("No ZoneCatcher Testing Requested.");
		if (!ZCMenu) ZCMenu = GM_registerMenuCommand("Turn ZoneCatcher Zone Display On", ZCAdj);
		return; // Do not proceed if fuzzyfactor is negative ---> reviewer does not want ZC test
	}

	// set up Tampmonkey menu item
	if (!ZCMenu) ZCMenu = GM_registerMenuCommand("Adjust/Turn Off Zonecatcher Zone Display", ZCAdj);


	// set up the ZC output
	let ZCPara = document.getElementById("ZCDisplay");
	if (!ZCPara) {
		let detailsTable=document.getElementsByClassName("Table")[0].querySelector("tbody > tr");
		let ZCCell = document.createElement("td");
		ZCPara = document.createElement("p");
		ZCPara.setAttribute("id","ZCDisplay");
		ZCCell.append(ZCPara);
		detailsTable.append(ZCCell);
		document.getElementsByClassName("Table")[0].querySelector("th").setAttribute("colspan",4);
	}
	else {
		ZCPara.innerText = "";
		ZCStatus = ""; // this is a global variable
	}
	let waypoints = scrapeForWaypoints();
	if (!waypoints || waypoints.length<1) return null;
	let pointList = "";
	for (var i=0; i<waypoints.length; i++) pointList+="&p["+i+"]="+waypoints[i].latitude+","+waypoints[i].longitude;
	if (!makeZCRequest(pointList,0)) ZCStatus += "PtError";
	if (fuzzyFactor == 0) ZCStatus += "noFuzzy";
	else if (!makeZCRequest(pointList,1*fuzzyFactor)) ZCStatus += "FuzzError";
	let ZCIterations = 0;
	let ZCTimer = setInterval(()=>{
		if (ZCIterations > 50 || ZCStatus.indexOf("Error") > -1 ) {
			clearInterval(ZCTimer);
			console.log("There was a problem with one of the ZC calls - ZC function timed out.");
			let ZCPara = document.getElementById("ZCDisplay");
			ZCPara.innerText="ZoneCatcher Server Error";
			return;
		}
		ZCIterations++;
		if (ZCStatus.indexOf("Fuzzy") < 0 || ZCStatus.indexOf("Point") < 0) return;
		clearInterval(ZCTimer);
		let fuzzyOff = ZCStatus.indexOf("noFuzzy");
		if (fuzzyOff > -1) ZCFuzzy = ZCPoint;
		ZCPoint = ZCUnique(ZCPoint);
		ZCFuzzy = ZCUnique(ZCFuzzy);
		ZCDisplay = "ZoneCatcher Zones\n";
		if (ZCPoint.length == ZCFuzzy.length) {
			ZCDisplay+=ZCListToString(ZCPoint)
			if (fuzzyOff < 0) ZCDisplay += "\nNo Nearby Zones";
		}
		else {
			ZCDisplay+=ZCListToString(ZCPoint)+"\nNearby Zones\n";
			for (var i = 0; i< ZCFuzzy.length; i++) {
				let printThis = true;
				for (var j=0; j< ZCPoint.length; j++) {
					if (ZCFuzzy[i].name == ZCPoint[j].name && ZCFuzzy[i].keyword == ZCPoint[j].keyword) {
						printThis=false;
						break;
					}
				}
				if (printThis) ZCDisplay += "  "+ZCFuzzy[i].name+"-"+ZCFuzzy[i].keyword+"\n";
			}
		}
		ZCPara.innerText=ZCDisplay;
	},100);
}

function ZCAdj(){
	let success = false;
	let response = "";
	let oldFuzzyFactor = 1*GM_getValue("ZCFuzzyFactor",-1);
	while (!success) {
		response = prompt(	"ZoneCatcher Zone Display Settings\n"+
				"\n"+
				"Enter the size in METERS of the distance to use\n"+
				"when searching ZoneCatcher for zones nearby each of the cache\n"+
				"waypoints.  Zones that contain a waypoint are always identified.\n"+
				"\n"+
				"Values between -100 and 100 are acceptable.  A negative\n"+
				"number turns OFF the ZoneCatcher feature.\n",oldFuzzyFactor);
		response = response*1;
//		console.log('response is: '+response);
		if (response <= 100 && response >= -100) success= true;
	}
//	console.log("The new fuzzy factor is: "+response);
	GM_setValue("ZCFuzzyFactor",response*1);
	if ( ((oldFuzzyFactor*response) < 0) || ((oldFuzzyFactor*response == 0) && (oldFuzzyFactor+response < 0)) ){ // The menu item needs an edit
//		console.log(ZCMenu);
		GM_unregisterMenuCommand(ZCMenu);
//		console.log('just unregistered ZC menu');
//		console.log(ZCMenu);
//		setTimeout(()=>{
			if (response < 0 ) ZCMenu = GM_registerMenuCommand("Turn ZoneCatcher Zone Display On", ZCAdj);
			else ZCMenu = GM_registerMenuCommand("Adjust/Turn Off Zonecatcher Zone Display", ZCAdj);
//		},250);
	}
	if (oldFuzzyFactor != response) {
		if (response < 0) {
			let ZCPara = document.getElementById("ZCDisplay");
			if (ZCPara) ZCPara.innerText = "";
		}
		getZCdata();
	}
}


function ZCListToString(ZCList){
	if (ZCList.length < 1) return "None\n";
	let outString = "";
	for (var i=0; i< ZCList.length; i++){
		outString+="  "+ZCList[i].name+"-"+ZCList[i].keyword+"\n";
	}
	return outString;
}

function ZCUnique(ZCLongList){
	if (ZCLongList.length <= 1) return ZCLongList;
	let ZCShortList = [];
	ZCShortList.push(ZCLongList[0]);
	for (var i=1; i<ZCLongList.length; i++) {
		let nextItem = true;
		for (var j=0; j<ZCShortList.length; j++) {
			if (ZCLongList[i].name == ZCShortList[j].name && ZCLongList[i].keyword == ZCShortList[j].keyword) {
				nextItem=false;
				break;
			}
		}
		if (nextItem) ZCShortList.push(ZCLongList[i]);
	}
	return ZCShortList;
}

function makeZCRequest(pointList, fuzzyFactor = 100){	
	let fuzzURL = "";
	if (fuzzyFactor != 0) fuzzURL = "&fuzz="+fuzzyFactor+"!METRES";
	let ZCdomain = 'zc.geocaching.com';
	if (document.URL.indexOf('www.geocaching') < 0)
		ZCdomain = 'zc-staging.geocaching.com';
	try {
		let responseDoc = GM_xmlhttpRequest({
			method: "GET",
			url: "https://"+ZCdomain+"/checkzones.php?package=reviewerlitmus&owner=friends"+fuzzURL+"&sendpoints=0"+pointList,
			headers:{"Content-Type": "application/json"},
			onload: function (response) {
				let ZCOutput = parseZC(response.responseText.substring(response.responseText.indexOf("<ZoneCatcher>")));
				if (response.finalUrl.indexOf("&fuzz=") > -1) {
					ZCFuzzy = ZCOutput;
					ZCStatus += "Fuzzy";
				}
				else {
					ZCPoint = ZCOutput;
					ZCStatus += "Point";
				} 
			}
		});
		return true;
	}
	catch {return false};
}

function parseZC(ZCBlob) {
	let ZCContents = [];
	let nextZone=ZCBlob.indexOf("</zone>");
	if (nextZone < 0) return [];
	let name = ZCBlob.substring(ZCBlob.indexOf("<name>")+6,ZCBlob.indexOf("</name>"));
	let keywordBlob = ZCBlob.substring(0,nextZone);
	while (keywordBlob.indexOf("<keyword>") > 0 ) {
		ZCContents.push({name: name, 
						keyword: keywordBlob.substring(keywordBlob.indexOf("<keyword>")+9,keywordBlob.indexOf('</keyword>'))});
		let nextKeyword = keywordBlob.indexOf("</keyword>"+10);
		if (nextKeyword < 0) keywordBlob = "";
		else keywordBlob = keywordBlob.substring(nextKeyword);
	}
	return ZCContents.concat(parseZC(ZCBlob.substring(nextZone+7)));
}

function scrapeForWaypoints(){
	let data = document.getElementById("ctl00_ContentBody_CacheDataControl1_CacheData");
	if (!data) return null;
	let pointList = [];
	let waypointCount = 0;
	for (var attr of data.attributes) {
		if ( (attr.name.indexOf("latitude") > -1 ) && (attr.name.indexOf("data-wp_") > -1) ) {
			let firstChar = attr.name.indexOf("wp_")+3;
			let wpNumber = 1*attr.name.substring(firstChar,attr.name.indexOf("_l"))+1;
			if ( waypointCount < wpNumber) waypointCount = wpNumber;
		}
	}
	pointList.push({latitude: data.getAttribute("data-latitude"),longitude: data.getAttribute("data-longitude")});
	for (var i=0; i<waypointCount; i++){
		pointList.push({latitude: data.getAttribute("data-wp_"+i+"_latitude"),
						longitude: data.getAttribute("data-wp_"+i+"_longitude")});
	}
	return pointList;
}
