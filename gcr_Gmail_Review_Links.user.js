﻿// ==UserScript==
// @name           GCR GMail Review Links v2
// @description    GMail Review Links
// @namespace      http://www.geocaching.com/admin
// @version        02.09
// @icon           http://i.imgur.com/iEntr3d.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @include        http://mail.google.com/*
// @include        https://mail.google.com/*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// rexquire http://code.jquery.com/jquery-3.3.1.min.js
// ==/UserScript==

/*
02.09	2024-03-05	Bugfix so the link isn't inserted when composing an email
02.08	2024-03-19	Missed a jQuery reference - this corrects it
02.07	2024-03-14	Fix for TrustedType errors thrown by jQuery
02.06	2023-05-31	Clip GC code followed by underscore to just GC code to form valid admin/review link
02.05	2023-04-25	Update link format for new website standard
02.04	2022-12-27	Stop inserting links in drafts, only one insert per email
02.03	2020-12-17	removed old redundant code
02.02	2018-08-31	update to work with new gmail layout (settings might not work)

Function:
 Adds Reviewer Page Links To Logs Viewed In GMail.  Adds the link the first time the cache link appears
 in an email (multiple emails in a thread each get one inserted link).

*/
console.log('Gmail script has loaded');
var TimeOutID = window.setInterval(insertLink, 1000);

//****************************************************************************************///
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

function formatLinkElement(reviewLink) {
	let anchor = document.createElement('a');
	anchor.href = reviewLink;
	anchor.style = "margin-left: 10px; font-weight: bold;";
	let text = document.createTextNode("Review page");
	anchor.appendChild(text);
	return anchor;
}

function fTextboxCheck(element) {
	if (element.tagName == 'DIV' && element.getAttribute('role') == 'textbox') return true;
	if (element.parentNode) return fTextboxCheck(element.parentNode);
	return false;
}

function insertLink() {
	if ( (document.URL.indexOf('/#inbox') > -1) || (document.URL.indexOf('/popout') > -1) ) {

		var cachePageLinks = document.querySelectorAll('a[href*="coord.info/GC"]');
		if (cachePageLinks.length >0) {
			cachePageLinks.forEach((element)=> { 
				var reviewLink = fInfo2Ref(element.href,'R');
				var testVal = element.parentNode.querySelectorAll("a[href*='"+reviewLink+"']");
				if (testVal.length == 0 && !fTextboxCheck(element.parentNode)) {
					element.insertAdjacentElement('afterend',formatLinkElement(reviewLink));
				}
			});
		}
//	if ( (location.hash.indexOf('#inbox') > -1) || (document.URL.indexOf('/popout') > -1) ) {
//		var cachePageLinks = $('a[href*="coord.info/GC"]'); //this is the old link format
		//			var reviewLink = fInfo2Ref($(this).attr('href'), 'R');
				//var testVal = $(this).siblings('a[href*="'+reviewLink+'"]');
	//			var isForm = $(this).parents('div[role="textbox"]');
	//			if (testVal.length == 0 && isForm.length == 0) {
	//				$(this).after('<a href="' + reviewLink + '" style="margin-left: 10px; font-weight: bold;">Review page</a>');

		cachePageLinks = document.querySelectorAll('a[href*="geocache/GC"]'); //and the new link format
		if (cachePageLinks.length >0) {
			cachePageLinks.forEach((element)=> { 
				var reviewLink = fInfo2Ref(element.href,'R');
				var testVal = element.parentNode.querySelectorAll("a[href*='"+reviewLink+"']");
				if (testVal.length == 0 && !fTextboxCheck(element.parentNode)) {
					element.insertAdjacentElement('afterend',formatLinkElement(reviewLink));
				}
			});
		}


	/*		var cachePageLinks = $('a[href*="geocache/GC"]'); //and the new link format
		cachePageLinks.each(function() {
			var reviewLink = fInfo2Ref($(this).attr('href'), 'R');
			var testVal = $(this).siblings('a[href*="'+reviewLink+'"]');
			var isForm = $(this).parents('div[role="textbox"]');
			if (testVal.length == 0 && isForm.length == 0) {
//				$(this).after('<a href="' + reviewLink + '" style="margin-left: 10px; font-weight: bold;">Review page</a>');
				this.insertAdjacentElement('afterend',formatLinkElement(reviewLink));
				console.log('gmail script attempted inserting a link');
			}
		});
	*/	}
}

// Converts INFO cache page URL to old style Cache page or Review Page URL (Pass "C" or "R").
// .com/geocache/GCxxxx - added new format to recognize
// http://coord.info/GC2AEJX - still recognizes this format
// http://www.geocaching.com/seek/cache_details.aspx?wp=GC2AEJX
// http://www.geocaching.com/admin/review.aspx?wp=GC2AEJX
function fInfo2Ref(infoUrl, pagetype) {
	var RtnVar = "";
	var i = infoUrl.toUpperCase().lastIndexOf("/GC");
	if (i >= 0) {
		let clip = infoUrl.indexOf('_');
		if (clip>-1) infoUrl = infoUrl.substr(0,clip);
		switch(pagetype.toUpperCase())
		{
		case 'C':
			RtnVar = "https://www.geocaching.com/geocache/" + infoUrl.substr(i + 1);
			break;
		case 'R':
			RtnVar = "https://www.geocaching.com/admin/review.aspx?wp=" + infoUrl.substr(i + 1);
			break;
		}
	}

	return RtnVar;
}

