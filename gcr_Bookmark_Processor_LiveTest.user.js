// ==UserScript==
// @name           GCR Bookmark Processor Live Test v3
// @description    Automates Opening Caches From Bookmark Lists - testing for react to next migration
// @namespace      http://www.geocaching.com/admin
// @version        03.09
// @icon           http://i.imgur.com/kIEMexQ.png
// @include        http*://*.geocaching.com/live/plan/lists/BM*
// @grant          GM_addStyle
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// @grant          GM_setValue
// ==/UserScript==

/*
Function:
 Opens bookmark list cache pages in review or cache page mode. Can
 position the page, and customize the number of logs. When opening
 in review mode, pages can be held/unheld or locked/unlocked.

/* Change history
 * 3.09 24-03-05	Update for update of react site to /live/ 
 * 3.08 24-01-09	revert 3.06, as TM5.0.1 fixed the OpenInTab problem 
 * 3.07 23-12-12	begin Tag Manager logging for basic analytics 
 * 3.06 23-12-11	Remediate TM5.0.0 tab-spamming 
 * 3.05 23-09-06	Correct auto-update link 
 * 3.04 23-09-05	Update log links for new log flow 
 * 3.03 Refactored to update review page links on change of "show" count
 * 3.02 Reordered script to eliminate looping even if there's an error
 * 3.01 Eliminated loop glitch with rated lists, enabled on .staging
 * 3.00 Complete rewrite of script to adapt functionality to new List pages
*/

/*
NOTE:
 The following configuration must be set to use on local files,
 using about:config
	greasemonkey.fileIsGreaseable = true
*/

//****************************************************************************************//
//                                                                                        //
//                         			Functions                         	                  //
//                                                                                        //
//****************************************************************************************//
return; //no more testing - make the script null and void
function fStartObserver(){
    //	tableObserver.observe(document.getElementsByClassName('geocache-table')[0],{
        tableObserver.observe(document.querySelectorAll('table')[0],{
        childList: true,
            subtree: true});
    }
    
    function fUpdateCachesShowing (){
        if (pageLoadComplete) {
            tableObserver.disconnect(); // avoids disconnect on bootup
        }
        pageLoadComplete = false;
        setTimeout(function(){
            if (document.getElementsByClassName('no-geocaches').length>0) {
                pageLoadComplete= true;
                fStartObserver();
                maxLinks =0;
                cachesShowing = 0;
                return;
            }
            fRepopulateReviewPageLinks();
        },250); // allows time for the page to react to the clicks
    }
    
    /* tm5 remediation - tab spamming instead of openInTab - todo (remove) 
    function setClickLink (targetUrl, targetID)  {
        if (!document.getElementById(targetID)) {
            let searchLink = document.createElement('a');
            searchLink.setAttribute('href',targetUrl);
            searchLink.setAttribute('target','_blank');
            searchLink.setAttribute('id',targetID);
            scriptDiv.appendChild(searchLink);
        }
        else {
            document.getElementById(targetID).href = targetUrl;
        }
    } */
    
    function fRepopulateReviewPageLinks() {
            // clear old revPageUrls, if any
            for (let i=0, max=revPageUrls.length; i<max; i++) {
                revPageUrls.pop();
            }
    
            let links = document.getElementsByTagName('a');
            const regex = RegExp('https?:\/\/((www)|((staging)(-one)?))\.geocaching.com\/geocache\/GC*');
            for(let i=0, max=links.length; i<max; i++) {
                if(regex.test(links[i].href) === true) {
                    let tempUrl = links[i].href.replace(/geocache\//g, 'admin/review.aspx?wp=');
                    revPageUrls.push(tempUrl);
                }
            }
    
            // Add review page links to page & have links open in new tab.
            let cacheLinkBoxes = document.getElementsByClassName('list-geocache-details'); //was list-item-details
    
            for (let i=0, max=cacheLinkBoxes.length; i<max; i++) {
                if (cacheLinkBoxes[i].getElementsByTagName('a').length > 1) {
                    let revPageDiv = document.createElement('div');
                    revPageDiv.style.padding = '5px';
                    let targetDiv = cacheLinkBoxes[i].querySelectorAll("td>div");
                    let dupReviewLink = targetDiv[0].getElementsByClassName("review_link");
                    if (dupReviewLink[0]) { // if there's an old review link, remove it
                        dupReviewLink[0].parentElement.remove();
                    }
                    targetDiv[0].appendChild(revPageDiv);
                    let revPageLink = document.createElement('a');
                    revPageLink.innerHTML = 'REVIEW PAGE';
                    revPageLink.setAttribute('href', revPageUrls[i]);
                    revPageLink.setAttribute('class', 'review_link');
                    revPageLink.target = '_blank';
                    revPageLink.title = 'Open Review Page';
                    revPageDiv.appendChild(revPageLink);
                }
            }
                
            maxLinks = revPageUrls.length;
        
            // Get array of all GC-codes
            allGcCodes =revPageUrls.slice();
            for (let i=0, max=allGcCodes.length; i<max; i++) {
                allGcCodes[i] = allGcCodes[i].replace(/https:\/\/((www)|(staging))\.geocaching\.com\/admin\/review\.aspx\?wp=/g, '');
            };
                pageLoadComplete = true;
                fStartObserver();
    }
    
    // Copy list of displayed caches to the clipboard.
    function fCopyListToClipboard1() { fCopyListToClipboard(false); }
    function fCopyListToClipboard2() { fCopyListToClipboard(true); }
    function fCopyListToClipboard(bWptSort) {
        var myInterval = setInterval(function (){
            if (!pageLoadComplete) return;
            var cList = allGcCodes.slice();
            if (bWptSort) cList.sort();
            let cListX = cList.join('\n');
            GM_setClipboard(cListX);
            alert(`${allGcCodes.length} caches copied to the clipboard.`);
            clearInterval(myInterval);
        },250);
    }
    
    // Change browser page position for all open review pages.
    function fPositionRevPages() {
        if (this.getAttribute('hash') == '~publish~') {
            var rslt = confirm('Publish all open Review pages?');
            if (!rslt) { return; }
        }
        window.localStorage.setItem(REV_PAGE_POS_KEY, '');
        window.localStorage.setItem(REV_PAGE_POS_KEY, this.getAttribute('hash'));
    }
    
    // Open next group of cache pages.
    function fLoadNextGroup() {
        if (pageLoadComplete) fGetNextGroup();
        else {
            var myCount = 0;
            var myInterval = setInterval(function(){
                if ((mycount>4)||pageLoadComplete) {
                    fGetNextGroup();
                    clearInterval(myInterval);
                }
                myCount++;
            },2000)
        }
    }
    
    function fGetNextGroup(){	
        // Launch cache page function.
        var fLaunchCachePage = function() {
            var idx = GM_getValue("UrlIndex", 1)
    
            // Calculate index to load group in reverse order.
            var c = startIndex + (endIndex - idx) - 1;
            var thisLink = revPageUrls[c];
            var thisGcCode = allGcCodes[c];
    
            // Define CacheUrl and gcCode variables based on above.
            var CacheUrl = thisLink;
            var gcCode = thisGcCode;
    
            if (selHold.value == 'open-rev') {
                CacheUrl = thisLink + '&nc=12';
            } else if (selHold.value == 'lock') {
                CacheUrl = thisLink + '&action=lock';
            } else if (selHold.value == 'unlock') {
                CacheUrl = thisLink + '&action=unlock';
            } else if (selHold.value == 'watch') {
                CacheUrl = thisLink + '&action=watch';
            } else if (selHold.value == 'remwatch') {
                CacheUrl = thisLink + '&action=remwatch';
            } else if (selHold.value == 'putonhold') {
                CacheUrl = thisLink + '&action=hold';
            } else if (selHold.value == 'remhold') {
                CacheUrl = thisLink + '&action=remhold';
            } else if (selHold.value == 'postlog') {
                CacheUrl = domain +'/live/geocache/' + gcCode +'/log';
                //			CacheUrl = domain +'/seek/log.aspx?gccode=' + gcCode;  //old log flow version
            } else if (selHold.value == 'logbook') {
                CacheUrl = domain +'/seek/cache_logbook.aspx?code=' + gcCode + '#tabs-1';
    // the next two are depricated - they actually don't work now anyway.
    //			} else if (selHold.value == 'oakill') {
    //				CacheUrl = domain +'/seek/cache_logbook.aspx?code=' + gcCode + '&killoawarn=true';
    //			} else if (selHold.value == 'kzkill') {
    //				CacheUrl = domain +'/seek/cache_logbook.aspx?code=' + gcCode + '&killkzwarn=true';
            }
    
            // Open cache in new tab.
            GM_openInTab(CacheUrl); //tm5 remediation todo is remove work-around
    //todo		setClickLink(CacheUrl,"targetUrlLink");
    //todo		document.getElementById('targetUrlLink').click();
    
            // Increment counters.
            idx++;
            inputNextIndex.value = idx;
            GM_setValue("UrlIndex", idx);
        }
    
        // Get first checked.
        var firstChecked = maxLinks;
        var xPathSearch = "//input[(@type='checkbox' and @name='BID') ]";
        var allCheckboxes = document.evaluate(
                xPathSearch, document, null,
                XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
                null);
        var ac = allCheckboxes.snapshotLength;
        if (ac > 0) {
            for (var i = 0; i < ac; i++) {
                if (allCheckboxes.snapshotItem(i).checked) {
                    firstChecked = i + 1;
                    break;
                }
            }
        }
    
        if (selHold.value == '-1') {
            alert('No action selected');
        } else {
            // Get starting index and range.
            var startIndex = inputNextIndex.value - 0;
            GM_setValue("UrlIndex", startIndex);
    
            // Calculate range to open.
            var endIndex = startIndex + GroupSize - 1;
            endIndex = Math.min(endIndex, maxLinks, firstChecked);
            var thisGroupSize = endIndex - startIndex;
    
            // Invoke function for each cache in range.
            var qTimes = 0;
            for (i = startIndex; i <= endIndex; i++) {
                var TimeOutID = window.setTimeout(fLaunchCachePage, TimeDelay * qTimes);
                qTimes++;
            }
        }
    }
    
    // Reset start index back to 1.
    function fReset() {
        GM_setValue("UrlIndex", 1);
        inputNextIndex.value = 1;
    }
    
    
    
    // END FUNCTIONS END FUNCTIONS END FUNCTIONS END FUNCTIONS END FUNCTIONS END FUNCTIONS END FUNCTIONS END FUNCTIONS
    
    // set up a set of global variables
    let pageLoadComplete = false;
    let revPageUrls = [];
    let cachesShowing = 0;
    let allGcCodes = [];
    let maxLinks = 0;
    let tableObserver = new MutationObserver(fUpdateCachesShowing);	
    var GroupSize = 10; 	// Number of pages to open in a group
    var TimeDelay = 75;		// Miliseconds to wait before opening next page
    
    // Get current domain;
    var domain = location.protocol + '//' + location.hostname;
    
    // Unique ID for positioning open browser pages in review mode.
    const REV_PAGE_POS_KEY = 'a3148ed3-e28f-4105-a8ab-dcbacbd3b457';
    
    
    let loaded = function () {
        console.log("Bookmark Processor script is running.");
        GM_addStyle(".Warning, .OldWarning, .Validation, .red {color: #ff0000 !important; }	" );
        GM_addStyle(".rqnText {background-color: yellow; margin-left: 8px; }");
        GM_addStyle("a.noul {text-decoration:none }");
    
        GM_registerMenuCommand('Copy Cache List to Clipboard (Bookmark List Order)', fCopyListToClipboard1);
        GM_registerMenuCommand('Copy Cache List to Clipboard (Waypoint ID Order)', fCopyListToClipboard2);
    
        // set up TM5.0.0 Firefox tab-spawning work around - todo remove
    //	let scriptDiv = document.createElement('div');
    //	scriptDiv.setAttribute('hidden',true);
    //	scriptDiv.setAttribute('id','scriptDiv');
    //	document.querySelector('body').appendChild(scriptDiv);
    
        // Google Tag Manager setup
        function gtagBP(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
        if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
        function googleTagLogBP(dataAction){ //Method to do a gtag sending an action name
            if (document.URL.indexOf("www") < 0) return;
            try {
                gtagBP('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'BookmarkProcessor',/*'debug_mode':true,*/
                    'send_to':'G-GRQE2910DL'});
                console.log('Script '+dataAction+' logged to Tag Manager.');
            }
            catch {console.log('There was an error using gtag.  Logging attempt failed.')}
        }
        googleTagLogBP('initialize');
    
        // Add links to reposition all open review pages.
        var pCrumbs = document.createElement('div');
    
        var spanPosToLable = document.createElement('span');
        spanPosToLable.style.color = 'black';
        spanPosToLable.style.marginLeft = '30px';
        spanPosToLable.style.marginRight = '12px';
        spanPosToLable.appendChild(document.createTextNode('Position to:'));
        pCrumbs.appendChild(spanPosToLable);
    
        var spanPosTo = document.createElement('span');
        spanPosTo.style.marginRight = '12px';
        var aPosTo = document.createElement('a');
        aPosTo.href = 'javascript:void(0);';
        spanPosTo.appendChild(aPosTo);
    
        let spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#hd');
        spanClone.firstChild.title = 'Position open review pages to Top';
        spanClone.firstChild.appendChild(document.createTextNode('Top'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_CacheDetails_ShortDesc');
        spanClone.firstChild.title = 'Position all open review pages to Description';
        spanClone.firstChild.appendChild(document.createTextNode('Description'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#map_canvas');
        spanClone.firstChild.title = 'Position all open review pages to Map';
        spanClone.firstChild.appendChild(document.createTextNode('Map'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_Waypoints');
        spanClone.firstChild.title = 'Position all open review pages to Waypoints';
        spanClone.firstChild.appendChild(document.createTextNode('Waypoints'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_lnkNearbyCaches');
        spanClone.firstChild.title = 'Position all open review pages to Nearby Caches';
        spanClone.firstChild.appendChild(document.createTextNode('Nearby Caches'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '#log_table');
        spanClone.firstChild.title = 'Position all open review pages to Logs';
        spanClone.firstChild.appendChild(document.createTextNode('Logs'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        spanClone = spanPosTo.cloneNode(true);
        spanClone.firstChild.setAttribute('hash', '~publish~');
        spanClone.firstChild.title = 'Publish all open review pages';
        spanClone.firstChild.appendChild(document.createTextNode('Publish'));
        spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
        pCrumbs.appendChild(spanClone);
    
        // Add pCrumbs to page (after header).
        document.getElementById('gc-header').append(pCrumbs);
    
        fUpdateCachesShowing();
    
        // Create span to hold new controls.
        var kzSpan = document.createElement('div');
        kzSpan.id = 'kzSpan';
        kzSpan.style.marginBottom = '5px';
        kzSpan.style.backgroundColor = 'rgb(245, 247, 200)';
    
        var selHold = document.createElement("select");
        selHold.id = "selHold";
        selHold.title = 'Automated action to take.';
        selHold.style.marginRight = '12px';
    
        let optHold = document.createElement("option");
        optHold.value = -1;
        optHold.setAttribute("style", "font-weight: bold; text-align: center; font-style: italic;");
        optHold.appendChild(document.createTextNode("-Action to Perform-"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "open-rev";
        optHold.appendChild(document.createTextNode("Open Review Page"));
        optHold.selected = true;
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "logbook";
        optHold.appendChild(document.createTextNode("Open Logbook"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "putonhold";
        optHold.appendChild(document.createTextNode("Hold"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "remhold";
        optHold.appendChild(document.createTextNode("Unhold"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "watch";
        optHold.appendChild(document.createTextNode("Watch"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "remwatch";
        optHold.appendChild(document.createTextNode("Unwatch"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "lock";
        optHold.appendChild(document.createTextNode("Lock"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "unlock";
        optHold.appendChild(document.createTextNode("Unlock"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "postlog";
        optHold.appendChild(document.createTextNode("Post Log"));
        selHold.appendChild(optHold);
    
        // removing these options, as it's a bad practise for reviewers to kill w/o looking at the review page
    /*	optHold = document.createElement("option");
        optHold.value = "oakill";
        optHold.appendChild(document.createTextNode("Owner Action - Kill"));
        selHold.appendChild(optHold);
    
        optHold = document.createElement("option");
        optHold.value = "kzkill";
        optHold.appendChild(document.createTextNode("Kill Zone - Kill"));
        selHold.appendChild(optHold);
    */
        kzSpan.appendChild(selHold);
    
        // Add controls to span.
        var aNextGroup = document.createElement('a');
        aNextGroup.href = 'javascript:void(0)';
        aNextGroup.id = 'aNextGroup';
        aNextGroup.title = 'Launch next group in tabs';
        aNextGroup.appendChild(document.createTextNode('Open Group'));
        kzSpan.appendChild(aNextGroup);
        kzSpan.appendChild(document.createTextNode(' starting with '));
        let inputNextIndex = document.createElement('input');
        inputNextIndex.type = 'text';
        inputNextIndex.name = 'inputNextIndex';
        inputNextIndex.id = 'inputNextIndex';
        inputNextIndex.size = 4;
        inputNextIndex.maxlength = 4;
        inputNextIndex.value = GM_getValue("UrlIndex", 1);
        kzSpan.appendChild(inputNextIndex);
    
        var aReset = document.createElement('a');
        aReset.href = 'javascript:void(0)';
        aReset.id = 'aReset';
        aReset.title = 'Reset starting index';
        aReset.style.marginLeft = '14px';
        aReset.appendChild(document.createTextNode('Reset'));
        kzSpan.appendChild(aReset);
        kzSpan.appendChild(document.createElement('br'));
    
        // Add span to page before the section controls - this isn't populated as fast as the list that triggers fLoaded.
        let controlLocation = document.querySelectorAll("section.content>div");
        if (!controlLocation[1]) {
            let insertControlsTimer = document.setInterval(()=>{
                controlLocation = document.querySelectorAll("section.content>div");
                if (controlLocation[1]) {
                    clearInterval(insertControlsTimer);
                    controlLocation[1].insertAdjacentElement("beforebegin",kzSpan);
                }
            },100);
        }
        else controlLocation[1].insertAdjacentElement("beforebegin",kzSpan);
    
        // Add event listeners
        aNextGroup.addEventListener("click", fLoadNextGroup, true);
        aReset.addEventListener("click", fReset, true);
    }
    
    let intervalID = setInterval(function() {
        if (!(document.getElementsByTagName("tr").length<2)){ //proceed with script once list elements are populated
            clearInterval(intervalID);
            loaded();
        }
    }, 250);