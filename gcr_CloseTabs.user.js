/*
Script to close non-log browser tabs

// ==UserScript==
// @name           GC Close Tabs v1
// @description    Closes a tab on command from the new log flow page
// @namespace      http://www.geocaching.com/admin
// @version        01.02
// @match          https://*.geocaching.com/*
// @run-at		   document-start
// @grant		   window.close
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_CloseTabs.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_CloseTabs.user.js
// ==/UserScript==

Change Log:
* v01.02    2023-12-12	- begin Tag Manager logging for basic analytics
* v01.01    2023-09-12	- Reduce CloseTabs logging for cleaner console
* v01.00    2023-09-12	- Split off close tabs script from New Log Entry script

Long Description:
The New Log script runs in React, which utilizes lazy loading.  That means that new log tab actions do
not take place unless that tab is in focus.  Many new log script actions will autoclose their tab when
they complete successfully.  This allows mass log actions to process in series - each completed tab
closes, and focus passes to another tab.  This helper script allows non-Log tabs to be closed from the 
log page.  That means the non-log pages do not interrupt the processing series.

*/

let closeOtherPageKey = '812f9b8f-e8fc-40ba-9af9-c2c2f87b2ae3';

fLaunchLogPageActions();
fLaunchOtherPageActions();
console.log('CloseTab script has launched.');

// Google Tag Manager setup
function gtagCT(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
function googleTagLogCT(dataAction){ //Method to do a gtag sending an action name
    if (document.URL.indexOf("www") < 0) return;
    try {
        gtagCT('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'CloseTabs',/*'debug_mode':true,*/
            'send_to':'G-GRQE2910DL'});
        console.log('Script '+dataAction+' logged to Tag Manager.');
    }
    catch {console.log('There was an error using gtag.  Logging attempt failed.')}
}

function fLaunchLogPageActions(){
    let logPageIterations = 0;
    let counter=0;
    let logPageActionsInterval = setInterval(()=>{
        counter++;
//        console.log('CloseTabs looked for the buttons with counter = '+counter);
        if (logPageIterations>20) {
            clearInterval(logPageActionsInterval);
            console.log('CloseTabs is stopping the log page listener');
        }
        if (document.URL.indexOf('/live/geocache/') < 0) logPageIterations++;
        else if (document.getElementById('imgCloseTabs')) {
            let imgCloseTabs = document.getElementById('imgCloseTabs');
            imgCloseTabs.title = 'Close all non-log-page tabs';
            imgCloseTabs.addEventListener("click",fSignalCloseTabs,false);
            console.log('CloseTabs set up the button listener.');
            clearInterval(logPageActionsInterval);
        }
    },500);
}

function fLaunchOtherPageActions() {
//launch monitor to close a review page tab when signaled from a log page
    let otherPageIterations = 0;
    let otherPageInterval = setInterval(()=>{
        if (!document.URL) {
            otherPageIterations++;
            if (otherPageIterations > 20) clearInterval(otherPageInterval);
            return;
        }
        else if (document.URL.indexOf('/live/geocache/') > -1) {
            clearInterval(otherPageInterval); 
            console.log('CloseTabs is stopping the non-log page listener.');
        }
        else {
            clearInterval(otherPageInterval);
            window.addEventListener('storage', fOtherPageCloser, false);
            console.log('CloseTabs set up the close tab listener.');
        }
    },260);
}

// on a log page, signal non-log tabs that they should close
function fSignalCloseTabs() {
    if (confirm('Close all open tabs that are not Log pages?')) {
        var jsObj = new Object;
        jsObj['close']= "true";
        jsObj['random']  = Math.random();
        var jsonString = JSON.stringify(jsObj);
        localStorage.setItem(closeOtherPageKey, jsonString);
        googleTagLogCT('request_close_tabs');
        console.log("CloseTabs just signaled non-log pages to close.");
    }
}

//  On a non-log page, close if asked by a log page
function fOtherPageCloser(e) {
	var jsObj = new Object;
	if (e.key == closeOtherPageKey) {
		var jsonString = e.newValue;
		jsObj = JSON.parse(jsonString);
		if ((jsObj['close']) == 'true') {
			console.log('This page just recieved a new log page request to close.');
			window.close();
		}	
	}
}

