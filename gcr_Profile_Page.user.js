// ==UserScript==
// @name		   GCR Profile Page v3
// @description	   Reviewer Enhancements For Profile Page.
// @namespace      http://www.geocaching.com/admin
// @version    	   03.06
// @icon           http://i.imgur.com/z2OLuP3.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Profile_Page.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Profile_Page.user.js
// @include		   https://*.geocaching.com/*
// @grant		   unsafeWindow
// @require        http://code.jquery.com/jquery-3.3.1.min.js

// @grant          GM_xmlhttpRequest
// ==/UserScript==

/*
Change log:
	03.06	07/09/2024	Change search parameters to reflect updated search API (thanks Matoran_Balta)
	03.05	12/12/2023	Begin Tag Manager logging for basic analytics
	03.04	01/17/2023	Add a warning to search results when known bug is encountered
	03.03	05/05/2021	Adjusted URLs to allow adding to List, made links open in new tab
	03.02	02/16/2020	Fixed profile URL to restore functionality across all profile pages
	03.01	12/17/2020	Adjusted links to work on new search and no longer depend on seek/nearest
	03.00	09/25/2020	Rewrite script to run on new profile page
*/

/*
Function:
	Adds quick links to view all hide and finds to Profile Page.
	There's something funky about the transition from play/search to play/results that turns 
	the script off so the warning won't display if only those two pages are in the include
	list.  That's why all of geocaching.com is included. 
*/

console.log('gcr_profile_page started');

var ImgSrcOwned =
"data:image/gif,GIF89a%0F%00%0B%00%B3%00%00%00%00%00%08%08%08%0C%" +
"10%0C%25%2B%1E%2BB%22Ls4%60g%5Br~m%FF%00%FF%84%84%88%A4%C2%91%C1" +
"%C0%C1%E2%E4%E9%EC%EF%F1%F3%F7%FB%FF%FF%FF!%F9%04%01%00%00%08%00" +
"%2C%00%00%00%00%0F%00%0B%00%40%04Y%10I%C9Z%028%03%0A%9E%7F%C7b%1" +
"8%0D31%D7Q%18j%B1%3C%CCF1%C7p%10x%BD%C8%0D%86%0B%01%85%E31r%9C4%" +
"80Di%C2d%D0%08%8Aht%20s%26%A0%85l%01%BA%08Hz%02Ca%20%200%86%84%C" +
"3%17%B08%24%16%84%04%91dB%C4%18%9FK%C2)%89%00%00%3B";

var ImgSrcFound =
"data:image/gif,GIF89a%0D%00%0B%00%B3%00%00%00%00%00%BD%AD%00%C6%" +
"AD%00%FF%00%FF%FF%E7%00%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%F" +
"F%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF!%F9%0" +
"4%01%00%00%03%00%2C%00%00%00%00%0D%00%0B%00%40%04%2FpH%09j%9D%13" +
"%90%CD%09%C8%9B%A0%01%C2%F6Q%1Dw%0E%9A%A7%9A%18k%AD%18%10p%01m%1" +
"3%A2W%E6%A8%94%8Ar%F3T6%C0L%91%90%8C%00%00%3B";

// Declare global variables.
var UserName;

if (document.location.pathname.indexOf("/p/") > -1) { // on the profile page

	// Google Tag Manager setup
	function gtagProfile(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
	if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
	function googleTagLogProfile(dataAction){ //Method to do a gtag sending an action name
		if (document.URL.indexOf("www") < 0) return;
		try {
			gtagProfile('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'ProfilePage',/*'debug_mode':true,*/
				'send_to':'G-GRQE2910DL'});
			console.log('Script '+dataAction+' logged to Tag Manager.');
		}
		catch {console.log('There was an error using gtag.  Logging attempt failed.')}
	}
	googleTagLogProfile('initialize');

	// Get User Name.
	var e_HomeLink = document.getElementById("ctl00_ProfileHead_ProfileHeader_lblMemberName");
	UserName = e_HomeLink.firstChild.data;
	UserName = UserName.replace(/<\&amp;>/g, '&');

	// Add All Finds and All Hides links.
	e_UserProfile = document.getElementById("ctl00_ProfileHead_ProfileHeader_lblMemberName");
	if (e_UserProfile) {
		var LinkImgFinds = document.createElement('img');
		LinkImgFinds.src = 'http://www.geocaching.com/images/icons/icon_smile.gif';
		LinkImgFinds.height = 16;
		LinkImgFinds.width = 16;
		LinkImgFinds.border = 0;
		LinkImgFinds.style.marginLeft = '12px';
		LinkImgFinds.title = 'Show all caches found by ' + UserName;
		var LinkFinds = document.createElement('a');
		LinkFinds.href = 'https://www.geocaching.com/play/results?sort=foundDate&asc=false&sa=1&fb=' + encodeURIComponent(UserName);
		LinkFinds.target = '_blank';
		LinkFinds.appendChild(LinkImgFinds);

		var LinkImgOwned = document.createElement('img');
		LinkImgOwned.src = 'http://www.geocaching.com/images/WptTypes/sm/2.gif';
		LinkImgOwned.height = 16;
		LinkImgOwned.width = 16;
		LinkImgOwned.border = 0;
		LinkImgOwned.style.marginLeft = '8px';
		LinkImgOwned.title = 'Show all caches owned by ' + UserName;
		var LinkOwned = document.createElement('a');
		LinkOwned.href = 'https://www.geocaching.com/play/results?sort=placeDate&asc=false&sa=1&su=1&hb=' + encodeURIComponent(UserName);
		LinkOwned.target = '_blank';
		LinkOwned.appendChild(LinkImgOwned);

		e_UserProfile.appendChild(LinkFinds);
		e_UserProfile.appendChild(LinkOwned);
	}
} 

else if (document.location.pathname.indexOf('/play/results') > -1) { // on /play/results
	$(document).ready(function (){
		
		// find out if the user has reviewer rights
		if (unsafeWindow._gcUser.hasReviewerRights != true) return; //not a reviewer, so exit

		// way to check for reviewer rights before I discovered unsafeWindow!
/*		var scriptList = $('script');
		for (var i=0; i<scriptList.length;i++) {
			var rightsLoc = scriptList[i].innerHTML.indexOf("hasReviewerRights");
			if ( rightsLoc > -1) {
				rightsLoc = scriptList[i].innerHTML.indexOf(":",rightsLoc) +1;
				var endLoc = scriptList[i].innerHTML.indexOf(",",rightsLoc);
				var piece = scriptList[i].innerHTML.substring(rightsLoc,endLoc).trim();
				if (!(piece == "true")) return; // not a reviewer - exit
				break;
			}
		} */
		//continue with code

		var checkFilter = window.setInterval(function () {
			if (document.location.search.indexOf("sd=")>0) { //the enabled/disabled flag is in use
				if (document.getElementById('warning') == null) {
					var nextDiv = $("div[class='search-results-header']")[0];
					$(nextDiv).before("<div id='warning'><a id='warningtext' target='_blank' title='Open Reviewer Watering Hole discussion' style='color:red'>WARNING - Known Search Bug. "+
						"Disabled/Enabled filter feature is overridden by All/Active/Archived filter</a></div>");
					document.getElementById("warningtext").href="https://forums.geocaching.com/REV/index.php?/topic/"+
						"31775-filtering-not-working-for-disabled-caches/&do=findComment&comment=429665";
				}
			}
		},1000);
	})
}




