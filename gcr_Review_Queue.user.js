// ==UserScript==
// @name           GCR Review Queue v2
// @description    Review Queue
// @namespace      http://www.geocaching.com/admin
// @version        02.25
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Queue.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Queue.user.js
// @include        http*://*.geocaching.com/admin/queue.aspx*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// ==/UserScript==

/* Change history
 * 02.25 2024-03-17	Bump versioning to make sure correct code is auto-distributed
 * 02.24 2024-03-14	fix integration with QLocHighlighter
 * 02.23 2024-03-13	Fix the 'publish one' button
 * 02.22 2024-03-12	Pushed versioning by one to force update after bugfixes to v2.21 in beta
 * 02.21 2024-03-05	Refactored filtering.  Added hiding for Timed Publish.
 * 02.20 2024-02-28	fix "hide" checkboxes
 * 02.19 2024-02-27	implement "skip" checkboxes for open-all
 * 02.18 2024-02-20	make "open all caches" button respect the "exclude" checkboxes
 * 02.17 2024-01-09	revert v2.15.  OpenInTab now fixed in TM5.0.1
 * 02.16 2023-12-12	begin Tag Manager logging for basic analytics
 * 2.15 2023-12-10 Fix for TM5.0 bug causing multiple tabs to open after GM_OpenInTab
 * 2.14 2021-06-01 Changed "copy to memory" to "copy to clipboard" function
 * 2.13 2020-04-03 cleaned up legacy code.
 * 2.12 2019-12-10 bug fix to avoid logouts when pressing enter in filter box
 * 2.11 2018-11-07 removed code relating to the script review queue note
 * 2.10 2018-10-05 add master checkbox to un-exclude all rows - srebeelis
 * 2.06 2018-05-03 timepublish clean up & auto close toggle for holding caches
 * 2.05 2018-03-29 removed code that duplicated 'held by' info
 * 2.04	2018-02-21	made relative links for publish and hold from queue; fixed publish all above

NOTE: 	There's a dependency to the Queue Location Highlighter script.  The 'hl_filtered' flag is set
		by that script, and RQ needs to respect it.

*/

	// Icon image data.
	var UpArrowImg =
		'data:image/gif,GIF89a%0C%00%0E%00%B3%00%00%00%00%00c%7B%94k%' +
		'8C%9Cs%8C%9C%84%9C%AD%8C%9C%AD%94%A5%B5%A5%B5%BD%B5%BD%CE%B5' +
		'%C6%CE%C6%CE%D6%CE%D6%DE%D6%DE%DE%E7%EF%EF%F7%F7%F7%FF%FF%FF' +
		'!%F9%04%01%00%00%00%00%2C%00%00%00%00%0C%00%0E%00%00%04E%10%' +
		'C8I%AB%BD%0B%A1e%D7%08%600p%D2%12%9E%01%E7%08%A1b%82%82%93%8' +
		'4%CB%F3(ab%80%8Am%BF%85B%E0%C0%F0%3D%18%87%00a%072%86%0A%B8%' +
		'A6%AF%E5%F8%04%9C%81%18%A0%C1r%0A%1A%93%C6%C0H%00%03%22%00%3B';

	var HoldImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAALGPC%2FxhBQAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjIyt5EXfQ" +
		"AAALtJREFUOE9jYBgK4D%2FQkSBMFvif5iXy%2F%2F%2FWXpghJBn0%2F%2F%2FjK%2F9z" +
		"vKTBBkTYAg3qCyTJNWADGuOBBhxc8D%2FeBWjAjESCBiArAGtsjIAYkBMIpIkx4D8QQAMN" +
		"YgDUBVUgg4gyIFfu%2F6dPn8DOBxkApmFsIsMAHFhPnz5FaAYZcHkPUS6AxTU45OG2gww4" +
		"s5EkA0AGQQwB2QzCu6b%2B%2F99oRzAWkFPb%2F%2F%2B3T0AMWdcM1kyqAWBXYMFkJWna" +
		"aQIA%2B%2FnHBNYa1kYAAAAASUVORK5CYII%3D";

	var UpHoldImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABd0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjW9Ra" +
		"PJAAABnUlEQVQ4T5XSTUsCQRgHcD9Et6APEtQhOklIFpnmmliQJWZQSGCE2IumpmJiWBka" +
		"vhyyQqEISojq0iEKSurQQUm8FB4y6hLxb2fWVcLatoFhhtl5fjPzPCuRCDTz%2BDlIF9rz" +
		"6zfTaBorrjwC7gLI%2FF%2FI2HAEqUQZ8c1nJCNlOtfrNsUhI8wWfI4cdhKv2N%2F9wGEa" +
		"2I69wDV3jSF1RBhh5FGsOksI%2B18QcBXJZtp99jyCrif47Y%2FQdEd%2FRgyDSTgsd9hw" +
		"vyPs%2FaSBI9Im4MBTg0LuNzgt99CrY9%2BRgZ4ZLM2ewTp5BaupQANQzMEobaaAso2FvH" +
		"K6vmC%2BwaLlBArZdB2ZMnqgVfgw3J%2BCpitbA2xaFjiNQtvJAiEdXTcwGTC9y5gYdTY8" +
		"hW7Qq445gA20KTnAKGfHKiBUTpDGJ44C1RtYCCQKGG9BpVKh7ycAHfl5NQd%2F%2FVA0Wa" +
		"VSqR5MgNusqBvwOM187XQCXGbEA0zPGpdEgpCTST8KArZ2Pj%2FCr1DJ1jng4YJD9uZpsG" +
		"hAp4hD25eqVYOvChk7Wh0Ntf8CLzhbljEHqb4AAAAASUVORK5CYII%3D";

	var UnHoldImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAAOwwAADsMBx2%2BoZAAAABh0RVh0U29m" +
		"dHdhcmUAUGFpbnQuTkVUIHYzLjM2qefiJQAAAYlJREFUOE91k79Kw1AUxm8XBxcHRSiCoF" +
		"IHwUGKIIjSwamDoFOGQnFxqb5At%2BJYdBMcMvgEAQcHxUWyCOqkk7pJXiAv8Hm%2Bc%2B" +
		"%2BNN2lz4dDk9ny%2F8zcNU3NgTFv%2B2hGbE3toGPNe51u6F%2BFQ7EssFRu7dz7zLhZr" +
		"1oKcKHSSK0PTE8CZXfk48SC4xWl3Abi%2F9BAFuUyYzXrh68hxKMbvJ866SwqI9gR0dYRU" +
		"9K6MgfwmIYDEsDYQMOoL4PkW%2FQMBrC1CIoTlUNPhTZvpu1q9gwpHkQX8zM8Au61qP7TB" +
		"vjFjAng8SAHMYLOF19VZ4OakCuhoYFf%2FUAHny8jzXNMnANtbQHRsn6UHlYnYzFmHK0Gb" +
		"TMcsy%2F7FhH08TcuAjYwJaHI0pQlsrNjIFNPe7qYBEtH1%2FILwhWUQmLLbHJ9Gpj1eA6" +
		"P9ogTuQCmoz4JiTYuA7xcLSS5UXAHQz0b3Ry4OSRXz21hMpJiMjeyDTGxzsRPOiR8VO03r" +
		"ibFMBihHnsRoHzgZLgqjacQ64R%2BfjECTJf12xAAAAABJRU5ErkJggg%3D%3D";

	var UpUnHoldImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAALGPC%2FxhBQAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjM2qefiJQ" +
		"AAAgpJREFUOE9jYMAC6vXXCzCo%2FtvOoPb%2FP5B%2Bw6D63%2BC%2Fvb8ANrUYYpZ6t%" +
		"2BQZ1P6dB2tW%2B%2FcbSn8HGhRB0BBVzef6QIX3oTZfZ1D%2Fpw40ZD7EELBr2nEaIqz%" +
		"2B0Q%2Bo%2BD3UxuMM8v8RTlb5lwGU%2Bw6V2x9ruE8Nxen86l9D4QpAfpf%2FzwFVsBRI" +
		"gzADg8o%2FC6ALnvOqfPo%2Fmy%2Fp614el06IGrV%2FBUhO7EcyeWmal8jS%2F1t7YYYs" +
		"3cXhYrKX0%2BH1CQ7z%2F7u4XP5LKz4qBxoA9R%2FIIARY%2Bv%2FxlaU5XtJgAyJsRZb%" +
		"2BizLee5mB4eV%2FBoYTBzhszwkrv4GEC9jpwBBGiw6wAY3xQAN2zVq%2BU5bj%2FH9xvj" +
		"c3GBje%2FGNgKAPqYgNqDgCHGSiOMfwL9Pf%2FgwuW9vlJrPlnoXfnBQ%2FLm3%2BSAi9j" +
		"GBj2olik9l8Dmb%2F0%2F%2F%2F%2FcP%2F%2B7ynZ%2FkyI9fl%2FWYk390XYHv8v99kE" +
		"D1AcKWrp%2F1y5pZ8%2BfVr6vzDrBEjjfyWFN%2F%2FcnG793zR51f%2B%2BQESM4EqSwg" +
		"wMq%2F4ZyVz9raQE0ZyWcBYUFv8v71n6f0YifgPAwcnAsBIcUEpSz%2F9P69kN1gzCZzbi" +
		"NwCo0RWID4GiCGSIDgPDBlD0gW0G4V1Tl%2F5vtMPuAqCGLJBGqOZaIM0LCqz%2Ft0%2BA" +
		"08D%2Fdc1gzTgNAGpIgWrGSAvQUIfHDLawAwCFFjDZQBBXSgAAAABJRU5ErkJggg%3D%3D";

	var OnePublishImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjIyt5EXfQ" +
		"AAAoZJREFUOE%2Blk%2F1LU1EYx%2Btf0V8KhKTELBTUlBIslJXNpqj4li7nbIpu7W4unb" +
		"pNHZnOiS8VtaHbaKTbdL4Mp5iJU0sXqdehRBtFENSGv367Z7C76Q8SdOG5hwPn8znPec5z" +
		"zgM4918fEcSGZq4xvsvZQLU7BHTrVG1Q%2Bq462Py2nBaZSynBeFH86fWnYBGnc6Yh8Gaz" +
		"H%2BuBZXh%2FbmHzxxpmfFbInXWoMXADla%2FucmIlrEAzR2BhaO7AiqM%2FB1j45oCBHs" +
		"bLPR2shwasfF%2BEekmMkrG8EG84l5WEBepZURwD%2B6f3TTj4vYvR3T483%2BhCr6cdOq" +
		"8GA141erfbMH1kBeXko0B%2Fy5%2FfnxUXrh%2F5MbBscFWJL7%2B2mR0H0L%2BjhuqDHL" +
		"UWXjiUG2JIl%2FmQrdbDfmjB%2FbFs5D5Ll7GCNnsd7fBZmFSN6F5XoGNNCvGCgBUQSfN8" +
		"KUSzRRj6pEHfSitu9qbSrEA2WXPs%2BurA4GcNA0tOgJEsyCicKYTMXQOTdxSZmpRjViCx" +
		"Vh5P%2BSag3qKg8DSdKaBc1TB7R5DWlRQVNFnKaL1HDd2OCpL3fAjd5ag032NFZGcSj508" +
		"DHqU6HE9wTVlYvQIwoli2UMzBzafCWJ3BRpcJeEzR9InsMDOReN0EWz7RtzQpiDpaUK0iH" +
		"xjYVzV6wI%2F5azFJD3OFoyA9XYSBOZhas%2BAR6YHBPYnyi9Gr5EUo%2BxFPqd45HaoxV" +
		"YBOyPRezpBzVdBOlsB%2FXoHbAzMN3GRpEgIMfDJRoq0Jncoh8PRZQfyhjKgXaJg%2FKiD" +
		"cUsHlasFmT3JBA7EwuwtxPb2nb6M%2BBxtGpXVfZ1OVyUHUzsvB6%2B2X6KvKBKoRPmFsx" +
		"%2FT6Zf2L%2FO%2FLF9%2FXbmFqeQAAAAASUVORK5CYII%3D";

	var UpPublishImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjM2qefiJQ" +
		"AAAi1JREFUOE%2BV0ttL0wEUwPEfEQhBSNPIlmVeyKx1v2hFamCZkGlrQ6ebusV%2Ba7%2" +
		"Fpz033W%2BbmLaHCSw%2FRQxB0ISTo9tJqFVvimIOlXXxIswcv2O25f%2BCb1FNkzs7LeT" +
		"iHD4dzjiAsEocas9lmUbNYzz9rW8UMOocVWkIN7Kjf9H9IupjNxel%2B7vAQc0yiJiiy0Z" +
		"61NOSAN5%2BGiAc%2FYcRPCrXjTk4GTVQ%2BOUOOc%2FviyK6GXPqmrvGYAPUT5zC9saEN" +
		"GzHFRE75DZy4qyNDzFkYyTTv5Pa3BzxjkNbpLuxjTtxTPqoidRijFrQvDZQ%2B0qEfqCNH" +
		"3vcnkttZwsD3p%2FR%2BuM6FmR76f1xFfttETcj8K58OVKANVlB8v4zDV0oov1FHsjXzN3" +
		"LQe5TEUjW77QXs8RSialqL%2BL6erq892GNOdP5qpHcu8u4Vou5IR%2BPJJdW4mb3eAlJt" +
		"C1wnrS0bedaHbbyVymgjurADbcRK8aAelbKEn8j0bUGe89I0cwnXVC%2FVsWb0UYljr3So" +
		"nCnxT5nuzsIyJmEbUZBGfFjmn8kwdJbjL8pIcqyOD2jaNbTMnaf1YyfKaDtS2I15yEZ5qJ" +
		"w0ZUN8IMuTgfeLh%2FaJNpyDMlJQwvTcQEmgiBTXmvjAOrca%2BbOEMunBHnXQOOrCFKmm" +
		"KJRPUvOq%2BMD67jT0s1VUTlqpmZQpe13LkeFS9g%2FnkdyxhB0I0jISbiUidCcg9M5v%2" +
		"FbIKoW8ly2%2BuQHAIf03wEz08OpOH9X%2BCAAAAAElFTkSuQmCC";

	var TitleImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjM2qe" +
		"fiJQAAAKBJREFUOE%2FNUcsNhSAQ9Mj9HajDjuzFIiyBFuyAEiiBEjjue0McA7qr3nwkE5" +
		"IJ89llGP72OOeE%2BJUUBXZ3CJd1VtEYmQZVOE6fDuBgHEJgm7MBk6%2FEpoH33qzN5FKK" +
		"3gBibWngMXNKSSA2DbiYdnHkKOS98af5tW%2BqXGtgieEmOWeJMVa0aXfJrNLNeTS4St4N" +
		"8D3GvBjl0bH28Ej83qMvQQ78xptD%2BLgAAAAASUVORK5CYII%3D";

	var NotTitleImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABd0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjW9Ra" +
		"PJAAABMUlEQVQ4T61TIY7DQAw0DK0Kwg%2BXlBVUB%2FqAQ31AUb5xoDS8L4hCCquooDz3" +
		"g5Wu5FieEFCQ7lieyIm20oFGsrLZeMbj8a5I4hlEmhjDLJpU7mQvAkoDFfPkuL%2Bzf2WS" +
		"yKqOVbIsqxgRUDG%2BRX5qkfCqcuQRAfB0PSYDRCCBWiWJiw%2FKZu8Arw%2FLSWAPxHVd" +
		"qxrD7ECAvlU6En4%2FVwE%2FPYEHOwLgShDA8SLP81EySdgGK%2Fd97xWoqSAYvhaLy9w0" +
		"GKUJUW4IoQL4JcFW5IJEbxy%2BSUIw3jYNMI8KGhL4cXH92O9ViQeb%2BQVbKDGWruuqtm" +
		"01WBEkWJPEz968UxN1jPfb7eylsiLfHLGvjjaUFAr%2BNpuQImDPBvR3ZHqkYZgd0fHYej" +
		"Dlz4%2F8%2By5TwqB%2FXecn0UpMs%2BAT2AEAAAAASUVORK5CYII%3D";

	var OwnerImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjM2qe" +
		"fiJQAAAklJREFUOE%2B1kN1P0nEUxvkv%2Bg%2Fswlu31kVdtqxo1lYrygI32%2FQqZbbK" +
		"NDQTzfzRwDZNkxcNXIIKvqHTlEQRFNyaqa31IgK6JpJvgfnyCeiqrUk3ne1sZ%2BfZ8znf" +
		"5ysS%2FY9aW1%2FB1NCAPFtKgUSKsb6exO6fbn3bXkavVVMkk1CeK%2BPBjesUZF%2BmuU" +
		"lFQjsUEowEOGCdrMxjXDqeToX0IqVXxGRlpCE%2BlZHUQt%2BDh0MgjPjkUZyGarbdXWw4" +
		"LTi0lZw7kUZCSxmjXlNC%2BhERa%2BMm8FnAaybsNCZ3CS0lYHLUQm3RVfhkJ2wXkp2YhT" +
		"sS3I6OwwFb2xHYD6N7UshHaw2rA09Z7a9JzvoaeTxChGhsI%2FUrFn3D2IR8%2FH3V%2BL" +
		"uVWGvz%2BOod4mBnM7U5kTG2FqTsVibtVTm8VspQ5J4mGg6kNodi4J4N4Rz3UFogZWbwJV" +
		"57IyW3bzLmdOOaDRKM8nfQ%2BJeftAz7eW6extTnRqdvwvW2k4nRLrS6Rtp6J6lrn8IwtI" +
		"jz886fkJG5AGV6Fwr9LM9sfoxjQepeWegc7KFjoBdNqxmTI4DKukip9h0K3QRv3i%2F9hq" +
		"zE%2F7aqpQ%2Bl%2BQOCfQvN8A4m7x6PtD0E9mFpD8qbuzFO76IeilHbv0ll%2BwJKQy%2" +
		"FLca9o6QDkmjbyNS7krcsobFG0PihpdbAQN8%2FvQrFhhGYvPLT%2BoLAlRJ56gkK1CX%2" +
		"F8gMg2M8%2B9xh7OF9u48NjH2bIprqmmyFF1obJ7EPrdyIROJIKHMwoPWRVexPet3H3Rjd" +
		"U3xy%2BIOdbPzcXblwAAAABJRU5ErkJggg%3D%3D";

	var NotOwnerImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABd0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjW9Ra" +
		"PJAAACkElEQVQ4T22T7UuTURiH91%2F0H2jktxAkSKMvhaVtCoVJxswKlPqgUlamqFmpKw" +
		"tXoWnOTXNZvk5nS5S05svs2QwaaUSQL7OIpmaJE1%2BunudsM7UdOIfDOee%2B7t%2FvPu" +
		"eoVCEaKpUkd3Z0KdTZbWtygFEJWmxrwlxVRXaKlsxkLY2VlSyM9AeBxpCgQFbpx9I3jIYK" +
		"LqUmU3QuleunT5GZcoLamnsoe4Ek29UEMyvZN%2FiFJjaK4%2FsiKNYmkp8UjyYyjPhDkW" +
		"JvdsEThPiVyEG7A15zgt7jY8Kxm0pZcrSzaG9hwHCLuOgwwCsf8bdAjFqZKL43Jb0%2FGC" +
		"W8zg2awdUCzma89kYidqmo1OdtBShxRgWgVDwnSB7pb0GKDBcQr61cdL7YKL%2BcjGOgdS" +
		"tArZz5J0Um%2FFmah3UvdWVZzKljBOTnSx2fO3QYddmyhXmWfYsCIg%2F%2FA4IqJl19WM" +
		"ozWEyIFpCOu%2Bl8dfaysfI7pALFgnrr3frmPBSej%2BVFyRk%2B7Y8QkGXvzGZwQIFSdG" +
		"HBXwy5zfrA4Z7FPjhKfqaWsZ4nOG3V2PfuEZBhtwfPsjIVFpTEoojiGh2Sh%2Fq%2BKR42" +
		"S5i7HdQZaxh%2B08ZQfzuGumomNCcFxNQ7yQejNfgq%2FconisoYOpxEgdHNfcsUjW89PH" +
		"jaQltPF62vrOgbmjEPzCCpz27%2Bj%2FHCUr%2Bl73JtS%2Bq7ccSlIiVeRN%2B3gtm5xg" +
		"1DFzPrML0GRbWdNEqrVPT6kBIuMBKn5bbJivywUU1vQLb%2BGRn6YZrS7ogMtkc95DUMMC" +
		"EHj69Cruk1rc%2FdYq8pTUd6xRBZFWam5AQqy9g4V6u7OJZrIeGmi6OF77AeSN75lcXakY" +
		"JRNMVO4q91cOVxJx2uj%2FwFtXwTBuq9OecAAAAASUVORK5CYII%3D";

	var CrossImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAIhSURBVDjLlZPrThNRFIWJicmJz6BWiYbIkYDEG0JbBiitDQgm0PuFXqSAtKXtpE2h" +
		"NuoPTXwSnwtExd6w0pl2OtPlrphKLSXhx07OZM769qy19wwAGLhM1ddC184%2Bd18QMzoq" +
		"3lfsD3LZ7Y3XbE5DL6Atzuyilc5Ciyd7IHVfgNcDYTQ2tvDr5crn6uLSvX%2BAv2Lk36FF" +
		"pSVENDe3OxDZu8apO5rROJDLo30%2BNlvj5RnTlVNAKs1aCVFr7b4BPn6Cls21AWgEQlz2" +
		"%2BDl1h7IdA%2Bi97A%2FgeP65WhbmrnZZ0GIJpr6OqZqYAd5%2FgJpKox4Mg7pD2YoC2b" +
		"0%2F54rJQuJZdm6Izcgma4TW1WZ0h%2By8BfbyJMwBmSxkjw%2BVObNanp5h%2FadwGhaT" +
		"XF4NWbLj9gEONyCmUZmd10pGgf1%2FvwcgOT3tUQE0DdicwIod2EmSbwsKE1P8QoDkcHPJ" +
		"5YESjgBJkYQpIEZ2KEB51Y6y3ojvY%2BP8XEDN7uKS0w0ltA7QGCWHCxSWWpwyaCeLy0Bk" +
		"A7UXyyg8fIzDoWHeBaDN4tQdSvAVdU1Aok%2BnsNTipIEVnkywo%2FFHatVkBoIhnFisOB" +
		"oZxcGtQd4B0GYJNZsDSiAEadUBCkstPtN3Avs2Msa%2BDt9XfxoFSNYF%2FBh9gP0bOqHL" +
		"Am2WUF1YQskwrVFYPWkf3h1iXwbvqGfFPSGW9Eah8HSS9fuZDnS32f71m8KFY7xs%2FQZy" +
		"u6TH2%2B2%2BFAAAAABJRU5ErkJggg%3D%3D";

	var FilterImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABd0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjW9Ra" +
		"PJAAACT0lEQVQ4T42TTUtqURSGtwMn%2FYH%2BRuDUn1CDRtLAypC0ohDFDHGoGQ0yaVBN" +
		"hDRzopIOLAeFKArOtIIEC00T%2Bx4kfpfvda3w3i5EuCf7nLP3etZ617uORPRXr9dDvV4X" +
		"hUJBVKtV0Wq1hEQiEQDoWHx8fIhOpyNGR0eFTCYTIyMjQiqVSviw3W7j9PQUZrMZa2tr2N" +
		"nZQSAQQDqdxvX1NfpAgvdZwPv7Ow4PD3F2doZarfZF39%2Ffh1wux%2FT0NEwmE7a2tuDz" +
		"%2BZBMJnF5eYm7uzu8vLygXxVDaDkcDsTj8S%2FI0dERxsbGMDExgYWFBdhsNrjdbiQSCV" +
		"xdXaFSqXDm72uQqFwuQ1AGj8eDpaUlKBQKLC8vY3d3lwHFYhGNRoNj%2Bz1AMBjE1NQU5u" +
		"bmWCrBBV2IxWJ4eHgAEU9OTnBwcACXy4W9vT2sr69jfn4ek5OTGB8fh1ar5X6lUimGcx9e" +
		"X19xfHyMZrOJt7c35PN5Lj8ajcLpdEKv10OpVEKn02FlZYWTUAwH0%2Fr8%2FMT9%2FT28" +
		"Xi9Tb25ukMlkEAqFYLVaoVarOZB2ajCVTjF%2FAfTQ7XaRy%2BXYJrKNLPT7%2FTAYDCxh" +
		"ZmaGe3N7ewu6%2B1%2Fw4IUkUPO2t7c5%2B8bGBts7OzsLu92Oi4sLtvPH4MFHsoz6kc1m" +
		"Wb9KpYJGo8H5%2Bfm%2F4fmNQNrC4TAeHx%2FZCdJtNBpZ3q%2BZvx9GIhGePhoyAlgsFt" +
		"Y%2BNKBUKrH%2B1dVVLC4u8izQUA0NoEaRCzShm5ub7Mbz8%2FPwAMpEkKenJ%2F6ZaKdR" +
		"%2FqmCP5zZH%2FMYJgQ2AAAAAElFTkSuQmCC";

	var EraseImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAALEgAACxIB0t1%2B%2FAAAABd0RVh0U2" +
		"9mdHdhcmUAUGFpbnQuTkVUIHYzLjW9RaPJAAABXUlEQVQ4T9WSS0sCURTHj87LfMzkmGWC" +
		"htHSTYu%2BQ0LUIiECKbQiWiRi44PMSZ0mIXc9PlvrXtAD0WoVEf%2FuCLWrGdt1dxfu73" +
		"fP%2F5xD9G%2BPrBCqtST%2BFCDoIezrAh57teEFCk8w9Ai6z%2BtomPxwglEf4agVQv8l" +
		"i6ZpRRCdC0SBsJufw0O3iEODoBsc6nrCmYAnQu%2F1FsSylw%2BiTBBGUUtAEsleIJOE%2" +
		"Fk0XH3jDO%2B4geglaaRZWRbYTmKJJLMRSCFMYjMb90zVMQ4N%2FxAEcY1AulgGugHQkDY" +
		"WCrPsmeBLsfx5jZW%2FGGXwJlAIFbKtbWE2uwE8ee9jKZQnOlzvIyRnk1R1sqGuIU9QZ%2" +
		"FNWYCZKxODOPpekUxin0MyyyUbhcBFUR0art4bTd%2FH7sIzciUnBwvzjuoFGuIOARwLGx" +
		"Kn4OHOPIgmXZjUI%2Bi7ZR%2BbXMsxMDzboGr0QDSYBt5ifdXaPbJRpm2gAAAABJRU5Erk" +
		"Jggg%3D%3D";

	var MemoryImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuNt" +
		"CDrVoAAAIsSURBVDhPjZPdS1NhHMf1f%2BhySHQTXRS76Caz2EVdFEG7CIKKGIhUK3OlS4" +
		"01jy%2FppguxInKFx8BGibHaCDWXB4IGwdygmeVLHZxhGdh6x17s0%2B%2BMBNGz6IHnHM" +
		"7L9%2FP7%2Fr7P8xQW%2FB0ul4rFsmbpcdV9YmK2IBg8Upj3h0AgysrxW14syuXnIlS4Qz" +
		"jLVf4bsFz8%2FRdUNdyjrn%2BGsuNd5pDlDlaKv%2F4Af2uUFpnN%2FignK0ycLAFUVWUk" +
		"mcrZPudVOHTYwacFmP8GHVdVZj5Cky%2B62oUBMCrX1SnEhjWGHmocFLEv0M7loMrbL3Cq" +
		"RuHle2hsyQMwAvMK4IGIB2IaV66pRAY1KmsVMlK5%2FIzC2Duob84DMGx7xHb%2FkEZmLs" +
		"vGTVaKt9mIP9WZkspOt0JyVlyeNwG0SQsLkvbYpJ4TGz2Pv84yOp1lah6eSeXoE514RrJp" +
		"MgG0tkUx0q71KJRstxGLp%2Bi9r9ETkTxGdDYX29hf6iL2Cs42mgD8AkiP6%2BzaYyf2OM" +
		"W%2BAw5uilgNaxytUrh0W2PrTjvXh3VqGkwAPlnj1HOdMqeLtNjesdtO912Nzj6N0tMKPY" +
		"909jpc%2BPpSuOtNAMYmefMZtpTYsBStpaM7TPCOxkWpfCGksW6DlaL1VrqSUKmYAIwdNv" +
		"0BJiWw0TlISNoD6SzhVJbIC7iVJifuTOQBuKtD3OhN4G8fRJF19spSeSRtI7Bq6dmwbVQ2" +
		"5rET%2FzhUeU9bng9%2FAG8cBABPvbmCAAAAAElFTkSuQmCC";

	var EarthcacheImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMU" +
		"7nOPkAAAC4SURBVDhPY2CgEfgPNBcXJmjlf4VIgf8M5%2F%2BD8f8nMSgYajBOQ8CaMQw4" +
		"KE6UIXDNMAPAtoM0wzCSa9BdgqEZ7nxkA%2FC4BGzAgisPUP0O0gADBFxBuQEgJ2NzwelZ" +
		"ov%2FR8Zltnv9BGDkcwE4HGYBsCEgjejQi83EagGwILD3AaFgMgWlIYgMDFBfAXIJMgwyA" +
		"pREQLWLOhWIAXkPQNaPbjpwssboE3QCCyRmbv%2BFJHMnf%2BHIV0bkRADwOuX%2F5Y8Yg" +
		"AAAAAElFTkSuQmCC";

	var USAflagImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA%2Bf8AAIDpAAB1MAAA6m" +
		"AAADqYAAAXb5JfxUYAAAFuSURBVHjapJM%2FSEJRGMV%2FLx9lWGoENjhkOCVBBUYRUUNZ" +
		"0aBE2VbQHOLaoBA4GrRGEIoIgdLQ1GASGlTjW2oI4U06GCTRH8yQ2yKKPIdeHbj8uMN37u" +
		"HjHkkIwX8kAd1AX4N6VAPeZcDi8RyV%2Ff4Z0ul7OtF%2FdtjRYeD63CYBjuPjW7VQKLKw" +
		"4CKXe9QwGt3sHF%2BSRmTAkEzeEAyuEovn8XndxON5vF43sVgOn2%2BK1%2FXdtsGuwQG%" +
		"2BH54ADDLAxsY0qdQdK8sTJBJ5trZm27hzFdO8Xq%2FXQZaRAU5PswQCa5ycZNnentfwZX" +
		"yxPbrVwmf5uXl3HhxciOHhfREOXwqrNahhJ1UqFQE4AZx2%2B56IRDLCbA6IUOhSw3Kvo%" +
		"2B0828bEw5CrZaCqqtArVVUF4JQBSqNzmIwmRPXrd7%2FP2EOp%2BtHagaIouhMoitJMUC" +
		"tPLpH5WxVqEmAB7EC%2FzuE3oCgBhkaRDDoN6kBN%2Bm%2BdfwYAI5QcdaFOn%2BAAAAAA" +
		"SUVORK5CYII%3D";

	var TableSizerUpImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJJREFUeNqkU1tLVFEY" +
		"Xec2MzpzZpqZzKZGGyeLLgZWWtJDFiVRYb4UBUaSIETvFRQIUb3UQ1QP3SAy6QZBhS%2FV" +
		"QzcrpyyjVLpIiTpkM6PO6NzOdU7fzAn6AW1YZ2%2FO3t%2F6vm%2FttZmB%2Fs%2FID4Hn" +
		"wBka2GL33ERR2UFGzW5lJ0fKDEUW4J4b18XS93Zt%2BoowE36aUTUYYKFpGvh8sEFgdRWy" +
		"N9im6PxF14uznHe0C3b9O5hcEgoX9MTFuoXxuoO7pfIVXZbIl72qLM0ADJjBfAWaDMa%2F" +
		"7DiTiLf7O5vgkD8AfmK1whwqIQHkokB48zWkNu7%2Fqg8PbFRl%2BTcrZTPgSoONFlVqD1" +
		"xqgIOh4PUUMIdQvQvY0wsEFgBLqcqVQPn9VrhfXV%2FCV1RdsAk82BzDg3N7jpU8OA6b%2" +
		"FgWopcA4oZxYajsBsQaofwBwXsCGwn7Jw0NwxYZ2srP929li34J6x8jgWmt%2FB7CEDiQJ" +
		"i7cBdU%2F%2B9WCvBjY9BxzzAQ%2FA8xMo7r4D3uPdwQrO2fuE0Q%2FgctNUIx0WfcDqjr" +
		"%2FB1LyhEzQiqQJqrpFe9LuYChp7Basq1fIkZNBIJszgIsLkONB%2FhkSsoFQi4GoGIudI" +
		"cEo9eMcUlM6xSgSQNQ%2BvKfp3XfRuSFMBrnzvFsKn08BPmuvvmi1MDQKhq4BCa7p4JQZI" +
		"JXRNAiZZaWL0BhOoRTzjgxKGWaLyVwuFMwkkO5CmmbqBDMSGaCvQAN1mf8smo%2BOvFX%2" +
		"Bgm1%2FXgnAIeW8AZBHE8mQWkyAfHEWhzRmyTYorg3VTEyaHhx%2ByDDkw%2FGP4pPPAYU" +
		"i%2BNRi6ZWaBRJiOmgTZeMGuiffA0EuyyJHziDtLb6ciY4%2BZUKgHcjYNsXzR0YUe16lI" +
		"%2Bx4kux%2FBOw9wllJS6jlL%2BkyNEW%2BRExUnbiO1akvfSN%2B7BqvATzE9PW8KVWfS" +
		"KYj%2BiuZgZeVl7XmXPfnsHpSxbuhyAoK7CvaaRohNLYjZZt0c%2F%2Fiu1WrhFY4XUCAo" +
		"PKicDlVVYbA2t295dZvLIWxjMhnyMCtAYKZk3t7769vXq%2BloOCS6nPlnRHrR1zAM%2FM" +
		"%2F4I8AAveYyMfSGOwwAAAAASUVORK5CYII%3D";

	var TableSizerDnImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFJREFUeNqkU1lrE1EY" +
		"PffOTNI2k9Qk1baY2jYVLbjiCoLUohUXVBBRUXFFUd98UETBBRX0TfRBtIhSkfoguIMKWp" +
		"dK69IiplVxqTWtttUkTduMyWwZv2mCf8A7HJjL3HPud873DWsLhWAvUeAQLB08z1cUzw3s" +
		"hp5czKPfS6BrEhtR2G%2B4i97IRvyCNNj9WNENWOAwDAOiTbYsgJsaNH9wu2Y6zuU%2FPS" +
		"34w3cha%2B%2FAtCg0Z6Wv31tVEZu9a02ydPIdZ%2B%2F7DZqaHCQWWHtbGyxdBRtdeZQN" +
		"9B8qqVsBOdUCBEjZIwASAxIG8AtIR4Gu%2BRehVG%2F9aH4LVasptZcnlSGIo0qXOQ31UN" +
		"m5GsiMyHOJPJpQfQpYrwPz9gDj6b7JQOmNbfA2Xq4UyyedzXVK4GkmQPD5DxbcPIJc8wMw" +
		"y%2FZEcBHy%2FMP5wOmxqwUKCTOAglt74Yl8XsULAkt5XnFZlet7%2B%2BycUB1QSQfULN" +
		"lrE%2FWMQC7L7E1CEbkSI3A1XoPo9S%2FnkmfkRke4FYIZzxDtm4yskJEV0CmvVLYy0oKb" +
		"uhZuRI6hzuSMI2gNEVnMfhwijDkJTLhEqZlZC1OAabVEnA8M2ntbtI8u0XyioZufTNk3Tx" +
		"kA8n%2FShwIyGtxMh2zDdgVpYOSGTGkqtSb0CFoPvRYHYEk8ylO%2Fw3WsfCbiSjH0GJ2L" +
		"kPK91cQjRUhZT4T%2BduDhjuEcIp0kXVaDtNP1kid%2B9bzQA%2BXPxTmb0P0y4w8tz4A6" +
		"KtfOxV7Rt0BtFTG7MNhBY8FL4FiwAtHOjlucpQ10d3w77t65D2rRLHy5mu3AZ5qH%2BnU0" +
		"QI3AlZUUYhQDYeBrAzBq%2FxnE3YX1id6uB6y5uRlqUoFnzLgDFT73ib7Da5F4fh%2F%2B" +
		"EhpEGRAogj%2FUkViEfEselB2rR2L6otbOllc1TkmIsaampuEqU4oCOVC%2BPjg2eN58ct" +
		"s11HAdWt8baoQCUR4L16SFkFduQcTpvdrz9vVWhyRogijin4CVTlPbNVhCjrd44tTtHpe4" +
		"hCnJUgtMoiemOVyvf374WKv8%2FtEs53uGZ4IxBmbZv%2BJ%2FrL8CDAAtGDlMBzStGQAA" +
		"AABJRU5ErkJggg%3D%3D";

	var srcImgGuage =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAC" +
		"G0lEQVQ4jd2SMWhTURSGv%2FPyEoprHUxGsU6lg6IgJnkpagNFoSmIQkEkIVWwCiqt1K2T" +
		"uDiIoHRoulQ0GRJsaVGXJNVBcBBRESKpQ1W6Sgwlr3nHwTStado0k%2BC%2FvcN3vnvOux" +
		"f%2Bi9gZ60IlY53eDWs2FioZywP0ocx7IjkHAJWbAsvAHEAlHdqD6AAqSU8ku7a532gUCt" +
		"InyKwI59drin5C9HOdEa4JMiNCd8sJVZkX0SxIGHhcUwyh6mwwelxEpt0D2XdN955%2BG2" +
		"HuY7gut9Mh085YHU3hPyublXSw%2Fl0M%2BfcWQ4GN4RYL1sTrL8Ef%2BYJ1cjvJdin2%2" +
		"Bi8WQ4FfS72B21D7hwoLCl8N0dFWgng8fjAWi92pF5SzIjqnqk8AZLeTjIyMdJTL5XHHcc" +
		"ZE5FkikTjXjNtyKY0ZHh722bZ9q1Qq9QMHgJ9ut%2Fv6dry5WLD6RfSQo5IPduXyjYBt22" +
		"lVPQqgqrhcrvHJycnvm5liyD8IckpE3xgieklgwhC92ihLpVKoas%2B6zDCMcmdn56NGTo" +
		"QToAOqWDL7IWzajmEM9ixUmq0QjUYfVqvVywAul%2BvB1NTUloP%2FWvlM9%2FO1nQCv13" +
		"tlZWXlKYDP58vuxELtlu%2Flhzjs%2FRYUdF%2BwK5ds1bQ5S73%2BKLCqSnJ%2F9tWaAX" +
		"DEt3zDFCdniMbakQGgHAOZAe5C7dmo8sJBRler5v22hUIceInwvu3ef5LfulzF72L2rC8A" +
		"AAAASUVORK5CYII%3D";

	var arColorCodes = new Array(
		"FF0000","FFFF00","00FF00","00FFFF","0000FF","FF00FF","FFFFFF","F5F5F5",
		"DCDCDC","D3D3D3","C0C0C0","A9A9A9","808080","696969","000000","2F4F4F",
		"708090","778899","4682B4","4169E1","6495ED","B0C4DE","7B68EE","6A5ACD",
		"483D8B","191970","000080","00008B","0000CD","1E90FF","00BFFF","87CEFA",
		"87CEEB","ADD8E6","B0E0E6","F0FFFF","E0FFFF","AFEEEE","48D1CC","20B2AA",
		"008B8B","008080","5F9EA0","00CED1","00FFFF","40E0D0","7FFFD4","66CDAA",
		"8FBC8F","3CB371","2E8B57","006400","008000","228B22","32CD32","00FF00",
		"7FFF00","7CFC00","ADFF2F","98FB98","90EE90","00FF7F","00FA9A","556B2F",
		"6B8E23","808000","BDB76B","B8860B","DAA520","FFD700","F0E68C","EEE8AA",
		"FFEBCD","FFE4B5","F5DEB3","FFDEAD","DEB887","D2B48C","BC8F8F","A0522D",
		"8B4513","D2691E","CD853F","F4A460","8B0000","800000","A52A2A","B22222",
		"CD5C5C","F08080","FA8072","E9967A","FFA07A","FF7F50","FF6347","FF8C00",
		"FFA500","FF4500","DC143C","FF0000","FF1493","FF00FF","FF69B4","FFB6C1",
		"FFC0CB","DB7093","C71585","800080","8B008B","9370DB","8A2BE2","4B0082",
		"9400D3","9932CC","BA55D3","DA70D6","EE82EE","DDA0DD","D8BFD8","E6E6FA",
		"F8F8FF","F0F8FF","F5FFFA","F0FFF0","FAFAD2","FFFACD","FFF8DC","FFFFE0",
		"FFFFF0","FFFAF0","FAF0E6","FDF5E6","FAEBD7","FFE4C4","FFDAB9","FFEFD5",
		"FFF5EE","FFF0F5","FFE4E1","FFFAFA");

	var arColorNames = new Array(
		"red","yellow","lime","cyan","blue","magenta","white","whitesmoke",
		"gainsboro","lightgrey","silver","darkgray","gray","dimgray","black",
		"darkslategray","slategray","lightslategray","steelblue","royalblue",
		"cornflowerblue","lightsteelblue","mediumslateblue","slateblue",
		"darkslateblue","midnightblue","navy","darkblue","mediumblue","dodgerblue",
		"deepskyblue","lightskyblue","skyblue","lightblue","powderblue","azure",
		"lightcyan","paleturquoise","mediumturquoise","lightseagreen","darkcyan",
		"teal","cadetblue","darkturquoise","aqua","turquoise","aquamarine",
		"mediumaquamarine","darkseagreen","mediumseagreen","seagreen","darkgreen",
		"green","forestgreen","limegreen","lime","chartreuse","lawngreen",
		"greenyellow","palegreen","lightgreen","springgreen","mediumspringgreen",
		"darkolivegreen","olivedrab","olive","darkkhaki","darkgoldenrod",
		"goldenrod","gold","khaki","palegoldenrod","blanchedalmond","moccasin",
		"wheat","navajowhite","burlywood","tan","rosybrown","sienna",
		"saddlebrown","chocolate","peru","sandybrown","darkred","maroon","brown",
		"firebrick","indianred","lightcoral","salmon","darksalmon","lightsalmon",
		"coral","tomato","darkorange","orange","orangered","crimson","red",
		"deeppink","fuchsia","hotpink","lightpink","pink","palevioletred",
		"mediumvioletred","purple","darkmagenta","mediumpurple","blueviolet",
		"indigo","darkviolet","darkorchid","mediumorchid","orchid","violet",
		"plum","thistle","lavender","ghostwhite","aliceblue","mintcream",
		"honeydew","lightgoldenrodyellow","lemonchiffon","cornsilk","lightyellow",
		"ivory","floralwhite","linen","oldlace","antiquewhite","bisque",
		"peachpuff","papayawhip","seashell","lavenderblush","mistyrose","snow");
	
	var stopwatchSrc =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAE" +
		"yElEQVRIx7WVa0xTZxjHyaJzRBlBpqnTxgphQkNtIUBplDrDxRE2Cw0QxTBr1C3i3AYCce" +
		"C4jQpaoJWbF1CPgjqug4GXaeJxToYtSECJy8a0SguCQk8R9Qsf%2FnvPqSINGozbTvLL6e" +
		"mH3%2FO8%2F%2Ffm4DDDk%2FxdCs5fOA99hwEtZ1tBURTi4uKQlJQEh3%2F7JKXuQl1DHa" +
		"xWKyYmJtDd04Pa%2BjoUFhX%2BN0X%2B%2FwJE0NDUiKu%2FX8W5i%2BfR2PoT6psacPJM" +
		"FfYXaZCwI4F5K%2FHPF1oEBLq2kUL5wa%2FxQ1YgdictRMo385CezMPeHClKSr7EUaoYx6" +
		"soupKqFLyxvOlcs6q%2B6QxTVLgBe5LnoqpiDtovz8bfPbNgPMDHnZtzYLjiiNoT85Cf7Y" +
		"SiAgXKj%2BiYssNlqhnlDc2NqprGamTtEeFomSNMf7yD4buzCO9i2DgHD42OhHkEJzy654" +
		"yBv1xQQ7lAs9cL2uJ9KNAVvr7Ij401glO1FJOTIcLpY45EOnuKdK5NanyfiF0I8zFy%2Fw" +
		"PCQoz089BS5wrtPk%2Fka7IYdb761XFV15yiiwqiceIw2%2BF7MBYv47q1SZ2nSBdw0tH%" +
		"2BRYTFuFfqBYtpKZprFqBwfwgyczLpaXKqmpJUHtchL8sJ5j9fStPyMjFqWsIx0r9wUsp%" +
		"2Bs1KLWUBwI7iT0XqAOjgf32fuRGpaqsSuQAVVqdXpNqO1zvl5BK5cp6P9H9oVsZj4k7D%" +
		"2FW8wfEZaDGfAiCPHbRT7ycsPx7a5ErV2B8iPltGavL3o7XCdzZeUvYGV1Z6STsN%2FWQT" +
		"EnZQZF5PcKWB9IcLfXCwd1XkjYmWAfk65UxxSoeXhwh0RgYiPgP49gmV0R1VfbXspZKXlb" +
		"H%2FgQfDE25I9Rsz9OVfKwZdsW%2Bw3I7soD%2Bc64XyqckquHLYIBT64A2zl7PLDvqdKx" +
		"oQBCIB4Py2A6FIiGKlfEb4q3P0Jy89VMaQGPTJTbtFzZCNgCvQEBkxHZpFJO%2Bnh4FSEI" +
		"48OrYRmUo%2F4kD7HrY%2B1HkJGdQZcUitDX426XK%2FtmYeWGtijor61DZ3sUuq7H4mbn" +
		"RiKVEz7G%2BMM1hBCY%2B2Q4Vu4ORZTCfg52p%2B%2FW5qsjcO0Sn4glXART5devKTjYIp" +
		"3tMejSb8CtG5s46fijUMJaPBn5BLcM3shI80V4RLj9KkpMTpSkpG4l255sJJMvF8ELub4t" +
		"mkQg5yKwdRrMSXu7NnPSJyMRhE8xNhyBK2cXITpajuCQEMm0zbZ9x3a6IE%2BKXy8s5XI1" +
		"ZgeSrpUcbBFDWww62teTeDaiu0NFCmzFk9HPYD4SjKeWKHS3e0KjXg75ajn9yqOCLC3Bti" +
		"82MtShZdBfcbd1ykUQ9jyCcK5TVvp0VEGkkQQlnlmicbtzBZncJQgODmBkMtnrj%2B64%2" +
		"BDhV%2FOdKnKwQcEWsQ2G2CDjpOkIk1y0rfWaJJYWV6DUIydJcDLncF37%2BfjMf2ZHKKF" +
		"VMbDijUXuDbuXh9g0xhozsaBQYqAjlpA%2F716Lvph%2F09GJoct2wauUKRiwRq9740iGr" +
		"QBAaFkorlauQniIil85SXGrmoe2XBbjcwsPpo3xyw3kgZI0QIpGIJgje6uoMkgdJZCtlWq" +
		"lUSvv5%2BTE%2BPj4Qi8UMK%2FX29tYKhULJTI5%2FAJbzGAID9%2BU1AAAAAElFTkSuQm" +
		"CC";

	window.addEventListener('refreshRQ',fTriggerRQRefresh,false);

	// Google Tag Manager setup
	function gtagRQ(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
	if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
	function googleTagLogRQ(dataAction){ //Method to do a gtag sending an action name
		if (document.URL.indexOf("www") < 0) return;
		try {
			gtagRQ('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'ReviewQueue',/*'debug_mode':true,*/
				'send_to':'G-GRQE2910DL'});
			console.log('Script '+dataAction+' logged to Tag Manager.');
		}
		catch {console.log('There was an error using gtag.  Logging attempt failed.')}
	}
	googleTagLogRQ('initialize');

	// Get current domain;
	var domain = location.protocol + '//' + location.hostname;

	var e_ddFilter = window.document.getElementById("ctl00_ContentBody_ddFilter");
	var e_ddCacheType = window.document.getElementById("ctl00_ContentBody_ddCacheType");
	var e_ddCountry = window.document.getElementById("ctl00_ContentBody_ddCountry");
	var e_ddState = window.document.getElementById("ctl00_ContentBody_ddState");

	// Unique ID for positioning open browser pages in review mode.
	const REV_PAGE_POS_KEY = 'a3148ed3-e28f-4105-a8ab-dcbacbd3b457';

	var lnkSubmittedSort = document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortSubmitted');

	var allTables = document.getElementsByClassName('Table');
	if (allTables.length > 0) {
		var qtable = allTables[0];
		var tableCells = qtable.rows[0].cells.length;
		if (tableCells == 13) {
			for (var i = qtable.rows.length - 1; i >= 0; i--) {
				var dataDiv = qtable.rows[i].getElementsByClassName('CacheData')[0];
				if (dataDiv) {
					if (dataDiv.getAttribute('data-datesubmitted')) {
						dataDiv.setAttribute('formatted_datesubmitted',
									qtable.rows[i].cells[5].firstChild.data);
					}
				}
				qtable.rows[i].deleteCell(5);
			}
		}
	}

	// Get currently signed-on geocaching.com profile.
	var SignedInAs = document.getElementById('ctl00_LoginUrl');
	if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
//	SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
//	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	SignedInAs = SignedInAs.parentNode.parentNode.querySelectorAll('a')[0].innerText.trim();
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	console.log(SignedInAs);	


	// Add class style to perform hover outline.
	fRowHoverHighlightlingApply();

	// Create class for 50% opaque.
	GM_addStyle(".gms_faded { opacity:0.35; }");

	// Get maximum number of excluded caches allowed in the internal list.
	var max_excluded = parseInt(GM_getValue('MaxExcluded', 300));
	var max_rqnotes = parseInt(GM_getValue('MaxRQNotes', 250));

	// Find country selector, and reduce lengthy country names.
	e_ddCountry.style.width = '177px';

	// Remove title and "(Refreshed)" to get back more queue space.
		var e_H2 = document.getElementById("ctl00_ContentBody_lbHeading").parentNode;
		if (e_H2.nextSibling.nodeName == '#text') {
			removeNode(e_H2.nextSibling);
		}
		removeNode(e_H2);

	// Remove "bread-crumb" to get back more queue space.
	var e_HomeLink = document.getElementById("ctl00_Breadcrumbs");
	if (e_HomeLink) {
		removeNode(e_HomeLink);
	}

	// Set table spacing to customized value.
	fSetTableSpacing();

	// Add padding back to table cellsides.
	GM_addStyle("td { padding-left: 3px !important; " +
			"padding-right: 3px !important; " +
			"padding-top: 0px !important; " +
			"padding-bottom: 0px !important; } ");

	// Add border to rows.
	GM_addStyle("tr.outline_box { border-top: 1px solid rgb(68,142,53) !important; " +
			"border-bottom: 1px solid rgb(68,142,53) !important; }");

	// Set width for country/state widgets.
	GM_addStyle("ul.selectlist-list { width: 177px !important; }");
	GM_addStyle(".permalink { margin-left:15px; !important; }");

	// Fix pager font size and alignment
	GM_addStyle("td.PageBuilderWidget{font-size:100%;");
	var pbw = document.getElementsByClassName('PageBuilderWidget');
	if (pbw.length) {
		var tdPosToLinks = document.createElement('td');
		tdPosToLinks.style.fontSize = 'smaller';
		tdPosToLinks.align = 'right';
		insertAfter(tdPosToLinks, pbw[1]);

		var spanPosTo = document.createElement('span');
		spanPosTo.style.marginLeft = '12px';
		var aPosTo = document.createElement('a');
		aPosTo.href = 'javascript:void(0);';
		spanPosTo.appendChild(aPosTo);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#hd');
		spanClone.firstChild.title = 'Position open review pages to Top';
		spanClone.firstChild.appendChild(document.createTextNode('Top'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_CacheDetails_ShortDesc');
		spanClone.firstChild.title = 'Position all open review pages to Description';
		spanClone.firstChild.appendChild(document.createTextNode('Description'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#map_canvas');
		spanClone.firstChild.title = 'Position all open review pages to Map';
		spanClone.firstChild.appendChild(document.createTextNode('Map'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_Waypoints');
		spanClone.firstChild.title = 'Position all open review pages to Waypoints';
		spanClone.firstChild.appendChild(document.createTextNode('Waypoints'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_lnkNearbyCaches');
		spanClone.firstChild.title = 'Position all open review pages to Nearby Caches';
		spanClone.firstChild.appendChild(document.createTextNode('Nearby Caches'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#log_table');
		spanClone.firstChild.title = 'Position all open review pages to Logs';
		spanClone.firstChild.appendChild(document.createTextNode('Logs'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '~publish~');
		spanClone.firstChild.title = 'Publish all open review pages';
		spanClone.firstChild.appendChild(document.createTextNode('Publish'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		tdPosToLinks.appendChild(spanClone);
	}

	par_sizer = fUpGenToType(e_ddFilter, 'P')
	if (par_sizer) {
		par_sizer.style.marginTop = '0px';
	}

	par_sizer = fUpGenToType(e_ddCountry, 'P')
	if (par_sizer) {
		par_sizer.style.marginTop = '6px';
		par_sizer.style.marginBottom = '3px';
	}

	// Add table resizer controls.
	var spanTblSizer = document.createElement('span');
	spanTblSizer.id = 'spanTblSizer';
	var aTblSizerUp = document.createElement('a');
	aTblSizerUp.href = 'javascript:void(0)';
	aTblSizerUp.setAttribute('adjust', -0.02);
	aTblSizerUp.style.marginLeft = '7px';
	aTblSizerUp.title = 'Adjust table size';
	var imgTblSizerUp = document.createElement('img');
	imgTblSizerUp.src = TableSizerUpImg;
	imgTblSizerUp.style.verticalAlign = 'text-bottom';
	aTblSizerUp.appendChild(imgTblSizerUp);
	var aTblSizerDn = aTblSizerUp.cloneNode(true);
	aTblSizerDn.setAttribute('adjust', 0.02);
	aTblSizerDn.firstChild.src = TableSizerDnImg;
	spanTblSizer.appendChild(aTblSizerUp);
	spanTblSizer.appendChild(aTblSizerDn);

	aTblSizerUp.addEventListener("click", fChangeTableSpacing, true);
	aTblSizerDn.addEventListener("click", fChangeTableSpacing, true);

	var headerDiv = document.evaluate("//div[contains(@class, 'yui-u first')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
	if (headerDiv) {
		headerDiv.childNodes[1].appendChild(spanTblSizer);
	}

	// Remove background image.
	GM_addStyle("#bd{ background:#ffffff; !important");

	// Add event listeners to drop-down selectors.
	e_ddFilter.addEventListener("change", SetCacheStatusTitle, true);
	e_ddFilter.addEventListener("keypress", SetCacheStatusTitle, true);

	e_ddCountry.addEventListener("change", SetCountryTitle, true);
	e_ddCountry.addEventListener("keypress", SetCountryTitle, true);
	var e_ddState = document.getElementById("ctl00_ContentBody_ddState");
	e_ddState.addEventListener("change", SetStateTitle, true);
	e_ddState.addEventListener("keypress", SetStateTitle, true);

	// Trigger events to set initial values.
	SetCacheStatusTitle();
	SetCountryTitle();
	SetStateTitle();

	// Create URL from current settings.
	GM_registerMenuCommand('Display Resulting URL', ShowUrl);

	// Determine type of caches being shown. Set switch showHold on or off.
	// 0 = All unpublished caches	(All)
	// 1 = All caches not on hold	(NotHeld)
	// 2 = All caches on hold		(AllHolds
	// 3 = Caches I'm holding		(MyHolds)
	var showHold = (e_ddFilter.value == 2 || e_ddFilter.value == 3);

	// Get number of caches to show.
	var CachesToShow = GM_getValue("CachesToShow", 5) - 0;

	var bgcolor_standard = GM_getValue("bgcolor_standard", "F8F8FF");
	var bgcolor_filter = GM_getValue("bgcolor_filter", "A9A9A9");

	// Get current open order. 0=top down; 1=bottom up.
	var OpenOrder = GM_getValue("OpenOrder", 0) - 0;
	// Set asterisk to indicate setting.
	var t2bStar = '';
	var b2tStar = '';
	SetOpenOrderStar(OpenOrder);

	// Add cache name / owner name filter.
	var TitleOwnerFilter = GM_getValue('TitleOwnerFilter', '');
	var filterTitleOwnerSpan = document.createElement('span');
	filterTitleOwnerSpan.name = 'filterTitleOwnerSpan';
	filterTitleOwnerSpan.id = 'filterTitleOwnerSpan';
	filterTitleOwnerSpan.style.marginLeft = '20px';
	filterTitleOwnerSpan.style.whiteSpace = 'nowrap';

	var lnkSwitchTitleOwner = document.createElement('a');
	lnkSwitchTitleOwner.id = 'lnkSwitchTitleOwner';
	lnkSwitchTitleOwner.name = 'lnkSwitchTitleOwner';
	lnkSwitchTitleOwner.href = 'javascript:void(0)';
	lnkSwitchTitleOwner.addEventListener("click", fSwitchTitleOwner, true);

	var TitleOwnerMode = (GM_getValue('TitleOwnerMode', 'Owner'));
	lnkSwitchTitleOwner.setAttribute('mode', TitleOwnerMode);

	var imgSwitchTitleOwner = document.createElement('img');
	imgSwitchTitleOwner.style.border = '0px';
	imgSwitchTitleOwner.style.verticalAlign = 'text-bottom';
	imgSwitchTitleOwner.src = eval(TitleOwnerMode + 'Img');
	imgSwitchTitleOwner.title = 'Toggle Owner / Cache Name Filter';
	lnkSwitchTitleOwner.appendChild(imgSwitchTitleOwner);
	filterTitleOwnerSpan.appendChild(lnkSwitchTitleOwner);

	var inputTitleOwnerFilter = document.createElement('input');
	inputTitleOwnerFilter.addEventListener("keydown", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("lnkRefreshFilter").click();
		}
	});
	inputTitleOwnerFilter.type = 'text';
	inputTitleOwnerFilter.name = 'inputTitleOwnerFilter';
	inputTitleOwnerFilter.id = 'inputTitleOwnerFilter';
	inputTitleOwnerFilter.title = 'Filter by Cache Owner or Cache Title';
	inputTitleOwnerFilter.style.width = '130px';
	inputTitleOwnerFilter.style.height = '14px';
	inputTitleOwnerFilter.style.marginLeft = '6px';
	inputTitleOwnerFilter.value = TitleOwnerFilter;
	filterTitleOwnerSpan.appendChild(inputTitleOwnerFilter);

	var lnkRefreshFilter = document.createElement('a');
	lnkRefreshFilter.id = 'lnkRefreshFilter';
	lnkRefreshFilter.name = 'lnkRefreshFilter';
	lnkRefreshFilter.href = 'javascript:void(0)';
	lnkRefreshFilter.addEventListener("click", fRefreshFilter, true);

	var imgRefreshFilter = document.createElement('img');
	imgRefreshFilter.style.border = '0px';
	imgRefreshFilter.style.verticalAlign = 'text-bottom';
	imgRefreshFilter.style.marginLeft = '6px';
	imgRefreshFilter.src = FilterImg;
	imgRefreshFilter.title = 'Apply Filter';

	lnkRefreshFilter.appendChild(imgRefreshFilter);
	filterTitleOwnerSpan.appendChild(lnkRefreshFilter);

	var lnkEraseFilter = document.createElement('a');
	lnkEraseFilter.id = 'lnkEraseFilter';
	lnkEraseFilter.name = 'lnkEraseFilter';
	lnkEraseFilter.href = 'javascript:void(0)';
	lnkEraseFilter.addEventListener("click", fEraseFilter, true);

	var imgEraseFilter = document.createElement('img');
	imgEraseFilter.style.border = '0px';
	imgEraseFilter.style.verticalAlign = 'text-bottom';
	imgEraseFilter.style.marginLeft = '6px';
	imgEraseFilter.src = EraseImg;
	imgEraseFilter.title = 'Erase Filter';

	lnkEraseFilter.appendChild(imgEraseFilter);
	filterTitleOwnerSpan.appendChild(lnkEraseFilter);

	var lnkMemoryFilter = document.createElement('a');
	lnkMemoryFilter.id = 'lnkMemoryFilter';
	lnkMemoryFilter.name = 'lnkMemoryFilter';
	lnkMemoryFilter.href = 'javascript:void(0)';
	lnkMemoryFilter.addEventListener("click", fMemoryFilter, true);

	var imgMemoryFilter = document.createElement('img');
	imgMemoryFilter.style.border = '0px';
	imgMemoryFilter.style.verticalAlign = 'text-bottom';
	imgMemoryFilter.style.marginLeft = '6px';
	imgMemoryFilter.src = MemoryImg;

	imgMemoryFilter.title = 'Save List of caches to Clipboard';

	lnkMemoryFilter.appendChild(imgMemoryFilter);
	filterTitleOwnerSpan.appendChild(lnkMemoryFilter);

	insertAfter(filterTitleOwnerSpan, e_ddFilter);

	// Get Hold mode and Publish Mode.
	var bHldUpMode = eval(GM_getValue('HoldUpMode', 'false'));
	var bPubUpMode = eval(GM_getValue('PublshUpMode', 'false'));

	// Add Hold Mode switcher.
	var hldSwitchSpan = document.createElement('span');
	hldSwitchSpan.name = 'hldSwitchSpan';
	hldSwitchSpan.id = 'hldSwitchSpan';
	hldSwitchSpan.style.marginLeft = '10px';
	hldSwitchSpan.style.whiteSpace = 'nowrap';
	hldSwitchSpan.appendChild(document.createTextNode('Hold Mode:'));

	var hldSwitchLink = document.createElement('a');
	hldSwitchLink.id = 'hldSwitchLink';
	hldSwitchLink.name = 'hldSwitchLink';
	hldSwitchLink.href = 'javascript:void(0)';
	hldSwitchLink.addEventListener("click", fSwitchHoldMode, true);

	var hldSwitchImg = document.createElement('img');
	hldSwitchImg.id = 'hldSwitchImg';
	hldSwitchImg.name = 'hldSwitchImg';
	hldSwitchImg.style.marginLeft = '2px';
	hldSwitchImg.style.border = '0px';
	hldSwitchImg.style.verticalAlign = 'text-bottom';

	fUpdateHoldSwitch();
	hldSwitchLink.appendChild(hldSwitchImg);
	hldSwitchSpan.appendChild(hldSwitchLink);
	insertAfter(hldSwitchSpan, filterTitleOwnerSpan)

	// Add Publish Mode switcher.
	var pubSwitchSpan = document.createElement('span');
	pubSwitchSpan.style.marginLeft = '10px';
	pubSwitchSpan.style.whiteSpace = 'nowrap';
	pubSwitchSpan.appendChild(document.createTextNode('Publish Mode:'));

	var pubSwitchLink = document.createElement('a');
	pubSwitchLink.id = 'pubSwitchLink';
	pubSwitchLink.name = 'pubSwitchLink';
	pubSwitchLink.href = 'javascript:void(0)';
	pubSwitchLink.addEventListener("click", fSwitchPublishMode, true);

	var pubSwitchImg = document.createElement('img');
	pubSwitchImg.id = 'pubSwitchImg';
	pubSwitchImg.name = 'pubSwitchImg';
	pubSwitchImg.style.marginLeft = '4px';
	pubSwitchImg.style.marginRight = '-10px';
	pubSwitchImg.style.border = '0px';
	pubSwitchImg.style.verticalAlign = 'text-bottom';
	pubSwitchImg.src = UpPublishImg;
	pubSwitchLink.title = 'Publish top group. Click to change.';

	fUpdatePublishSwitch();
	pubSwitchLink.appendChild(pubSwitchImg);
	pubSwitchSpan.appendChild(pubSwitchLink);
	insertAfter(pubSwitchSpan, hldSwitchSpan)

	// Add earthcache filter checkbox.
	// Shorten state selector.
	e_ddState.style.width = '180px';

	// Add earthcache image.
	var imgEC = document.createElement("img");
	imgEC.src = EarthcacheImg;
	imgEC.id = 'imgEC';
	imgEC.border = '0';
	imgEC.style.marginLeft = '10px';
	imgEC.style.marginRight = '4px';
	imgEC.style.verticalAlign = 'text-bottom';
	insertAheadOf(imgEC, hldSwitchSpan);

	// Create checkbox.
	var cbEC = document.createElement("input");
	cbEC.type = 'checkbox';
	cbEC.id = 'cbEC';
	cbEC.style.marginRight = '6px';
	cbEC.title = 'Check to filter out & hide Earthcaches';
	cbEC.checked = eval(GM_getValue("FilterEarthcaches", false));
	if (cbEC.checked) { imgEC.classList.add('gms_faded'); }
	insertAfter(cbEC, imgEC);
	cbEC.addEventListener("click", fEarthcacheFilterClicked, true);
	bEC = cbEC.checked;

	// Add USA filter checkbox.
	// Add USA flag image.
	var imgUSA = document.createElement("img");
	imgUSA.src = USAflagImg;
	imgUSA.id = 'imgUSA';
	imgUSA.border = '0';
	imgUSA.style.marginLeft = '10px';
	imgUSA.style.marginRight = '4px';
	imgUSA.style.verticalAlign = 'text-bottom';
	insertAfter(imgUSA, e_ddState);
	insertAheadOf(imgUSA, hldSwitchSpan);

	// Create checkbox.
	var cbUSA = document.createElement("input");
	cbUSA.type = 'checkbox';
	cbUSA.id = 'cbUSA';
	cbUSA.style.marginRight = '6px';
	cbUSA.title = 'Check to filter out & hide USA caches';
	cbUSA.checked = eval(GM_getValue("FilterUSAcaches", false));
	if (cbUSA.checked) { imgUSA.classList.add('gms_faded'); }
	insertAfter(cbUSA, imgUSA);
	cbUSA.addEventListener("click", fUSAFilterClicked, true);
	bUSA = cbUSA.checked;

	// Add controls to hide timed-publish caches
	let stopwatchImg = fCreateImg(stopwatchSrc,'stopwatchImg');
	stopwatchImg.style.width='16px';
	stopwatchImg.style.height='16px';
	insertAfter(stopwatchImg,cbUSA);
	let timedP = fCreateCheckbox('timedP','Click to filter and hide caches set to timed publish',false);
	insertAfter(timedP,stopwatchImg);
	timedP.addEventListener("click", fTimedPubFilterClicked, true);

	// Shorten cache type selector.
	e_ddCacheType.style.width = '85px';

	// Add drop-down to set number of nearby caches to show.
	var e_btnRefresh = document.getElementById("ctl00_ContentBody_btnRefresh");
	var ltsSpacer = document.createTextNode('\u00A0 ');
	e_btnRefresh.parentNode.insertBefore(ltsSpacer, e_btnRefresh.nextSibling);
	var ltsSpan = document.createElement('span');
	ltsSpan.id = 'ltsSpan';
	ltsSpan.style.whiteSpace = 'nowrap';
	ltsSpan.title = 'Number of nearby caches to show';
	var ltsText = document.createTextNode('Nearby: ')
	ltsSpan.appendChild(ltsText);
	e_btnRefresh.parentNode.insertBefore(ltsSpan, ltsSpacer.nextSibling);
	var ltsDropdown = document.createElement("select");
	ltsDropdown.id = 'ltsDropdown';
	for (var i = 0; i <= 25; i++) {
		var ltsOption = document.createElement("option");
		ltsOption.value = i;
		ltsOption.id = 'ltsOption' + i;
		if (i == CachesToShow) {
			ltsOption.selected = true;
		}
		ltsOption.appendChild(document.createTextNode(i));
		ltsDropdown.appendChild(ltsOption);
	}
	ltsText.parentNode.insertBefore(ltsDropdown, ltsText.nextSibling);
	ltsDropdown.addEventListener("change", ltsChange, true);

	// Add checkbox to filter out archived nearby caches.
	var bFilterArchived = eval(GM_getValue("FilterArchived", false));
	var imgFilterArchived = document.createElement('img');
	imgFilterArchived.id = 'imgFilterArchived';
	imgFilterArchived.src = '../images/logtypes/5.png';
	imgFilterArchived.title = 'Check to filter out archived nearby caches.';
	imgFilterArchived.style.marginLeft = '18px';
	imgFilterArchived.style.marginRight = '6px';
	imgFilterArchived.style.verticalAlign = 'text-bottom';
	ltsSpan.appendChild(imgFilterArchived);
	var cbFilterArchived = document.createElement('input');
	cbFilterArchived.id = 'cbFilterArchived';
	cbFilterArchived.type = 'checkbox';
	cbFilterArchived.title = 'Check to filter out archived nearby caches.';
	cbFilterArchived.checked = bFilterArchived;
	if (bFilterArchived) { imgFilterArchived.classList.add('gms_faded'); }
	ltsSpan.appendChild(cbFilterArchived);
	cbFilterArchived.addEventListener("change", fArcFilterChanged, true);

	var lnkGuage = document.createElement('a');
	lnkGuage.id = 'lnkGuage';
	// lnkGuage.href = 'javascript:void(0)';
	lnkGuage.title = 'Analyze High Submissions by Owner.\nShift-click to change limit.';
	lnkGuage.style.marginLeft = '18px';
	var imgGuage = document.createElement('img');
	imgGuage.src = srcImgGuage;
	imgGuage.style.verticalAlign = 'text-bottom';
	imgGuage.style.cursor = 'pointer';
	lnkGuage.appendChild(imgGuage);
	ltsSpan.appendChild(lnkGuage);
	lnkGuage.addEventListener('mousedown', fAnalyzeByOwner, false);

	// Hide "Open all caches on this page" link and add new 'open-all with skip' feature
	let skipHeld,skipTimedPublish,skipHidden,skipExcluded,skipEC;
	let nativeOpenAll = document.querySelector('button[class="btn-link open-all-caches-btn"]');
	if (nativeOpenAll) {
		nativeOpenAll.style.display = 'none';
		let openSpan = document.createElement('span');
		openSpan.id = 'openAllControls';
		openSpan.setAttribute('style',"float:right; width:700px");
		let skipText = 	document.createElement('text');
		skipText.innerText = 'Open-All Skip Settings:';
		openSpan.appendChild(skipText);

		// skipHeld
		openSpan.appendChild(fCreateImg(HoldImg,"skipHeldImg"));
		skipHeld = fCreateCheckbox('skipHeld','Check to skip on-hold caches during open-all.',false);
		openSpan.appendChild(skipHeld);
		skipHeld.addEventListener("click", fProcessSkipCheckboxes, true);

		// skipTimedPublish
//		var timed = document.createElement('text');
//		timed.innerText = "Timed Publish";
//		timed.style.marginLeft = '10px';
//		timed.style.marginRight = '4px';
		duplicateTPImg = stopwatchImg.cloneNode(true);
		openSpan.appendChild(duplicateTPImg);
		skipTimedPublish = fCreateCheckbox('skipTimedPublish','Check to skip timed publish caches during open-all.',false);
		openSpan.appendChild(skipTimedPublish);
		skipTimedPublish.addEventListener("click", fProcessSkipCheckboxes, true);

		// skipHidden
		var hidden = document.createElement('text');
		hidden.innerText = "Hidden";
		hidden.style.marginLeft = '10px';
		hidden.style.marginRight = '4px';
		openSpan.appendChild(hidden);
		skipHidden = fCreateCheckbox('skipHidden','Check to skip hidden caches during open-all.',true);
		openSpan.appendChild(skipHidden);
		skipHidden.addEventListener("click", fProcessSkipCheckboxes, true);

		// skipExcluded
		openSpan.appendChild(fCreateImg(CrossImg,'skipExcludedImg'));
		skipExcluded = fCreateCheckbox('skipExcluded','Check to skip excluded caches during open-all.',true);
		openSpan.appendChild(skipExcluded);
		skipExcluded.addEventListener("click", fProcessSkipCheckboxes, true);

		// skipEC
		openSpan.appendChild(fCreateImg(EarthcacheImg,'imgSkipEC'));
		skipEC = fCreateCheckbox('skipEC','Check to skip Earth Caches during open-all.',false);
		openSpan.appendChild(skipEC);
		skipEC.addEventListener("click", fProcessSkipCheckboxes, true);

		// open all link
		let scriptOpenAll = document.createElement('a');
		scriptOpenAll.id = 'lnkOpenWExclude';
		scriptOpenAll.href = 'javascript:void(0)';
		scriptOpenAll.title = 'Open review page for all non-excluded caches.';
		let myText = document.createTextNode('Open-all review pages with skipping.');
		scriptOpenAll.setAttribute('style','float:right');
		scriptOpenAll.appendChild(myText);
		openSpan.appendChild(scriptOpenAll);
		nativeOpenAll.parentElement.insertBefore(openSpan,nativeOpenAll);
		scriptOpenAll.addEventListener('click',()=>{
			document.querySelectorAll('[name="cb_exclude"]').forEach((element)=>{
				let row=document.getElementById('cacheRow'+element.getAttribute('numid'));
				if (
					(!(skipHeld.checked && row.querySelector("span[class='cache-status']"))) &&
					(!(skipTimedPublish.checked && row.querySelector("span[class='time-publish-status']"))) &&
					(!(skipHidden.checked && row.style.visibility == 'collapse')) &&
					(!(skipExcluded.checked && element.checked)) &&
					(!(skipEC.checked && row.querySelector('img[title="Earthcache"]')))
				) {GM_openInTab(document.getElementById('revlnk'+element.getAttribute('numid')).href,'_blank');}
			})
		});
	}
	
	// Get total of caches by type, and add to drop-down selector.
	var typeTotals = new Object;
	var dataList = document.getElementsByClassName('CacheData');
	var ic = dataList.length;
	for (var i = 0; i < ic; i++) {
		var ct = dataList[i].getAttribute('data-cachetypeid');
		if (!typeTotals.hasOwnProperty(ct)) { typeTotals[ct] = 0; }
		typeTotals[ct] += 1;
	}
	ic = e_ddCacheType.options.length;
	for (var i = 0; i < ic; i++) {
		ct = e_ddCacheType.options[i].value;
		if (typeTotals.hasOwnProperty(ct)) {
			e_ddCacheType.options[i].firstChild.data += ' (' + typeTotals[ct] + ')';
		}
	}

	// Get list of review page links.
	xPathSearch = "//a[contains(@href, '/admin/review.aspx?guid=')]";
	var RevList = document.evaluate(
		xPathSearch,
		document,
		null,
		XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
		null);
	var ic = RevList.snapshotLength;
	var max_caches = ic;

	// Get list of excluded caches.
	var ar_excluded = GM_getValue("ExcludedCaches", '').split(' ');

	// If at least one cache page link.
	if (ic > 0) {
		// Find queue table node.
		var ThisLink = RevList.snapshotItem(0);
		var mainTable = fUpGenToType(ThisLink, 'TABLE');
		mainTable.id = 'mainTable';

		// Change alignment in all cells.
		var mainTableBody = fUpGenToType(ThisLink, 'TBODY');
		mainTableBody.style.verticalAlign = 'top';

		lnkPlacedSort = document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortPlacedDate');
		lnkLstUpdateSort = document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortLastUpdate');
		lnkTerSort = document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortTerrain');
		lnkDifSort = document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortDifficulty');
		lnkLstUpdateSort.firstChild.data = 'Upd';

		lnkLstUpdateSort.style.marginLeft = '3px';
		lnkSubmittedSort.style.marginLeft = '3px';
		lnkTerSort.style.marginLeft = '5px';
		lnkDifSort.style.marginLeft = '5px';
		mainTable.rows[0].cells[5].appendChild(lnkLstUpdateSort);
		mainTable.rows[0].cells[5].appendChild(lnkSubmittedSort);
		mainTable.rows[0].cells[2].appendChild(lnkDifSort);
		mainTable.rows[0].cells[2].appendChild(lnkTerSort);

		lnkPlacedSort.firstChild.data = 'Plc';
		lnkSubmittedSort.firstChild.data = 'Sbm';

		removeNode(mainTable.rows[0].cells[6]);	// Last Update
		removeNode(mainTable.rows[0].cells[4]);	// Terrain
		removeNode(mainTable.rows[0].cells[3]); // Difficulty

		// Add cross image to table header.
		crossCell = mainTable.rows[0].cells[4];
		var newIcon = document.createElement("img");
		newIcon.src = CrossImg;
		newIcon.style = "display:block; margin-left:auto; margin-right:auto";
		newIcon.title = 'Click icon to uncheck all "filtered out" checkboxes.';
		newIcon.addEventListener("click", fUnExcludeAll, true);
		removeNode(crossCell.firstChild);
		crossCell.appendChild(newIcon);

		// Reduce column headings.
		document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortCacheType').firstChild.data = 'T';
		document.getElementById('ctl00_ContentBody_CacheList_LinkButton_SortLocationName').firstChild.data = 'Loc';

		// Move watchlist icon to first cell.
		xPathSearch = "//img[@src='http://www.geocaching.com/images/icons/icon_watchlist.gif']";
		var ImgList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
			null);
		var im = ImgList.snapshotLength;
		for (var i = 0; i < im; i++) {
			var watchicon = ImgList.snapshotItem(i);
			var ThisRow = watchicon.parentNode.parentNode;
			ThisRow.cells[1].appendChild(document.createElement('br'));
			ThisRow.cells[1].appendChild(watchicon);
		}

		// Get earthcache filter setting.
		var bFilterEC = eval(GM_getValue("FilterEarthcaches", false))

		// Get USA filter setting.
		var bFilterUSA = eval(GM_getValue("FilterUSAcaches", false))

		// Examine permalink to determine table sorting.
		var pLink = document.getElementById('ctl00_ContentBody_HyperLink_Permalink');
		var tabSort = UrlParm('sortcolumn', true, pLink.href)

		// Change number of caches to show, and add up-icon after every review page link.
		for (var i = 0; i < ic; i++) {
			var ThisLink = RevList.snapshotItem(i);
			var ThisRow = ThisLink.parentNode.parentNode;

			dataDiv = ThisRow.getElementsByClassName('CacheData')[0];

			ThisLink.href += '&nc=' + CachesToShow + (bFilterArchived ? '&a=false' : '');
			ThisLink.name = 'revlnk';
			ThisLink.id = 'revlnk' + i;
			ThisLink.setAttribute('filtered', 'false');
			ThisLink.setAttribute('excluded', 'false');
			var ThisData = ThisRow.getElementsByClassName('CacheData')[0];

			// set attribute for USA cache
//			if (cbUSA.checked) {
				var dataCountryId = ThisData.getAttribute('data-countryid').trim()
				if (dataCountryId == '2') {
					ThisLink.setAttribute('usa-omit', 'true');
				}
//			}

			// set attribute for hiding Timed Publish caches
			if (ThisRow.querySelector("span[class='time-publish-status']")) ThisLink.setAttribute('timedP',true);
			else ThisLink.setAttribute('timedP',false);

			// set attribute for earthcaches
			cacheIconSrc = ThisRow.cells[0].firstChild.src.toLowerCase();
			var isEC = (cacheIconSrc.indexOf('earthcache.gif') > 0);
			ThisLink.setAttribute('earthcache', isEC.toString());

			// Move Last Changed date under Placed date.
			var txtDtPlcd = ThisRow.cells[5].firstChild;
			var txtLstChg = ThisRow.cells[6].firstChild;
			var spnDtPlcd = ThisRow.cells[5].appendChild(document.createElement('span'));
			spnDtPlcd.appendChild(txtDtPlcd);
			ThisRow.cells[5].appendChild(document.createElement('br'));
			var spnLstChg = document.createElement('span');
			spnLstChg.appendChild(txtLstChg);
			ThisRow.cells[5].appendChild(spnLstChg);

			// Add Submitted date under Last Changed date.
			ThisRow.cells[5].style.lineHeight = '100%';
			var txtDtSbm = dataDiv.getAttribute('formatted_datesubmitted');
			if (!txtDtSbm) {
				txtDtSbm = 'n/a';
			}
			ThisRow.cells[5].appendChild(document.createElement('br'));
			var spnDtSbm = document.createElement('span');
			spnDtSbm.appendChild(document.createTextNode(txtDtSbm));
			ThisRow.cells[5].appendChild(spnDtSbm);

			spnDtPlcd.title="Date cache was physically placed";
			spnLstChg.title="Last date cache listing was updated"
			spnDtSbm.title="Date cache page was Submitted"

			// Bold if date is used as the sort. placed lastupdate submitted
			if (tabSort == 'lastupdate') {
				spnLstChg.style.fontWeight = 'bold';
			} else {
				if (tabSort == 'submitted') {
					spnDtSbm.style.fontWeight  = 'bold';
				} else {
					spnDtPlcd.style.fontWeight = 'bold';
				}
			}

			removeNode(ThisRow.cells[6]);

			// Remove Difficulty and Terrain columns, and move data under waypoint id.
			var difText = ThisRow.cells[3].firstChild.data.trim();
			var terText = ThisRow.cells[4].firstChild.data.trim();
			removeNode(ThisRow.cells[4]);
			removeNode(ThisRow.cells[3]);
			ThisRow.cells[2].appendChild(document.createElement('br'));
			ThisRow.cells[2].appendChild(document.createTextNode('(' + difText + '/' + terText + ')'));

			// Set last cell to no-wrap, so all icons are on the same line.
			ThisLink.parentNode.style.whiteSpace = 'nowrap';

			// Create link.
			newLink = document.createElement("a");
			newLink.name = 'opnlnk' + i;
			newLink.id = 'opnlnk' + i;
			newLink.title = 'Open top review pages';
			newLink.href = 'javascript:void(0)';
			newLink.setAttribute('numid', i);

			// Create image.
			newIcon = document.createElement("img");
			newIcon.height = 14;
			newIcon.width = 12;
			newIcon.border = 0;
			newIcon.style.marginLeft = '6px';
			newIcon.src = UpArrowImg;
			newIcon.align = 'baseline';

			// Attach image to link.
			newLink.appendChild(newIcon);

			// Add link/image to page, after spacer.
			insertAfter(newLink, ThisLink);

			// Attach function to link.
			newLink.addEventListener("click", OpenTabs, true);

			// Ensure litmus and GPX links don't get pushed to another line.
			newLink.nextSibling.nextSibling.nextSibling.nextSibling.data = ']\u00a0[';
			newLink.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.data =
					']\u00a0[';

			var thisGuid = UrlParm('guid', true, ThisLink.href);

			var pubLink = document.createElement("a");
			pubImg = document.createElement("img");
			pubImg.src = pubSwitchImg.src;
			pubImg.style.border = '0px';
			pubImg.style.marginLeft = '6px';
			pubImg.style.verticalAlign = 'text-bottom';
			pubLink.appendChild(pubImg);
			insertAfter(pubLink, newLink);
			pubLink.href = 'javascript:void(0)';
			pubLink.id = 'publnk' + i.toString();
			pubLink.title = pubSwitchLink.title;
			pubLink.setAttribute('guid', thisGuid);
			pubLink.setAttribute('numid', i.toString());
			pubLink.addEventListener("click", PublishCache, true);

			var hldLink = document.createElement("a");
			hldImg = document.createElement("img");
			hldImg.src = hldSwitchImg.src;
			hldLink.title = hldSwitchLink.title;
			if (!showHold) {
				hldLink.setAttribute('action', 'hold');
			} else {
				hldLink.setAttribute('action', 'remhold');
			}
			hldImg.style.border = '0px';
			hldImg.style.marginLeft = '6px';
			hldImg.style.verticalAlign = 'text-bottom';
			hldLink.appendChild(hldImg);
			insertAheadOf(hldLink, pubLink);
			hldLink.href = 'javascript:void(0)';
			hldLink.id = 'hldlnk' + i.toString();
			hldLink.setAttribute('guid', thisGuid);
			hldLink.setAttribute('numid', i.toString());
			hldLink.addEventListener("click", HoldCache, true);

			// Add exclude-checkbox.
			var cacheRow = ThisLink.parentNode.parentNode;
			cacheRow.bgColor = '#' + bgcolor_standard;
			cacheRow.id = 'cacheRow' + i.toString();
			var checkboxCell = cacheRow.cells[4];
			checkboxCell.align = 'center';
			var wptid = checkboxCell.parentNode.childNodes[5].firstChild.data.trim();
			checkboxCell.id = 'cb_' + wptid;
			var cbExclude = document.createElement("input");
			cbExclude.type = 'checkbox';
			cbExclude.name = 'cb_exclude';
			cbExclude.id = 'exclude' + i.toString();
			cbExclude.title = 'Filter Out';
			cbExclude.style.marginTop = '2px';
			cbExclude.setAttribute('numid', i.toString());
			cbExclude.setAttribute('wptid', wptid);
			checkboxCell.appendChild(cbExclude);
			cbExclude.addEventListener("click", fCheckboxClicked, true);

			// Add class to row, to perform hover outlining.
			cacheRow.setAttribute('class', 'outline_box');

			// Add waypoint id attribute to review link.
			ThisLink.setAttribute('wptid', wptid);

			// Check if cache is on excluded list.
			if (ar_excluded.indexOf(wptid) >= 0) {
				cbExclude.click();
			}

		}

	}

	// Add open-order links to bottom of the page.
	xPathSearch = "//a[@href='http://www.geocaching.com/admin/queue.aspx?stateid=48,6']";
	var LnkList = document.evaluate(
		xPathSearch,
		document,
		null,
		XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
		null);
	var ik = LnkList.snapshotLength;
	if (ik > 0) {
		par = window.document.getElementById("ctl00_ContentBody_ddPageSize");

		// Create new paragraph.
		var h3_Configuration = document.createElement("h3");
		h3_Configuration.id = 'h3_Configuration';
		h3_Configuration.appendChild(document.createTextNode('Review Queue Configuration: '));
		insertAfter(h3_Configuration, par);

		// Create new paragraph.
		var p_OpenOrder = document.createElement("p");
		p_OpenOrder.id = 'p_OpenOrder';
		//par.parentNode.insertBefore(p_OpenOrder, par);
		insertAfter(p_OpenOrder, h3_Configuration);

		// Create leading text.
		p_OpenOrder.appendChild(document.createTextNode('Change sequence of Multi-Open:\u00A0 '));

		// Create top to bottom link.
		var top2bottom = document.createElement("a");
		top2bottom.href = 'javascript:void(0)';
		top2bottom.title = 'Click to change open order';
		top2bottom.setAttribute('status', 0);
		top2bottom.appendChild(document.createTextNode(t2bStar + 'Top to Bottom'));
		p_OpenOrder.appendChild(top2bottom);

		// Create spacer between links.
		p_OpenOrder.appendChild(document.createTextNode('\u00A0 '));

		// Create bottom to top link.
		var bottom2top = document.createElement("a");
		bottom2top.href = 'javascript:void(0)';
		bottom2top.title = 'Click to change open order';
		bottom2top.setAttribute('status', 1);
		bottom2top.appendChild(document.createTextNode(b2tStar + 'Bottom to Top'));
		p_OpenOrder.appendChild(bottom2top);

		// Create additional text.
		p_OpenOrder.appendChild(document.createTextNode('\u00A0 ("Bottom" indicates the entry you clicked, ' +
				'not the bottom of the page. Current setting is indicated by the *.)'));

		// Attach function to links.
		top2bottom.addEventListener("click", ToggleOpenOrder, true);
		bottom2top.addEventListener("click", ToggleOpenOrder, true);

		// Create color pickers.
		var p_cPicker = document.createElement("p");
		p_cPicker.id = 'p_cPicker';
		insertAfter(p_cPicker, p_OpenOrder);
		p_cPicker.appendChild(document.createTextNode('Standard backcolor: '));

		var cPicker = document.createElement("select");
		cPicker.style.fontFamily = 'Courier,Courier New,Lucida Console,Monaco,monospace';
		cPicker.style.fontWeight = 'bold';
		cPicker.style.width = '100px';
		cPicker.setAttribute('type', 'bgcolor_standard');
		p_cPicker.appendChild(cPicker);
		var ccl = arColorCodes.length;
		for (var i = 0; i < ccl; i++) {
			oColor = arColorCodes[i];
			var cfOption = document.createElement("option");
			cfOption.value = oColor;
			cPicker.appendChild(cfOption);
			if (oColor == bgcolor_standard) {
				cfOption.selected = true;
				cPicker.title = oColor + ' - ' + arColorNames[i];
			}
			cfOption.style.backgroundColor = '#' + oColor;
			cfOption.style.fontWeight = 'bold';
			cfOption.style.fontFamily = 'Courier,Courier New,Lucida Console,Monaco';
			cfOption.style.color = getContrastYIQ(oColor);
			cfOption.appendChild(document.createTextNode(' ' + oColor +	' - ' + arColorNames[i] + ' '));
			cfOption.setAttribute('colorname', arColorNames[i]);
		}
		cPicker.addEventListener("click", fColorChanged, true);
		cPicker.addEventListener("keyup", fColorChanged, true);

		var cSwatch = document.createElement("spanz");
		cSwatch.style.marginLeft = '5px';
		cSwatch.style.border = 'thin solid dimgray';
		cSwatch.id = 'cSwatch';
		cSwatch.name = 'cSwatch';
		cSwatch.title = cPicker.title;
		cSwatch.style.marginRight = '20px';
		cSwatch.style.backgroundColor = '#' + bgcolor_standard;
		cSwatch.appendChild(document.createTextNode('\u00A0\u00A0Example\u00A0\u00A0'));
		p_cPicker.appendChild(cSwatch);

		p_cPicker.appendChild(document.createTextNode('Filtered backcolor: '));

		xPicker = cPicker.cloneNode(true);
		xPicker.id = 'xPicker';
		xPicker.name = 'xPicker';
		xPicker.setAttribute('type', 'bgcolor_filter');
		xPicker.value = bgcolor_filter;
		xPicker.title = xPicker.value + ' - ' +
				xPicker.options[xPicker.selectedIndex].getAttribute('colorname', '');

		p_cPicker.appendChild(xPicker);

		xSwatch = cSwatch.cloneNode(true);
		xSwatch.id = 'xSwatch';
		xSwatch.name = 'xSwatch';
		xSwatch.style.backgroundColor = '#' + bgcolor_filter;
		p_cPicker.appendChild(xSwatch);
		xSwatch.title = xPicker.title;
		xPicker.addEventListener("click", fColorChanged, true);
		xPicker.addEventListener("keyup", fColorChanged, true);

		var cbHideFiltered = document.createElement('input');
		cbHideFiltered.id = 'cbHideFiltered';
		cbHideFiltered.name = 'cbHideFiltered';
		cbHideFiltered.type = 'Checkbox';
		cbHideFiltered.title = 'Hide rows with the exclude box checked';
		cbHideFiltered.style.marginLeft	= '4px';
		cbHideFiltered.checked = GM_getValue('HideFiltered', false);
		var lblHideFiltered = document.createElement('label');
		lblHideFiltered.style.marginLeft = '2px';
		lblHideFiltered.style.fontWeight = 'normal';
		lblHideFiltered.setAttribute('for', 'cbHideFiltered');
		lblHideFiltered.appendChild(document.createTextNode('Hide Entry When Filtered:'));
		p_cPicker.appendChild(lblHideFiltered);
		p_cPicker.appendChild(cbHideFiltered);
		cbHideFiltered.addEventListener("click", fHideFilteredClicked, true);

		// Add hover mode selector.
		p_hoverMode = document.createElement('P');
		p_hoverMode.id = 'p_hoverMode';
		insertAfter(p_hoverMode, p_cPicker);
		p_hoverMode.appendChild(document.createTextNode('Enable Row Hover Highlightling: '));
		var cbHoverMode = document.createElement("input");
		cbHoverMode.type = 'checkbox';
		cbHoverMode.id = 'cbHoverMode';
		cbHoverMode.title = 'Check to use Row Hover Highlighting';
		cbHoverMode.checked = eval(GM_getValue("RowHoverHighlightling", 'true'));
		p_hoverMode.appendChild(cbHoverMode);
		cbHoverMode.addEventListener("click", fRowHoverHighlightlingClicked, true);

		// Add earthcache filter type selector.
		p_ecFilterMode = document.createElement('P');
		p_ecFilterMode.id = 'p_ecFilterMode';
		insertAfter(p_ecFilterMode, p_hoverMode);
		p_ecFilterMode.appendChild(document.createTextNode('Hide EarthCaches When Filtered: '));
		var cbEcFilterMode = document.createElement("input");
		cbEcFilterMode.type = 'checkbox';
		cbEcFilterMode.id = 'cbEcFilterMode';
		cbEcFilterMode.title = 'Check to hide EarthCaches when filtering is used';
		cbEcFilterMode.checked = eval(GM_getValue("RowEcFilterHide", 'true'));
		p_ecFilterMode.appendChild(cbEcFilterMode);
		cbEcFilterMode.addEventListener("click", fRowEcFilterHideClicked, true);

		// Add autoclose on hold toggle
		p_holdAutoclose = document.createElement('P');
		p_holdAutoclose.id = 'p_holdAutoclose';
		insertAfter(p_holdAutoclose, cbEcFilterMode);
		p_holdAutoclose.appendChild(document.createTextNode('Auto-Close Review Page on Hold: '));
		var cbHoldAutoclose = document.createElement("input");
		cbHoldAutoclose.type = 'checkbox';
		cbHoldAutoclose.id = 'cbHoldAutoclose';
		cbHoldAutoclose.title = 'Check to auto-close review page on hold';
        if(GM_getValue("HoldAutoClose") === undefined) GM_setValue("HoldAutoClose", false);
		cbHoldAutoclose.checked = eval(GM_getValue("HoldAutoClose"));
		p_holdAutoclose.appendChild(cbHoldAutoclose);
		cbHoldAutoclose.addEventListener("click", fcbHoldAutocloseClicked, true);

		// Add hold or hold & watch toggle
		p_holdAndWatch = document.createElement('P');
		p_holdAndWatch.id = 'p_holdAndWatch';
		insertAfter(p_holdAndWatch, cbHoldAutoclose);
		p_holdAndWatch.appendChild(document.createTextNode('Automatically watch caches on hold: '));
		var cbHoldAndWatch = document.createElement("input");
		cbHoldAndWatch.type = 'checkbox';
		cbHoldAndWatch.id = 'cbHoldAndWatch';
		cbHoldAndWatch.title = 'Check to automatically watch listings on hold';
        if(GM_getValue("HoldAndWatch") === undefined) GM_setValue("HoldAndWatch", false);
		cbHoldAndWatch.checked = eval(GM_getValue("HoldAndWatch"));
		p_holdAndWatch.appendChild(cbHoldAndWatch);
		cbHoldAndWatch.addEventListener("click", fcbHoldAndWatchClicked, true);

		var hr_End = document.createElement("hr");
		hr_End.id = 'hr_End';
		hr_End.style.border = '1px solid #448e35';
		hr_End.style.display = 'block';
		insertAfter(hr_End, cbHoldAndWatch);
	}

	// Calculate complement to background color:
	GM_addStyle(".holdername { color:" + getContrastYIQ(bgcolor_standard.substr(1)) +
			"; font-style:italic; whitespace:no-wrap;}");

	GM_addStyle("UL.gm-hidden { display:table; visibility:collapse; }");
	var coltblint = window.setInterval(fCollapseLists, 150);
	var hoverTimer = new Array();
	var hoverElement = new Array();

	// // Add listener to check for JSON database change by another process.
	// window.addEventListener('storage', fStorageChanged, false);

	// Place country and state lists in collapsible tables.
	function fCollapseLists() {
		var placeLists = document.getElementsByClassName('selectlist-list');
		if (placeLists.length) {
			clearInterval(coltblint);
			var placeSelect = document.getElementsByClassName('selectlist-select');
			var ic = placeSelect.length;
			for (var i = 0; i < ic; i++) {
				placeSelect[i].setAttribute('hover-index', i);
				placeSelect[i].addEventListener('mouseover', fShowPlaceList, true);
				placeSelect[i].addEventListener('mouseout', fHidePlaceList, true);
			}
			var ic = placeLists.length;
			for (var i = 0; i < ic; i++) {
				hoverElement[i] = placeLists[i];
				placeLists[i].setAttribute('hover-index', i);
				placeLists[i].classList.add('gm-hidden');
				placeLists[i].addEventListener('mouseover', fShowPlaceList, true);
				placeLists[i].addEventListener('mouseout', fHidePlaceList, true);
				var cn = placeLists[i].childNodes.length;
				if (cn) {
					var pnames = '';
					for (var j = 0; j < cn; j++) {
						if (pnames.length) { pnames += ' / '; }
						pnames += (placeLists[i].childNodes[j].firstChild.data);
					}
					placeSelect[i].options[0].firstChild.data = pnames;
					placeSelect[i].title = pnames;
				}
			}
		}
	}

	function fShowPlaceList() {
		var idx = this.getAttribute('hover-index');
		clearTimeout(hoverTimer[idx]);
		var delay = function() { fHoverShowDelay(idx); };
		hoverTimer[idx] = setTimeout(delay, 250);
	}

	function fHoverShowDelay(idx) {
		hoverElement[idx].classList.remove('gm-hidden');
	}

	function fHidePlaceList() {
		var idx = this.getAttribute('hover-index');
		clearTimeout(hoverTimer[idx]);
		var delay = function() { fHoverHideDelay(idx); };
		hoverTimer[idx] = setTimeout(delay, 250);
	}

	function fHoverHideDelay(idx) {
		hoverElement[idx].classList.add('gm-hidden');
	}

	fDoTitleOwnerFiltering();
	fRefreshHideSetting();
	console.log('just refreshed hide rows on startup');

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Refresh the filter coloring and hiding
	function fTriggerRQRefresh(){
		fRefreshAllRows();
		fRefreshHideSetting();
	}

	// Set customized table spacing.
	function fSetTableSpacing() {
		var curLineHeight = GM_getValue("TableLineHeight", '1');
		GM_addStyle(".QueueTable { line-height:" + curLineHeight + "em !important; }");
	}

	// Change table spacing when controls clicked.
	function fChangeTableSpacing() {
		var delta = parseFloat(this.getAttribute('adjust', '0'));
		var curLineHeight = parseFloat(GM_getValue("TableLineHeight", '1'));
		curLineHeight = roundNumber(curLineHeight + delta, 3);
		GM_setValue("TableLineHeight", curLineHeight.toString());
		fSetTableSpacing();
	}

	// Earthcache Filter Checkbox Changed.
	function fEarthcacheFilterClicked() {
		GM_setValue("FilterEarthcaches", cbEC.checked);
		imgEC.classList.toggle('gms_faded');
		fRefreshAllRows();
		fRefreshHideSetting();
		//	alert('Click Filter or Refresh to see results');
	}

	// USA Filter Checkbox Changed.
	function fUSAFilterClicked() {
		GM_setValue("FilterUSAcaches", cbUSA.checked);
		imgUSA.classList.toggle('gms_faded');
		fRefreshAllRows();
		fRefreshHideSetting();
//		alert('Click Filter or Refresh to see results');
	}

	function fTimedPubFilterClicked(){
		GM_setValue(this.id,this.checked);
		fRefreshAllRows();
		fRefreshHideSetting();
	}

	// Change browser page position for all open review pages.
	function fPositionRevPages() {
		if (this.getAttribute('hash') == '~publish~') {
			var rslt = confirm('Publish all open Review pages?');
			if (!rslt) { return; }
		}
		window.localStorage.setItem(REV_PAGE_POS_KEY, '');
		window.localStorage.setItem(REV_PAGE_POS_KEY, this.getAttribute('hash'));
	}

	// Process click to hover highlighting checkbox.
	function fRowHoverHighlightlingClicked() {
		var cbHoverMode = document.getElementById("cbHoverMode");
		GM_setValue("RowHoverHighlightling", eval(cbHoverMode.checked == true));
		fRowHoverHighlightlingApply();
	}

	// Process click to hide filtered Earthcaches highlighting checkbox.
	function fRowEcFilterHideClicked() {
		var cbEcFilterMode = document.getElementById("cbEcFilterMode");
		GM_setValue("RowEcFilterHide", eval(cbEcFilterMode.checked == true));
		fRefreshHideSetting();
		//		alert('Click Filter or Refresh to see results');
	}

	// Process click to toggle auto close of review page on hold
	function fcbHoldAutocloseClicked() {
		var cbHoldAutoclose = document.getElementById("cbHoldAutoclose");
		GM_setValue("HoldAutoClose", eval(cbHoldAutoclose.checked == true));
        location.reload(true);
	}

	// Process click to toggle auto close of review page on hold
	function fcbHoldAndWatchClicked() {
		var cbHoldAndWatch = document.getElementById("cbHoldAndWatch");
		GM_setValue("HoldAndWatch", eval(cbHoldAndWatch.checked == true));
        location.reload(true);
	}

	// Process click to hide all filtered entries.
	function fHideFilteredClicked() {
		var cbHideFiltered = document.getElementById('cbHideFiltered');
		GM_setValue('HideFiltered', (cbHideFiltered.checked == true));
		fRefreshHideSetting();
//		alert('Click Filter or Refresh to see results');
	}

	// Apply current setting of hover highlighting checkbox.
	function fRowHoverHighlightlingApply() {
		if (eval(GM_getValue("RowHoverHighlightling", 'true'))) {
			GM_addStyle("tr.outline_box:hover { outline: 5px ridge yellow !important; }");
		} else {
			GM_addStyle("tr.outline_box:hover { outline: 0px none transparent !important; }");
		}
	}

	// Color Selector changed.
	function fColorChanged() {
		var Swatch = this.nextSibling;
		var gmx = this.getAttribute('type', '');
		GM_setValue(gmx, this.value);
		eval(gmx + ' = "' + this.value + '"');
		Swatch.style.backgroundColor = '#' + this.value;
		this.title = this.value + ' - ' +
				this.options[this.selectedIndex].getAttribute('colorname', '');
		Swatch.title = this.title;
		fRefreshAllRows();
	}

	// Color contrast calcualtor.
	// Pass 6-character hex color code. Returns best choice of black or white for back/foreground.
	function getContrastYIQ(hexcolor){
		var r = parseInt(hexcolor.substr(0,2),16);
		var g = parseInt(hexcolor.substr(2,2),16);
		var b = parseInt(hexcolor.substr(4,2),16);
		var yiq = ((r*299)+(g*587)+(b*114))/1000;
		return (yiq >= 128) ? 'black' : 'white';
	}

	// Dropdown changed. Save value and update links.
	function ltsChange() {
		CachesToShow = ltsDropdown.value - 0;
		GM_setValue("CachesToShow", CachesToShow);
		for (var i = 0; i < ic; i++) {
			var ThisLink = RevList.snapshotItem(i);
			var LinkUrl = ThisLink.href;
			if (LinkUrl.match(/[?|&]nc=/)) {
				LinkUrl = LinkUrl.replace(/(\?|&)nc=.*?(&|$)/, '$1')
				LinkUrl = LinkUrl.replace(/&+$/, '');
			}
			ThisLink.href = LinkUrl + '&nc=' + CachesToShow;
		}
	}

	// Archived Filter Checkbox changed. Save value and update links.
	function fArcFilterChanged() {
		var cbFilterArchived = document.getElementById("cbFilterArchived");
		bFilterArchived = eval(cbFilterArchived.checked == true);
		imgFilterArchived.classList.toggle('gms_faded');
		GM_setValue("FilterArchived", bFilterArchived);
		var RevLinks = document.getElementsByName('revlnk');
		var ic = RevLinks.length - 1;
		for (var i = 0; i < ic; i++) {
			var lnkSrc = RevLinks[i].href;
			lnkSrc = lnkSrc.replace(/&a=false/, '');
			if (bFilterArchived) {
				lnkSrc = lnkSrc + '&a=false';
			}
		RevLinks[i].href = lnkSrc;
		}
	}

	// Change Open Order status.
	function ToggleOpenOrder() {
		OpenOrder = this.getAttribute('status') - 0;
		GM_setValue('OpenOrder', OpenOrder);
		SetOpenOrderStar(OpenOrder);
		top2bottom.firstChild.data = t2bStar + 'Top to Bottom';
		bottom2top.firstChild.data = b2tStar + 'Bottom to Top';
	}

	// Open tabs in specified order.
	function OpenTabs() {
		var idx = this.getAttribute('numid');
		// If top down.
		if (OpenOrder == 0) {
			for (var i = 0; i <= idx; i++) {
/*				var excluded = eval(document.getElementById("revlnk" + i).getAttribute('excluded', false));
				var filtered = eval(document.getElementById("revlnk" + i).getAttribute('filtered', false));
				var usaOmit = eval(document.getElementById("revlnk" + i).getAttribute('usa-omit', false));
				var earthcache = eval(document.getElementById("revlnk" + i).getAttribute('earthcache', false)); //todo correct logic
				if (!excluded && !filtered && !hl_filtered && !earthcache) { */
				if (!fRowGrayFilterResult(i)){
					var RevLink = document.getElementById("revlnk" + i);
					GM_openInTab(RevLink.href,{'loadInBackground':true}); 
				}
			}
		// If bottom up.
		} else {
			for (var i = idx; i >= 0; i--) {
/*				excluded = eval(document.getElementById("revlnk" + i).getAttribute('excluded', false));
				filtered = eval(document.getElementById("revlnk" + i).getAttribute('filtered', false));
				usaOmit = eval(document.getElementById("revlnk" + i).getAttribute('usa-omit', false));
//				hl_filtered = eval(document.getElementById("revlnk" + i).getAttribute('hl_filtered', false));
				earthcache = eval(document.getElementById("revlnk" + i).getAttribute('earthcache', false));
				if (!excluded && !filtered && !usaOmit &&/* !hl_filtered && !earthcache) { */
				if (!fRowGrayFilterResult(i)) {
					RevLink = document.getElementById("revlnk" + i);
					GM_openInTab(RevLink.href);
				}
			}
		}
	}

	// Publish one or group of caches.
	function PublishCache() {
		var numid = this.getAttribute('numid');
		if (bPubUpMode) {
			var resp = confirm('Publish top caches?');
			if (!resp) { return; }

			for (var i = 0; i <= numid; i++) {
/*				var excluded = eval(document.getElementById("revlnk" + i).getAttribute('excluded', false));
				var filtered = eval(document.getElementById("revlnk" + i).getAttribute('filtered', false));
				var usaOmit = eval(document.getElementById("revlnk" + i).getAttribute('usa-omit', false));
//				var hl_filtered = eval(document.getElementById("revlnk" + i).getAttribute('hl_filtered', false));
				var earthcache = eval(document.getElementById("revlnk" + i).getAttribute('earthcache', false));
				if (!excluded && !filtered && !usaOmit && /*!hl_filtered &&  !earthcache) { */
				if (!fRowGrayFilterResult(i)) {
                    var RevLink = document.getElementById("revlnk" + i);
					GM_openInTab(RevLink.href + '&action=publish');
				}
			}
		} else {
			var RevLink = document.getElementById("revlnk" + numid);
			GM_openInTab(RevLink.href + '&action=publish');
		}
	}

	// Hold/Unhold cache(s).
	function HoldCache() {
		var hldAction = this.getAttribute('action');

		// set up href modifier
		var myHref = '&action=' + hldAction;
		if (eval(GM_getValue("HoldAutoClose"))) {
			myHref += "&autoclose=yes"
		}
		else if (!eval(GM_getValue("HoldAutoClose"))) {
			myHref += "&autoclose=no"
		}

		if (eval(GM_getValue("HoldAndWatch"))) {
			myHref += "&holdandwatch=yes"
		}
		else if (!eval(GM_getValue("HoldAndWatch"))) {
			myHref += "&holdandwatch=no"
		}

		if (bHldUpMode) {
			if (!showHold) {
				var resp = confirm('Hold top caches?');
			} else {
				resp = confirm('Remove Hold from top caches?');
			}
			if (!resp) { return; }


			var idx = this.getAttribute('numid');
			for (var i = 0; i <= idx; i++) {
//				var excluded = eval(document.getElementById("revlnk" + i).getAttribute('excluded', false));
//				var filtered = eval(document.getElementById("revlnk" + i).getAttribute('filtered', false));
//				var usaOmit = eval(document.getElementById("revlnk" + i).getAttribute('usa-omit', false));
//				var hl_filtered = eval(document.getElementById("revlnk" + i).getAttribute('hl_filtered', false));
//				var earthcache = eval(document.getElementById("revlnk" + i).getAttribute('earthcache', false));//todo correct logic
				if (!fRowGrayFilterResult(i)) {
					var RevLink = document.getElementById("revlnk" + i);
					GM_openInTab(RevLink.href + myHref);
//					console.log(RevLink.href+myHref);
}
			}
		} else {
            var numid = this.getAttribute('numid');
            var RevLink = document.getElementById("revlnk" + numid);
//			console.log(RevLink.href + myHref);
			GM_openInTab(RevLink.href + myHref);
		}
	}

	// Set Open Order star value.
	function SetOpenOrderStar(q) {
		switch (q) {
			case 0: t2bStar = '*'; b2tStar = ''; break;
			case 1: t2bStar = ''; b2tStar = '*'; break;
		}
	}

	// Set Cache Status Selector Hover Text.
	// 0 = All unpublished caches	(All)
	// 1 = All caches not on hold	(NotHeld)
	// 2 = All caches on hold		(AllHolds
	// 3 = Caches I'm holding		(MyHolds)
	function SetCacheStatusTitle() {
		switch(e_ddFilter.value) {
			case '1':
				e_ddFilter.title = 'filter=' + 'NotHeld';
				break;
			case '2':
				e_ddFilter.title = 'filter=' + 'AllHolds';
				break;
			case '3':
				e_ddFilter.title = 'filter=' + 'MyHolds';
				break;
			default:
				e_ddFilter.title = 'filter=' + 'All';
		}
	}

	// Set Country Selector Hover Text.
	function SetCountryTitle() {
		if (e_ddCountry.value) {
			e_ddCountry.title = 'countryid=' + e_ddCountry.value;
		} else {
			e_ddCountry.title = '';
		}
	}

	// Set State Selector Hover Text.
	function SetStateTitle() {
		if (e_ddState.value) {
			e_ddState.title = 'stateid=' + e_ddState.value;
		} else {
			e_ddState.title = '';
		}
	}

	// Show resulting URL based on current drop-down settings.
	function ShowUrl() {
		var newUrl = window.document.location.href + '';
		var qmamp = '?';

		if (newUrl.indexOf('?', 0) > -1) {
			newUrl = newUrl.substring(0, newUrl.indexOf('?', 0));
		}
		if (e_ddFilter.value != '1') {
			newUrl += qmamp + e_ddFilter.title;
			qmamp = '&';
		}
		if (e_ddCountry.title) {
			newUrl += qmamp + e_ddCountry.title;
			qmamp = '&';
		}
		if (e_ddCountry.value == '' || e_ddCountry.value == '2') {
			if (e_ddState.title) {
				newUrl += qmamp + e_ddState.title;
				qmamp = '&';
			}
		}
		var resp = prompt('URL string based on current selector settings.\n\n' +
				'Press OK to reload the page using this URL address.\n' +
				'Otherwise, press Cancel', newUrl);
		if (resp) {
			window.document.location.href = newUrl;
		}
	}

	// Toggle filter by cache name / owner name.
	function fSwitchTitleOwner() {
		TitleOwnerMode = this.getAttribute('mode');
		switch (TitleOwnerMode) {
		   case 'Owner': TitleOwnerMode = 'NotOwner'; break;
		   case 'NotOwner': TitleOwnerMode = 'Title'; break;
		   case 'Title': TitleOwnerMode = 'NotTitle'; break;
		   case 'NotTitle': TitleOwnerMode = 'Owner'; break;
		}
		GM_setValue('TitleOwnerMode', TitleOwnerMode);
		lnkSwitchTitleOwner.setAttribute('mode', TitleOwnerMode);
		imgSwitchTitleOwner.src = eval(TitleOwnerMode + 'Img');
	}

	// Refresh filter effects.
	function fRefreshFilter() {
		TitleOwnerFilter = inputTitleOwnerFilter.value.trim();
		inputTitleOwnerFilter.value = TitleOwnerFilter;
		GM_setValue('TitleOwnerFilter', TitleOwnerFilter);
//		if (!TitleOwnerFilter && !bEC && !bUSA) { //old
		if (!TitleOwnerFilter) {
			fEraseFilter();
		} else {
			fDoTitleOwnerFiltering();
		}
	}

	// Erase filter contents.
	function fEraseFilter() {
		inputTitleOwnerFilter.value = "";
		GM_setValue('TitleOwnerFilter', "");
		if (max_caches) {
			for (var i = 0; i < max_caches; i++) {
				document.getElementById("revlnk" + i.toString()).setAttribute('filtered', false);
				fSetFilter(i,fRowGrayFilterResult(i));
/*				var isFiltered = eval(document.getElementById("revlnk" + i.toString()).getAttribute('filtered', false));
				var isUsaOmit = eval(document.getElementById("revlnk" + i.toString()).getAttribute('usa-omit', false));
				var isExcluded = eval(document.getElementById("revlnk" + i.toString()).getAttribute('excluded', false));
				var isEarthcache = eval(document.getElementById("revlnk" + i.toString()).getAttribute('earthcache', false));
				if (isFiltered || isUsaOmit) {
					document.getElementById("revlnk" + i.toString()).setAttribute('filtered', false);
					fSetFilter(i, (isExcluded || isUsaOmit));
				}*/
			}
		}
		inputTitleOwnerFilter.focus();
	}

	// Report high-submission users.
	function fAnalyzeByOwner(e) {
		var minLimit = GM_getValue('HighSubmissionLimit', 8);
		if (e.shiftKey) {
			var newMinLimit = prompt('Enter New Limit Value', minLimit).trim();
			if (newMinLimit == null) {
				alert('Change canceled.')
			} else {
				if (newMinLimit && isNumeric(newMinLimit)) {
					GM_setValue('HighSubmissionLimit', newMinLimit);
				} else {
					alert('Non-numeric entry detected.');
				}
			}
		} else {
			var setToClipboard = false;
			var dataList = document.getElementsByClassName('CacheData');
			var ic = dataList.length;
			var nameCount = new Object;
			for (var i = 0; i < ic; i++) {
				var co = dataList[i].getAttribute('data-ownerusername').trim();
				if (!nameCount.hasOwnProperty(co)) { nameCount[co] = 0; }
				nameCount[co] += 1;
			}
			var output = 'Users submitting ' + minLimit + ' or more caches:\n';
			for (var i in nameCount) {
				if (nameCount[i] >= minLimit) {
					if (!setToClipboard) {
						GM_setClipboard(i);
						setToClipboard = true;
					}
					output += '\n' + i + ': ' + nameCount[i].toString();
				}
			}
			alert(output);
		}
	}

	// Save caches to clipboard.
	function fMemoryFilter() {
		// Add caches that passed filter to the array.
		var clipboardArray = new Array();
		var revlinks = document.getElementsByName("revlnk");
		for (var i = 0; i < revlinks.length; i++) {
			var revlink = revlinks[i];
/*			var bExcluded = eval(revlink.getAttribute('excluded'));
			var bFiltered = eval(revlink.getAttribute('filtered'));
			var bUsaOmit = eval(revlink.getAttribute('usa-omit'));
			var bEarthcache = eval(revlink.getAttribute('earthcache', 'false'));
			if (!bExcluded && !bFiltered && !bUsaOmit && !hl_filtered && !bEarthcache) { */
			if (!fRowGrayFilterResult(i)) {
				var tWptid = revlink.getAttribute('wptid');
				if (clipboardArray.indexOf(tWptid) == -1) {
					clipboardArray.push(tWptid);
				}
			}
		};

		// Create and remove temporary text area to execute copy to clipboard operations, throw comfirming alert
		const tempTextArea = document.createElement('textarea');
  		tempTextArea.value = clipboardArray;
  		document.body.appendChild(tempTextArea);
  		tempTextArea.select();
  		document.execCommand('copy');
  		document.body.removeChild(tempTextArea);
  		alert(`Success, ${clipboardArray.length} caches copied to clipboard.`);
	}

	// Click either Prev or Next link to change page, via location hack.
	function fClickPrevNext(e_PrevNext) {
		location.assign(e_PrevNext.href + ";void(0)");
	}

	// Refresh all rows.
	function fRefreshAllRows() {
		if (max_caches) {
			for (var i = 0; i < max_caches; i++) {
/*				var isFiltered = eval(document.getElementById("revlnk" + i.toString()).getAttribute('filtered', false));
				var isUsaOmit = eval(document.getElementById("revlnk" + i.toString()).getAttribute('usa-omit', false));
				var isExcluded = eval(document.getElementById("revlnk" + i.toString()).getAttribute('excluded', false));
				var isEarthcache = eval(document.getElementById("revlnk" + i.toString()).getAttribute('earthcache', false));
				var isTP = eval(document.getElementById("revlnk" + i.toString()).getAttribute('timedP', false));*/
	/*			fSetFilter(i, (isFiltered || 
					(isUsaOmit && cbUSA.checked) || 
					isExcluded ||
					(isEarthcache && cbEC.checked) ||
					(isTP && timedP.checked))
				);*/
				fSetFilter(i,fRowGrayFilterResult(i));
			}
		}
	}

	// This function returns the background color status of a row based on whether it is:
	//    - identified by a Title/Owner filter; or,
	//	  - identified by a check in that row's "exclude" checkbox; or,
	//    - subject to one of the checkboxes that result in that row being hidden.
	// The color is based on the attributes in revlnk and the checkbox settings.  This function
	// differs from fRefreshHideSetting in that it determines whether a mass action applies to a row, not
	// whether or not the row is visible. 
	function fRowGrayFilterResult(row){
		var myDataNode = document.getElementById("revlnk" + row.toString());
		var isFiltered = eval(myDataNode.getAttribute('filtered', false));
		var isUsaOmit = eval(myDataNode.getAttribute('usa-omit', false));
		var isExcluded = eval(myDataNode.getAttribute('excluded', false));
		var isEarthcache = eval(myDataNode.getAttribute('earthcache', false));
		var isTP = eval(myDataNode.getAttribute('timedP', false));
		var isQLocationHighlighted = eval(myDataNode.getAttribute('hl_filtered'),false);
		return (isFiltered || 
			(isUsaOmit && cbUSA.checked) || 
			isExcluded ||
			(isEarthcache && cbEC.checked) ||
			(isTP && timedP.checked) ||
			isQLocationHighlighted
		);
	}

	function fRefreshHideSetting(){
		if (max_caches) {
//			var HideEcWhenFiltered = eval(GM_getValue("RowEcFilterHide", true));
//			var HideEc = eval(GM_getValue("FilterEarthcaches", false));
//			var HideFo = eval(GM_getValue('HideFiltered', false));
			
			for (var i = 0; i<max_caches; i++){
				let currentItem = document.getElementById("revlnk" + i.toString());
				let cacheRow = currentItem.parentNode.parentNode;

				var filteredOut = eval(currentItem.getAttribute('filtered', false));
				var excluded = eval(currentItem.getAttribute('excluded', false));
				var isUsaOmit = eval(currentItem.getAttribute('usa-omit', false));
				var isEarthcache = eval(currentItem.getAttribute('earthcache', false));
				var isTP = eval(currentItem.getAttribute('timedP', false));
				var isQLocationHighlighted = eval(currentItem.getAttribute('hl_filtered'),false);
	
				cacheRow.style.visibility = 'visible';
				if ((isEarthcache && cbEC.checked) ||
					(cbEcFilterMode.checked && isEarthcache && (filteredOut || excluded)) ||
					(cbHideFiltered.checked && (filteredOut || excluded)) ||
					(isUsaOmit && cbUSA.checked) ||
					(timedP.checked && isTP) ||
					(isQLocationHighlighted) // this attribute is actively toggled by QLocHighlighter
				) cacheRow.style.visibility = 'collapse';
			}
		}
	}

	// Toggle Hold Mode.
	function fSwitchHoldMode() {
		bHldUpMode = !bHldUpMode;
		GM_setValue('HoldUpMode', bHldUpMode.toString());
		fUpdateHoldSwitch();
	}

	// Update Hold Switch.
	function fUpdateHoldSwitch() {
		if (!showHold) {
			if (bHldUpMode) {
				hldSwitchImg.src = UpHoldImg;
				hldSwitchLink.title = 'Hold top group of caches.';
			} else {
				hldSwitchImg.src = HoldImg;
				hldSwitchLink.title = 'Hold individual cache';
			}
		} else {
			if (bHldUpMode) {
				hldSwitchImg.src = UpUnHoldImg;
				hldSwitchLink.title = 'Remove hold of top group of caches';
			} else {
				hldSwitchImg.src = UnHoldImg;
				hldSwitchLink.title = 'Remove hold of individual cache.';
			}
		}

		for (var i = 0; i < max_caches; i++) {
			var xImg = document.getElementById("hldlnk" + i.toString()).firstChild;
			xImg.src = hldSwitchImg.src;
			xImg.parentNode.title = hldSwitchImg.parentNode.title;
		}
	}

	// Toggle Publish Mode.
	function fSwitchPublishMode() {
		bPubUpMode = eval(GM_getValue('PublshUpMode', 'false'));

		bPubUpMode = !bPubUpMode;
		GM_setValue('PublshUpMode', bPubUpMode.toString());
		fUpdatePublishSwitch();
	}

	function fUpdatePublishSwitch() {
		var bPubUpMode = eval(GM_getValue('PublshUpMode', 'false'));
		if (bPubUpMode) {
			pubSwitchImg.src = UpPublishImg;
			pubSwitchLink.title = 'Publish top group of caches.';
		} else {
			pubSwitchImg.src = OnePublishImg;
			pubSwitchLink.title = 'Publish individual cache';
		}

		for (var i = 0; i <= max_caches; i++) {
			var xImg = document.getElementById("publnk" + i.toString()).firstChild;
			xImg.src = pubSwitchImg.src;
			xImg.parentNode.title = pubSwitchLink.title;
		}
	}

	// If exclude checkbox is clicked.
	function fCheckboxClicked() {
		var wptid = this.getAttribute('wptid', '');
		var ar_excluded = GM_getValue("ExcludedCaches", '').split(' ');
		var idx = ar_excluded.indexOf(wptid);
		if (this.checked) {
			if (idx < 0) {
				var t = ar_excluded.unshift(wptid);
				if (t > max_excluded) { ar_excluded.splice(max_excluded); }
				var excluded = ar_excluded.join(' ');
				GM_setValue("ExcludedCaches", excluded);
			}
		} else {
			if (idx >= 0) {
				ar_excluded.splice(idx, 1);
				var excluded = ar_excluded.join(' ');
				GM_setValue("ExcludedCaches", excluded);
			}
		}
		var numid = this.getAttribute('numid', '');
		document.getElementById("revlnk" + numid).setAttribute('excluded', this.checked);
//		var isFiltered = eval(document.getElementById("revlnk" + numid).getAttribute('filtered', this.checked));
//		var isUsaOmit = eval(document.getElementById("revlnk" + numid).getAttribute('usa-omit', this.checked));
//		var isEarthcache = eval(document.getElementById("revlnk" + numid).getAttribute('earthcache', this.checked));
//		fSetFilter(numid, (this.checked || isFiltered || isUsaOmit ));
		fSetFilter(numid, fRowGrayFilterResult(numid));
}

	// Perform title/owner filtering.
	function fDoTitleOwnerFiltering() {
		var TitleOwnerFilter = GM_getValue("TitleOwnerFilter", '').toUpperCase();
//		var HideEcWhenFiltered = eval(GM_getValue("RowEcFilterHide", true));
//		var HideEc = eval(GM_getValue("FilterEarthcaches", false));
//		var HideFo = (GM_getValue('HideFiltered', false) == true);
		if (TitleOwnerFilter || cbEC.checked || cbUSA.checked || timedP.checked) {
			var TitleOwnerMode = GM_getValue("TitleOwnerMode", 'Owner');
			if (max_caches) {
				for (var i = 0; i < max_caches; i++) {
					var cacheRow = document.getElementById("cacheRow" + i);
					var ThisData = cacheRow.getElementsByClassName('CacheData')[0];

					var filteredOut = false; //set the default as off

					if (TitleOwnerFilter) {

						switch (TitleOwnerMode) {
							case "Title":
								var TitleOwner = ThisData.getAttribute('data-cachename').trim().toUpperCase();
								var filteredOut = (TitleOwner.indexOf(TitleOwnerFilter) >= 0);
								break;
							case "NotTitle":
								TitleOwner = ThisData.getAttribute('data-cachename').trim().toUpperCase();
								filteredOut = (TitleOwner.indexOf(TitleOwnerFilter) < 0);
								break;
							case "Owner":
								TitleOwner = ThisData.getAttribute('data-ownerusername').trim().toUpperCase();
								filteredOut = (TitleOwnerFilter == TitleOwner);
								break;
							case "NotOwner":
								TitleOwner = ThisData.getAttribute('data-ownerusername').trim().toUpperCase();
								filteredOut = (TitleOwnerFilter != TitleOwner);
								break;
						}

					}  

					// and set the filter attribute and visual representation according to the result
					document.getElementById("revlnk" + i.toString()).setAttribute('filtered', filteredOut);
					fSetFilter(i,fRowGrayFilterResult(i));

/* commenting this code out in favor of fRefreshHideSetting
					var excluded = eval(document.getElementById("revlnk" + i).getAttribute('excluded', false));
					var isUsaOmit = eval(document.getElementById("revlnk" + i).getAttribute('usa-omit', false));
//					var hl_filtered = eval(document.getElementById("revlnk" + i).getAttribute('hl_filtered', false));
					var isEarthcache = eval(document.getElementById("revlnk" + i).getAttribute('earthcache', false));
					var isTP = eval(document.getElementById("revlnk" + i).getAttribute('timedP', false));
//					fSetFilter(i, filteredOut || excluded);
//					fSetFilter(i, filteredOut || excluded || hl_filtered || isEarthcache);
					fSetFilter(i,fRowGrayFilterResult(i));

/*					// Collapse table row, if earthcache, and filtering is on.
					if ((isEarthcache && HideEc) || isUsaOmit) {
						cacheRow.style.visibility = 'collapse';
					} else {
						if (!isEarthcache && (filteredOut || hl_filtered)) {
							cacheRow.style.visibility = (HideFo) ? 'collapse' : 'visible';
						} else if (!isEarthcache && !filteredOut) {
							cacheRow.style.visibility = 'visible';
						}
					}
*/ /*
					// new logic for collapsing rows
					cacheRow.style.visibility = 'visible';
					if ((isEarthcache && HideEc) ||
						(HideEcWhenFiltered && isEarthcache && (filteredOut || excluded)) ||
						(HideFo && (filteredOut || excluded)) ||
						(isUsaOmit && cbUSA.checked) ||
						(timedP.checked && isTP)
					) cacheRow.style.visibility = 'collapse'; */
				}
			}
		}
		fRefreshHideSetting();
		console.log('just did a titleownerfilter hide row refresh');
	}

	// Set filtering. Pass id number and boolean filter value.
	function fSetFilter(i, bOnOff) {
		if (bOnOff) {
			var vis = 'hidden';
			var bg = '#' + bgcolor_filter;
		} else {
			vis = 'visible';
			bg = '#' + bgcolor_standard;
		}
		var opnlnk = document.getElementById("opnlnk" + i);
		opnlnk.style.visibility = vis;
		var hldlnk = document.getElementById("hldlnk" + i);
		hldlnk.style.visibility = vis;
		var publnk = document.getElementById("publnk" + i);
		publnk.style.visibility = vis;
		var cacheRow = document.getElementById("cacheRow" + i);
		cacheRow.bgColor = bg;

		// The following changes each cell to the appropriate color. Changing the row
		// color no longer works after the May-2011 site release.
		var kids = cacheRow.getElementsByTagName("td");
		for (var i = 1; i < kids.length; i++) {
			kids[i].style.backgroundColor = bg;
		}
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location.href + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Return True if a number is even (or zero). Else, return False.
	function isEven(value) {
		return (value % 2 == 0);
	}

	// Round number to specific number of decimal places.
	function roundNumber(num, dec) {
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		return result;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Move up the DOM tree a specific number of generations.
	function fUpGen(gNode, g) {
		var gNode;
		var g;
		for (var i = 0; i < g; i++) {
			gNode = gNode.parentNode;
		}
		return gNode;
	}

	// Move up the DOM tree until a specific DOM type is reached.
	function fUpGenToType(gNode, gType) {
		var gNode;
		var gType = gType.toUpperCase();
		while (gNode.nodeName.toUpperCase() != gType) {
			gNode = gNode.parentNode;
		}
		return gNode;
	}

	function getContrastYIQ(hexcolor){
		var r = parseInt(hexcolor.substr(0,2),16);
		var g = parseInt(hexcolor.substr(2,2),16);
		var b = parseInt(hexcolor.substr(4,2),16);
		var yiq = ((r*299)+(g*587)+(b*114))/1000;
		return (yiq >= 128) ? 'black' : 'white';
	}

	// Test for numeric-only data.
	function isNumeric(value) {
		if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) {
			return false;
		} else {
			return true;
		}
	}

	// ---------------------------------------------
	function fUnExcludeAll(e) {
		for (var i = 0; i < ic; i++) {
			var ThisLink = RevList.snapshotItem(i);
//			var ThisRow = ThisLink.parentNode.parentNode;
//			var checkboxCell = cacheRow.cells[4];
			var checkbox = document.querySelector('#exclude' + i.toString())
			if (checkbox) {
				checkbox.checked = false;
				document.getElementById("revlnk" + i).setAttribute('excluded', false);
			}
		}
		fRefreshAllRows();
		GM_setValue("ExcludedCaches", '');
	}

	function fProcessSkipCheckboxes(){
		GM_setValue(this.id,this.checked);
	};

	function fCreateImg(imgData,imgID){
		var imgHeld = document.createElement("img");
		imgHeld.src = imgData;
		imgHeld.id = imgID;
		imgHeld.border = '0';
		imgHeld.style.marginLeft = '10px';
		imgHeld.style.marginRight = '4px';
		imgHeld.style.verticalAlign = 'text-bottom';
		return imgHeld;
	}

	function fCreateCheckbox(cbID,cbTitle,cbDefault){
		let skipHeld = document.createElement("input");
		skipHeld.type = 'checkbox';
		skipHeld.id = cbID;
		skipHeld.style.marginRight = '6px';
		skipHeld.title = cbTitle;
		skipHeld.checked = GM_getValue(cbID,cbDefault);
		return skipHeld;
	}

