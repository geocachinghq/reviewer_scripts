﻿// ==UserScript==
// @name           GCR Word Highlighter v2
// @description    Highlights Specified Words In Cache Descriptions
// @namespace      *.geocaching.com/admin
// @version        02.09
// @icon           http://i.imgur.com/MleOpfw.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Word_Highlighter.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Word_Highlighter.user.js
// @match	       http*://*.geocaching.com/*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// @require 	   https://code.jquery.com/jquery-3.3.1.min.js
// ==/UserScript==

/*
Change Log:
*2.09	2023-12-12  Begin Tag Manager logging for basic analytics
*2.08	2023-06-13  add <style> tags to the list of things that highlighter ignores, move to @match
*2.07	2023-05-21  suspend highlighting when mouse is down (prevents slow map scrolling)
*2.06	2023-05-18  insert highlighting spans without disturbing onClick events
*2.05	2023-05-05  correct error caused by search term duplicates and generate user alert on bad search terms
*2.04	2023-05-04	immediately update the list of words to highlight based on saved edits, allow port of old list
*2.03	2023-04-06	will now highlight on any page and is NextJS compliant
*2.02	2020-07-31	added Change Log & removed include for http*://*.geocaching.com/seek/cache_details.aspx?*
*2.01	last version before implementing changelog

Function:
 Highlights specified words in the short & long descriptions.
 By default, runs on cache page and review page.  Highlight word list and page list are browser dependent, not
 reviewer dependent.  Can add and remove web pages for the highlighter to act on through the script options menu.
 */

// Google Tag Manager setup
function gtagWH(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
function googleTagLogWH(dataAction){ //Method to do a gtag sending an action name
	if (document.URL.indexOf("www") < 0) return;
	try {
		gtagWH('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'WordHighlighter',/*'debug_mode':true,*/
			'send_to':'G-GRQE2910DL'});
		console.log('Script '+dataAction+' logged to Tag Manager.');
	}
	catch {console.log('There was an error using gtag.  Logging attempt failed.')}
}

 let wordList = "(http)?s?(://)?(www\.)?%(?<!geocaching|waymarking)\.(com|se)"; //Wordlist defaults
 let highlitePageList = '.com/admin/review.aspx;.com/geocache/GC'; //default pages to run the script
 
 // Set menu option for settings.
 GM_registerMenuCommand('Set Words to Highlite', fSetOptions);
 
 let fail=false;
 
 let pageURL = document.URL;
 if (pageURL.indexOf('#') > -1) pageURL = pageURL.substring(0,pageURL.indexOf('#'));
 if (pageURL.indexOf('?') > -1) pageURL = pageURL.substring(0,pageURL.indexOf('?'));
 wordList = GM_getValue('Highlitelist',wordList);
 
 highlitePageList = GM_getValue('highlitePageList',highlitePageList);
 let highlitePages = highlitePageList.split(';');
 let foundURL = false;
 for (i=0;i<highlitePages.length;i++) {
	 if (pageURL.indexOf(highlitePages[i]) > -1) {
		 foundURL = true;
		 break;
	 }
 }
 if (!foundURL) {
	 GM_registerMenuCommand('Use Word Highliter On This Page', fUseWordHighliter);
	 return;
 }
 else GM_registerMenuCommand('Turn Off Word Highliter On This Page', fTurnOffWordHighliter);

 googleTagLogWH('initialize');

 let aWords = wordList.split(';');
 for (i=0; i<aWords.length;i++) {
	 aWords[i]=aWords[i].trim();
	 aWords[i] = aWords[i].replace(/\s+/g, '\\s\+');
	 aWords[i] = aWords[i].replace(/%/g, '\\w\*\?');
	 //todo - remove aWord array items that are null
 }
 let leftHilite = '\u0152\u0152\u0152';
 let rightHilite = '\u0153\u0153\u0153';
 let leftRegExCombine = new RegExp(leftHilite+"("+leftHilite+')+','g');
 let rightRegExCombine = new RegExp(rightHilite+'('+rightHilite+')+','g');
 
 GM_addStyle('.gchl { background-color: #FFFF00; color: #000000; ' +
 'font-weight: bold; display: inline; }');
 let leftSpan = '<span class="gchl">';
 let rightSpan = '</span>';
 let leftSpanLength = leftSpan.length;
 let rightSpanLength = rightSpan.length;
 let RegExLeftHilite = new RegExp(leftHilite, 'g');
 let RegExRightHilite = new RegExp(rightHilite, 'g');
 const observerConfig = { attributes: false, childList: true, subtree: true };
 const highliteObserver = new MutationObserver(handleObserver);
 let mouseDown=false;
 
 let bodyLoaded = document.getElementsByTagName('body')[0];
 let ready = setInterval( ()=> {
	 if (!bodyLoaded) {
		 bodyLoaded = document.getElementsByTagName('body')[0];
		 return;
	 }
	 clearInterval(ready);
	 bodyLoaded.addEventListener('mousedown',fMouseDown);
	 bodyLoaded.addEventListener('mouseup',fMouseUp);
	 highliteObserver.observe(bodyLoaded,observerConfig);
	 handleObserver();
 },1000);
 
 //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FUNCTIONS
function fMouseDown() {
 	mouseDown= true;
}

function fMouseUp(){
	mouseDown = false;
}

 function handleObserver(){
	 highliteObserver.disconnect();
	 if (mouseDown) {
		let waiting = setInterval(()=>{
			if (mouseDown) return;
			clearInterval(waiting);
			insertHighlites();
			highliteObserver.observe(bodyLoaded,observerConfig);
		},1000);
		return;
	 }
	 setTimeout(() => {
		 insertHighlites();
		 highliteObserver.observe(bodyLoaded,observerConfig);
	 },250);
 }
 
 function insertHighlites() {
	 if (fail) return;
	 console.log('Word Highliter is running a scan.');
	 localNodeList = document.evaluate(
		 ".//div//text()",
		 document.body,
		 null,
		 XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
		 null);
	 for (i=0; ((i<localNodeList.snapshotLength) && (i<9999)); i++) {
		 var isStyle = $(localNodeList.snapshotItem(i)).parents('style');
		 var isForm = $(localNodeList.snapshotItem(i)).parents('textarea');
		 var isScript = $(localNodeList.snapshotItem(i)).parents('script'); //turns out highlighter can screw up scripts
		 if ((isForm.length) == 0 && (isScript.length == 0) && (isStyle.length == 0)) {
			 var z = localNodeList.snapshotItem(i).data;
			 for (var j = 0; j < aWords.length; j++) {
				 let checkWord = aWords[j];
				 if (checkWord) {
					 try {
						 var RegEx1 = new RegExp('\\b(' + checkWord + ')\\b', 'ig');
						 z = z.replace(RegEx1, leftHilite + '$1' + rightHilite);
					 }
					 catch { //generate user errors for bad Regex
						 alert('There was an error with the Word Highlighter search term: '+aWords[j]+
						 '\n\nScript Terminated.  Please correct this search term and reload the page.');
						 fail = true;
						 return;
					 }
				 }
			 }
			 z = z.replace(leftRegExCombine,leftHilite);// remove duplicate flags cause by duplicate search terms
			 z = z.replace(rightRegExCombine,rightHilite);
			 if (!(localNodeList.snapshotItem(i).data == z)) {
				 try {target = localNodeList.snapshotItem(i).parentElement;}
				 catch {
					console.log('Parent element error on: '+localNodeList.snapshotItem(i));
					target = null;
					setTimeout(handleObserver,1000);
				 }
				 if (target) {
//					console.log(z);
					insertContent(z);

					function insertContent(inputString) {
						let firstMark = inputString.indexOf(leftHilite);
						if (firstMark == 0) {
							if (target.getAttribute('class') == 'gchl') return;
							let wordEnd = inputString.indexOf(rightHilite);
							let word = inputString.substring(3,wordEnd);
							let newSpan = document.createElement('span');
							newSpan.setAttribute('class','gchl');
							let newText = document.createTextNode(word);
							newSpan.appendChild(newText);
							target.insertBefore(newSpan,localNodeList.snapshotItem(i));
							if ((wordEnd+3) == inputString.length) {
								localNodeList.snapshotItem(i).data = " ";
								return;
							}
							insertContent(inputString.substring(wordEnd+3));
						}
						else if (firstMark>0) {
							let newText = document.createTextNode(inputString.substring(0,firstMark));
							target.insertBefore(newText,localNodeList.snapshotItem(i));
							insertContent(inputString.substring(firstMark));
						}
						else localNodeList.snapshotItem(i).data = inputString;
						return;
					}
				 }
			 }
		 }
	 }
 }
 
 function removeHilites(someHTML) {
	 if (!someHTML) return someHTML;
	 someHTML = someHTML.replace(RegExLeftHilite,"");
	 someHTML = someHTML.replace(RegExRightHilite,"");
	 return someHTML;
 }
 
 function fUseWordHighliter() {
	 if (!window.confirm('Press OK to enable Word Highliter on this page.\n'+
		 'Refresh the page to insert highlites.')) return;
	 GM_setValue('highlitePageList', highlitePageList+';'+fGeneralizeURL(pageURL));
 }
 
 function fTurnOffWordHighliter () {
	 if (!window.confirm('Press OK to disable Word Highliter on this page.\n'+
		 'Refresh the page to eliminate existing highlites.')) return;
	 let newHighlitePageList = "";
	 for (i=0;i<highlitePages.length;i++) {
		 if (pageURL.indexOf(highlitePages[i]) < 0 ) {
			 newHighlitePageList = newHighlitePageList + highlitePages[i] + ';';
		 } 
	 }
	 if (newHighlitePageList.length > 0) 
		 newHighlitePageList = newHighlitePageList.substring(0,newHighlitePageList.length -1);
	 GM_setValue('highlitePageList',newHighlitePageList);
 }
 
 function fGeneralizeURL(aURL) {
	 aURL = aURL.substring(aURL.indexOf('.com'),aURL.length);
	 if (aURL.indexOf('GC') > -1)
		 aURL = aURL.substring(0,aURL.indexOf('GC')+2);
	 if (aURL.indexOf('GL') > -1)
		 aURL = aURL.substring(0,aURL.indexOf('GL')+2);
	 return aURL;
 }
 
 
	 //********************************************************************************//
	 //                                                                                //
	 //                               Settings Functions                               //
	 //                                                                                //
	 //********************************************************************************//
 
	 function fSetOptions() {
		 if (!fCreateSettingsDiv('Word Highlighter Script Options')) { return; }
		 var divSet = document.getElementById('gm_divSet');
 
		 // Insert code to create interface controls here.
 
		 //vDftVal = GM_getValue('WordList_' + SignedInAs, 'Lorem; ipsum; consectetur; adipiscing');
		 vDftVal = wordList;
		 vLabel = 'List of words or phrases to highlight. ' +
				 'Separate words or phrases with a semicolon(;). Case is irrelevant.';
		 var txtaraWordList = fCreateSetting('WordList', 'textarea', vLabel, '', vDftVal, '215px');
		 var infoWordList = document.createElement('span');
		 infoWordList.innerHTML =
			 '<p>Available wildcards and expressions:</p>' +
			 '<p> &bullet; Use a percent sign % as a general wildcard. "rest%" will match "rest", ' +
			 '"restaurant", and "restroom".</p>' +
			 '<p> &bullet; To make a single non-blank character optional, follow it with a question mark. For example, ' +
			 '"trains?" will match "train" and "trains". "e-?mail" matches "email" and "e-mail".</p>' +
			 '<p> &bullet; To make a sequence of letters optional, surround them with parentheses ( ), and follow ' +
			 'with a question mark ?. For example, "trespass(ing)?" matches "trespass" and ' +
			 '"trespassing". This is also the way to make a blank space optional. For example, ' +
			 '"path( )?tag" will match the word "pathtag" and the phrase "path tag".</p>' +
			 '<p> &bullet; To make groups of alternate sequences, surround them with parentheses, and ' +
			 'separate each group with the vertical bar character |. For example "(over|under)pass" will match ' +
			 'both "overpass" and "underpass". You could combine this with optional characters, so '+
			 'that "(over|under)pass(es)?", matches "overpass", "underpass", "overpasses", and ' +
			 '"underpasses".</p>' +
			 '<p> &bullet; To look for a period, type "\\.", otherwise "." means any single character.</p>' +
			 '<p> &bullet; In general, the highlighter matches with the Regex engine, and there are more special features.' +
			 '</p><br>';
		 txtaraWordList.parentNode.insertBefore(infoWordList, txtaraWordList);
 
		 // Create Save/Cancel Buttons.
		 var ds_ButtonsP = document.createElement('div');
		 ds_ButtonsP.setAttribute('class', 'SettingButtons');
		 var ds_SaveButton = document.createElement('button');
		 ds_SaveButton = document.createElement('button');
		 ds_SaveButton.appendChild(document.createTextNode("Save"));
		 ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		 var ds_CancelButton = document.createElement('button');
		 ds_CancelButton.style.marginLeft = '6px';
		 ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		 ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		 ds_ButtonsP.appendChild(ds_SaveButton);
		 ds_ButtonsP.appendChild(ds_CancelButton);
		 divSet.appendChild(ds_ButtonsP);
 
		 fOpenSettingsDiv();
	 }
 
	 function fSaveButtonClicked() {
 
		 // Insert code to save settings here.
		 var txtaraWordList = document.getElementById('txtaraWordList');
		 wordList = txtaraWordList.value.trim();
		 GM_setValue('Highlitelist', wordList);
 
 
		 fCloseSettingsDiv();
		 alert('You must reload the page for changes in search terms to be displayed.');
	 }
 
	 function fCancelButtonClicked() {
		 var resp = confirm('Any changes will be lost. Close Settings?');
		 if (resp) {
			 fCloseSettingsDiv();
		 }
	 }
 
	 //********************************************************************************//
	 //                                                                                //
	 //                          Settings Interface Functions                          //
	 //                                                                                //
	 //********************************************************************************//
 
	 function fCreateSettingsDiv(sTitle) {
 
		 // If div already exists, reposition browser, and show alert.
		 var divSet = document.getElementById("gm_divSet");
		 if (divSet) {
			 var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			 window.scrollTo(window.pageXOffset, YOffsetVal);
			 alert('Edit Setting interface already on screen.');
			 return false;
		 }
 
		 // Set styles for titles and elements.
		 GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				 'text-align: center; margin-bottom: 12px; !important; } ' );
		 GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				 'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		 GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				 'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		 GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important;} ' ); +
 
		 // Create blackout div.
		 document.body.setAttribute('style', 'height:100%;');
		 var divBlackout = document.createElement('div');
		 divBlackout.id = 'divBlackout';
		 divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				 'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				 'height: 100%; width: 100%; display: block;');
 
		 // Create div.
		 divSet = document.createElement('div');
		 divSet.id = 'gm_divSet';
		 divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				 'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		 popwidth = parseInt(window.innerWidth * 0.5);
		 divSet.style.width = popwidth + 'px';
 
		 // Create heading.
		 var ds_Heading = document.createElement('div');
		 ds_Heading.setAttribute('class', 'SettingTitle');
		 ds_Heading.appendChild(document.createTextNode(sTitle));
		 divSet.appendChild(ds_Heading);
 
		 // Add div to page.
		 var toppos =  parseInt(window.pageYOffset +  60);
		 var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		 divSet.style.top = toppos + 'px';
		 divSet.style.left = leftpos + 'px';
		 divSet.setAttribute('YOffsetVal', window.pageYOffset);
 
		 // Add blackout and setting divs.
		 document.body.appendChild(divBlackout);
		 document.body.appendChild(divSet);
		 window.addEventListener('resize', fSetLeftPos, true);
 
		 return true;
	 }
 
	 /*
	 vID = GetSetting ID, and type + vID is control ID.
	 vType = checkbox (input + type.checkbox), text (input), textarea, select
	 vLabel = Text for control label.
	 vTitle = Title text for input control and label, or vLabel if not specified.
	 vDftVal = May be text, true/false, or matches a vSelVal array element.
	 vSize = Length of input (text) box, or height of textarea, in pixels.
	 vSelVal = Array of select option values.
	 vSelTxt = Array of select option text.
	 */
	  function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		 var divSet = document.getElementById('gm_divSet');
		 var kParagraph = document.createElement('p');
		 kParagraph.id = 'p' + vID;
		 kParagraph.setAttribute('class', 'SettingElement');
		 switch (vType) {
			 case 'checkbox':
				 var kElem = document.createElement('input');
				 kElem.id = 'cb' + vID;
				 kElem.type = 'checkbox';
				 kElem. checked = vDftVal;
				 break;
			 case 'text':
				 if (!vSize) { vSize = '150px'; }
				 var kElem = document.createElement('input');
				 kElem.id = 'txt' + vID;
				 kElem.style.width = vSize;
				 kElem.value = vDftVal;
				 kElem.addEventListener('focus', fSelectAllText, true);
				 break;
			  case 'textarea':
				 if (!vSize) { vSize = '80px'; }
				 var kElem = document.createElement('textarea');
				 kElem.id = 'txtara' + vID;
				 kElem.style.width = '100%';
				 kElem.style.height = vSize;
				 kElem.value = vDftVal;
				 break;
			 case 'select':
				 var kElem = document.createElement('select');
				 kElem.id = 'sel' + vID;
				 if (vSelVal) {
					 if (vSelVal.constructor == Array) {
						 for (var i in vSelVal) {
							 var kOption = document.createElement('option');
							 kOption.value = vSelVal[i];
							 kOption.selected = (vSelVal[i] == vDftVal);
							 if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								 var kTxtNode = vSelTxt[i];
							 } else {
								 var kTxtNode = vSelVal[i];
							 }
							 kOption.appendChild(document.createTextNode(kTxtNode));
							 kElem.appendChild(kOption);
						 }
					 }
				 }
				 break;
		 }
 
		 var kLabel = document.createElement('label');
		 kLabel.setAttribute('class', 'SettingLabel');
		 kLabel.setAttribute('for', kElem.id);
		 kLabel.appendChild(document.createTextNode(vLabel));
		 if (!vTitle) { vTitle = vLabel; }
		 kElem.title = vTitle;
		 kLabel.title = kElem.title;
 
		 if (vType == 'textarea') {
			 kParagraph.appendChild(kLabel);
			 kParagraph.appendChild(document.createElement('br'));
			 kParagraph.appendChild(kElem);
		 } else {
			 kParagraph.appendChild(kElem);
			 kParagraph.appendChild(kLabel);
		 }
		 divSet.appendChild(kParagraph);
 
		 return kElem;
	 }
 
	 // Resize/reposition on window resizing.
	 function fSetLeftPos() {
		 var divSet = document.getElementById('gm_divSet');
		 if (divSet) {
			 var popwidth = parseInt(window.innerWidth * .5);
			 divSet.style.width = popwidth + 'px';
			 var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			 divSet.style.left = leftpos + 'px';
		 }
	 }
 
	 function fOpenSettingsDiv() {
		 // Add blackout and setting divs.
		 var divBlackout = document.getElementById('divBlackout');
		 var divSet = document.getElementById('gm_divSet');
		 divSet.style.visibility = 'visible';
		 divBlackout.style.visibility = 'visible';
		 var op = 0;
		 var si = window.setInterval(fShowBlackout, 40);
 
		 // Function to fade-in blackout div.
		 function fShowBlackout() {
			 op = op + .05;
			 divBlackout.style.opacity = op;
			 if (op >= .75) {
				 window.clearInterval(si);
			 }
		 }
	 }
 
	 function fCloseSettingsDiv() {
		 var divBlackout = document.getElementById('divBlackout');
		 var divSet = document.getElementById('gm_divSet');
		 divSet.parentNode.removeChild(divSet);
		 divBlackout.parentNode.removeChild(divBlackout);
		 window.removeEventListener('resize', fSetLeftPos, true);
	 }
 
	 function fCancelButtonClicked() {
		 var resp = confirm('Any changes will be lost. Close Settings?');
		 if (resp) {
			 fCloseSettingsDiv();
		 }
	 }
 
	 function fSelectAllText() {
		 this.select();
	 }
 
 