// ==UserScript==
// @name           GCR Kill Zone/Owner Action for Review Page v2
// @description    Adds Kill Zone and Owner Action Needed links to Review Page, and display health status above the logs.
// @namespace      http://www.geocaching.com/admin
// @version        02.12
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_openInTab
// @grant          GM_addStyle
// @grant          GM_log
// ==/UserScript==

/*

Function:
 Automates month-end clean-up functions.
 Display health status above the logs.

Change Log:
* v02.12 2024-11-21	- Disable unused function that is breaking native map display
* v02.11 2024-01-09	- Revert v2.09 to OpenInTab, now fixed in TM5.0.1
* v02.10 2023-12-12	- begin Tag Manager logging for basic analytics
* v02.09 2023-12-12	- Remediate TM5.0.0/Firefox openInTab tab spamming bug
* v02.08 2023-10-10	- Remove old log flow transition code and links
* v02.07 2023-07-10	- Adjust to use native new log flow log type selection
* v02.06 2023-05-25	- Deconflict old vs new bookmarking
* v02.05 2022-11-15	- Adapt script to new React log page and logging flow
*/

// Images.
var imgsrcOanWarn =
	"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
	"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
	"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
	"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
	"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
	"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
	"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
	"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
	"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
	"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
	"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
	"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

var imgsrcOanKill =
	"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
	"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
	"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
	"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
	"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
	"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
	"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
	"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
	"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
	"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
	"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
	"D%AEB%60%82";

var imgsrcKzWarn =
	"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
	"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
	"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
	"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
	"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
	"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
	"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
	"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
	"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
	"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
	"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
	"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

var imgsrcKzKill =
	"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
	"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
	"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
	"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
	"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
	"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
	"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
	"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
	"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
	"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
	"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
	"D%AEB%60%82";

// Google Tag Manager setup
function gtagKZ(){dataLayer.push(arguments)} //Method to log on Google Tag Manager
if (typeof dataLayer === 'undefined') dataLayer = [];  //Set up the dataLayer array if there isn't one
function googleTagLogKZ(dataAction){ //Method to do a gtag sending an action name
	if (document.URL.indexOf("www") < 0) return;
	try {
		gtagKZ('event','data',{'data_label':'script_data','data_action':dataAction,'data_category':'KZOAN',/*'debug_mode':true,*/
			'send_to':'G-GRQE2910DL'});
		console.log('Script '+dataAction+' logged to Tag Manager.');
	}
	catch {console.log('There was an error using gtag.  Logging attempt failed.')}
}
googleTagLogKZ('initialize');

// Text checking function.
String.prototype.startsWith = function(str) {
	return (this.indexOf(str) == 0);
};

String.prototype.contains = function(str) {
	return (this.indexOf(str) >= 0);
};

/*	// Adding this function is causing errors that break the native map display.  Disabling.
String.prototype.endsWith = function(str) {
	return (this.lastIndexOf(str) == this.length() - str.length());
};
*/

// Needed DOM elements
var e_Health = document.getElementsByClassName('score')[0];
var e_Logs = document.getElementsByClassName('geocache-logs')[0];
var e_CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
var e_ncTbl = document.getElementById('ncTbl');
var wptID = document.getElementById('ctl00_ContentBody_CacheDetails_WptRef').innerText;

// Extracting data
if ( !e_CacheData ) {
	log.console("Unable to find cache data");
	return;
}

// Cache status
var CacheGuid        = e_CacheData.getAttribute('data-cacheguid');
var published        = e_CacheData.getAttribute('data-ispublished') == 'true';
var disabled         = e_CacheData.getAttribute('data-isdisabled') == 'true';
var archived         = e_CacheData.getAttribute('data-isarchived') == 'true';
var owner            = e_CacheData.getAttribute('data-ownerusername');
var attributeCount   = e_CacheData.getAttribute('data-attribute_count');
var needsMaintenance = false;

for (var i = 0; i < attributeCount; ++i) {
	var attribute = e_CacheData.getAttribute("data-attribute_" + i + "_type");
	if (attribute == 42)
		needsMaintenance = true;
}

// Log related data
var foundCount  = 0;
var dnfCount    = 0;
var noteCount   = 0;
var nmCount     = 0;
var logCount    = 0;
var maxLogCount = 10;

var lastFound;
var lastOwnerLog;
var lastNM;
var lastDisable;

// Compute log stats
var logs = document.querySelectorAll('.geocache-logs tbody tr');
var logsStats = document.createElement('span');
logsStats.classList.add('health_status_span');

for (var i = 0; i < logs.length; ++i) {
	// Log are formatted on two table rows, the first one with log info, the second one with the log itself
	// Skip odd rows
	if (i%2 == 1) {
		continue;
	}

	var logType     = logs[i].cells[0].firstChild;
	var logTitle    = logType.getAttribute('title');
	var logDate     = new Date(logs[i].cells[1].textContent);
	var logUsername = logs[i].cells[2].textContent;

	// Update log statistics only for maxLogCount first logs
	if (logCount < maxLogCount)
	{
		if (logTitle.includes("Found it"))
			foundCount++;
		else if (logTitle.includes("Didn't find it"))
			dnfCount++;
		else if (logTitle.includes("Write note"))
			noteCount++;
		else if (logTitle.includes("Needs Maintenance"))
			nmCount++;

		// Update log count
		logCount++;

		// Add the log picture
		logsStats.appendChild(logType.cloneNode(true));
	}


	// Update dates
	if (logTitle.includes("Found it"))
	{
		if (!lastFound)
			lastFound = logDate;
		else if (logDate > lastFound)
			lastFound = logDate;
	}
	else if (logTitle.includes("Needs Maintenance"))
	{
		if (!lastNM)
			lastNM = new Date(logDate);
		else if (logDate > lastNM)
			lastNM = logDate;
	}
	else if (logTitle.includes("Temporarily Disable Listing"))
	{
		if (!lastDisable)
			lastDisable = new Date(logDate);
		else if (logDate > lastDisable)
			lastDisable = logDate;
	}
	if (logUsername.indexOf(owner) != -1)
	{
		if (!lastOwnerLog)
			lastOwnerLog = new Date(logDate);
		else if (logDate > lastOwnerLog)
			lastOwnerLog = logDate;
	}
}

// Add styles for links.
GM_addStyle(".OAN_Background { background-color: rgb(252, 250, 164); " +
		"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
GM_addStyle(".KZ_Background { background-color: rgb(229, 246, 255); " +
		"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
GM_addStyle(".LogDate {	font-weight: bold !important } ");
GM_addStyle(".health_global_div { padding-top: 10px; }");
GM_addStyle(".health_status_div { margin: 10px; }");
GM_addStyle(".health_status_span { background-color: #e6e6e6; padding: 4px 5px; }");
GM_addStyle(".health_orange { color: orange; }");
GM_addStyle(".health_red { color: red; }");

var oakzDiv = document.createElement('div');
oakzDiv.id = 'oakzDiv';

var sep01 = document.createElement('br');
oakzDiv.appendChild(sep01);

if (document.getElementById('log_table')) {
	document.getElementById('log_table').removeAttribute('id');
}

sep01.id = 'log_table';

// Display cache status above the logs
// Create the html element to append in the page
var csDiv = document.createElement('div');
csDiv.classList.add('health_global_div');

// Cache status

var cacheStatusText = [];
if (published)
	cacheStatusText.push("published");
else
	cacheStatusText.push("not published");

if (disabled)
{
	var disabledDays = daysSince(lastDisable);
	if (disabledDays >= 60)
		cacheStatusText.push('<b class="health_red">disabled (>=60 days)</b>');
	else if (disabledDays >= 30)
		cacheStatusText.push('<b class="health_orange">disabled (>=30 days)</b>');
	else
		cacheStatusText.push("disabled");
}

if (needsMaintenance == true)
{
	var nmDays = daysSince(lastNM);
	if (nmDays >= 60)
		cacheStatusText.push('<b class="health_red">needs maintenance (>=60 days)</b>');
	else if (nmDays >= 30)
		cacheStatusText.push('<b class="health_orange">needs maintenance (>=30 days)</b>');
	else
		cacheStatusText.push("needs maintenance");
}

if (archived)
	cacheStatusText.push("archived");

csDiv.appendChild(createStatusDiv("<b>Cache status:</b>", createStatusSpan(cacheStatusText.join(", "))));

if (published)
{
	// Cache health score
	if (e_Health)
		csDiv.appendChild(createStatusDiv("<b>Health score:</b>", createStatusSpan(e_Health.firstChild.data)));

	// Log statistics
	var lastLogs = [];
	if (foundCount > 0)
		lastLogs.push(foundCount + " found (" + Math.round((foundCount/logCount) * 100) + "%)");
	if (dnfCount > 0)
		lastLogs.push(dnfCount + " dnf (" + Math.round((dnfCount/logCount) * 100) + "%)");
	if (noteCount > 0)
		lastLogs.push(noteCount + " write note (" + Math.round((noteCount/logCount) * 100) + "%)");
	if (nmCount > 0)
		lastLogs.push(nmCount + " need maintenance (" + Math.round((nmCount/logCount) * 100) + "%)");

	if  (logCount > 0)
	{
		logsStats.appendChild(createStatusSpan(" - "));
		logsStats.appendChild(createStatusSpan(lastLogs.join(", ")));
		csDiv.appendChild(createStatusDiv("<b>Last " + logCount + " logs:</b>", logsStats));
	}

	// Date information
	if (lastFound)
		csDiv.appendChild(createDateDiv("<b>Last found:</b>", lastFound));

	if (needsMaintenance && lastNM)
		csDiv.appendChild(createDateDiv("<b>Needs maintenance since:</b>", lastNM));

	if (disabled && lastDisable)
		csDiv.appendChild(createDateDiv("<b>Disabled since:</b>", lastDisable));
}

if (lastOwnerLog)
	csDiv.appendChild(createDateDiv("<b>Last CO log:</b>", lastOwnerLog));

// Add the messages just before the log table
oakzDiv.appendChild(csDiv);

// If the cache if published, display a set of links to automate month-end clean-up functions
if (published)
{
	var sep02 = document.createElement('br');
	oakzDiv.appendChild(sep02);

	var spanOanWarn = document.createElement('span');
	spanOanWarn.id = 'spanOanWarn';
	oakzDiv.appendChild(spanOanWarn);

	var lnkOanWarn = document.createElement("A");
	lnkOanWarn.title = 'Disable a Problem Cache';
	lnkOanWarn.classList.add('OAN_Background');
	lnkOanWarn.href = '/bookmarks/mark.aspx?view=legacy&guid=' + CacheGuid + '&WptTypeID=2&oan=yn';
	spanOanWarn.appendChild(lnkOanWarn);

	var imgOanWarn = document.createElement("IMG");
	imgOanWarn.border = '0';
	imgOanWarn.align = 'absmiddle';
	imgOanWarn.src = imgsrcOanWarn;
	lnkOanWarn.appendChild(imgOanWarn);
	lnkOanWarn.appendChild(document.createTextNode(' Warn Problem Cache'));
	lnkOanWarn.addEventListener('click', fWarn_Oan_clicked, false);

	var lnkOanKill = document.createElement("A");
	lnkOanKill.id = 'lnkOanKill';
	lnkOanKill.title = 'Archive a Problem Cache';
	lnkOanKill.classList.add('OAN_Background');
	lnkOanKill.href = '/live/geocache/' + wptID + '/log?logType=5&oan=y';
	spanOanWarn.appendChild(lnkOanKill);

	var imgOanKill = document.createElement("IMG");
	imgOanKill.border = '0';
	imgOanKill.align = 'absmiddle';
	imgOanKill.src = imgsrcOanKill;
	lnkOanKill.appendChild(imgOanKill);
	lnkOanKill.appendChild(document.createTextNode(' Kill Problem Cache'));

	var spanKzWarn = document.createElement('span');
	spanKzWarn.style.marginLeft = '20px';
	spanKzWarn.id = 'spanKzWarn';
	oakzDiv.appendChild(spanKzWarn);

	var lnkKzWarn = document.createElement("A");
	lnkKzWarn.title = 'Warn a Long-Disabled Cache';
	lnkKzWarn.classList.add('KZ_Background');
	lnkKzWarn.href = '/bookmarks/mark.aspx?view=legacy&guid=' + CacheGuid + '&WptTypeID=2&kz=yn';
	spanKzWarn.appendChild(lnkKzWarn);

	var imgKzWarn = document.createElement("IMG");
	imgKzWarn.border = '0';
	imgKzWarn.align = 'absmiddle';
	imgKzWarn.src = imgsrcKzWarn;
	lnkKzWarn.appendChild(imgKzWarn);
	lnkKzWarn.appendChild(document.createTextNode(' Warn Disabled Cache'));
	lnkKzWarn.addEventListener('click', fWarn_Kz_clicked, false);

	var lnkKzKill = document.createElement("A");
	lnkKzKill.id = 'lnkKzKill';
	lnkKzKill.title = 'Archive Long-Disabled Cache';
	lnkKzKill.classList.add('KZ_Background');
	lnkKzKill.href = '/live/geocache/' + wptID + '/log?logType=5&kz=y';
	spanKzWarn.appendChild(lnkKzKill);

	var imgKzKill = document.createElement("IMG");
	imgKzKill.border = '0';
	imgKzKill.align = 'absmiddle';
	imgKzKill.src = imgsrcKzKill;
	lnkKzKill.appendChild(imgKzKill);
	lnkKzKill.appendChild(document.createTextNode(' Kill Disabled Cache'));
}

// Add information to page.
var ncTbl = document.getElementById("ncTbl");
if (ncTbl) {
	insertAfter(oakzDiv, ncTbl);
}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

// Bookmark this cache, in a separate tab.
function fWarn_Oan_clicked() {
	GM_openInTab(curDomain() + '/live/geocache/' + wptID + '/log?logType=22&oan=y');
}

// Bookmark this cache, in a separate tab.
function fWarn_Kz_clicked() {
	GM_openInTab(curDomain() + '/live/geocache/' + wptID + '/log?logType=68&kz=y');
}

// Returns the current domain, including http or https protocol, without a trailing '/';
function curDomain() {
	return location.protocol + '//' + location.hostname;
}

// Returns a URL parameter.
//  ParmName - Parameter name to look for.
//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
//  UrlString - (optional) String to search. If omitted, document URL is used.
function UrlParm(ParmName, IgnoreCase, UrlString) {
	var RegRslt, sc = '', RtnVal = '', PageUrl = '';
	if (IgnoreCase) {sc = 'i';}
	if(UrlString) {
		PageUrl = UrlString;
	} else {
		PageUrl = document.location + '';
	}

	var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
	var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
	RegRslt = RegEx1.exec(ParmString);
	if (RegRslt) {RtnVal = RegRslt[2];}
	return RtnVal;
}

// Insert element after an existing element.
function insertAfter(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
}

// Insert element ahead of an existing element.
function insertAheadOf(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement);
}

// Remove element and everything below it.
function removeNode(element) {
	element.parentNode.removeChild(element);
}

// Create a basic span to display health status information
function createStatusSpan(str) {
	var span = document.createElement('span');
	span.classList.add('health_status_span');
	span.innerHTML = str;
	return span;
}

// Create a basic div to display health status information
function createStatusDiv(name, value) {
	var div = document.createElement('div');
	div.classList.add('health_status_div');
	div.appendChild(createStatusSpan(name));
	div.appendChild(value);
	return div;
}

// Create a span with the number of elapsed days
function createDateDiv(str, date) {
	var days = daysSince(date);
	return createStatusDiv(str, createStatusSpan(date.toDateString() + " (" + days + " days)"));
}

// Compute the number of days elapsed
function daysSince(date) {
	var today  = new Date();
	var oneDay = 1000*60*60*24;
	var days   = Math.ceil((today - date) / oneDay);
	return days;
}


